<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Session extends Model {

	protected $table = 'sessions';
	protected $_table_columns = Array(
		'id' => Array(),
		'last_active' => Array(),
		'contents' => Array(),
	);

	protected $_primary_key = 'session_id';

	protected $_has_many = array(
			'user_merchant_visits' => array(
			'model' => 'Skp_UserMerchantVisit',
			'foreign_key' => 'merchant_id',
		),
	);
}
