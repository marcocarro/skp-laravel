<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class UserMerchantAuth extends Model {

	protected $table = 'user_merchant_auth';
	protected $_table_columns = Array(
		'id' => Array(),
		'user_merchant_id' => Array(),
		'auth_id' => Array(),
		'accepted' => Array(),
		'changed' => Array(),
	);
	
	protected $_belongs_to = array(
		'user_merchant' => array(
			'model' => 'Skp_User_Merchant',
			'foreign_key' => 'user_merchant_id',
		),
	);

	
}

