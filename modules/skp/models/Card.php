<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Card extends Model {

	protected $table = "card";
	public $timestamps = false;

	public function user() {
		$this->belongsTo(User::class);
	}

	public function person() {
		$this->belongsTo(Person::class);
	}

	public function virtual_card() {
		$this->belongsTo(VirtualCard::class, 'virtual_card_id');
	}

}