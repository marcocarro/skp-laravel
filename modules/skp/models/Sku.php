<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Sku extends Model{

	protected $table = 'sku';
	
protected $casts = [
		'extra' => 'array',
	];
	public static $useDBCache = TRUE;
	
	protected $_table_columns = Array(
		'id' => Array(), 
		'merchant_id' => Array (),
		'catalog_id' => Array(),
		'product_id' => Array(),
		'product_external_id' => Array(),
		'pricecategory_id' => Array(),
		'pricecategory_external_id' => Array(),
		'need_card' => Array(),
		'b2b_need_card' => Array(),
		'min_qty' => Array(),
		'max_qty' => Array(),
		'mod_qty' => Array(),
		'aggregate' => Array(),
		'package' => Array(),
		'extra' => Array(),
		'archived' => Array(),
		'deleted' => Array(),
		);
	
	protected $_belongs_to = array(
		'product' => array(
			'model' => 'Skp_Product',
			'foreign_key' => 'product_id',
		),
		'pricecategory' => array(
			'model' => 'Skp_PriceCategory',
			'foreign_key' => 'pricecategory_id',
		),
		'catalog' => array(
			'model' => 'Skp_Catalog',
			'foreign_key' => 'catalog_id',
		),
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
		'mainskus'=> array(
			'model' => 'Skp_Sku',
			'through' => 'product_association',
			'foreign_key' => 'sub_sku_id',
			'far_key' => 'main_sku_id',
		),
	);
	
	protected $_has_many = array(
		'skugroups' => array(
			'model' => 'Skp_SkuGroup',
			'foreign_key' => 'sku_id',
			'through' => 'skugroup_sku',
			'far_key' => 'skugroup_id',
		),
		'pricecache' => array(
			'model' => 'Skp_PriceCache',
			'foreign_key' => 'sku_id',
		),
		'subproducts' => array(
			'model' => 'Skp_Product',
			'through' => 'product_association',
			'foreign_key' => 'main_sku_id',
			'far_key' => 'sub_product_id',
		),
		'subskus' => array(
			'model' => 'Skp_Sku',
			'through' => 'product_association',
			'foreign_key' => 'main_sku_id',
			'far_key' => 'sub_sku_id',
		),
		'product_association' => array(
			'model' => 'Skp_ProductAssociation',
			'foreign_key' => 'main_sku_id',
		),
		'product_contact_profiles' => array(
			'model' => 'Skp_ProductContactProfile',
			'foreign_key' => 'sku_id',
		),
	);

	const PRICETYPE_ORIGINAL	= 'original';
	const PRICETYPE_PRICELIST	= 'pricelist';
	const PRICETYPE_SHOP			= 'shop';
	
//	protected static $mem_pricecache = array();

	public function getName($lang, $sep = NULL)
	{
		$pc = $this->get('pricecategory');
		$fullname = $this->get('product')->getName($lang, $sep).' '.$this->getPriceCategoryExtendedName($lang, $sep);
		return $fullname;
	}

	public function getPriceCategoryExtendedNameArray()
	{
		// First we merge the PriceCategoryName with the Extra
		$pc = $this->get('pricecategory');
		$extra_names = $this->getExtra('extra_names', array());
		if ($pc->get('name') == NULL) return array();
		foreach ($pc->get('name') as $langcode => $name)
		{
			$fullname[$langcode] = $name;
			$extra_name = Arr::get($extra_names, $langcode);
			$extra_name != NULL AND ($fullname[$langcode] .= ' '.$extra_name.'');
		}
		return $fullname;
	}

	public function getPriceCategoryExtendedName($lang, $sep = NULL)
	{
		$fullname = $this->getPriceCategoryExtendedNameArray();
		if ($lang == NULL)
		{
			$result = '';
			foreach ($fullname as $lang=>$text)
			{
				$result .= "$lang=>'$text'".$sep;
			}
		}
		else
		{
			$result = NULL;
			if (! isset($fullname[$lang]))
			{
				$lang = key($fullname); // Return first language found
			}
			if (isset($fullname[$lang]))
			{
				$result = $fullname[$lang];
			}
		}
		return $result;
	}

	public function getDescription($lang , $sep = NULL)
	{
		return $this->product->getDescription($lang, $sep).' '.$this->pricecategory->getDescription($lang, $sep);
	}
	
	public function validityDateInPriceCalendar($par= array())
	{
		$validity_datetime = Arr::get($par, 'validity_date'); // be careful this is in server timestamp
		$day = NULL;
		$time = NULL;
		if ($validity_datetime != NULL)
		{
			$validity_datetime->setTimeZone($this->get('merchant')->getTimezone()); // We convert it back in merchant timezone
			$day = $validity_datetime->format('Y-m-d');
			$time = $validity_datetime->format('H:i:s');
		}
		$sql_query = 'SELECT plc.sku_id, plc.product_id FROM skp_availability_product_sku_calendar_pricelist as plc LEFT JOIN skp_calendar_date_time cdt ON cdt.calendar_id=plc.calendar_id AND (cdt.deleted=\'DN\' OR cdt.deleted=\'CN\')';
		if ($day != NULL)
		{
			$sql_query .= ' AND cdt.day=\'' . DB::expr($day) . '\'';
		}
		if ($time != NULL)
		{
			if ($time != '00:00:00' AND Arr::get($par, 'ignore_time', FALSE) === FALSE)
			{
				// if time is 00:00:00 no time filter is applied
				$sql_query .= ' AND cdt.time_start=\'' . DB::expr($time) . '\'';
			}
		}
		$sql_query .= ' WHERE (plc.sku_id=:sku_id OR (plc.sku_id IS NULL AND plc.product_id=:product_id)) AND plc.price_list_id IS NOT NULL AND (plc.calendar_id IS NULL OR cdt.calendar_id IS NOT NULL)';
		$query = DB::query(Database::SELECT, $sql_query);
		$query->param(':sku_id', $this->pk());
		$query->param(':product_id', $this->get('product_id'));
		$rows = $query->as_assoc()->execute();
		return (count($rows) > 0);
	}

	public function priceFromPriceList($par= array())
	{
		$validity_datetime = Arr::get($par, 'validity_date'); // be careful this is in server timestamp
		$day = NULL;
		$time = NULL;
		if ($validity_datetime != NULL)
		{
			$validity_datetime->setTimeZone($this->get('merchant')->getTimezone()); // We convert it back in merchant timezone
			$day = $validity_datetime->format('Y-m-d');
			$time = $validity_datetime->format('H:i:s');
		}
		$sql_query = 'SELECT pls.price_list_id, pls.amount AS amount, pls.currency AS currency, plc.sku_id, plc.product_id FROM skp_availability_product_sku_calendar_pricelist as plc JOIN skp_price_list_price_category as pls ON pls.price_category_id = :price_category_id AND (plc.sku_id=:sku_id OR (plc.sku_id IS NULL AND plc.product_id=:product_id)) AND plc.price_list_id=pls.price_list_id JOIN skp_price_list as pl ON pl.id=pls.price_list_id AND pl.catalog_id=:catalog_id AND pl.deleted=0 LEFT JOIN skp_calendar_date_time cdt ON cdt.calendar_id=plc.calendar_id AND (cdt.deleted=\'DN\' OR cdt.deleted=\'CN\')';

		if ($day != NULL)
		{
			$sql_query .= ' AND cdt.day=\'' . DB::expr($day) . '\'';
		}
		if ($time != NULL)
		{
			if ($time != '00:00:00' AND Arr::get($par, 'ignore_time', FALSE) === FALSE)
			{
				// if time is 00:00:00 no time filter is applied
				$sql_query .= ' AND cdt.time_start=\'' . DB::expr($time) . '\'';
			}
		}

		if ($day != NULL)
		{
			$sql_query .= ' WHERE (plc.calendar_id IS NULL OR cdt.calendar_id IS NOT NULL)';
		}else{
			$sql_query .= ' WHERE (plc.calendar_id IS NULL AND cdt.calendar_id IS NULL)';
		}

		$sql_query .= ' ORDER BY pls.amount ASC LIMIT 1';

		$query = DB::query(Database::SELECT, $sql_query);
		$query->param(':price_category_id', $this->get('pricecategory')->pk());
		$query->param(':product_id', $this->get('product_id'));
		$query->param(':sku_id', $this->pk());
		$query->param(':catalog_id', $this->get('catalog_id'));
		$rows = $query->as_assoc()->execute();
		if (isset($rows[0]['amount']))
		{
			return new Skp_Money($rows[0]['amount'], $rows[0]['currency']);
		}
		else
		{
			return new Skp_Money(NULL, NULL);
		}
	}

	private function dynamicPrice($params)
	{
		$price_type = Arr::get($params, 'price_type', static::PRICETYPE_SHOP);
		if ($price_type == static::PRICETYPE_SHOP)
		{
			// First we search in price list
			$price = $this->priceFromPriceList($params);
			if ($price->amount === NULL)
			{
				// then we ask the connector
				$price = $this->call('getPrice', $params);
			}
		}
		if ($price_type == static::PRICETYPE_PRICELIST)
		{
			$price = $this->priceFromPriceList($params);
		}
		if ($price_type == static::PRICETYPE_ORIGINAL)
		{
			$price = $this->call('getPrice', $params);
		}
		return $price;
	}

	public function canBeBought($params)
	{
		return $this->get('product')->canBeBought($params);
	}

	public function getPrice($params = array())
	{
		$validity_date = Arr::get($params, 'validity_date');  //validity_date is a DateTime object
		$price_type = Arr::get($params, 'price_type', static::PRICETYPE_SHOP); // Possible values are: Sku::PRICETYPE_SHOP (shop price, default), Sku::PRICETYPE_ORIGINAL (connector original price), "pricelist_price" (pricelist price)
		if ($validity_date != NULL)
		{
			$validity_date->setTimeZone(new DateTimeZone(date_default_timezone_get())); //The validity must always be converted in server local time zone
			$validity_date_price = $this->get('catalog')->get('connector')->getPriceDateTime($this, $validity_date); // This convert the validity date to the one relevant for pricing (and imporves the cache strategy)
			$validity_date_string = $validity_date_price->format('Y-m-d H:i:s');
			$cache_validity_datetime_key = $validity_date_string;
		}else{
			$cache_validity_datetime_key = 'fixed';
		}

		$mem_pricecache = Skp_Cache::instance();
		$cache_key = 'pc_'.$this->pk();
		$price_type == static::PRICETYPE_ORIGINAL AND $cache_key .= '_op'; // Cache for Original Price 
		$price_type == static::PRICETYPE_PRICELIST AND $cache_key .= '_plp'; // Cache for Pricelist Price 
		$force_update = Arr::get($params, 'force_update', FALSE);
		$ttl = $this->get('merchant')->getSkpParamConfig(array('price_cache', 'time_to_live_seconds'));
		$db_cached_time = NULL;
		$now = new DateTime('now');
		if (!$force_update)
		{
			$mpc_array = $mem_pricecache->get($cache_key);
			if ($mpc_array != NULL AND isset($mpc_array[$cache_validity_datetime_key]))
			{
				// Cache version is found and returned
				return $mpc_array[$cache_validity_datetime_key];
			}
			
			// DB Cache is only for the Original Price
			if (static::$useDBCache == TRUE AND $price_type == static::PRICETYPE_ORIGINAL)
			{
				$pc = $this->get('pricecache');
				if ($validity_date != NULL)
				{
					$pc->where('validity_date','=',$validity_date_string);
				}
				$pc->find();
				$db_cached_time = ($pc->loaded() == TRUE) ? new DateTime($pc->get('last_update')) : NULL;

				if ($pc->loaded() AND !$force_update)
				{
					$mpc = new Skp_Money($pc->amount, $pc->currency);
					if (!$force_update AND ($ttl==NULL OR ($now->getTimestamp() - $db_cached_time->getTimestamp()) <= $ttl))
					{
						// DB version is found and returned
						return $mpc;
					}
				}
			}
		}
		
		// We enter next code only if forced_update is TRUE or not Price was found in cache (including DB is use_DB is TRUE)
		try
		{
			if ($this->isPackage() == TRUE)
			{
				// Call dynamicPrice for all the Sku that make the package and return the total
				$price = new Skp_Money(NULL, NULL);
				foreach ($this->getPackageSubSkus()->find_all() as $package_sub_sku)
				{
					$package_sub_sku_price = $package_sub_sku->dynamicPrice($params);
					$price->currency = ($price->currency === NULL ? $package_sub_sku_price->currency : $price->currency);
					if ($package_sub_sku_price->amount === NULL)
					{
						// This subsku has no price, also package has no price then
						$price->amount = NULL;
						break; // exit the for loop
					}
					if ($price->currency != $package_sub_sku_price->currency)
					{
						throw new Exception('Subskus of Package returning different currencies');
					}
					$sku_assoc = ORM::factory('Skp_ProductAssociation')->where('main_sku_id', '=', $this->pk())->and_where('sub_sku_id', '=', $package_sub_sku->pk())->find();
					$price->amount += $package_sub_sku_price->amount * $sku_assoc->get('qty');
				}
					
			}else
			{
				// Call dynamicPrice for a single sku
				$price = $this->dynamicPrice($params);
			}
			if (get_class($price) != "Skp_Money")
			{
				throw new Exception('dynamicPrice not returning an object of type Skp_Money');
			}

			// Here we cache the price
			$mpc_array = $mem_pricecache->get($cache_key);
			is_array($mpc_array) OR $mpc_array = array();
			$mpc_array[$cache_validity_datetime_key] = $price;
			$mem_pricecache->set($cache_key, $mpc_array, $ttl);
			if (static::$useDBCache == TRUE)
			{
				// Here we save the DB version of the cached price
				$pc = $this->get('pricecache');
				if ($validity_date != NULL)
				{
					$pc->where('validity_date','=',$validity_date_string);
				}
				$pc->find();
				if ($pc->loaded() == FALSE)
				{
					$pc->set('sku_id', $this->pk());
					if ($validity_date != NULL)
					{
						$pc->validity_date = $validity_date_string;
					}
				}
				$pc->amount = $price->amount;
				$pc->currency = $price->currency;
				$pc->set('last_update', $now->format('Y-m-d H:i:s'));
				try{
					$pc->save();
				}catch (Database_Exception $e)
				{
					if ($e->getCode() == 1062)
					{
						// Do Nothing, it is just a duplicate entry because the same price was loaded in contemporary
					}else{
						// throw($e);
					}
				}
			}
		}catch (Exception $e)
		{
			if (static::$useDBCache == TRUE)
			{
				// we return the cached price until max ttl is reached
				$max_ttl = $this->get('merchant')->getSkpParamConfig(array('price_cache', 'getprice_problems_max_time_to_live_seconds'));
				if ($db_cached_time != NULL AND ($now->getTimestamp() - $db_cached_time->getTimestamp()) <= $max_ttl)
				{
					return new Skp_Money($pc->amount, $pc->currency);
				}
			}
			return new Skp_Money(NULL,NULL);
		}
		return $price;
	}

	public function price($params = array())
	{
		$validity_date = Arr::get($params, 'validity_date');  //validity_date is a DateTime object
		if ($validity_date != NULL)
		{
			$validity_date->setTimeZone(new DateTimeZone(date_default_timezone_get())); //The validity must always be converted in server local time zone
		}
		if ($this->canBeBought(array('validity_date' => $validity_date, 'ignore_time' => Arr::get($params, 'ignore_time', FALSE), 'qty' => Arr::get($params, 'qty'),'order' => Arr::get($params, 'order'))) == FALSE)
		{
			// even if there is a price or not this sku cannot be bought now because the buy_interval_before_validity hasn't been respected
			return new Skp_Money(NULL, NULL);
		}
		// We enter next code only if forced_update is TRUE or not Price was found in cache (including DB is use_DB is TRUE)
		if ($this->isPackage() == TRUE)
		{
			// Call dynamicPrice for all the Sku that make the package and return the total
			$price = new Skp_Money(NULL, NULL);
			$subsku_not_on_sale = FALSE;
			foreach ($this->getPackageSubSkus()->find_all() as $package_sub_sku)
			{
				if ($package_sub_sku->canBeBought(array('validity_date' => $validity_date, 'ignore_time'=>Arr::get($params, 'ignore_time', FALSE))) === FALSE)
				{
					// This subsku cannot be bought, then also package has no price then
					$subsku_not_on_sale = TRUE;
					break; // exit the for loop
				}
			}
			$subsku_not_on_sale == FALSE AND $price = $this->getPrice($params);
		}else
		{
			// Call dynamicPrice for a single sku
			$price = $this->getPrice($params);
		}
		return $price;
	}
	
	public static function getSkus($params = array())
	{
		$merchant = Arr::get($params,'merchant', ORM::factory('Skp_Merchant'));
		$merchant_id = $merchant->pk();
		$user = Arr::get($params, 'user', ORM::factory('Skp_User'));
		$group_id = Arr::get($params,'group_id');
		$producttype_id = Arr::get($params,'producttype_id');
		$only_active = Arr::get($params,'only_active', TRUE);
		$only_web_active = Arr::get($params,'only_web_active', TRUE);
		$only_for = Arr::get($params,'only_for');
		$pricecategory_id = Arr::get($params,'pricecategory_id');
		$pricecategory_ids = Arr::get($params,'pricecategory_ids');
		$sku_id = Arr::get($params,'sku_id');
		$pool_id = Arr::get($params,'pool_id');
		$promocode = Arr::get($params, 'promocode', FALSE);

		$b2b_company_id = $merchant->hasAddOn('B2B') ? Arr::get($params, 'b2b_company_id', $user->getB2BCompany($merchant)->pk()) : NULL;

		$product_id = Arr::get($params,'product_id');

		$r = ORM::factory('Skp_Sku');
		$r->and_where('skp_sku.deleted', '=', '0');
		if ($merchant_id !== NULL)
		{
			$r = $r->where('skp_sku.merchant_id','=',$merchant_id);
		}
		if ($sku_id !== NULL)
		{
			$r = $r->where('skp_sku.id','=',$sku_id);
		}
	
		if ($group_id !== NULL OR $only_active OR $only_for !== NULL OR $b2b_company_id !== NULL)
		{
			$r = $r->distinct(TRUE)
				->join('skugroup_sku', 'LEFT')->on('skugroup_sku.sku_id','=','skp_sku.id')
				->join('skugroup', 'LEFT')->on('skugroup_sku.skugroup_id','=','skugroup.id')
				->join('product')->on('product.id','=','skp_sku.product_id')
				->join('price_category')->on('price_category.id','=','skp_sku.pricecategory_id')
				->join('product_category', 'LEFT')->on('product_category.id', '=', 'product.productcategory_id')
				->join('pool_product', 'LEFT')->on('pool_product.product_id','=','skp_sku.product_id')
				->join('product_type', 'LEFT')->on('product_type.id','=','product.producttype_id');
			if ($b2b_company_id !== NULL)
			{
				if ($promocode)
				{
					$r = $r->join('promocode_skugroup')->on('promocode_skugroup.skugroup_id','=','skugroup_sku.skugroup_id')
							->join('promocode_b2b_company', 'LEFT')->on('promocode_b2b_company.promocode_id','=','promocode_skugroup.promocode_id')->on('promocode_b2b_company.b2b_company_id', '=', DB::expr($b2b_company_id))
							->join('promocode')->on('promocode.id', '=', 'promocode_skugroup.promocode_id')
							->and_where_open()
								->where('promocode_b2b_company.promocode_id', 'IS NOT', NULL)
								->and_where('promocode.company_list_limit', '=', 'Y')
								->or_where('promocode.valid_for_every_b2b', '=', 'Y')
							->and_where_close();
				} else
				{
					$r = $r->join('b2b_company_skugroup')->on('b2b_company_skugroup.skugroup_id','=','skugroup_sku.skugroup_id')->on('b2b_company_skugroup.b2b_company_id', '=', DB::expr($b2b_company_id));
				}
			}else{
				if ($promocode)
				{
					$r = $r->join('promocode_skugroup')->on('promocode_skugroup.skugroup_id','=','skugroup_sku.skugroup_id')
							->join('promocode_user', 'LEFT')->on('promocode_user.promocode_id','=','promocode_skugroup.promocode_id')->on('promocode_user.user_id', '=', DB::expr($user->pk()))
							->join('promocode')->on('promocode.id', '=', 'promocode_skugroup.promocode_id')
							->and_where_open()
								->where('promocode_user.promocode_id', 'IS NOT', NULL)
								->and_where('promocode.user_list_limit', '=', 'Y')
								->or_where('promocode.valid_for_everyone', '=', 'Y')
							->and_where_close();
				}
			}
			$r = $r->where(DB::expr('1'), '=', DB::expr('1'));
			$r->and_where_open()->where('product_category.active', '=', 1)->or_where('product_category.active', '=', NULL)->and_where_close();
			$r->and_where('product.active', '=', 1);
			$r->and_where('price_category.active', '=', 1);
			$r->and_where_open()->where('skugroup.deleted', '=', '0')->or_where('skugroup.deleted', '=', NULL)->and_where_close();
			$r->and_where('product.deleted', '=', '0');
			$r->and_where('price_category.deleted', '=', '0');
			$r->and_where_open()->where('product_category.deleted', '=', '0')->or_where('product_category.deleted', '=', NULL)->and_where_close();
			if ($group_id !== NULL)
			{
				$r = $r->and_where('skugroup.id','=',$group_id);
			}
			if ($product_id !== NULL)
			{
				$r = $r->and_where('product.id', '=', $product_id);
			}
			if ($producttype_id !== NULL)
			{
				$r = $r->and_where('product.producttype_id', '=', $producttype_id);
			}
			if ($pricecategory_id !== NULL)
			{
				$r = $r->and_where('price_category.id','=',$pricecategory_id);
			}
			if ($pricecategory_ids !== NULL AND sizeof($pricecategory_ids) > 0)
			{
				$pc_id_list = '(';
				foreach ($pricecategory_ids as $pc_id)
				{
					$pc_id_list .= ($pc_id_list != '('?',':'').$pc_id;
				}
				$pc_id_list .= ')';
				$r = $r->and_where('price_category.id','in',$pricecategory_ids);
			}
			if ($only_active)
			{
				$r = $r->and_where_open()->where('product.add_prod_only','=', 1)->or_where('skugroup.active','=',1);
				if ($b2b_company_id != NULL)
				{
					if ($promocode)
					{
						$r->or_where('promocode_skugroup.skugroup_id', '=', DB::expr('skp_skugroup.id'));
					} else {
						$r->or_where('b2b_company_skugroup.skugroup_id', '=', DB::expr('skp_skugroup.id'));
					}
					$r->and_where('skugroup.active','=',1);
				}
				$r->and_where_close();
			}
			if ($only_web_active)
			{
				$r->and_where_open()->where('product.add_prod_only','=', 1)->or_where('skugroup.web_active','=',1);
				if ($b2b_company_id != NULL)
				{
					if ($promocode)
					{
						$r->or_where('promocode_skugroup.skugroup_id', '=', DB::expr('skp_skugroup.id'));
					} else {
						$r->or_where('b2b_company_skugroup.skugroup_id', '=', DB::expr('skp_skugroup.id'));
					}
					$r->and_where('skugroup.active','=',1);
				}
				$r->and_where_close();
			}
			if ($only_for !== NULL)
			{
				$r->and_where_open()->where('product.add_prod_only','=', 1)->or_where('skugroup.'.$only_for,'=',1);
				if ($b2b_company_id != NULL)
				{
					if ($promocode)
					{
						$r->or_where('promocode_skugroup.skugroup_id', '=', DB::expr('skp_skugroup.id'));
					} else {
						$r->or_where('b2b_company_skugroup.skugroup_id', '=', DB::expr('skp_skugroup.id'));
					}
					$r->and_where('skugroup.active','=',1);
				}
				$r->and_where_close();
			}
			if ($pool_id !== NULL)
			{
				$r = $r->and_where('pool_product.pool_id', '=', $pool_id);
			}
			$r->order_by('product.sort')->order_by('price_category.sort');
		}
		return $r;
	}
	
	public function getAllSubSkus($params = array())
	{
		$include_identification_cards = Arr::get($params, 'include_identification_card') != NULL;
		$sku_id = Arr::get($params,'sku_id');
		$group_id = Arr::get($params,'group_id');
		$only_active = Arr::get($params,'only_active', FALSE);
		$only_web_active = Arr::get($params,'only_web_active', FALSE);
		$only_for = Arr::get($params,'only_for');
		$include_package_subskus = Arr::get($params, 'include_package_subskus', FALSE);
		$result = array();
		
		// First we add Product (of this Sku) associations
		
		$product_association = $this->get('product')->get('product_association')->where('deleted', '=', 0);
		if ($include_package_subskus == FALSE)
		{
			$product_association->where('relation', '<>', ProductAssociation::RELATION_PACKAGE);
		}
		
		foreach($product_association->find_all() as $pa)
		{
			$sub_product = $pa->get('sub_product');
			if ($sub_product->loaded() == TRUE)
			{
				$sku_query = $pa->get('sub_product')->get('skus');
				if ($sku_id != NULL)
					$sku_query->where('id', '=', $sku_id);
				foreach($sku_query->find_all() as $sub_sku)
				{
					$result[$sub_product->get('sort')][$sub_sku->get('pricecategory')->get('sort')] = $sub_sku;
				}
			}
			$sub_sku = $pa->get('sub_sku');
			if ($sub_sku->loaded() == TRUE)
			{
				$result[$sub_sku->get('product')->get('sort')][$sub_sku->get('pricecategory')->get('sort')] = $sub_sku;
			}
		}
		
		// Now we add Sku associations
		
		$sku_association = $this->get('product_association')->where('deleted', '=', 0);
		if ($include_package_subskus == FALSE)
		{
			$sku_association->where('relation', '<>', ProductAssociation::RELATION_PACKAGE);
		}
		
		foreach($sku_association->find_all() as $pa)
		{
			$sub_product = $pa->get('sub_product');
			if ($sub_product->loaded() == TRUE)
			{
				$sku_query = $pa->get('sub_product')->get('skus');
				if ($sku_id != NULL)
					$sku_query->where('id', '=', $sku_id);
				foreach($sku_query->find_all() as $sub_sku)
				{
					$result[$sub_product->get('sort')][$sub_sku->get('pricecategory')->get('sort')] = $sub_sku;
				}
			}
			$sub_sku = $pa->get('sub_sku');
			if ($sub_sku->loaded() == TRUE)
			{
				$result[$sub_sku->get('product')->get('sort')][$sub_sku->get('pricecategory')->get('sort')] = $sub_sku;
			}
		}

		// Now we filter the sub skus found based on the publishing status and the funciont parameters

		$sorted_result = array();
		$z = 0;
		ksort($result);
		foreach ($result as $i=>$subresult)
		{
			ksort($subresult);
			foreach ($subresult as $j=>$sku)
			{
				if ($only_active === FALSE OR $sku->isActive() == TRUE)
				{
					if ($include_identification_cards OR $sku->get('product')->get('producttype')->get('type') <> 'IdentificationCard')
					{
						$skugroups = $sku->get('skugroups');
						$skugroups->and_where('skp_skugroup.deleted', '=', '0');
						if ($only_active === TRUE ) $skugroups->and_where('active', '=', 1);
						if ($only_web_active === TRUE) $skugroups->and_where('web_active', '=', 1);
						if ($only_for != NULL) $skugroups->and_where($only_for,'=',1);
						if ($group_id != NULL) $skugroups->and_where('id', '=', $group_id);
						if ($sku->get('product')->add_prod_only == 1 OR $skugroups->count_all() > 0)
						{
							$sorted_result[$z++] = $result[$i][$j];
						}
					}
				}
			}
		}
		return $sorted_result;
	}
	
	public function isActive()
	{
		return ($this->get('product')->get('active') == 1 AND $this->get('pricecategory')->get('active') == 1);
	}
	
	public function getIdentificationCardSubSku()
	{
		foreach ($this->getAllSubSkus(array('include_identification_card'=>TRUE, 'only_active'=>TRUE)) as $ss)
		{
			if ($ss->product->producttype->type == 'IdentificationCard')
				return $ss;
		}
		return ORM::factory('Skp_Sku');
	}
	
	public static function getAvailableTickets($params = array())
	{
		$merchant = Arr::get($params, 'merchant');
		$user = Arr::get($params, 'user', ORM::factory('Skp_User'));
		$filter = array('merchant'=>$merchant, 'group_id'=>Arr::get($params, 'group_id'),'producttype_id'=>Arr::get($params, 'producttype_id'), 'only_active'=>Arr::get($params, 'only_active'), 'only_web_active'=>Arr::get($params, 'only_web_active'), 'only_for'=>Arr::get($params, 'only_for'), 'product_id'=>Arr::get($params, 'product_id'), 'pricecategory_id'=>Arr::get($params, 'pricecategory_id'), 'pool_id'=>Arr::get($params, 'pool_id'), 'user'=>$user, 'promocode'=>Arr::get($params, 'promocode'));
		$ticket_list = Sku::getSkus($filter)->find_all();
		$bookable_y_n_a = Arr::get($params, 'bookable_y_n_a');
		$filtered_result = array();
		foreach ($ticket_list as $ticket)
		{
			$price = $ticket->price(array('validity_date'=>Arr::get($params, 'validity_start'), 'ignore_time'=>Arr::get($params, 'ignore_time', FALSE)));
			if ($price->amount !== NULL)
			{
				$needsDateTimeBooking = $ticket->needsDateTimeBooking();
				if ($bookable_y_n_a === NULL OR $bookable_y_n_a == 'a' OR ($bookable_y_n_a == 'y' AND $needsDateTimeBooking == TRUE) OR ($bookable_y_n_a == 'n' AND $needsDateTimeBooking == FALSE))
				{
					$filtered_result[] = $ticket;
				}
			}
		}
		return $filtered_result;
	}
	
	public function isAggregate()
	{
		
		return $this->get('aggregate') != NULL ? $this->get('aggregate')==1 : $this->get('product')->get('producttype')->get('aggregate') == 1;
	}
	
	public function getAvailabilities($date_time = NULL)
	{
		return Availability::getSkuAvailabilities($this);
	}
	
	public function needsBooking()
	{
		return sizeof($this->getAvailabilities()) > 0;
	}

	// Check if this sku has an terms and conditions option
	public function needsTermsConditions()
	{
		return (Arr::get($this->get('extra'), 'enable_terms_and_conditions') != NULL OR $this->get('product')->needsTermsConditions());
	}

	public function getTermsConditionsCustomText($lang)
	{
		$results = array();
		if (Arr::get($this->get('extra'), 'enable_terms_and_conditions') != NULL)
		{
			$results = Arr::get($this->get('extra'), 'terms_and_conditions_custom_text', array());
		}
		if ($results == NULL AND $this->get('product')->needsTermsConditions())
		{
			$results = Arr::get($this->get('product')->get('product_extra'), 'terms_and_conditions_custom_text', array());
		}
		if (Arr::get($results, $lang) != NULL)
		{
			return Arr::get($results, $lang);
		}
		// No Extra Info text is found for the given language return the first one found
		foreach ($results as $lang => $value)
		{
			if ($value != NULL)
			{
				return $value;
			}
		}
		// No Extra Info text is found return NULL
		return NULL;
	}

	public function getTermsConditionsURL()
	{
		$result = NULL;
		if (Arr::get($this->get('extra'), 'enable_terms_and_conditions') != NULL)
		{
			$result = Arr::get($this->get('extra'), 'terms_and_conditions_url');
		}
		if ($result == NULL AND $this->get('product')->needsTermsConditions())
		{
			$result = Arr::get($this->get('product')->get('product_extra'), 'terms_and_conditions_url');
		}
		return $result;
	}
	
	public function needsDateTimeBooking()
	{
		$avps = $this->getAvailabilities();
		foreach ($avps as $avs)
		{
			if ($avs->get('availability')->get('calendars')->count_all() > 0)
			{
				return TRUE;
			}
		}
		return FALSE;
	}
	
	public function getAvailable($params = array())
	{
		$params['sku'] = $this;
		return Availability::getAvailable($params);
	}
	
	public function getPackageSubSkus()
	{
		return $this->get('subskus')->where('relation', '=', ProductAssociation::RELATION_PACKAGE)->and_where('product_association.archived', '=', '0')->and_where('product_association.deleted', '=', '0');
	}
	
	public function getProductAssociations($relation_type, $archived = FALSE, $deleted = FALSE)
	{
		$archived = $archived ? 1 : 0;
		$deleted = $deleted ? 1 : 0;
		return $this->get('product_association')->where('relation', '=', $relation_type)->and_where('archived','=',$archived)->and_where('deleted','=',$deleted)->find_all();
	}
	
	/*
	 * This function check if the Sku is a package or a single Sku
	 * 
	 */
	 
	public function isPackage()
	{
		return $this->get('package') == 'Y';
	}
	
	/*
	 * This function is used to clean the price Cache
	 *
	 */
	 
	public function cleanPriceCache()
	{
		$mem_pricecache = Skp_Cache::instance();
		$cache_keys = array('pc_'.$this->pk(), 'pc_'.$this->pk().'_op', 'pc_'.$this->pk().'_plp');
		foreach($cache_keys as $cache_key)
		{
			$mem_pricecache->delete($cache_key);
		}
		if (static::$useDBCache == TRUE)
		{
			foreach ($this->get('pricecache')->find_all() as $pc)
			{
				$pc->delete();
			}
		}
	}
	 
	public function save(Validation $validation = NULL)
	{
		$result = parent::save($validation);
		$this->cleanPriceCache();
		return $result;
	}
	 
	public function getPurchaseQtyLimit($limit_type)
	{
		if (!in_array($limit_type, array('min_qty', 'max_qty', 'mod_qty')))
		{
			throw new Exception('Purchase Quantity Limit type: '.$limit_type.' does not exists');
		}
		return $this->get($limit_type);
	}
	
	public function getPrintPriceFormat($is_b2b = FALSE)
	{
		$print_price_key = $is_b2b? 'print_price_b2b': 'print_price';
		$print_price = Arr::get($this->get('extra'), $print_price_key);
		$print_price = (($print_price === NULL OR $print_price == Merchant::PRICE_PARENT_SET) ? $this->get('product')->getPrintPriceFormat($is_b2b) : $print_price);
		return $print_price;
	}
	
	public function getTodaysPrice()
	{
		$saved_price = $this->getExtra('todays_price');
		$now = new DateTime('now', $this->get('merchant')->getTimeZone());
		if (is_array($saved_price) AND isset($saved_price[$now->format('Y-m-d')]))
		{
			return $saved_price[$now->format('Y-m-d')];
		} else if (is_array($saved_price))
		{
			$this->setExtra('todays_price', NULL);
			$this->save();
		}
		return NULL;
	}
	
	public function setTodaysPrice($price)
	{
		$now = new DateTime('now', $this->get('merchant')->getTimeZone());
		$todays_price = array($now->format('Y-m-d') => $price);
		$this->setExtra('todays_price', $todays_price, TRUE);
		$this->save();
	}

	public function hasExternalAvailability()
	{
		try{
			return $this->call('hasExternalAvailability');
		}catch (Skp_Exception_Missing $e){
			// In case the method is missining in the Connector it means the External Availability is not implemented for this Connector Class
		}
		return FALSE;
	}
}
