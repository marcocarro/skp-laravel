<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class UserMerchantPosition extends Model {
	protected $table = 'user_merchant_position';
	protected $_reload_on_wakeup = FALSE;
	protected $_table_columns = Array(
		'id' => Array(),
		'user_id' => Array(),
		'merchant_id' => Array(),
		'user_merchant_visit_id' => Array(),
		'domain' => Array(),
		'when' => Array(),
		'device_type' => Array(),
		'device_platform' => Array(),
		'latitude' => Array(),
		'longitude' => Array(),
		'street_num' => Array(),
		'street' => Array(),
		'city' => Array(),
		'area_level_3' => Array(),
		'area_level_2' => Array(),
		'area_level_1' => Array(),
		'country' => Array(),
		'zip' => Array(),
		'formatted_address' => Array(),
	);

	protected $_belongs_to = array(
		'user' => array(
			'model' => 'Skp_User',
			'foreign_key' => 'user_id'
		),
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id'
		),
		'user_merchant_visit' => array(
			'model' => 'Skp_UserMerchantVisit',
			'foreign_key' => 'user_merchant_visit_id'
		),
	);

	public function save(Validation $validation = NULL)
	{
		parent::save($validation);
		self::delFromCache('last_user_merchant_positions_userid_'.$this->get('user_id'));
	}

	public static function createFromGooglePos($gpos)
	{
		$new_pos = ORM::factory('Skp_UserMerchantPosition');
		$new_pos->when = gmdate('Y-m-d H:i:s');
		Log::instance()->add(LOG_DEBUG,print_r($gpos,TRUE));
		$geometry = $gpos->geometry;

		if ($geometry != NULL)
		{
			$location = $geometry->location;
			if ($location != NULL)
			{
				$new_pos->latitude = isset($location->latitude) ? $location->latitude : $location->lat;
				$new_pos->longitude = isset($location->longitude) ? $location->longitude : $location->lng;
			}
		}
		$address_components = $gpos->address_components;
		if ($address_components != NULL AND is_array($address_components))
		{
			foreach ($address_components as $ac)
			{
				if ($ac->long_name != NULL)
				{
					$a_types = $ac->types;
					if (in_array('street_number',$a_types))
					{
						$new_pos->street_num = $ac->long_name;
					}
					if (in_array('route',$a_types))
					{
						$new_pos->street = $ac->long_name;
					}
					if (in_array('locality',$a_types))
					{
						$new_pos->city = $ac->long_name;
					}
					if (in_array('administrative_area_level_3',$a_types))
					{
						$new_pos->area_level_3 = $ac->short_name?$ac->short_name:$ac->long_name;
					}
					if (in_array('administrative_area_level_2',$a_types))
					{
						$new_pos->area_level_2 = $ac->short_name!=NULL?$ac->short_name:$ac->long_name;
					}
					if (in_array('administrative_area_level_1',$a_types))
					{
						$new_pos->area_level_1 = $ac->short_name!=NULL?$ac->short_name:$ac->long_name;
					}
					if (in_array('country',$a_types))
					{
						$new_pos->country = $ac->short_name;
					}
					if (in_array('postal_code',$a_types))
					{
						$new_pos->zip = $ac->long_name;
					}
				}
			}
		}
		$new_pos->formatted_address = $gpos->formatted_address;
		return $new_pos;
	}

	public static function getStateField($country)
	{
		$state_mappings = Skp_Helper::getList('state_mapping');
		return Arr::get($state_mappings, $country, NULL);
	}

	public static function getState($position)
	{
		if ($position->country != NULL)
		{
			$state_field = static::getStateField($position->country);
			if ($state_field != NULL)
			{
				return $position->$state_field;
			}
		}
		return NULL;
	}
	
	public function anonymizeUserPosition($anon_user)
	{
		$this->set('user_id', $anon_user->pk());
		$this->set('latitude', round($this->get('latitude'), 1));
		$this->set('longitude', round($this->get('longitude'), 1));
		$this->set('street_num', NULL);
		$this->set('street', NULL);
		$this->set('zip', Skp_Helper::anonymizeValue($this->get('zip'),'zip'));
		$this->set('formatted_address', NULL);
		$this->save();
	}
}
