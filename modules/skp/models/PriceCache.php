<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class PriceCache extends Model {

	protected $table = 'price_cache';
	protected $_belongs_to = array(
		'sku' => array(
			'model' => 'Skp_Sku',
			'foreign_key' => 'sku_id',
		),
	);
}
