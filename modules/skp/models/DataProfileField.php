<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class DataProfileField extends Model {

	protected $table = 'data_profile_field';
	public $timestamps = false;

	public function data_profile() {
		$this->belongsTo(DataProfile::class, 'data_profile_id');
	}

	public function data_field() {
		$this->belongsTo(Skp_DataField::class, 'data_field_id');
	}
	
}