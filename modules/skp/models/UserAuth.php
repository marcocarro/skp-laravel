<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class UserAuth extends Model {

	protected $table = 'user_auth';
	protected $_table_columns = array(
		'id' => array(),
		'user_id' => array(),
		'merchant_id' => array(),
		'controller' => array(),
		'action' => array(),
		'filter' => array(),
		'permitted' => array(),
	);
	
	protected $_belongs_to = array(
		'user' => array(
			'model' => 'Skp_User',
			'foreign_key' => 'user_id',
		),
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
	);

}

