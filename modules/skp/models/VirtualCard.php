<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class VirtualCard extends Model {

	protected $table = "virtual_card";

	protected $_table_columns = array(
		'id'							=> array(),
		'merchant_id'				=> array(),
		'virtual_card_type_id'	=> array(),
		'card_serial'				=> array(),
		'b2b_company_id'			=> array(),
		'active'						=> array(),
		'active_b2b'				=> array(),
		'valid_from'				=> array(),
		'valid_to'					=> array(),
		'extra'						=> array(),
		'deleted'					=> array(),
	);

	
protected $casts = [
		'extra' => 'array',
	];

	protected $_belongs_to = array(
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
		'b2b_company' => array(
			'model' => 'Skp_B2BCompany',
			'foreign_key' => 'b2b_company_id',
		),
		'virtual_card_type' => array(
			'model' => 'Skp_VirtualCardType',
			'foreign_key' => 'virtual_card_type_id',
		),
	);
	
	protected $_has_many =	array(
		'real_cards' => array(
			'model' => 'Skp_VirtualCardRealCard',
			'foreign_key' => 'virtual_card_id',
		),
	);
	
	protected $_has_one = array(
		
	);
	
	public function rules()
	{
		return array(
			'card_serial' => array(
				array('not_empty'),
			),
			'merchant_id' => array(
				array('not_empty'),
			),
			'virtual_card_type_id' => array(
				array('not_empty'),
			),
			'valid_to'	=>	array(
				array(
					array(
						$this, 'date_check'
					),
				),
			),
		);
	}

	public function __toString()
	{
		return (String)$this->card_serial;
	}
	
	public function getExtra($key, $default = NULL)
	{
		if ($key != NULL)
		{
			return Arr::get($this->get('extra'), $key, $default);
		}
	}
	
	public function setExtra($extra_name, $extra_value)
	{
		$extras = $this->get('extra');

		if ($extra_value != NULL)
		{
			$extras[$extra_name] = $extra_value;
		}else{
			unset($extras[$extra_name]);
		}
		$this->set('extra', $extras);
	}
	
	public function date_check()
	{
		return $this->get('valid_from')<=$this->get('valid_to');
	}
	
	public function isValid($datetime_now = NULL)
	{
		$ok = TRUE;
		if ($this->get('deleted') == 1)
		{
			$ok = FALSE;
		}
		if ($datetime_now == NULL)
		{
			$datetime_now = (new DateTime('now', $this->get('merchant')->getTimeZone()))->format('Y-m-d H:i:s');
		}
		if ($this->get('active') == FALSE)
		{
			$ok = FALSE;
		}
		if ($this->get('merchant')->hasAddOn('Virtual Card', 'Virtual Card B2B Connect'))
		{
			if($this->get('b2b_company_id') != NULL AND $this->get('active_b2b') == FALSE)
			{
				$ok = FALSE;
			}
		}
		if ($this->get('valid_from') != NULL AND $this->get('valid_from') > $datetime_now)
		{
			$ok = FALSE;
		}
		if ($this->get('valid_to') != NULL AND $this->get('valid_to') <= $datetime_now)
		{
			$ok = FALSE;
		}
		return ($ok AND $this->check());
	}

	public function delete ()
	{
		$this->set('deleted', 1);
		$this->save();
	}

	public static function getVirtualCard($params = array())
	{
		$merchant = Arr::get($params,'merchant');
		if($merchant == NULL)
		{
			throw new Skp_Exception('Please pass a valid merchant'); 
		}
		$virtual_cards = $merchant->get('virtual_cards')->where('deleted', '=', '0');
		// filter_serial_from
		$serial_from = Arr::get($_REQUEST, 'filter_serial_from');
		if($serial_from !== NULL)
		{
			$virtual_cards->and_where(DB::expr('SUBSTRING(card_serial,1,'.strlen($serial_from).')'), '>=', $serial_from);
		}
		// filter_serial_to
		$serial_to = Arr::get($_REQUEST, 'filter_serial_to');
		if($serial_to !== NULL)
		{
			$virtual_cards->and_where(DB::expr('SUBSTRING(card_serial,1,'.strlen($serial_to).')'), '<=', $serial_to);
		}
		// filter_valid_from
		$valid_from = (Arr::get($_REQUEST, 'filter_valid_from')!=NULL ? DateTime::createFromFormat($merchant->get('date_format').' H:i', Arr::get($_REQUEST, 'filter_valid_from'), $merchant->getTimeZone()) : NULL);
		if($valid_from !== NULL)
		{
			$virtual_cards->and_where('valid_from', '>=', $valid_from->format('Y-m-d H:i:s'));
		}
		// filter_valid_to
		$valid_to = (Arr::get($_REQUEST, 'filter_valid_to')!=NULL ? DateTime::createFromFormat($merchant->get('date_format').' H:i', Arr::get($_REQUEST, 'filter_valid_to'), $merchant->getTimeZone()) : NULL);
		if($valid_to !== NULL)
		{
			$virtual_cards->and_where('valid_to', '<=', $valid_to->format('Y-m-d H:i:s'));
		}
		// filter_b2b_company_id
		$b2b_company_id = Arr::get($_REQUEST, 'filter_b2b_company_id');
		if($b2b_company_id != NULL)
		{
			$virtual_cards->and_where('b2b_company_id', '=', $b2b_company_id);
		}
		// filter_active
		$active = Arr::get($_REQUEST, 'filter_active', NULL);
		if($active === "0" OR $active === "1")
		{
			$virtual_cards->and_where('active', '=', $active);
		}
		// filter_active_b2b
		$active_b2b = Arr::get($_REQUEST, 'filter_active_b2b', NULL);
		if($active_b2b === "0" OR $active_b2b === "1")
		{
			$virtual_cards->and_where('active_b2b', '=', $active_b2b);
		}
		return $virtual_cards;
	}

}

