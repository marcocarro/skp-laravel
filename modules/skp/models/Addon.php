<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Addon extends Model
{

	protected $table = 'addon';
   public $timestamps = false;

	public function merchant() {
      return $this->belongsTo(Merchant::class);
	}

}