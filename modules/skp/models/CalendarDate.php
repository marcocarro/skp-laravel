<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class CalendarDate extends Model {

	protected $table = 'calendar_date';
	public $timestamps = false;

	public function calendar() {
		$this->belongsTo(Calendar::class);
	}

	public function calendar_date_time() {
		return $this->hasMAny(
			CalendarDateTime::class,
			'date_id'
		);
	}

}