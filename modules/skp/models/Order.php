<?php

 
/*
 *  VERY IMPORTANT, THIS CLASS IS EXTENDED BY B2BOrder, ANY CHANGE WILL IMPACT ALSO THAT CLASS
 */

class Order extends Model {

	protected $table = 'order';

	protected $_table_columns = array(
		'id' => array(),
		'reference_num' => array(),
		'merchant_id' => array(),
		'b2b_company_id' => array(),
		'user_id' => array(),
		'creation_date' => array(),
		'order_datetime' => array(),
		'lastupdate_date' => array(),
		'amount' => array(),
		'currency' => array(),
		'type' => array(),
		'user_profile_confirmed' => array(),
		'status' => array(),
		'status_info' => array(),
		'related_order_id' => array(),
		'marker' => array(),
		'note_id' => array(),
		'process_code' => array(),
	);

	protected $_belongs_to = array(
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
		'b2b_company' => array(
			'model' => 'Skp_B2BCompany',
			'foreign_key' => 'b2b_company_id',
		),
		'user' => array(
			'model' => 'Skp_User',
			'foreign_key' => 'user_id',
		),
		'original_order' => array(
			'model' => 'Skp_Order',
			'foreign_key' => 'related_order_id',
		),
		'note' => array(
			'model' => 'Skp_Note',
			'foreign_key' => 'note_id',
		),
	);

	protected $_has_many =	array(
		'items' => array(
			'model' => 'Skp_OrderItem',
			'foreign_key' => 'order_id',
		),
		'order_externals' => array(
			'model' => 'Skp_OrderExternal',
			'foreign_key' => 'order_id',
		),
		'payments' => array(
			'model' => 'Skp_Payment',
			'foreign_key' => 'order_id',
		),
		'invoice_receipt_orders' => array(
			'model' => 'Skp_InvoiceReceiptOrder',
			'foreign_key' => 'order_id',
		),
		'export_records' => array(
			'model' => 'Skp_ExportConnectorRecord',
			'foreign_key' => 'order_id'
		),
	);

	protected $_has_one = array(
		'related_order' => array(
			'model' => 'Skp_Order',
			'foreign_key' => 'related_order_id',
		),
	);

	const TYPE_BASKET								= 'BASKET';
	const TYPE_ORDER								= 'ORDER';
	const STATUS_NEW								= 'NEW';
	const STATUS_PRECHECK_OK					= 'PRECHECK-OK';
	const STATUS_BOOKED							= 'BOOKED';
	const STATUS_PENDING							= 'PENDING';
	const STATUS_TO_BE_PAYED					= 'TO BE PAYED';
	const STATUS_PAYMENT_AUTHORIZED			= 'PAYMENT AUTHORIZED';
	const STATUS_PAYMENT_TIMEOUT				= 'PAYMENT TIMEOUT';
	const STATUS_OK								= 'OK';
	const STATUS_ERROR							= 'ERROR';
	const STATUS_INFO_REMINDED					= 'REMINDED';
	const BASKET_TTL								= 900;
	const MAX_NUMBER_ITEM_IN_BASKET			= 100;
	
	const RELATION_B2C							= 'B2C';
	const RELATION_B2B							= 'B2B';
	
	public static $warning_status = array(self::STATUS_PENDING, self::STATUS_PAYMENT_AUTHORIZED, self::STATUS_NEW);
	public static $error_status = array(self::STATUS_TO_BE_PAYED, self::STATUS_ERROR, self::STATUS_PAYMENT_TIMEOUT);
	
	public static $bo_order_status_list = array('ALL', self::STATUS_BOOKED, self::STATUS_PAYMENT_TIMEOUT, self::STATUS_ERROR);

	public static $filemanager_profile_pdf_order_tickets = 'pdf_order_tickets';

	public function getCreationDate($time_zone = NULL)
	{
		$time_zone == NULL AND $time_zone = $this->get('merchant')->getTimeZone();
		$dt = DateTime::createFromFormat('Y-m-d H:i:s', $this->order_datetime);
		return ($dt != NULL ? $dt->setTimeZone($time_zone) : NULL);
	}

	public static function getBasket($merchant, $user, $sanitize = TRUE)
	{
		$basket = $user->get('orders')->where('merchant_id','=',$merchant->pk())->and_where('type', '=', static::TYPE_BASKET)->find();
		$sanitize AND $basket->sanitizeBasket();
		if ($basket->loaded() == FALSE)
		{
			
			$basket->merchant_id = $merchant->pk();
			$basket->currency = $merchant->get('currency');
			$b2b_company = $user->getB2BCompany($merchant);
			$user->setActiveB2BCompanyId($merchant->pk(), $b2b_company->pk());
			$basket->set('b2b_company_id', $b2b_company->pk());
			$basket->user_id = $user->pk();
			$basket->creation_date = gmdate('Y-m-d H:i:s');
			$basket->lastupdate_date = gmdate('Y-m-d H:i:s');
			$basket->set('type', static::TYPE_BASKET);
			$basket->set('status', static::STATUS_NEW);
			$basket->save();
		}
		return $basket;
	}

	public function basketIsValid()
	{
		if ($this->type == static::TYPE_BASKET AND $this->items->count_all() == 0) return FALSE;
		return TRUE;
	}

	public function sanitizeBasket()
	{
		if ($this->type != static::TYPE_BASKET) return;
		$b2b_company = $this->get('user')->getB2BCompany($this->get('merchant'));
		$this->get('user')->setActiveB2BCompanyId($this->merchant->pk(), $b2b_company->pk());
		$this->set('b2b_company_id', $b2b_company->pk());
		$nowInMerchantTZ = new DateTime('now', $this->get('merchant')->getTimeZone());
		$deleted_orderitem_ids = array();
		foreach ($this->get('items')->find_all() as $item)
		{	
			$validity_date = $item->getValidityDate();
			if ($validity_date != NULL)
			{
				$validFromInMerchantTZ = $validity_date->setTimeZone($this->get('merchant')->getTimeZone());
			}else{
				$validFromInMerchantTZ = NULL;
			}
			// Added ($item->needsBooking() AND !$item->needsDateTimeBooking() AND !$item->get('booking')->loaded()) in order to remove items that have expired bookings with only availability
			if ($item->canBeBought() == FALSE OR ($validFromInMerchantTZ != NULL AND $validFromInMerchantTZ->format('Y-m-d') < $nowInMerchantTZ->format('Y-m-d')) OR ($item->needsBooking() AND !$item->needsDateTimeBooking() AND !$item->get('booking')->loaded()))
			{
				try
				{
					$deleted_orderitem_ids = array_merge($deleted_orderitem_ids, $this->removeOrderItem($item, FALSE));
				}catch (Exception $e)
				{
					$main_orderitem = $item->get('mainorderitem');
					if ($e->getCode() == 10 AND $item->get('mainorderitem')->loaded() == TRUE)
					{
						// This is a subitem of a package, we need to remove his main item
						$deleted_orderitem_ids = array_merge($deleted_orderitem_ids, $this->removeOrderItem($item->get('mainorderitem')));
					}
				}
			}else{
				$item->updateValidityData($this->get('user')->get('last_language'));
				$item->save();
			}
		}
		// If the merchant currency is different than the latest basket for this merchant currency then we do delete it
		if ($this->get('currency') != $this->get('merchant')->get('currency'))
		{
			$this->deleteAllWithChildren();
		}
		return $deleted_orderitem_ids;
	}

	public function delete()
	{
		if ($this->get('type') != static::TYPE_BASKET)
		{
			throw new Exception ('An Order cannot be delted from DB!');
		}
		$success = TRUE;
		foreach ($this->get('items')->find_all() as $item)
		{
			!($item->delete()) AND $success = FALSE;
		}
		$success AND ($success = parent::delete());
		return $success;
	}

	public function deleteAllWithChildren()
	{
		$this->delete();
	}

	public static function convertBasketToOrder($merchant, $user, $order_id)
	{
		// First look for the current Basket
		$new_order = ORM::factory('Skp_Order')->where('merchant_id','=',$merchant->id)->and_where('user_id','=',$user->id)->and_where('type', '=', static::TYPE_BASKET)->where('id','=',$order_id)->and_where('status', '=', static::STATUS_PRECHECK_OK)->find();
		if ($new_order->loaded() == TRUE)
		{
			$new_order->status = ($new_order->get('amount') != 0 ? static::STATUS_TO_BE_PAYED: static::STATUS_PAYMENT_AUTHORIZED); // If the amount is 0 in order to skip any payment we define the order state as payment authorized
			$new_order->type = static::TYPE_ORDER;
			$new_order->save();
		}
		else
		{
			// then look for already converted basket
			$new_order = ORM::factory('Skp_Order')->where('merchant_id','=',$merchant->id)->and_where('user_id','=',$user->id)->and_where('type', '=', static::TYPE_ORDER)->where('id','=',$order_id)->and_where('status' ,'IN', array(static::STATUS_TO_BE_PAYED, static::STATUS_PAYMENT_AUTHORIZED))->find();
		}
		return $new_order;
	}

	public function convertOrderToBasket()
	{
		$old_basket = static::getBasket($this->merchant, $this->user);
		if ($old_basket->loaded() === TRUE)
		{
			$old_basket->delete();
		}
		if ($this->loaded() === TRUE AND $this->type == static::TYPE_ORDER AND $this->status == static::STATUS_TO_BE_PAYED)
		{
			$this->set('type', static::TYPE_BASKET);
			$this->status = static::STATUS_NEW;
			$this->save();
		}
	}
	
	public function copyOrderToBasket()
	{
		$errors = array();
		$old_basket = static::getBasket($this->merchant, $this->user);
		if ($old_basket->loaded() === TRUE)
		{
			$old_basket->delete();
		}
		$new_basket = static::getBasket($this->merchant, $this->user);
		$main_items = $this->getMainItems();
		$errors = $new_basket->copyOrderItemsToBasket($main_items);
		return $errors;
	}
	
	public function copyOrderItemsToBasket($items, $main_orderitem = NULL)
	{
		$errors = array();
		foreach ($items as $item)
		{
			$cloned_item = ORM::factory('Skp_OrderItem');
			$cloned_item->set('order_id', $this->pk());
			$main_orderitem != NULL AND $cloned_item->set('sub_orderitem_of_id', $main_orderitem->pk());
			$cloned_item->set('sku_id', $item->get('sku_id'));
			$cloned_item->set('pool_id', $item->get('pool_id'));
			$cloned_item->set('promocode_id', $item->get('promocode_id'));
			$cloned_item->set('product_name', $item->get('product_name'));
			$cloned_item->set('pricecategory_name', $item->get('pricecategory_name'));
			$cloned_item->set('original_connector_class', $item->get('original_connector_class'));
			$cloned_item->set('original_connector_id', $item->get('original_connector_id'));
			$cloned_item->set('qty', $item->get('qty'));
			$cloned_item->set('validity_date', $item->get('validity_date'));
			$cloned_item->set('single_amount', $item->get('single_amount'));
			$cloned_item->set('total_amount', $item->get('total_amount'));
			$cloned_item->set('currency', $item->get('currency'));
			$cloned_item->set('vat_id', $item->get('vat_id'));
			$cloned_item->set('vat_perc', $item->get('vat_perc'));
			$cloned_item->set('vat_amount', $item->get('vat_amount'));
			$cloned_item->set('orderitem_extra', $item->get('orderitem_extra'));
			$cloned_item->set('person_data', $item->get('person_data'));
			$cloned_item->set('person_photo', $item->get('person_photo'));
			
			$cloned_item->save();
			$cloned_item->addTags();

			$person = $item->get('person');
			if ($person->loaded())
			{
				$cloned_item->setPerson($person);
			}
			$card = $item->get('card');
			if ($card->loaded())
			{
				$cloned_item->setCard($card);
			}
			$booking = $item->get('booking');
			if ($booking->loaded())
			{
				try
				{
					$cdt_id = $booking->get('calendar_date_time_id');
					$avail_id = $booking->get('availability_id');
					$booking->delete();
					$cloned_item->book($cdt_id, $avail_id);
				} catch (Exception $e)
				{
					!isset($errors['booking']) AND $errors['booking'] = array(); 
					$errors['booking'][] = $item->pk();
				}
			}
			$cloned_item->save();
			$subitems = $item->get('suborderitems')->find_all();
			$errors = array_merge($errors, $this->copyOrderItemsToBasket($subitems, $cloned_item));
		}
		return $errors;
	}

	public function restoreAsBasket()
	{
		$process_code = $this->getProcessCode();
		if ($process_code == FALSE) throw new Skp_Exception('Order in process , not possible to restore it, try again later');
		$this->reload();
		$last_payment = $this->get('payments')->order_by('id', 'DESC')->find();
		// If an order has total amount = 0 the payment is an empty ORM object
		if (!in_array($this->get('status'), Order::$error_status) OR ($this->get('amount') != 0 AND $last_payment->get('status') != Payment::REGISTERED AND $last_payment->get('status') != Payment::CANCELLED)) {
			$this->releaseProcessCode($process_code);
			throw new Skp_Exception(__('g.order_not_restorable'));
		}
		$errors = $this->copyOrderToBasket();
		$this->releaseProcessCode($process_code);
		return $errors;
	}

	public function resetPrecheck()
	{
		//~ $process_code = $this->getProcessCode();
		//~ if ($process_code == FALSE) throw new Skp_Exception('Order in process , not possible to restore it, try again later');
		$this->set('type', static::TYPE_BASKET);
		$this->status = static::STATUS_NEW;
		$this->status_info = NULL;
		$this->save();
		//~ $this->releaseProcessCode($process_code);
	}

	public function isNotEmpty()
	{
		return $this->items->count_all()>0;
	}

	public function needsConfig()
	{
		foreach ($this->items->find_all() as $item)
		{
			// first we check if any order item needs a contact
			$need_contact = $item->needContactProfile();
			if ($need_contact == TRUE) return TRUE;

			// we check if order item needs terms and condition approved
			if ($item->get('sku')->get('product')->needsTermsConditions()) return TRUE;

			// we check if order item needs extra info
			if ($item->get('sku')->get('product')->needsExtraInfo()) return TRUE;

			// now we check if any order item needs a card
			// isB2B() is only for old B2B Orders, new B2B orders will use $item->get('sku')->get('need_card')
			$need_card = $this->isB2B() ? $item->get('sku')->get('b2b_need_card') : $item->get('sku')->get('need_card');
			if ($need_card != 'N') return TRUE;

         // now we check if the product needs booking	(with calendar)
			$needs_datetime_booking = $item->get('sku')->needsDateTimeBooking();
			if ($needs_datetime_booking == TRUE) return TRUE;

			// now we check if the product needs booking (with no calendar)
			$needs_booking = $item->get('sku')->needsBooking();
			if ($needs_booking == TRUE) return TRUE;

			// now we check if some orderitem have potential subproducts
			foreach ($this->get('items')->find_all() as $item)
			{
				if (sizeof($item->get('sku')->getAllSubSkus()) > 0 ) return TRUE;
			}
		}
		return FALSE;
	}

	public function areDependenciesValid()
	{
		$result = $this->checkOrderItemDependancies(FALSE);
		return $result->is_valid;
	}

	public function checkOrderValidities()
	{
		$this_step = 1;
		$ok = TRUE;
		foreach ($this->items->find_all() as $item)
		{
			if ($item->getValidityDate() != NULL)
			{
				$nowis = new DateTime();
				$validFromInMerchantTZ = $item->getValidityDate()->setTimeZone($this->merchant->getTimeZone());
				if ($validFromInMerchantTZ->format('Y-m-d') < $nowis->format('Y-m-d'))
				{
					if ($this->get('type') == static::TYPE_BASKET)
					{
						$item->status = OrderItem::STATUS_ERROR;
						$info = __('status.info_check_validities_error');
						$item->setStatusExtra('info', ($this_step.' - '.$info));
						$item->save();
					}
					$ok = FALSE;
				}
			}
		}
		if ($this->get('type') == static::TYPE_BASKET)
		{
			$this->set('status', $ok ? static::STATUS_OK : static::STATUS_ERROR);
			$info = $ok?__('status.info_check_validities_ok'):__('status.info_check_validities_error');
			$this->status_info = $this_step.' - '.$info;
			$this->save();
		}
		return $ok;
	}

	public function checkOrderPrices()
	{
		$this_step = 2;
		$ok = TRUE;
		$price_cache_sku_validity = array();
		foreach ($this->get('items')->find_all() as $item)
		{
			if ($item->get('sku')->isPackage() == FALSE)
			{
				$price_cache_key_sku = $item->get('sku_id');
				$price_cache_key_validity = $item->getValidityDate()!=NULL ? $item->getValidityDate()->format('Y-m-d H:i:s') : NULL;
				$single_price = Arr::get(Arr::get($price_cache_sku_validity, $price_cache_key_sku, array()), $price_cache_key_validity);
				if ($single_price === NULL)
				{
					$single_price = $item->get('sku')->price(array('validity_date'=>$item->getValidityDate(), 'force_update'=>TRUE));
				}
				$price_cache_sku_validity[$price_cache_key_sku][$price_cache_key_validity] = $single_price;
				$item_single_price = $single_price;
			}else{
				// If the Sku of the Order Item is a package it's price must be 0
				$item_single_price = new Skp_Money(0, $item->currency);
			}
			if ($item->getSinglePrice() != $item_single_price OR $item->currency != $this->currency OR $item->get('single_amount')*$item->get('qty') != $item->get('total_amount'))
			{
				if ($this->get('type') == static::TYPE_BASKET)
				{
					if ($item_single_price->amount !== NULL)
					{
						$item->single_amount = $item_single_price->amount;
						$item->total_amount = $item_single_price->amount * $item->qty;
						$item->currency = $item_single_price->currency;
						$vat = $item->get('sku')->get('product')->get('vat');
						if ($vat->loaded())
						{
							$item->set('vat_id', $vat->pk());
							$item->set('vat_perc', $vat->get('perc'));
							$item->set('vat_amount', Skp_Vat::reverseCalculate($item->get('total_amount'), $vat->get('perc'), $item->get('currency'))->getAmountRounded());
						}else{
							$item->set('vat_id', NULL);
							$item->set('vat_perc', NULL);
							$item->set('vat_amount', NULL);
						}
						$item->status = OrderItem::STATUS_ERROR;
						$info = __('status.info_check_prices_error');
						$item->setStatusExtra('info', ($this_step.' - '.$info));
						// Print Price Info Save
						$print_price = $item->getPrintPriceFormat();
						$item->setExtra('print_price', $print_price);
						if ($print_price === Merchant::PRICE_ENCRYPT)
						{
							$item->setExtra('print_price_encrypt', Arr::get($this->get('merchant')->get('merchant_extra'), 'print_price_encrypt', '0123456789'));
						}
						$item->save();
					}
					else
					{
						$this->removeOrderItem($item, TRUE);
					}
				}
				$ok = FALSE;
			}
			else if ($this->get('type') == static::TYPE_BASKET)
			{
				$item->status = OrderItem::STATUS_OK;
				$info = __('status.info_check_prices_ok');
				$item->setStatusExtra('info', ($this_step.' - '.$info));
				// Print Price Info Save
				$print_price = $item->getPrintPriceFormat();
				$item->setExtra('print_price', $print_price);
				if ($print_price === Merchant::PRICE_ENCRYPT)
				{
					$item->setExtra('print_price_encrypt', Arr::get($this->get('merchant')->get('merchant_extra'), 'print_price_encrypt', '0123456789'));
				}
				$vat = $item->get('sku')->get('product')->get('vat');
				if ($vat->loaded())
				{
					$item->set('vat_id', $vat->pk());
					$item->set('vat_perc', $vat->get('perc'));
					$item->set('vat_amount', Skp_Vat::reverseCalculate($item->get('total_amount'), $vat->get('perc'), $item->get('currency'))->getAmountRounded());
				}else{
					$item->set('vat_id', NULL);
					$item->set('vat_perc', NULL);
					$item->set('vat_amount', NULL);
				}
				$item->save();
			}
		}
		if ($this->get('type') == static::TYPE_BASKET)
		{
			$this->set('status', $ok ? static::STATUS_OK : static::STATUS_ERROR);
			$info = $ok?__('status.info_check_prices_ok'):__('status.info_check_prices_error');
			$this->status_info = $this_step.' - '.$info;
			$this->save();
		}
		return $ok;
	}

	public function checkOrderItems($return_on_error = TRUE)
	{
		$this_step = 3;
		$ok = TRUE;
		$now_merchant_TZ = new DateTime('now', $this->get('merchant')->getTimeZone());
		$qty_totals = array();
		$error_msg = NULL;
		foreach ($this->get('items')->find_all() as $item)
		{
			if ($item->get('sku')->isActive() == FALSE)
			{
				$ok = FALSE;
				$error_msg .= __('status.info_check_product_on_sale_error', array('%sku_name%'=>$item->get('sku')->getName(I18n::lang())));
				if ($return_on_error) break;
			}
			if ($item->get('promocode')->loaded() == TRUE AND $item->get('promocode')->isValid($this->get('user'), $this->get('user')->getB2BCompany($this->get('merchant'))) == FALSE)
			{
				$ok = FALSE;
				$error_msg .= __('status.info_check_orderitems_error.promotion_not_valid_for', array('%1'=>$item->get('sku')->getName(Request::$lang)));
				if ($return_on_error) break;
			}
			if ($item->get('sku')->isPackage() == FALSE)
			{
				if ($item->getValidityDate() != NULL AND $item->getValidityDate()->format('Y-m-d') < $now_merchant_TZ->format('Y-m-d'))
				{
					$ok = FALSE;
					$error_msg .= __('status.info_check_tickets_error');
					if ($return_on_error) break;
				}
				if ($item->get('sku')->needsBooking() == TRUE)
				{
					$booking = $item->get('booking');
					if ($booking->loaded() == FALSE)
					{
						$ok = FALSE;
						$error_msg .= __('status.info_check_tickets_error');
						if ($return_on_error) break;
					}
					if ($item->get('sku')->needsDateTimeBooking() == TRUE)
					{
						$cdt = $booking->get('calendar_date_time');
						if ($cdt->loaded() == FALSE OR $cdt->format($this->get('merchant')->getTimeZone(), 'Y-m-d') < $now_merchant_TZ->format('Y-m-d'))
						{
							$ok = FALSE;
							$error_msg .= __('status.info_check_tickets_error');
							if ($return_on_error) break;
						}
					}
				}
			}
			$qty_totals_index = $item->getValidityDate() != NULL ? $item->getValidityDate()->format('Y-m-d') : 'not dated';

			// here we check if the min, max mod quantities are respected
			if ( !isset($qty_totals[$qty_totals_index]) )
			{
				$qty_totals[$qty_totals_index] = array('sku'=>array(), 'prod'=>array());
			}
			if ( !isset($qty_totals[$qty_totals_index]['prod'][$item->get('sku')->get('product_id')]) )
			{
				$qty_totals[$qty_totals_index]['prod'][$item->get('sku')->get('product_id')] = $item->get('qty');
			}else{
				$qty_totals[$qty_totals_index]['prod'][$item->get('sku')->get('product_id')] += $item->get('qty');
			}
			if ( !isset($qty_totals[$qty_totals_index]['sku'][$item->get('sku')->pk()]) )
			{
				$qty_totals[$qty_totals_index]['sku'][$item->get('sku')->pk()] = $item->get('qty');
			}else{
				$qty_totals[$qty_totals_index]['sku'][$item->get('sku')->pk()] += $item->get('qty');
			}
		}
		try{
			foreach ($qty_totals as $day => $qty_prodsku)
			{
				$formatted_day = DateTime::createFromFormat('Y-m-d', $day);
				$formatted_day != NULL AND $formatted_day = '('.$formatted_day->format($this->get('merchant')->get('date_format')).')';
				foreach ($qty_prodsku as $qty_type => $qty)
				{
					foreach ($qty as $obj_id => $qty_s)
					{
						$obj = ORM::factory($qty_type=='prod' ? 'Skp_Product': 'Skp_Sku', $obj_id);
						if ($obj->loaded() == FALSE) throw new Exception('Item not found', 10);
						$min_qty = $obj->getPurchaseQtyLimit('min_qty');
						$max_qty = $obj->getPurchaseQtyLimit('max_qty');
						$mod_qty = $obj->getPurchaseQtyLimit('mod_qty');
						if ($min_qty != NULL AND $qty_s < $min_qty)
						{
							$error_msg .= __('status.info_check_orderitems_error.sku_min_qty_required', array('%1'=>$obj->getName(Request::$lang),'%2'=>$formatted_day,'%3'=>$qty_s,'%4'=>$min_qty));
							$ok = FALSE;
							if ($return_on_error) throw new Exception('exit all loops', 10);
						}
						if ($max_qty != NULL AND $qty_s > $max_qty)
						{
							$error_msg .= __('status.info_check_orderitems_error.sku_max_qty_required', array('%1'=>$obj->getName(Request::$lang),'%2'=>$formatted_day,'%3'=>$qty_s,'%4'=>$max_qty));
							$ok = FALSE;
							if ($return_on_error) throw new Exception('exit all loops', 10);
						}
						if ($mod_qty != NULL AND $qty_s % $mod_qty != 0)
						{
							$error_msg .= __('status.info_check_orderitems_error.sku_mod_qty_required', array('%1'=>$obj->getName(Request::$lang),'%2'=>$formatted_day,'%3'=>$qty_s,'%4'=>$mod_qty));
							$ok = FALSE;
							if ($return_on_error) throw new Exception('exit all loops', 10);
						}
					}
				}
			}
		}catch (Exception $e)
		{
			if ($e->getCode() != 10)
			{
				throw $e;
			}
		}

		if ($this->get('type') == static::TYPE_BASKET)
		{
			$this->set('status', $ok ? static::STATUS_OK : static::STATUS_ERROR);
			$info = $ok?__('status.info_check_tickets_ok'):$error_msg;
			$this->status_info = $this_step.' - '.$info;
			$this->save();
		}
		return $ok;
	}

	public function checkOrderPersons()
	{
		$this_step = 4;
		$ok = TRUE;
		if ($this->get('type') == static::TYPE_BASKET)
		{
			// unique_sku_person_validity check array
			$unique_sku_person_validity = array();
			foreach ($this->items->find_all() as $item)
			{
				// Virtual Order Item aren't connected to a person because they cannot be configured, we skip the check
				if ($item->isVirtualOrderItem() == TRUE)
				{
					$need_contact = FALSE;
				}else{
					// need_contact values TRUE OR FALSE
					$person = $item->get('person');
					$need_contact = $item->needContactProfile();
					if ($need_contact == TRUE AND ($person->loaded() === FALSE OR $person->get('deleted') == 1 OR $person->isProfileComplete(['contact_profile' => $item->getContactProfile()]) == FALSE OR $item->checkCompatibilePerson($person) == FALSE))
					{
						$ok = FALSE;
						if ($this->get('type') == static::TYPE_BASKET)
						{
							$item->set('person', NULL);
							$item->status = OrderItem::STATUS_ERROR;
							$info = __('status.info_check_contacts_error');
							$item->setStatusExtra('info', ($this_step.' - '.$info));
						}
					}else{
						if ($need_contact == TRUE AND Arr::get($item->get('sku')->get('extra'), 'only_one_per_person_per_validity_date') === TRUE)
						{
							$unique_sku_person_validity_key = $item->get('sku')->pk().'_'.$item->get('person')->pk().'_'.$item->get('validity_date');
							if (in_array($unique_sku_person_validity_key, $unique_sku_person_validity))
							{
								$ok = FALSE;
								if ($this->get('type') == static::TYPE_BASKET)
								{
									$item->status = OrderItem::STATUS_ERROR;
									$info = __('status.info_check_contacts_error_only_one_person_allowed');
									$item->setStatusExtra('info', ($this_step.' - '.$info));
								}
							}else{
								$unique_sku_person_validity[] = $unique_sku_person_validity_key;
							}
						}
					}
				}
				if ($this->get('type') == static::TYPE_BASKET)
				{
					if ($need_contact == FALSE)
					{
						$item->status = OrderItem::STATUS_OK;
						$info = __('status.info_check_contacts_ok_no_need');
						$item->setStatusExtra('info', ($this_step.' - '.$info));
					}
					$item->save();
				}
			}
			if ($this->get('type') == static::TYPE_BASKET)
			{
				$this->set('status', $ok ? static::STATUS_OK : static::STATUS_ERROR);
				$info = $ok?__('status.info_check_contacts_ok'):__('status.info_check_contacts_error');
				$this->status_info = $this_step.' - '.$info;
				$this->save();
			}
		}
		return $ok;

	}

	public function checkOrderCards()
	{
		$this_step = 5;
		$ok = TRUE;
		$all_error_message = NULL;
		if ($this->get('type') == static::TYPE_BASKET)
		{
			foreach ($this->items->find_all() as $item)
			{
				$is_this_card_ok = TRUE;
				// need_card values Y = an existing card is mandatory, YA = an existing or new card is mandatory, B = buying a card is mandatory, O = a card (new or existing) is optional, N = no card is needed
				$card = $item->get('card');
				// isB2B() is only for old B2B orders, new B2B Orders will use $item->get('sku')->get('need_card')
				$need_card = $this->isB2B() ? $item->get('sku')->get('b2b_need_card') : $item->get('sku')->get('need_card');
				$card_data_set = $card->loaded();
				if (($need_card == 'Y' AND $card_data_set === FALSE) OR ($need_card == 'YA' AND ($card_data_set === FALSE AND $item->isWithIdentificationCard() === FALSE)) OR ($need_card == 'B' AND ($card_data_set === TRUE OR $item->isWithIdentificationCard() === FALSE)) OR ($card_data_set === TRUE AND $need_card == 'BB'))
				{
					$is_this_card_ok = FALSE;
					if ($this->get('type') == static::TYPE_BASKET)
					{
						$item->status = OrderItem::STATUS_ERROR;
						$info = __('status.info_check_cards_error');
						$item->setStatusExtra('info', ($this_step.' - '.$info));
					}
				}
				if ($is_this_card_ok == TRUE AND $card_data_set === TRUE AND $card->validForSku($item->get('sku'), $item->getValidityDate()) === FALSE)
				{
					$is_this_card_ok = FALSE;
					if ($this->get('type') == static::TYPE_BASKET)
					{
						$item->status = OrderItem::STATUS_ERROR;
						$info = __('status.info_check_cards_error.card_not_valid', array(':1' => $card->__toString()));
						$item->setStatusExtra('info', ($this_step.' - '.$info));
					}
				}
				if ($is_this_card_ok == FALSE)
				{
					$all_error_message .= $all_error_message != NULL ? "<br>".$info: $info;
					$ok = FALSE;
				}else if ($this->get('type') == static::TYPE_BASKET){
					$item->status = OrderItem::STATUS_OK;
					$info = __('status.info_check_cards_ok_no_need');
					$item->setStatusExtra('info', ($this_step.' - '.$info));
				}
				if ($this->get('type') == static::TYPE_BASKET)
				{
					$item->save();
				}
			}
			if ($this->get('type') == static::TYPE_BASKET)
			{
				$this->set('status', $ok ? static::STATUS_OK : static::STATUS_ERROR);
				$order_info = $ok ? __('status.info_check_cards_ok') : $all_error_message;
				$this->status_info = $this_step.' - '.$order_info;
				$this->save();
			}
		}
		return $ok;
	}

	public function checkOrderLimits() // Check Order Limits
	{
		$this_step = 6;
		$all_error_message = NULL;
		$ok = TRUE;
		if ($this->get('merchant')->hasAddOn('Advanced Sales Functionalities') == TRUE)
		{
			$max_total_amount = Arr::get($this->get('merchant')->get('merchant_extra'), 'max_total_amount', NULL);
			if ($max_total_amount !== NULL AND $this->amount > $max_total_amount)
			{
				$ok = FALSE;
				$money_amount = new Skp_Money($this->amount, $this->currency);
				$money_max_total_amount = new Skp_Money($max_total_amount, $this->get('merchant')->get('currency'));
				$msg = __('status.info_check_limits_amount_error', array('%current'=>$money_amount, '%limit'=>$money_max_total_amount));
				$all_error_message == NULL || $all_error_message.='<br>';
				$all_error_message .= $msg;
			}
			$max_orderitems_num = Arr::get($this->get('merchant')->get('merchant_extra'), 'max_orderitems_num', NULL);
			if ($max_orderitems_num !== NULL AND ($total_items = $this->get('items')->count_all()) AND $total_items > $max_orderitems_num)
			{
				$ok = FALSE;
				$msg = __('status.info_check_limits_items_error', array('%current'=>$total_items, '%limit'=>$max_orderitems_num));
				$all_error_message == NULL || $all_error_message.='<br>';
				$all_error_message .= $msg;
			}
		}
		if ($this->get('type') == static::TYPE_BASKET)
		{
			$this->set('status', $ok ? static::STATUS_OK : static::STATUS_ERROR);
			$order_info = $ok ? __('status.info_check_order_limits_ok') : $all_error_message;
			$this->status_info = $this_step.' - '.$order_info;
			$this->save();
		}
		return $ok;
	}

	public function checkOrderItemDependancies($update_order_status = TRUE) // Check Item dependancies
	{

		$this_step = 7;
		$needs = array();
		$orm_all_items = $this->items->find_all();
		$all_items = array();
		$all_items_indx = 0;
		$sub_item_indx = 0;
		$last_unsatisfied_sub_order_item_id = NULL;
		$error_sub_order_item_ids = array();
		foreach ($orm_all_items as $item)
		{
			$this_item_key = ($item->get('validity_date') != NULL ? substr($item->get('validity_date'), 0, 10) : 'x') . '_' . ($item->get('sku')->get('product')->pk()) . '_' . ($item->get('sku')->get('pricecategory') != NULL ? $item->get('sku')->get('pricecategory')->pk() : 'x');
			$item_already_found = FALSE;
			foreach ($all_items as $old_items_index => $elem)
			{
				if ($elem[0] == $this_item_key)
				{
					// The item_key already exists we do only increase the qty
					$all_items[$old_items_index][1] += $item->get('qty');
					$item_already_found = TRUE;
					break;
				}
			}
			if ($item_already_found == FALSE)
			{
				$all_items[$all_items_indx] = array($this_item_key, $item->get('qty'));
				$all_items_indx++;
			}

			// First we search if there is a specific rule for this Sku (product-pricecategory)
			$is_child_of = ORM::factory('Skp_ProductDependency')->where('sub_product_id', '=', $item->get('sku')->get('product')->pk())->and_where('sub_pricecategory_id', '=', $item->get('sku')->get('pricecategory')->pk())->find_all();
			if (count($is_child_of) == 0)
			{
				// In case there is not a specific rule for the SKU we look for rules based on product - all categoris or all products - category
				$is_child_of = ORM::factory('Skp_ProductDependency')->where('sub_product_id', '=', NULL)->and_where('sub_pricecategory_id', '=', $item->get('sku')->get('pricecategory')->pk())->find_all();
				if (count($is_child_of) == 0)
				{
					$is_child_of = ORM::factory('Skp_ProductDependency')->where('sub_product_id', '=', $item->get('sku')->get('product')->pk())->and_where('sub_pricecategory_id', '=', NULL)->find_all();
				}
			}

			if (count($is_child_of) > 0)
			{
				$needs[$sub_item_indx] = array();
				$item_need_idx = 0;
				foreach ($is_child_of as $pc_rel)
				{
					$key = ($pc_rel->get('same_day') == TRUE ? substr($item->get('validity_date'), 0, 10) : 'x') . '_' . ($pc_rel->get('main_product')->loaded() ? $pc_rel->get('main_product')->pk() : ($pc_rel->get('same_product') ? '=' : 'x')) . '_' . ($pc_rel->get('main_pricecategory')->loaded() ? $pc_rel->get('main_pricecategory')->pk() : ($pc_rel->get('same_pricecategory') ? '=': 'x'));
					$sub_products[$sub_item_indx]['product_id'] =  $item->get('sku')->get('product')->pk();
					$sub_products[$sub_item_indx]['pricecategory_id'] =  $item->get('sku')->get('pricecategory')->loaded() ? $item->get('sku')->get('pricecategory')->pk() : NULL;
					$sub_products[$sub_item_indx]['orderitem_id'] =  $item->pk();
					$needs[$sub_item_indx][$item_need_idx] = array($key, $item->get('qty') / $pc_rel->get('child_parent_rate')); // we use an array to save the combination key - qty needed
					$item_need_idx++;
				}
				$sub_item_indx++;
			}
		}

		$total_items = $all_items_indx;
		$total_sub_items = $sub_item_indx;
		// here $total_items is the number of items in total
		Log::instance()->add(LOG_DEBUG, 'needs: ' . print_r($needs, TRUE));
		Log::instance()->add(LOG_DEBUG, 'total_items: ' . $total_items . ' total_sub_items: ' . $total_sub_items);
		if ($total_sub_items == 0)
		{
				$is_valid = TRUE; //there aren't subitems, no need to check
		} else
		{
			$permutations_sub_items_indexes = array();
			Skp_Utils::permute(range(0, $total_sub_items-1), array(), $permutations_sub_items_indexes); // now $permutations_sub_items_indexes is an array of ($total_sub_items)! elements each of which is a permutation of the indexes of

			// now we loop all the permutations of sub items

			for ($k = 0; $k < count($permutations_sub_items_indexes); $k++)
			{
				Log::instance()->add(LOG_DEBUG, 'Permutation: '.print_r($permutations_sub_items_indexes[$k], TRUE));

				$is_valid = TRUE;
				$available_parents = $all_items;
				$error_sub_order_item_ids = array();
				Log::instance()->add(LOG_DEBUG, 'Available Parents: '.print_r($available_parents, TRUE));
				Log::instance()->add(LOG_DEBUG, 'Sub Items: '.print_r($sub_products, TRUE));
				for ($j = 0 ; $j  < $total_sub_items; $j++)
				{
					$sub_item_indx = $permutations_sub_items_indexes[$k][$j];
					for ($all_item_indx = 0 ; $all_item_indx < $total_items; $all_item_indx++)
					{
						$subitem_satisfied = FALSE;
						// we check the combination with this parent only if it as some quantity left
						if ($available_parents[$all_item_indx][1] > 0)
						{
							// now we check if at least one of the need is satisfied by this parent item
							for ($item_need_indx = 0 ; $item_need_indx < count($needs[$sub_item_indx]); $item_need_indx++)
							{
								$nke = explode('_', $needs[$sub_item_indx][$item_need_indx][0]); // $needs[$sub_item_indx][$item_need_idx][0] is the sub item need key
								$pke = explode('_', $available_parents[$all_item_indx][0]); // $available_parents[$all_item_indx][0] is the parent item key
								Log::instance()->add(LOG_DEBUG, 'Checking Need: '.$needs[$sub_item_indx][$item_need_indx][0].' (qty: '.$needs[$sub_item_indx][$item_need_indx][1].' with available_item: '.$available_parents[$all_item_indx][0].' (qty: '.$available_parents[$all_item_indx][1].')');

								if ($nke[0] != 'x' AND $nke[0] != $pke[0]) continue;  // We skip this parent if the validity check isn't satisfied
								Log::instance()->add(LOG_DEBUG, '1 - validity date check PASSED');
								if ($nke[1] != 'x' AND (($nke[1] != '=' AND $nke[1] != $pke[1]) OR ($nke[1] == '=' AND $sub_products[$sub_item_indx]['product_id']  != $pke[1]))) continue;  // We skip this parent if the product check isn't satisfied
								Log::instance()->add(LOG_DEBUG, '2 - product check PASSED');
								if ($nke[2] != 'x' AND (($nke[2] != '=' AND $nke[2] != $pke[2]) OR ($nke[2] == '=' AND $sub_products[$sub_item_indx]['pricecategory_id']  != $pke[2]))) continue;  // We skip this parent if the pricecategory check isn't satisfied
								Log::instance()->add(LOG_DEBUG, '3 - pricecategory check PASSED');
								if ($needs[$sub_item_indx][$item_need_indx][1] > $available_parents[$all_item_indx][1]) continue;  // We skip this parent if the quantity of the parent items isn't enough
								// if we get here, all the conditions are passed correctly and the needs is satisfied, now we update the parent available quantity
								Log::instance()->add(LOG_DEBUG, '4 - quantity check PASSED');
								$available_parents[$all_item_indx][1] -= $needs[$sub_item_indx][$item_need_indx][1];

								$subitem_satisfied = TRUE;
								break(2); // This take us to point -- check next subitem
							}
						}
					}
					// -- check next subitem
					if ($subitem_satisfied == FALSE)
					{
						// if we get here it means in the previous loop we never break(2) and that means this permutation of sub items is not valid
						$is_valid = FALSE;
						$last_unsatisfied_sub_order_item_id = $sub_products[$sub_item_indx]['orderitem_id'];
						$error_sub_order_item_ids[] = $sub_products[$sub_item_indx]['orderitem_id'];
						//break(2);  // This take us to point -- check next permutation
					}
				}
				if ($is_valid == TRUE) break; // the current permutation was valid for all subitems, the check is passed correctly
				// -- check next permutation
			}
		}

		if ($is_valid == TRUE)
		{
			// Here we check the Package Order Item consistency
			foreach ($this->get('items')->find_all() as $item)
			{
				$is_valid = $item->checkPackageConsistency();
				if ($is_valid == FALSE)
				{
					Log::instance()->add(LOG_DEBUG, 'Package consistency FAILED for item: ' . $item->pk() . ' SkuId: '.$item->get('sku_id'));
					$last_unsatisfied_sub_order_item_id = $item->pk();
					$error_sub_order_item_ids[] = $item->pk();
					break;
				}
			}
		}

		$result = new StdClass;
		$result->is_valid = $is_valid;
		$result->error_order_item_id = $last_unsatisfied_sub_order_item_id;
		$result->error_sub_order_item_ids = $error_sub_order_item_ids;
		if ($this->get('type') == static::TYPE_BASKET)
		{
			foreach ($result->error_sub_order_item_ids as $orderitem_id)
			{
				$item = $this->items->where('id', '=', $orderitem_id)->find();
				$item->status = OrderItem::STATUS_ERROR;
				$info = __('status.info_check_dependencies_error');
				$item->setStatusExtra('info', ($this_step . ' - ' . $info));
				$item->save();
			}
			if ($update_order_status == TRUE)
			{
				$this->set('status', $is_valid ? static::STATUS_OK : static::STATUS_ERROR);
				$info = $is_valid ? __('status.info_check_dependancies_ok') : __('status.info_check_dependancies_error');
				$this->status_info = $this_step . ' - ' . $info;
				$this->save();
			}
		}
		return $result;
	}

	private function checkInvoiceReceiptChoice()
	{
		$invoice_receipt_of_order = $this->get('invoice_receipt_orders')->find();
		if ($invoice_receipt_of_order->loaded() == TRUE)
		{
			if ($this->get('merchant')->hasAddOn('Invoice/Receipt'))
			{
				$config = $this->get('merchant')->get('invoice_receipt_config');
				if ($config->getExtra('invoice_request') != 'Y' AND $invoice_receipt_of_order->get('model') == InvoiceReceiptProfile::MODEL_INVOICE)
				{
					$invoice_receipt_of_order->delete();
				}
				if ($config->getExtra('receipt_request') != 'Y' AND $invoice_receipt_of_order->get('model') == InvoiceReceiptProfile::MODEL_RECEIPT)
				{
					$invoice_receipt_of_order->delete();
				}
			}else{
				$invoice_receipt_of_order->delete();
			}
		}
	}
	
	public function checkOrderItemsQtyLimits($return_on_error = TRUE, $add_orderitem_id = NULL, $add_item_qty = 0)
	{
		$ok = TRUE;
		$qty_totals = array();
		$error_msg = NULL;
		$promo_id = NULL;
		foreach ($this->get('items')->find_all() as $item)
		{
			$qty_totals_index = $item->getValidityDate() != NULL ? $item->getValidityDate()->format('Y-m-d') : 'not dated';

			// here we check if the min, max mod quantities are respected
			if ( !isset($qty_totals[$qty_totals_index]) )
			{
				$qty_totals[$qty_totals_index] = array('sku'=>array(), 'prod'=>array());
			}
			if ( !isset($qty_totals[$qty_totals_index]['prod'][$item->get('sku')->get('product_id')]) )
			{
				$qty_totals[$qty_totals_index]['prod'][$item->get('sku')->get('product_id')] = $item->get('qty');
			}else{
				$qty_totals[$qty_totals_index]['prod'][$item->get('sku')->get('product_id')] += $item->get('qty');
			}
			$item->pk() == $add_orderitem_id AND $qty_totals[$qty_totals_index]['prod'][$item->get('sku')->get('product_id')] += $add_item_qty;
			if ( !isset($qty_totals[$qty_totals_index]['sku'][$item->get('sku')->pk()]) )
			{
				$qty_totals[$qty_totals_index]['sku'][$item->get('sku')->pk()] = $item->get('qty');
			}else{
				$qty_totals[$qty_totals_index]['sku'][$item->get('sku')->pk()] += $item->get('qty');
			}
			$item->pk() == $add_orderitem_id AND $qty_totals[$qty_totals_index]['sku'][$item->get('sku')->pk()] += $add_item_qty;
			
			$item->pk() == $add_orderitem_id AND $item->get('promocode_id') != NULL AND $promo_id = $item->get('promocode_id');
		}
		try{
			foreach ($qty_totals as $day => $qty_prodsku)
			{
				$formatted_day = DateTime::createFromFormat('Y-m-d', $day);
				$formatted_day != NULL AND $formatted_day = '('.$formatted_day->format($this->get('merchant')->get('date_format')).')';
				foreach ($qty_prodsku as $qty_type => $qty)
				{
					foreach ($qty as $obj_id => $qty_s)
					{
						$obj = ORM::factory($qty_type=='prod' ? 'Skp_Product': 'Skp_Sku', $obj_id);
						if ($obj->loaded() == FALSE) throw new Exception('Item not found', 10);
						$min_qty = $obj->getPurchaseQtyLimit('min_qty');
						$max_qty = $obj->getPurchaseQtyLimit('max_qty');
						$mod_qty = $obj->getPurchaseQtyLimit('mod_qty');
						if ($add_orderitem_id == NULL AND $min_qty !== NULL AND $qty_s < $min_qty)
						{
							$error_msg .= __('status.info_check_orderitems_error.sku_min_qty_required', array('%1'=>$obj->getName(Request::$lang),'%2'=>$formatted_day,'%3'=>$qty_s,'%4'=>$min_qty));
							$ok = FALSE;
							if ($return_on_error) throw new Exception('exit all loops', 10);
						}
						if ($max_qty !== NULL AND $qty_s > $max_qty)
						{
							$error_msg .= __('status.info_check_orderitems_error.sku_max_qty_required', array('%1'=>$obj->getName(Request::$lang),'%2'=>$formatted_day,'%3'=>$qty_s,'%4'=>$max_qty));
							$ok = FALSE;
							if ($return_on_error) throw new Exception('exit all loops', 10);
						}
						if ($add_orderitem_id == NULL AND $mod_qty != NULL AND $qty_s % $mod_qty != 0)
						{
							$error_msg .= __('status.info_check_orderitems_error.sku_mod_qty_required', array('%1'=>$obj->getName(Request::$lang),'%2'=>$formatted_day,'%3'=>$qty_s,'%4'=>$mod_qty));
							$ok = FALSE;
							if ($return_on_error) throw new Exception('exit all loops', 10);
						}
						if (isset($promo_id))
						{
							$promo = ORM::factory('Skp_Promocode', $promo_id);
							// We don't use isValid() function at the moment because it returns FALSE if the promo is either sold out or reached max user limit or ended
							// If this function will return also a message explaining why the result is FALSE then we can use directly this function
							$b2b_company = $this->user->getB2BCompany($this->merchant);
							$bought_items = $promo->countBoughtPromoItems($this->user, $b2b_company);
							if ($promo->loaded() AND $promo->getExtra('max_products_per_user') != NULL AND $promo->getExtra('max_products_per_user') <= $bought_items)
							{
								$error_msg .= __('status.info_check_orderitems_error.promo_max_qty_per_user', array('%1'=>$obj->getName(Request::$lang),'%2'=>$formatted_day,'%3'=>$promo->getExtra('max_products_per_user')));
								$ok = FALSE;
								if ($return_on_error) throw new Exception('exit all loops', 10);
							}
							$basket_items = $promo->countBasketPromoItems($this->user, $b2b_company);
							$promo_items_in_purchase_qty = $promo->getExtra('sold_products') + $promo->countPromoItemsInOtherBaskets($this->user, $b2b_company) + $basket_items;
							if ($promo->loaded() AND $promo->getExtra('max_products') != NULL AND $promo->getExtra('max_products') <= $promo_items_in_purchase_qty)
							{
								$error_msg .= __('status.info_check_orderitems_error.promo_sold_out', array('%1'=>$obj->getName(Request::$lang),'%2'=>$formatted_day,'%3'=>$promo->getExtra('max_products')));
								$ok = FALSE;
								if ($return_on_error) throw new Exception('exit all loops', 10);
							}
						}
					}
				}
			}
		}catch (Exception $e)
		{
			if ($e->getCode() != 10)
			{
				throw $e;
			}
		}

		$info = $ok?__('status.info_check_tickets_ok'):$error_msg;
		$this->status_info = $info;
		$this->save();
		return $ok;
	}

	public function precheckStatus()
	{
		$now = new DateTime('now');
		if (strtoupper($this->get('status')) == static::STATUS_PRECHECK_OK)
		{
			$booking_status = $this->getBookingExpirationDateTime();
			if (($now->format('U') - (DateTime::createFromFormat('Y-m-d H:i:s', $this->get('lastupdate_date'))->format('U'))) > self::BASKET_TTL
			OR $booking_status->expired == TRUE)
			{
				if ($booking_status->expired == TRUE)
				{
					Booking::cleanOrderBookings($this);
				}
				$this->resetPrecheck();
			}
		}
		$this->checkInvoiceReceiptChoice();
		return (strtoupper($this->get('status')) == static::STATUS_PRECHECK_OK);
	}

	// isB2B() is only for old B2B Orders, those with different model
	public function isB2B()
	{
		return FALSE;
	}

	// isNewB2B() is to regognize if the Order is B2C or new B2B
	public function isNewB2B()
	{
		return ($this->get('b2b_company_id') != NULL);
	}

	public function getOrderExternals($connector_id = NULL)
	{
		$o_ext = $this->get('order_externals');
		$connector_id === NULL OR $o_ext->where('connector_id', '=', $connector_id);
		return $o_ext->find_all();
	}

	public function getOrderExternal($connector_id, $external_order_type = NULL)
	{
		$o_ext = $this->get('order_externals')->where('connector_id', '=', $connector_id);

		if ($external_order_type != NULL)
		{
			$o_ext->where('external_order_type', '=', $external_order_type);
		}
		$o_ext->find();
		if ($o_ext->loaded() == FALSE)
		{
			// isB2B() is only for old B2B orders, for new B2B orders we will use $o_ext->set('order_id', $this->pk());
			if ($this->isB2B() == FALSE)
			{
				$o_ext->set('order_id', $this->pk());
			}else{
				$o_ext->set('b2b_order_id', $this->pk());
			}
			$o_ext->set('connector_id', $connector_id);
		}
		return $o_ext;
	}

	public function createOrderInternal()
	{
		$some_errors = FALSE;
		if ($this->type == static::TYPE_ORDER AND $this->status == static::STATUS_PAYMENT_AUTHORIZED)
		{
			$connector_class_list = array_merge(Skp_Skidata_Connector::$implemented_classes, Skp_Teamaxess_Connector::$implemented_classes, Skp_Internalshop_Connector::$implemented_classes, Skp_TeamaxessDCI4WTP_Connector::$implemented_classes);
			$exceptions_list = array();
			foreach ($connector_class_list as $conn_class)
			{
				$conn_class .= '_Connector';
				$global_results = $conn_class::createOrder($this);
				foreach ($global_results as $external_order_type => $results)
				{
					foreach($results as $connector_id => $result)
					{
						$o_ext = $this->getOrderExternal($connector_id, $external_order_type);
						$o_ext->status = $result->status;
						$o_ext->external_reference = $result->external_reference;
						$o_ext->set('external_extra', $result->external_extra);
						$o_ext->set('external_order_type', $result->external_order_type);
						$o_ext->set('next_sync', ($result->next_sync_date != NULL ? $result->next_sync_date->format('Y-m-d H:i:s') : NULL));
						$o_ext->save();
						if ($result->status != static::STATUS_BOOKED AND $result->status != static::STATUS_PENDING)
						{
							$some_errors = TRUE;
							$exceptions_list[] = $result->error; // This ia an Exception
						}
					}
				}
				if ($some_errors == TRUE)
				{
					break;
				}
			}

			if ($some_errors === FALSE)
			{
				$this->set('type', static::TYPE_ORDER);
				$this->set('status', static::STATUS_BOOKED);
				$this->order_datetime = gmdate('Y-m-d H:i:s');
				$this->save();
				$this->confirmOrder();
			}else{
				$cancellation_some_errors = FALSE;
				$connector_classes_list = $this->getConnectorClassesList(OrderExternal::STATUS_BOOKED);
				$results = array();
				foreach($connector_classes_list as $connector_class_name)
				{
					$connector_class_name = $connector_class_name . '_Connector';
					$results[] = $connector_class_name::cancelOrder($this);
				}
				// If some orders were booked succesfully we delete them
				if (sizeof($results) > 0)
				{
					$cancelation_order = $this->getCancelationOrder();
					foreach ($results as $result)
					{
						foreach($result as $connector_id => $connector_result_order_type)
						{
							foreach ($connector_result_order_type as $orderType =>$connector_result)
							{
								$o_ext = $cancelation_order->getOrderExternal($connector_id, $orderType);
								$o_ext->status = $connector_result->status;
								$o_ext->external_reference = $connector_result->external_reference;
								$o_ext->set('external_extra', $connector_result->external_extra);
								$o_ext->set('external_order_type', $connector_result->external_order_type);
								$o_ext->save();
								if ($connector_result->status != OrderExternal::STATUS_BOOKED)
								{
									$cancellation_some_errors = TRUE;
								}
							}
						}
					}
					if ($cancellation_some_errors != FALSE)
					{
						$exceptions_list[] = new Exception(__('Cancellation of Order Failed')); // throw new Exception ();
					}
				}
				$this->set('type', static::TYPE_ORDER);
				$this->set('status', static::STATUS_ERROR);
				$this->save();
				
				$skp_e = new Skp_Exception('Error creating order (see property data)');
				$skp_e->setData($exceptions_list);
				throw $skp_e;
			}
			// now we update the user type for this merchant
			UserMerchant::updateUserType($this->get('user'), $this->get('merchant'));
		}
		else
		{
			throw new Exception('exception.error_order_already_produced_or_not_prechecked (type: '.$this->type.' status: '.$this->status);
		}
	}

	public function createOrder()
	{
		$this->checkOrderConfiguration();
		$this->createOrderInternal();
		sleep(2);
		$this->updateOrderStatus(); // To get all external References
	}
	
	public function checkOrderConfiguration()
	{
		try
		{
			foreach ($this->get('items')->find_all() as $item)
			{
				$sku = $item->get('sku');
				if ($sku->isActive() == FALSE)
				{
					throw new Exception('Sku ['.$sku->getName(I18n::lang()).' (ID: '.$sku->pk().')] not active anymore.');
				}
				if ($sku->isPackage() == FALSE)
				{
					if ($sku->needsBooking() AND $item->get('booking')->loaded() == FALSE)
					{
						throw new Exception('Missing booking for orderitem ['.$item->getName(I18n::lang()).' (ID: '.$item->pk().')]');
					}
					if ($item->needContactProfile() AND $item->get('person')->loaded() == FALSE)
					{
						throw new Exception('Missing person for orderitem ['.$item->getName(I18n::lang()).' (ID: '.$item->pk().')]');
					}
					$need_card = $this->isB2B() ? $item->get('sku')->get('b2b_need_card') : $item->get('sku')->get('need_card');
					$card_data_set = $item->get('card')->loaded();
					$card_bought = $item->isWithIdentificationCard();
					if (($need_card == 'Y' AND $card_data_set === FALSE) OR ($need_card == 'YA' AND ($card_data_set === FALSE AND $card_bought === FALSE)) OR ($need_card == 'B' AND ($card_data_set === TRUE OR $card_bought === FALSE)) OR ($card_data_set === TRUE AND $need_card == 'BB'))
					{
						throw new Exception('Wrong card configuration for orderitem ['.$item->getName(I18n::lang()).' (ID: '.$item->pk().')]. Need Card: '.$need_card.' - Registered card: '.($card_data_set?'Yes':'No').' - Bought card: '.($card_bought?'Yes':'No'));
					}
				}
			}
		} catch (Exception $e)
		{
			throw new Exception('Check order configuration failed: '.$e->getMessage());
		}
	}

	public function createOrderInvoice()
	{
		$created_invoice = FALSE;
		// has_extra_addon Invoice/Receipt => Generate Automatically
		if ($this->get('merchant')->hasAddOn('Invoice/Receipt', 'Generate Automatically'))
		{
			$invoice_receipts_order = $this->getInvoiceReceiptOrderPending();
			if ($invoice_receipts_order != NULL)
			{
				$created_invoice = $invoice_receipts_order->createPdfFile($this->get('user')->get('last_language'));
			}
		}
		return $created_invoice;
	}

	public function getInvoiceReceiptOrderPending()
	{
		foreach ($this->get('invoice_receipt_orders')->find_all() as $inv_rec_ord)
		{
			if ($inv_rec_ord->getStatus() == InvoiceReceiptOrder::PENDING)
			{
				return $inv_rec_ord;
			}
		}
		return NULL;
	}

	public function confirmOrder()
	{
		if ($this->get('type') == static::TYPE_ORDER AND $this->get('status') == static::STATUS_BOOKED)
		{
			$active_export_connectors = array();
			foreach ($this->get('items')->find_all() as $oi)
			{
				$confirm_purchase_result = $oi->confirmOrderItemPurchase();
				$active_export_connectors = array_merge(Arr::get($confirm_purchase_result, 'active_export_connectors', array()), $active_export_connectors);
			}
			$this->get('user')->createExportRecords($this->get('merchant'), array('active_export_connectors' => $active_export_connectors));
		}
	}

	public function getConnectorsList($checking_only_items_in_status = NULL)
	{
		$connectors_list = array();
		$order_items = $this->get('items');
		if ($checking_only_items_in_status != NULL)
		{
			$order_items->where('status', '=', $checking_only_items_in_status);
		}
		foreach ($order_items->find_all() as $item)
		{
			$connector = $item->get('sku')->get('catalog')->get('connector');
			if (Arr::get($connectors_list, $connector->pk()) == NULL)
			{
				$connectors_list[$connector->pk()] = $connector;
			}
		}
		return $connectors_list;
	}

	public function getConnectorClassesList($checking_only_order_external_in_status = NULL)
	{
		$connector_classes_list = array();
		$order_externals = $this->get('order_externals');
		if ($checking_only_order_external_in_status != NULL)
		{
			$order_externals->where('status', '=', $checking_only_order_external_in_status);
		}
		foreach ($order_externals->find_all() as $oe)
		{
			$connector = $oe->get('connector');
			if (in_array($connector->get('connector_class'), $connector_classes_list) == FALSE)
			{
				$connector_classes_list[] = $connector->get('connector_class');
			}
		}
		return $connector_classes_list;
	}

	public function getCancelationOrder()
	{
		$canc_order = ORM::factory('Skp_Order')->where('related_order_id', '=', $this->pk())->find();
		if ($canc_order->loaded() == FALSE)
		{
			$canc_order = $this->createCancelationOrder();
		}
		return $canc_order;
	}
	
	public function isCancelledOrder()
	{
		$canc_order = ORM::factory('Skp_Order')->where('related_order_id', '=', $this->pk())->find();
		return $canc_order->loaded();
	}

	public function createCancelationOrder()
	{
		// The Cancelation Order wasn't found we have a new one and need to mark it
		$original_order_as_array = $this->as_array();
		array_shift($original_order_as_array); // to remove the primary key
		// isB2B() is used for old B2B orders, for new B2B orders are always of Order
		$canc_order = ORM::factory(( $this->isB2B() ? 'Skp_B2BOrder' : 'Skp_Order' ));
		$canc_order->values($original_order_as_array);
		// Now we remove what we don't need
		$canc_order->set('creation_date', gmdate('Y-m-d H:i:s'));
		$canc_order->set('reference_num', NULL);
		$canc_order->set('order_datetime', NULL);
		$canc_order->set('status', Order::STATUS_NEW);
		$canc_order->set('related_order_id', $this)->set('status', static::STATUS_NEW);
		$canc_order->set('marker', NULL);
		$canc_order->set('note_id', NULL);
		$canc_order->set('process_code', NULL);
		$canc_order->save();

		foreach ($this->getMainItems() as $original_item)
		{
			$original_item->createCancelationOrderItem($canc_order);
		}
		$canc_order->save();

		// Clone the Invoice Receipt Data of Completed Invoice Receipt (that are those that need a credit note)
		foreach ($this->get('invoice_receipt_orders')->find_all() as $inv_rec_ord)
		{
			if ($inv_rec_ord->getStatus() == InvoiceReceiptOrder::COMPLETED)
			{
				$inv_rec = ORM::factory('Skp_InvoiceReceiptOrder');
				$inv_rec->set('merchant', $canc_order->get('merchant'));
				$inv_rec->set('order', $canc_order);
				$inv_rec->set('reference_num', NULL);
				$inv_rec->set('creation_date', NULL);
				$inv_rec->set('user', $canc_order->get('user'));
				$inv_rec->set('model', $inv_rec_ord->get('model'));
				$inv_rec->set('header', $inv_rec_ord->get('header'));
				$inv_rec->set('extra', $inv_rec_ord->get('extra'));
				$inv_rec->save();
			}
		}
		return $canc_order;
	}

	public function cancelOrder()
	{
		if ($this->get('type') != static::TYPE_ORDER)
		{
			throw new Exception ('Only Order with TYPE = ' . static::TYPE_ORDER . ' can be cancelled');
		}
		if ($this->get('status') != static::STATUS_BOOKED)
		{
			throw new Exception ('Only Order in STATUS = ' . static::STATUS_BOOKED . ' can be cancelled');
		}
		if ($this->get('related_order')->loaded() == TRUE AND $this->get('related_order')->get('status') == static::STATUS_BOOKED)
		{
			throw new Exception ('This Order has a Cancelation Order ( Order ' . $this->get('related_order')->get('reference_num') . ' - id: ' . $this->get('related_order')->pk() . ' ) and it cannot be cancelled');
		}

		if ($this->get('original_order')->loaded() == TRUE)
		{
			throw new Exception ('This Order is a Cancelation Order of Order ' . $this->get('original_order')->get('reference_num') . ' - id: ( ' . $this->get('original_order')->pk() . ' ) and it cannot be cancelled');
		}

		$cancelation_order = $this->getCancelationOrder();
		if ($cancelation_order->get('status') == static::STATUS_BOOKED)
		{
			throw new Exception ('Order Already Cancelled');
		}
		$some_errors = FALSE;
		$some_success = FALSE;
		$connector_classes_list = $this->getConnectorClassesList(OrderExternal::STATUS_BOOKED);
		$results = array();
		foreach($connector_classes_list as $connector_class_name)
		{
			$connector_class_name = $connector_class_name . '_Connector';
			$results[] = $connector_class_name::cancelOrder($this);
		}
		foreach ($results as $result)
		{
			foreach($result as $connector_id => $connector_order_type_result)
			{
				foreach ($connector_order_type_result as $orderType => $connector_result)
				{
					$o_ext = $cancelation_order->getOrderExternal($connector_id, $orderType);
					$o_ext->status = $connector_result->status;
					$o_ext->external_reference = $connector_result->external_reference;
					$o_ext->set('external_extra', $connector_result->external_extra);
					$o_ext->set('external_order_type', $connector_result->external_order_type);
					$o_ext->save();
					if ($connector_result->status != OrderExternal::STATUS_BOOKED)
					{
						$some_errors = TRUE;
					}else{
						$some_success = TRUE;
					}
				}
			}
		}
		if ($some_errors == FALSE)
		{
			$cancelation_order->set('status', static::STATUS_BOOKED);
			$cancelation_order->order_datetime = gmdate('Y-m-d H:i:s');
			$cancelation_order->save();
			// has_extra_addon Invoice/Receipt => Generate Automatically the Credit Note
			if ($cancelation_order->get('merchant')->hasAddOn('Invoice/Receipt', 'Generate Automatically'))
			{
				$invoice_receipts_order = $cancelation_order->getInvoiceReceiptOrderPending();
				if ($invoice_receipts_order != NULL)
				{
					$invoice_receipts_order->createPdfFile($cancelation_order->get('user')->get('last_language'));
				}
			}
		}else{
			if ($some_success == FALSE)
			{
				// Cancel all invoice receipt order pending
				$inv_rec = $cancelation_order->getInvoiceReceiptOrderPending();
				if ($inv_rec != NULL)
				{
					$inv_rec->delete();
				}
				$cancelation_order->set('type', static::TYPE_BASKET);
				$cancelation_order->delete();
			}
			throw new Exception (__('Cancellation of Order Failed'));
		}
		// Create cancellation payment
		$payment = $this->getLatestPaymentByStatus(array(Payment::CAPTURED, Payment::AUTHORIZED));
		if ($payment->loaded() == TRUE)
		{
			$cancelation_payment = $payment->createCancelationPayment($cancelation_order);
		}
		return $cancelation_order;
	}

	public function getRelatedClass($related, $without_model_prefix = TRUE)
	{
		$original_full_classname = get_class($this->$related);
		$related_classname = ($without_model_prefix == TRUE) ? str_replace('Model_', '', $original_full_classname) : $original_full_classname;
		return $related_classname;
	}

	public function addOrderItem(Sku $sku, $added_qty, DateTime $validity_date = NULL, $pool_id = NULL, OrderItem $main_orderitem = NULL, $promocode_id = NULL)
	{
		if ($this->loaded() === FALSE)
		{
			throw new Exception('exception.error_order_impossible_add_item');
		}

		$single_price = $sku->price(array('validity_date'=>$validity_date));
		
		// because we store the validity date in Server local time we need to convert it here
		$validity_date != NULL AND $validity_date->setTimeZone(new DateTimeZone(date_default_timezone_get()));

		Log::instance()->add(LOG_DEBUG,'Price Sku_id:'.$sku->pk().' validity:'.(($validity_date!=NULL)?$validity_date->format('Y-m-d H:i:s'):'NULL').' found: '.print_r($single_price,TRUE));
		// If amount is NULL or 0 it's not a currency mismatch
		if ($this->currency != $single_price->currency OR $single_price->amount === NULL)
		{
			throw new Exception(__('exception.error_order_in_different_currency', array(':1'=>$sku->pk(), ':2'=>$this->currency, ':3'=>$single_price->currency, ':4'=>$single_price->amount)));
		}
		$promocode_id != NULL AND !$this->skuBelongsToPromotion($sku, $promocode_id) AND $promocode_id = NULL;
		$item_s = array();
		if ($sku->isAggregate() == FALSE)
		{
			for ($i=1; $i<=$added_qty; $i++)
			{
				$item = ORM::factory($this->getRelatedClass('items'));
				$item->set('order_id', $this->pk());
				if ($main_orderitem != NULL)
				{
					$item->set('sub_orderitem_of_id', $main_orderitem->pk());
					if ($sku->get('product')->get('producttype')->get('type') == 'IdentificationCard')
					{
						// Beacuse we are adding a subproduct of type IdentificationCard we need to set the orderitem Identification card to NULL
						$main_orderitem->setCard(NULL);
						$main_orderitem->save();
					}
				}
				$item->set('sku_id', $sku->pk());
				$pool_id == '' AND $pool_id = NULL;
				$item->set('pool_id', $pool_id);
				$item->set('promocode_id', $promocode_id);
				$item->set('product_name', $sku->get('product')->get('name'));
				$item->set('pricecategory_name', $sku->getPriceCategoryExtendedNameArray());
				$item->set('original_connector_class', $sku->get('catalog')->get('connector')->get('connector_class'));
				$item->set('original_connector_id', $sku->get('catalog')->get('connector')->pk());
				$item->set('qty',  1);
				// because we store the validity date in Server local time we need to convert it here
				$item->set('validity_date', ($validity_date != NULL) ? $validity_date->setTimeZone(new DateTimeZone(date_default_timezone_get()))->format('Y-m-d H:i:s') : NULL);
				$item->set('single_amount', $single_price->amount);
				$item->set('total_amount', $item->get('single_amount'));
				$item->set('currency', $single_price->currency);
				$item->updateValidityData($this->get('user')->get('last_language'));
				$vat = $sku->get('product')->get('vat');

				if ($vat->loaded())
				{
					$item->set('vat_id', $vat->pk());
					$item->set('vat_perc', $vat->get('perc'));
					$item->set('vat_amount', Skp_Vat::reverseCalculate($item->get('total_amount'), $vat->get('perc'), $item->get('currency'))->getAmountRounded());
				}else{
					$item->set('vat_id', NULL);
					$item->set('vat_perc', NULL);
					$item->set('vat_amount', NULL);
				}
				$item->save();

				if ($item->isVirtualOrderItem() == TRUE)
				{
					$item->set('single_amount', 0);
					$item->set('vat_amount', 0);
					$item->set('total_amount', $item->get('single_amount'));
					$item->set('vat_amount', 0);
					$item->save();
					foreach ($sku->getPackageSubSkus()->find_all() as $sub_sku)
					{
						$sku_assoc = ORM::factory('Skp_ProductAssociation')->where('main_sku_id', '=', $sku->pk())->and_where('sub_sku_id', '=', $sub_sku->pk())->find();
						$this->addOrderItem($sub_sku, $sku_assoc->get('qty'), $validity_date, $pool_id, $item, $promocode_id);
						// removed $promocode_id, a sub order item must not count as a promo product
					}
				}
				$item_s[] = $item;
				// Log::instance()->add(LOG_DEBUG,print_r($item,TRUE));
			}
		}
		else
		{
			$item = $this->items->where('sku_id','=',$sku->id);
			if ($validity_date != NULL)
			{
				$item->and_where('validity_date', '=', $validity_date->format('Y-m-d H:i:s'));
			}else{
				$item->and_where('validity_date', 'is', NULL);
			}
			if ($main_orderitem != NULL)
			{
				$item = $item->and_where('sub_orderitem_of_id','=', $main_orderitem->pk());
			}
			else
			{
				$item = $item->and_where('sub_orderitem_of_id', 'is', NULL);
			}
			$item = $item->find();
			if ($main_orderitem != NULL)
			{
				$item->set('sub_orderitem_of_id',$main_orderitem->pk());
				if ($sku->get('product')->get('producttype')->get('type') == 'IdentificationCard')
				{
					// Beacuse we are adding a subproduct of type IdentificationCard we need to set the orderitem Identification card to NULL
					$main_orderitem->setCard(NULL);
					$main_orderitem->save();
				}
			}
			$item->set('order_id', $this->pk());
			$item->set('sku_id', $sku->pk());
			$item->set('pool_id', $pool_id);
			$item->set('promocode_id', $promocode_id);
			$item->set('product_name', $sku->get('product')->get('name'));
			$item->set('pricecategory_name', $sku->get('pricecategory')->get('name'));
			$item->set('original_connector_class', $sku->get('catalog')->get('connector')->get('connector_class'));
			$item->set('original_connector_id', $sku->get('catalog')->get('connector')->pk());
			$item->set('qty',  $item->qty + $added_qty);
			// because we store the validity date in Server local time we need to convert it here
			$item->set('validity_date', ($validity_date != NULL) ? $validity_date->setTimeZone(new DateTimeZone(date_default_timezone_get()))->format('Y-m-d H:i:s') : NULL);
			$item->set('single_amount', $single_price->amount);
			$item->set('total_amount', $item->get('single_amount') * $item->get('qty'));
			$item->set('currency', $single_price->currency);
			$item->updateValidityData($this->get('user')->get('last_language'));
			$vat = $sku->get('product')->get('vat');
			if ($vat->loaded())
			{
				$item->set('vat_id', $vat->pk());
				$item->set('vat_perc', $vat->get('perc'));
				$item->set('vat_amount', Skp_Vat::reverseCalculate($item->get('total_amount'), $vat->get('perc'), $item->get('currency'))->getAmountRounded());
			}else{
				$item->set('vat_id', NULL);
				$item->set('vat_perc', NULL);
				$item->set('vat_amount', NULL);
			}
			$item->save();
			if ($sku->isPackage() == TRUE)
			{
				$item->set('single_amount', 0);
				$item->set('total_amount', $item->get('single_amount'));
				$item->set('vat_amount', 0);
				$item->save();
				foreach ($sku->getPackageSubSkus()->find_all() as $sub_sku)
				{
					$sku_assoc = ORM::factory('Skp_ProductAssociation')->where('main_sku_id', '=', $sku->pk())->and_where('sub_sku_id', '=', $sub_sku->pk())->find();
					$this->addOrderItem($sub_sku, $added_qty*$sku_assoc->get('qty'), $validity_date, $pool_id, $item, $promocode_id);
					// removed $promocode_id, a sub order item must not count as a promo product
				}
			}
			$item_s[] = $item;
			// Log::instance()->add(LOG_DEBUG,print_r($item,TRUE));
		}
		// Add tag for future report
		foreach ($item_s as $item)
		{
			$item->addTags();
		}
		$this->resetPrecheck();
		return $item_s;
	}


	public function removeOrderItem(OrderItem $oi, $if_subitem_remove_main_too = FALSE, $delete_all_same = FALSE, $override_belongs_to_package = FALSE)
	{
		if ($this->get('type') == static::TYPE_ORDER) return array();
		$deleted_orderitem_ids = array();
		Log::instance()->add(LOG_DEBUG,'Called remove with:'.$oi->pk());
		if ($this->loaded() === FALSE)
		{
			throw new Exception('exception.error_order_impossible_remove_item');
		}
		$item = $this->items->where('id','=',$oi->pk())->find();
		if ($item->loaded() == TRUE)
		{
			if ($item->get('mainorderitem')->loaded() == TRUE AND $if_subitem_remove_main_too === TRUE)
			{
				$deleted_orderitem_ids = array_merge($deleted_orderitem_ids, $this->removeOrderItem($item->get('mainorderitem'), TRUE));
			}else{
				if ($item->canBeDeletedFromBasket() == FALSE AND $override_belongs_to_package == FALSE)
				{
					throw new Exception(__('exception.error_order_impossible_remove_item_that_belongs_to_package'), 10);
				}
				Log::instance()->add(LOG_DEBUG,'Called remove with:'.$oi->pk());
				if ($delete_all_same == TRUE) {
					$deleted_orderitem_ids = array_merge($deleted_orderitem_ids, $item->deleteAllSameWithAllChildren());
				} else {
					$deleted_orderitem_ids = array_merge($deleted_orderitem_ids, $item->deleteWithAllChildren());
				}
			}
		}
		$this->resetPrecheck();
		return $deleted_orderitem_ids;
	}

	public function getMainItemPools($type_id = NULL)
	{
		$unique_pools_ids = array();
		$result = array();
		foreach ($this->items->where('sub_orderitem_of_id','=',NULL)->find_all() as $item)
		{
			if(!isset($type_id) OR $item->sku->product->producttype == $type_id) {
				$pools = $item->sku->product->getPools();
				foreach ($pools as $p)
				{
					if (!in_array($p->pk(), $unique_pools_ids))
					{
						$unique_pools_ids[] = $p->pk();
						$result[] = $p;
					}
				}
			}
		}
		return $result;
	}

	public function getItemTypes($only_main_items = TRUE)
	{
		$unique_types_ids = array();
		$result = array();
		$items = $this->get('items');
		if ($only_main_items == TRUE)
		{
			$items->where('sub_orderitem_of_id','=',NULL);
		}
		foreach ($items->find_all() as $item)
		{
			$type = $item->sku->product->producttype;
			if (!in_array($type->pk(), $unique_types_ids))
			{
				$unique_types_ids[] = $type->pk();
				$result[] = $type;
			}
		}
		return $result;
	}

	public function getOrderItem($item_id = NULL)
	{
		return (($item_id != NULL) ? $this->get('items')->where('id', '=', $item_id)->find() : NULL);
	}

	public function getMainItems($pool_id = 'all_pools', $aggregate_similar_sku = FALSE, $aggregate_in_config = FALSE)
	{
		$result = $this->get('items')->where('sub_orderitem_of_id','=',NULL)->find_all();
		$filtered_result = array();
		$orderitems_aggregable_found = array();
		foreach ($result as $r)
		{
			$pools = $r->get('sku')->get('product')->getPools();
			if (sizeof($pools) > 0)
			{
				foreach ($pools as $p)
				{
					if ($p->pk() == $pool_id OR $pool_id == 'all_pools')
					{
						if ($aggregate_similar_sku == FALSE OR $r->isAggregable($aggregate_in_config) == FALSE)
						{
							$filtered_result[$r->pk()] = $r;
						}else{
							if (isset($orderitems_aggregable_found[$r->get('sku_id')]) == FALSE)
							{
								$orderitems_aggregable_found[$r->get('sku_id')] = array();
							}
							if (isset($orderitems_aggregable_found[$r->get('sku_id')][$r->get('validity_date')]) == FALSE)
							{
								$orderitems_aggregable_found[$r->get('sku_id')][$r->get('validity_date')] = array();
							}
							$booking_key = '';
							$r->get('booking')->loaded() AND $booking_key = $r->get('booking')->get('availability_id') . '-' . $r->get('booking')->get('calendar_date_time_id');
							$terms_condition = '';
							$r->get('sku')->needsTermsConditions() AND ($terms_condition = ($r->getExtra('terms_condition') == '1' ? 'Y' : 'N' ));
							if (isset($orderitems_aggregable_found[$r->get('sku_id')][$r->get('validity_date')][$booking_key]) == FALSE)
							{
								$orderitems_aggregable_found[$r->get('sku_id')][$r->get('validity_date')][$booking_key] = array();
							}
							if (isset($orderitems_aggregable_found[$r->get('sku_id')][$r->get('validity_date')][$booking_key][$terms_condition]) == FALSE)
							{
								$orderitems_aggregable_found[$r->get('sku_id')][$r->get('validity_date')][$terms_condition] = array();
							}
							if (isset($orderitems_aggregable_found[$r->get('sku_id')][$r->get('validity_date')][$booking_key][$terms_condition][$p->pk()]) == FALSE)
							{
								$orderitems_aggregable_found[$r->get('sku_id')][$r->get('validity_date')][$booking_key][$terms_condition][$p->pk()] = $r;
							}else{
								$r2 = $orderitems_aggregable_found[$r->get('sku_id')][$r->get('validity_date')][$booking_key][$terms_condition][$p->pk()];
								$r2->set('qty', $r2->get('qty') + $r->get('qty'));
								$r2->set('total_amount', $r2->get('total_amount') + $r->get('total_amount'));
								$orderitems_aggregable_found[$r->get('sku_id')][$r->get('validity_date')][$booking_key][$terms_condition][$p->pk()] = $r2;
							}
						}
					}
				}
			}
		}
		foreach ($orderitems_aggregable_found as $array_of_same_skus)
		{
			foreach ($array_of_same_skus as $orderitem_of_same_skus_same_validity_date)
			{
				foreach ($orderitem_of_same_skus_same_validity_date as $orderitem_of_same_skus_same_validity_date_same_booking)
				{
					foreach ($orderitem_of_same_skus_same_validity_date_same_booking as $orderitem_of_same_skus_same_validity_date_same_booking_same_termcond)
					{
						foreach ($orderitem_of_same_skus_same_validity_date_same_booking_same_termcond as $orderitem_of_same_skus_same_validity_date_same_booking_same_termcond_same_pool)
						{
							$filtered_result[$orderitem_of_same_skus_same_validity_date_same_booking_same_termcond_same_pool->pk()] = $orderitem_of_same_skus_same_validity_date_same_booking_same_termcond_same_pool;
						}
					}
				}
			}
		}
		ksort($filtered_result);
		$result = $filtered_result;
		return $result;
	}

	public function removeOrderItemBySku(Sku $sku)
	{
		Log::instance()->add(LOG_DEBUG,'Called removebySku with:'.print_r($sku,TRUE));
		if ($this->loaded() === FALSE)
		{
			throw new Exception('exception.error_order_impossible_remove_item');
		}
		$item = $this->get('items')->where('sku_id','=',$sku->id)->find();
		if ($item->loaded() === TRUE)
		{
			$item->delete();
		}
		$this->resetPrecheck();
	}

	public function userProfileConfirmed()
	{
		return $this->get('user_profile_confirmed');
	}

	public function allIdentificationsSet()
	{
		$check_unique_card_validity_date = array();
		foreach ($this->items->find_all() as $order_item)
		{
			$card = $order_item->get('card');
			// isB2B() is only for Old B2B Order for new B2B Order we use $order_item->get('sku')->get('need_card')
			$need_card = $this->isB2B() ? $order_item->get('sku')->get('b2b_need_card') : $order_item->get('sku')->get('need_card');
			if ($need_card == 'Y' AND $order_item->validity_date != NULL AND ! isset($check_unique_card_validity_date[$order_item->validity_date]))
			{
				$check_unique_card_validity_date[$order_item->validity_date] = array();
			}
			if ($need_card == 'Y')
			{
				if ($card->loaded() === FALSE)
				{
					return FALSE;
				}
				else
				if (!isset($check_unique_card_validity_date[$order_item->validity_date][$card->pk()]))
				{
					$check_unique_card_validity_date[$order_item->validity_date][$card->pk()] = 1;
				}
				else
				{
					return FALSE;
				}
			}
			// need_card values Y = an existing card is mandatory, YA = an existing or new card is mandatory, B = buying a card is mandatory, BB = buy or bring, O = a card (new or existing) is optional, N = no card is needed
			if (($need_card == 'YA' AND $card->loaded() === FALSE AND $order_item->isWithIdentificationCard() === FALSE) OR ($need_card == 'B' AND ($card->loaded() === TRUE OR $order_item->isWithIdentificationCard() === FALSE)) OR ($need_card == 'BB' AND $card->loaded() === TRUE))
			{
				return FALSE;
			}
		}
		return TRUE;
	}

	public function getTotalPrice()
	{
		return new Skp_Money($this->amount, $this->currency);
	}

	public function getSinglePrice()
	{
		return new Skp_Money($this->single_mount, $this->currency);
	}

	public function updateTotalPrice()
	{
		$first = TRUE;
		$this->amount = 0;
		foreach ($this->items->find_all() as $item)
		{
			if ($first)
			{
				$this->currency = $item->getTotalPrice()->currency;
				$first = FALSE;
			}else if ($item->getTotalPrice()->currency != $this->getTotalPrice()->currency)
			{
				throw new Exception ('exception.error_currency_mismatch');
			}
			$this->amount += $item->getTotalPrice()->amount;
		}
	}

	public function getRegisteredPayment($paychannel, $credit_card, $redirect_url)
	{
		$this->type = static::TYPE_ORDER;
		if ($paychannel->loaded() == TRUE)
		{
			if ($this->get('amount') == 0)
			{
				$e = new Skp_Exception(__('error.cannot_create_payment_with_total_amount_zero for orderId: ').$this->pk());
				throw $e;
			}
			$payment = $paychannel->call('register', array('order.order'=>$this, 'terminal.redirect_url' => $redirect_url, 'creditCard' => $credit_card));
		}
		return $payment;
	}

	public function getLatestPaymentByStatus ($status = Payment::AUTHORIZED)
	{
		$payment_status_list = is_array($status) ? $status : array($status);
		$result = ORM::factory('Skp_Payment');
		foreach ($payment_status_list as $status)
		{
			if (!$result->loaded())
			{
				$result = $this->get('payments')->where('amount', '=', $this->amount)->and_where('currency', '=', $this->currency);
				$status != NULL AND $result = $result->and_where('status', '=', $status);
				$result = $result->order_by('id', 'DESC')->find();
			}
		}
		return $result;
	}

	private function setNewReferenceNum()
	{
		do
		{
			$new_ref = $this->get('merchant')->pk().'-'.strtoupper(base_convert(rand(100000000,(int)(100000000*1000)),10,36));
		}
		while (ORM::factory('Skp_Order')->where('reference_num','=',$new_ref)->count_all() >0 );
		$this->reference_num = $new_ref;
	}

	public function confirmAllBookings()
	{
		foreach ($this->get('items')->find_all() as $order_item)
		{
			$booking = $order_item->get('booking');
			if ($booking->loaded() == TRUE)
			{
				$booking->confirm();
			}
		}
	}

	public function setAsTemporaryAllBookings($renew_expiration = FALSE)
	{
		foreach ($this->get('items')->find_all() as $order_item)
		{
			$booking = $order_item->get('booking');
			if ($booking->loaded() == TRUE)
			{
				$booking->setAsTemporary($renew_expiration);
			}
		}
	}

	public function save(Validation $validation = NULL)
	{

		if ($this->reference_num == NULL )
		{
			$this->setNewReferenceNum();
		}
		$this->updateTotalPrice();  // Always update total on save
		if ($this->changed())
		{
			$this->lastupdate_date = gmdate('Y-m-d H:i:s');
		}
		if ($this->status == static::STATUS_BOOKED)
		{
			$this->confirmAllBookings();
		}
		parent::save($validation);
	}

	public function needsPdfReceipt()
	{
		$merchant_pdf_receipt = Arr::get($this->get('merchant')->get('merchant_extra'), 'pdf_receipt');
		return ($merchant_pdf_receipt != 'Never');
	}

/**
 * 
 * this function returns the email receipt of an order or main orderitem
 * @param String lang the language of the receipt
 * @param Array params array of parameters:
 * 		orderitem_id if set get the receipt of only this main orderitem id
 * 		forwarded_receipt if set the email returned is the email of a forwarded receipt
 * 		user_message is the message used for forwarded email
 * @return String the receipt email body
 * 
 */
	public function getReceipt($lang, $params = array())
	{
		$email_type = Arr::get($params, 'email_type', '');
		$email_type == '' OR $email_type .= '_';
		$multilang_config = Kohana::$config->load('multilang');
		// Set the language in I18n mapped from Multilang to I18n (example: en => en_US)
		I18n::lang($multilang_config->languages[$lang]['i18n'].'-'.$this->get('merchant')->get('domain'));
		$default_email_receipt_file = 'shop' . DIRECTORY_SEPARATOR . 'merchants' . DIRECTORY_SEPARATOR . 'default' . DIRECTORY_SEPARATOR . 'receipts' . DIRECTORY_SEPARATOR .'order_'.$email_type.'email_receipt';
		$merchant_email_receipt_file = 'shop' . DIRECTORY_SEPARATOR . 'merchants' . DIRECTORY_SEPARATOR . $this->get('merchant')->get('domain') . DIRECTORY_SEPARATOR . 'receipts' . DIRECTORY_SEPARATOR .'order_'.$email_type.'email_receipt';
		
		if (is_file(MODPATH . 'skiperformance' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . $merchant_email_receipt_file . '.php') == TRUE)
		{
			$receipt = View::factory($merchant_email_receipt_file);
		} else {
			$receipt = View::factory($default_email_receipt_file);
		}
		$receipt->order = $this;
		$receipt->lang = $lang;
		$receipt->user = $this->user;
		$receipt->base_url = Skp_Helper::getProtocol() . Skp_Helper::getDomainEnv($this->get('merchant')->get('domain')) . Route::url($lang.'.shop');
		$receipt->base_ajax_url = Skp_Helper::getProtocol() . Skp_Helper::getDomainEnv($this->get('merchant')->get('domain'));
		$receipt->base_img_url = Skp_Helper::getProtocol() . Skp_Helper::getDomainEnv($this->get('merchant')->get('domain'));
		$receipt->merchant = $this->get('merchant');
		$receipt->pdf_receipt_type = Arr::get($this->get('merchant')->get('merchant_extra'), 'pdf_receipt');

		$domain_array = explode('.' , $this->get('merchant')->get('domain'));
		$receipt->merchant_name = $domain_array[0];

		$receipt->no_show = $this->get('merchant')->getSkpParamConfig(array('email_receipt', 'no_show'));
		$receipt->default_receipt_colors = MODPATH . 'skiperformance' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'shop' . DIRECTORY_SEPARATOR . 'merchants' . DIRECTORY_SEPARATOR . 'default' . DIRECTORY_SEPARATOR . 'receipts' . DIRECTORY_SEPARATOR .'order_email_receipt_colors.php';
		$receipt->merchant_receipt_colors = MODPATH . 'skiperformance' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'shop' . DIRECTORY_SEPARATOR . 'merchants' . DIRECTORY_SEPARATOR . $this->get('merchant')->get('domain') . DIRECTORY_SEPARATOR . 'receipts' . DIRECTORY_SEPARATOR .'order_email_receipt_colors.php';

		$receipt->text_information = __('text.EmailTextInformation', array('merchant'=>$this->merchant));

		$main_order_items = $this->getMainItems();	// replaced: $this->items->where('sub_orderitem_of_id','is',NULL);
		$all_order_items = $this->get('items');
		$receipt->user_message = NULL;
		if ($orderitem_id = Arr::get($params, 'orderitem_id', NULL) AND $orderitem_id != NULL)
		{
			$main_order_items = $this->get('items')->and_where('id', '=', $orderitem_id)->find_all();
			$all_order_items->and_where('id', '=', $orderitem_id);
		}
		if (Arr::get($params, 'hide_price', FALSE) != FALSE) {
			$receipt->no_show[] = 'price';
		}
		if (Arr::get($params, 'forwarded_receipt') !== TRUE)
		{
			// Normal receipt case
			$receipt->no_show[] = 'forwarded_receipt';
		}else{
			// Forwarded message case
			$receipt->no_show[] = 'account_data';
			$receipt->no_show[] = 'order_totals';
			$receipt->user_message = Arr::get($params, 'user_message');
		}
		if (Arr::get($params, 'payment_cancelled') !== NULL) {
			$receipt->payment_cancelled = Arr::get($params, 'payment_cancelled');
		}
		if (Arr::get($params, 'payment') !== NULL) {
			$receipt->payment = Arr::get($params, 'payment');
		}
		if (Arr::get($params, 'error_message') !== NULL) {
			$receipt->error_msg = Arr::get($params, 'error_message');
		}
		$receipt->main_order_items = $main_order_items;
		$receipt->all_order_items = $all_order_items->find_all();
		foreach($receipt->main_order_items as $item) {
			if ($item->sku->product->producttype->type == 'Activity')
				$receipt->activities_information = __('text.EmailActivitiesInformation', array('merchant'=>$this->merchant));
		}
		$receipt->customer_service_phone = __('text.customer_service_phone', array('merchant'=>$this->merchant));
		$receipt->customer_service_email = __('text.customer_service_email', array('merchant'=>$this->merchant));
		$receipt->customer_service_hours = __('text.customer_service_hours', array('merchant'=>$this->merchant));

		$receipt->order_status = $this->get('status');
		$receipt->show_vat = false;
		$receipt->show_card = false;
		foreach($receipt->all_order_items as $oi)
		{
			$receipt->show_vat = ($oi->get('vat_perc') != NULL) ? TRUE : $receipt->show_vat;
			$receipt->show_card = ($oi->get('card_serial') != NULL) ? TRUE : $receipt->show_card;
			if ($receipt->show_vat == TRUE AND $receipt->show_card == TRUE)
			{
				// Since they are already both TRUE we don't need to loop anymore
				break;
			}
		}
		return $receipt;
	}

	public function clean_registered_payment()
	{
		$query = DB::delete('payment')->where('order_id', '=', $this->pk())->and_where('status', '=', Payment::REGISTERED);
		$query->execute();
	}

	public function getReceiptPdfFilename($withPath = TRUE, $orderitem_id = NULL)
	{
		$filename = ($withPath == TRUE) ? $this->get('merchant')->getRelativeMediaPath() . 'receipts-pdf' . DIRECTORY_SEPARATOR : '';
		if ($orderitem_id != NULL) {
			$filename .= $this->get('reference_num') . '-' . $orderitem_id . '.pdf';
		} else {
			$filename .= $this->get('reference_num') . '.pdf';
		}
		return $filename;
	}

	public function getReceiptPdfUrl($absolute = TRUE, $orderitem_id = NULL, $also_if_not_exist = FALSE)
	{
		$pathname = $this->getReceiptPdfFilename(TRUE, $orderitem_id);
		if (Skp_StaticFileManager::existsFile($pathname, static::$filemanager_profile_pdf_order_tickets) == TRUE OR $also_if_not_exist)
		{
			$url_domain_base = (($absolute == TRUE) ? Skp_Helper::getProtocol() . Skp_Helper::getDomainEnv($this->get('merchant')->get('domain')) : '') . URL::base();
			$url = $this->get('merchant')->getRelativeMediaPath() . 'receipts-pdf' . DIRECTORY_SEPARATOR . $this->getReceiptPdfFilename(FALSE, $orderitem_id);
			return Skp_StaticFileManager::getUrl($url, $url_domain_base.$url, TRUE, static::$filemanager_profile_pdf_order_tickets);
		} else {
			return NULL;
		}
	}

	public function getAllProductsPDF()
	{
		return Skp_Helper::getProtocol() . Skp_Helper::getDomainEnv($this->get('merchant')->get('domain')).Route::url('newshop.getAllProductsPDF', array('data_type'=>'pdf', 'lang'=>Request::$lang, 'controller'=>'Shop_Widget_Order', 'action'=>'get_pdf_allTickets', 'reference_num' => $this->get('reference_num')));
	}

	public function getReceiptPdfTemplate($filename)
	{
		$merchant_template_file = MODPATH . 'skiperformance' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'shop' . DIRECTORY_SEPARATOR . 'merchants' . DIRECTORY_SEPARATOR . $this->get('merchant')->get('domain') . DIRECTORY_SEPARATOR . 'receipts' . DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR . $filename . '.php';
		$default_template_file = MODPATH . 'skiperformance' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'shop' . DIRECTORY_SEPARATOR . 'merchants' . DIRECTORY_SEPARATOR . 'default' . DIRECTORY_SEPARATOR . 'receipts' . DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR . $filename . '.php';
		return is_file($merchant_template_file) == TRUE ? $merchant_template_file : $default_template_file;
	}

	public function getReceiptPdfSnippetTemplate($what, $product_type = NULL)
	{
		$view_dir = MODPATH . 'skiperformance' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'shop' . DIRECTORY_SEPARATOR . 'merchants' . DIRECTORY_SEPARATOR ;

		$template_product_type_file = 'order-receipt-' . $what . '-' . ($product_type != NULL ? strtolower($product_type) . '-' : '' ) . 'pdf.php';
		$template_default_type_file = 'order-receipt-' . $what . '-pdf.php';

		$merchant_specific_product_type_specific_file = $view_dir . $this->get('merchant')->get('domain') . DIRECTORY_SEPARATOR . 'receipts' . DIRECTORY_SEPARATOR . $template_product_type_file;
		$merchant_specific_default_type_file = $view_dir . $this->get('merchant')->get('domain') . DIRECTORY_SEPARATOR . 'receipts' . DIRECTORY_SEPARATOR . $template_default_type_file;

		$global_product_type_specific_file = $view_dir . 'default' . DIRECTORY_SEPARATOR . 'receipts' . DIRECTORY_SEPARATOR . $template_product_type_file;
		$global_default_type_file = $view_dir . 'default' . DIRECTORY_SEPARATOR . 'receipts' . DIRECTORY_SEPARATOR . $template_default_type_file;

		if (is_file($merchant_specific_product_type_specific_file) == TRUE) return $merchant_specific_product_type_specific_file;
		if (is_file($global_product_type_specific_file) == TRUE) return $global_product_type_specific_file;
		if (is_file($merchant_specific_default_type_file) == TRUE) return $merchant_specific_default_type_file;
		if (is_file($global_default_type_file) == TRUE) return $global_default_type_file;
	}

	public function getReceiptPdfProductTemplate($product_type = NULL)
	{
		return $this->getReceiptPdfSnippetTemplate('product', $product_type);
	}

	public function getReceiptPdfBookingTemplate($product_type = NULL)
	{
		return $this->getReceiptPdfSnippetTemplate('booking', $product_type);
	}

	public function getReceiptPdfContactProfileTemplate()
	{
		return $this->getReceiptPdfSnippetTemplate('contact-profile');
	}

	public function createReceiptPdfFile($lang)
	{
		$merchant = $this->get('merchant');
		$multilang_config = Kohana::$config->load('multilang');
		// Set the language in I18n mapped from Multilang to I18n (example: en => en_US)
		I18n::lang($multilang_config->languages[$lang]['i18n'].'-'.$merchant->get('domain'));

		$dPath = MODPATH . 'skiperformance' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'shop' . DIRECTORY_SEPARATOR . 'merchants' . DIRECTORY_SEPARATOR.'default' . DIRECTORY_SEPARATOR . 'receipts' . DIRECTORY_SEPARATOR;
		$order = $this; // this is used in the template

		include($this->getReceiptPdfTemplate('overview-loop'));
		$pdf_receipt_type = Arr::get($this->get('merchant')->get('merchant_extra'), 'pdf_receipt');
		if ($pdf_receipt_type == Merchant::PDF_RECEIPT_RECEIPT_AND_PRODUCTS OR $pdf_receipt_type == Merchant::PDF_RECEIPT_RECEIPT_AND_PRODUCTS_AND_EMAIL_ALL)
		{
			include($this->getReceiptPdfTemplate('single-item-loop'));
		}
		//Uncomment this to create pdf all_tickets in filesystem
		//$this->mergeReceiptPdfFile(TRUE);
	}
	
	public function mergeReceiptPdfFile($need_save = TRUE)
	{
		// define some files to concatenate
		$files = array();
		foreach ($this->getMainItems() as $mainitem)
		{
			$filename = $this->getReceiptPdfFilename(TRUE, $mainitem->pk());
			if (Skp_StaticFileManager::existsFile($filename, static::$filemanager_profile_pdf_order_tickets)){
				$files[] = $filename;
			}
		}
		if (count($files) > 0) {
			require_once(Kohana::find_file('vendor', 'tcpdf/tcpdf_include'));
			require_once(Kohana::find_file('vendor', 'fpdi/fpdi'));
			// initiate FPDI
			$pdf = new Skp_FPDI();
			// Important: prevent black line in header and footer
			$pdf->setPrintHeader(FALSE);
			$pdf->setPrintFooter(FALSE);
			// iterate through the files
			foreach ($files AS $file) {
				if ($file !== NULL) {
					try
					{
						$temp = tmpfile();
						fwrite($temp, Skp_StaticFileManager::readFile($file, static::$filemanager_profile_pdf_order_tickets));
						fseek($temp, 0);
						$file = stream_get_meta_data($temp)['uri'];
						// get the page count
						$pageCount = $pdf->setSourceFile($file);
						// iterate through all pages
						for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
							// import a page
							$templateId = $pdf->importPage($pageNo);
							// get the size of the imported page
							$size = $pdf->getTemplateSize($templateId);

							// create a page (landscape or portrait depending on the imported page size)
							if ($size['w'] > $size['h']) {
								$pdf->AddPage('L', array($size['w'], $size['h']));
							} else {
								$pdf->AddPage('P', array($size['w'], $size['h']));
							}

							// use the imported page
							$pdf->useTemplate($templateId);
						}
					}catch(Exception $e)
					{
						
					}finally
					{
						fclose($temp);
					}
				}
			}
			// Close and save file
			if ($need_save)
			{
				$pathname = $this->getReceiptPdfFilename(TRUE, 'all_tickets');
				// This method has several options, check the source code documentation for more information.
				$pdf->Output($pathname, 'F', 'public-read');
			} else {
				$pdf->Output($this->getReceiptPdfFilename(FALSE, 'all_tickets'), 'I');
			}
		}
	}

	public function updateOrderStatus()
	{
		foreach ($this->get('order_externals')->find_all() as $oe)
		{
			$oe->updateStatus();
		}
	}

	public function getNumberOfItems()
	{
		$total_items = 0;
		foreach($this->get('items')->find_all() as $item)
		{
			if ($item->isVirtualOrderItem() == FALSE)
			{
				$total_items += $item->get('qty');
			}
		}
		return $total_items;
	}

	public function getVatTotals()
	{
		$sql_query = 'SELECT oi.vat_perc AS vat_perc, SUM(oi.total_amount) AS total_amount, oi.currency AS currency FROM skp_order_item oi WHERE oi.order_id=:order_id AND oi.vat_perc IS NOT NULL GROUP BY vat_perc, currency ORDER BY currency, vat_perc';
		$query = DB::query(Database::SELECT, $sql_query);
		$query->param(':order_id', $this->pk());

		$rows = $query->as_object()->execute();
		$result = array();
		foreach ($rows as $row)
		{
			$r = new StdClass;
			$money_vat = Skp_Vat::reverseCalculate($row->total_amount, $row->vat_perc, $row->currency);
			$result[] = new Skp_Vat($row->vat_perc, $money_vat->amount, $money_vat->currency);
		}
		return $result;
	}
	
	public function getOrderVatAmount()
	{
		$amount = 0;
		foreach ($this->getVatTotals() as $money_vat)
		{
			$amount += $money_vat->vat_amount;
		}
		return $amount;
		$sql_query = 'SELECT SUM(oi.vat_amount) AS vat_amount FROM skp_order_item oi WHERE oi.order_id=:order_id AND oi.vat_perc IS NOT NULL';
		$query = DB::query(Database::SELECT, $sql_query);
		$query->param(':order_id', $this->pk());

		$rows = $query->as_object()->execute();		
		$amount = 0;
		foreach ($rows as $row)
		{
			$amount += $row->vat_amount;
		}
		return $amount;
	}

	function getSalesReport($params = array())
	{
		$merchant = Arr::get($params, 'merchant');
		if ($merchant == NULL)
		{
			throw new Exception('merchant not set');
		}
		$default_dateto = new DateTime('today');
		$date_to = Arr::get($params, 'date_to', $default_dateto);
		$default_datefrom = clone $date_to;
		$default_datefrom->sub(new DateInterval('P366D'));
		$date_from = Arr::get($params, 'date_from', $default_datefrom);
		$add_first_date = Arr::get($params, 'add_first_date', FALSE);
		$add_last_date = Arr::get($params, 'add_last_date', TRUE);
		$order_relation_type = Arr::get($params, 'order_relation_type', NULL);

		$sql_query = 'SELECT DATE(o.order_datetime) AS date, SUM(o.amount) as amount, o.currency AS currency, COUNT(o.id) AS num_orders FROM skp_order o WHERE o.merchant_id=:merchant_id AND o.type=\''.static::TYPE_ORDER.'\' AND o.status=\'' . static::STATUS_BOOKED . '\' AND DATE(o.order_datetime)>=:date_from AND DATE(o.order_datetime)<=:date_to';
		if ($order_relation_type == Order::RELATION_B2C)
		{
			$sql_query .= ' AND o.b2b_company_id IS NULL';
		}
		else if ($order_relation_type == Order::RELATION_B2B)
		{
			$sql_query .= ' AND o.b2b_company_id IS NOT NULL';
		}

		$sql_query .= ' GROUP BY date, currency ORDER BY date, currency';
		$query = DB::query(Database::SELECT, $sql_query);
		$query->param(':merchant_id', $merchant->pk());
		$query->param(':date_from', $date_from->format('Y-m-d'));
		$query->param(':date_to', $date_to->format('Y-m-d'));

		$rows = $query->as_object()->execute();
		$result = array();
		$result['sales'] = array();
		$result['overall'] = array();
		$result['overall']['sales_amount'] = 0;
		$result['overall']['orders_count'] = 0;
		foreach ($rows as $row)
		{
			$r = new StdClass;
			$r->date = $row->date;
			$r->amount = new Skp_Money($row->amount, $row->currency);
			$result['sales'][] = $r;
			$result['overall']['sales_amount'] += $row->amount;
			$result['overall']['orders_count'] += $row->num_orders;
		}
		if (sizeof($result['sales'])>0)
		{
			if ($add_first_date == TRUE AND $result['sales'][0]->date != $date_from->format('Y-m-d'))
			{
				$empty_data = new StdClass;
				$empty_data->date = $date_from->format('Y-m-d');
				$empty_data->amount = new Skp_Money(0, $result['sales'][0]->amount->currency);
				array_unshift($result['sales'], $empty_data);
			}
			if ($add_last_date == TRUE AND $result['sales'][sizeof($result['sales'])-1]->date != $date_to->format('Y-m-d'))
			{
				$empty_data = new StdClass;
				$empty_data->date = $date_to->format('Y-m-d');
				$empty_data->amount = new Skp_Money(0, $result['sales'][0]->amount->currency);
				$result['sales'][sizeof($result['sales'])] = $empty_data;
			}
		}
		return $result;
	}

	static function getProductReport($params = array())
	{
		$merchant = Arr::get($params, 'merchant');
		if ($merchant == NULL)
		{
			throw new Exception('merchant not set');
		}
		$lang = Arr::get($params, 'lang', 'en');
		$default_dateto = new DateTime('today');
		$date_to = Arr::get($params, 'date_to', NULL);
		$date_to = ($date_to == NULL ? $default_dateto : $date_to);
		$default_datefrom = clone $date_to;
		$default_datefrom->sub(new DateInterval('P366D'));
		$date_from = Arr::get($params, 'date_from', $default_datefrom);
		$order_relation_type = Arr::get($params, 'order_relation_type', NULL);
		
		$sql_query = 'SELECT s.product_id, oi.product_name, s.pricecategory_id, oi.pricecategory_name, p.producttype_id, SUM(oi.qty) AS qty, SUM(oi.total_amount) AS amount FROM skp_order o JOIN skp_order_item oi on oi.order_id=o.id LEFT JOIN skp_sku s ON s.id=oi.sku_id LEFT JOIN skp_product p ON p.id=s.product_id WHERE o.merchant_id=:merchant_id AND o.type=\''.static::TYPE_ORDER.'\' AND o.status=\'' . static::STATUS_BOOKED . '\' AND DATE(o.order_datetime)>=:date_from AND DATE(o.order_datetime)<=:date_to'; //oi2 IS NULL is to not included the package products in the total count but only their subproducts
		if ($order_relation_type == Order::RELATION_B2C)
		{
			$sql_query .= ' AND o.b2b_company_id IS NULL';
		}
		else if ($order_relation_type == Order::RELATION_B2B)
		{
			$sql_query .= ' AND o.b2b_company_id IS NOT NULL';
		}
		$sql_query .= ' GROUP BY s.product_id, s.pricecategory_id, oi.product_name, oi.pricecategory_name';
		$query = DB::query(Database::SELECT, $sql_query);
		$query->param(':merchant_id', $merchant->pk());
		$query->param(':date_from', $date_from->format('Y-m-d'));
		$query->param(':date_to', $date_to->format('Y-m-d'));
		
		$rows = $query->as_object()->execute();
		$result = array();
		$product_id = NULL;
		$product_name = NULL;
		$pricecategory_name = NULL;
		$product_key = '-';
		$product_qty = 0;
		$pricecategories_details = array();
		$product_type_sales = array();
		$all_products_sales = array();
		$results['product_type_sales'] = array();
		$results['all_products_sales']['count'] = 0;
		$results['all_products_sales']['amount'] = 0;
		
		foreach ($rows as $row)
		{
			$new_product_name_arr = (Array)json_decode($row->product_name);
			$new_product_name = (($product_name != NULL AND isset($new_product_name_arr[$lang])) ? $new_product_name_arr[$lang]." (id: $row->product_id)": (count($new_product_name_arr)>0 ? array_shift($new_product_name_arr)." (id: $row->product_id)" : ($row->product_name != NULL ? $row->product_name." (id: $row->product_id)" : 'missing')));
			$new_product_key = ($row->product_id != NULL ? $row->product_id : $new_product_name);
			if ($new_product_key != '-' AND $product_key != $new_product_key)
			{
				if ($product_qty > 0)
				{
					$results['products'][$product_key] = array('product_id' => $product_id, 'product_name' => $product_name, 'product_qty' => $product_qty, 'pricecategory' => $pricecategories_details);
				}
				$product_id = NULL;
				$product_name = NULL;
				$product_key = '-';
				$product_qty = 0;
				$pricecategories_details = array();
			}
			$product_type = $row->producttype_id;
			$product_type==NULL AND $product_type='-';
			if (!array_key_exists($product_type, $results['product_type_sales']))
			{
				$results['product_type_sales'][$product_type] = array();
				$results['product_type_sales'][$product_type]['count'] = 0;
				$results['product_type_sales'][$product_type]['amount'] = 0;
			}
			$results['product_type_sales'][$product_type]['count'] += floatval($row->qty);
			$results['product_type_sales'][$product_type]['amount'] += json_decode($row->amount);
			
			$results['all_products_sales']['count'] += floatval($row->qty);
			$results['all_products_sales']['amount'] += json_decode($row->amount);
			
			if ($product_name == 'missing' OR ($product_key == '-'))
			{
				$product_name = $new_product_name;
			}

			if ($product_key == '-')
			{
				$product_id = $row->product_id;
				$product_key = $new_product_key;
			}

			$pricecategory_name_arr = (Array)json_decode($row->pricecategory_name);

			$pricecategory_name = (($pricecategory_name_arr!=NULL AND isset($pricecategory_name_arr[$lang])) ? $pricecategory_name_arr[$lang]." (id:$row->pricecategory_id)" : (count($pricecategory_name_arr)>0 ? array_shift($pricecategory_name_arr)." (id:$row->pricecategory_id)" : ($row->pricecategory_name != NULL ? $row->pricecategory_name." (id:$row->pricecategory_id)" : 'missing')));
			$pricecategory_key = ($row->pricecategory_id != NULL ? $row->pricecategory_id : $pricecategory_name);
			$new_qty_found = floatval($row->qty);
			$qty2 = (isset($pricecategories_details[$pricecategory_key]) ? $pricecategories_details[$pricecategory_key]['pricecategory_qty'] : 0) + $new_qty_found;
			$pricecategories_details[$pricecategory_key] = array('pricecategory_name' => $pricecategory_name, 'pricecategory_qty' => $qty2);
			$product_qty += $new_qty_found;
		}
		if ($product_qty > 0)
		{
			$results['products'][$product_key] = array('product_id' => $product_id, 'product_name' => $product_name, 'product_qty' => $product_qty, 'pricecategory' => $pricecategories_details);
		}
		return $results;
	}
	
	static function getOrderProductReport($params = array())
	{
		$merchant = Arr::get($params, 'merchant');
		if ($merchant == NULL)
		{
			throw new Exception('merchant not set');
		}
		$m_tz = $merchant->getTimeZone();
		$lang = Arr::get($params, 'lang', 'en');
		$default_dateto = new DateTime('today');
		$date_to = Arr::get($params, 'date_to', NULL);
		$date_to = ($date_to == NULL ? $default_dateto : $date_to);
		$default_datefrom = clone $date_to;
		$default_datefrom->sub(new DateInterval('P366D'));
		$date_from = Arr::get($params, 'date_from', $default_datefrom);
		$order_relation_type = Arr::get($params, 'order_relation_type', NULL);
		$date_type = Arr::get($params, 'date_type', 'purchase_date');
		$only_promocode = Arr::get($params, 'only_promocode', NULL);
		$promocode_active = Arr::get($params, 'promocode_active', NULL);
		$promocode_id = Arr::get($params, 'promocode_id', NULL);
		
		$query = DB::select_array(array(
				array('o.id', 'id'),
				array('p.producttype_id', 'producttype_id'),
				array(DB::expr('SUM(skp_oi.total_amount)'), 'amount'),
				array(DB::expr('SUM(skp_oi.qty)'), 'qty')
			))
			->from(array('order','o'))
			->join(array('order_item', 'oi'))
				->on('oi.order_id', "=", 'o.id')
			->join(array('sku', 's'), 'LEFT')
				->on('s.id', "=", 'oi.sku_id')
			->join(array('product', 'p'), 'LEFT')
				->on('p.id', "=", 's.product_id')
			->where('o.merchant_id', '=', DB::expr(':merchant_id'))
			->and_where('o.type', '=', static::TYPE_ORDER)
			->and_where('o.status', '=', static::STATUS_BOOKED);
		if ($order_relation_type == Order::RELATION_B2C)
		{
			$query->and_where('o.b2b_company_id', 'IS', NULL);
		}
		else if ($order_relation_type == Order::RELATION_B2B)
		{
			$query->and_where('o.b2b_company_id', 'IS NOT', NULL);
		}
		if ($date_type == 'purchase_date')
		{
			$query->select(array(DB::expr('DATE(MAX(skp_o.order_datetime))'), 'date'));
			$query->and_where(DB::expr('DATE(skp_o.order_datetime)'), ">=", $date_from->setTimeZone(new DateTimeZone('UTC'))->format('Y-m-d H:i:s'));
			$query->and_where(DB::expr('DATE(skp_o.order_datetime)'), "<=", $date_to->setTimeZone(new DateTimeZone('UTC'))->format('Y-m-d H:i:s'));
		} else
		{
			$query->select(array(DB::expr('DATE(MAX(skp_oi.validity_date))'), 'date'));
			$query->and_where(DB::expr('DATE(skp_oi.validity_date)'), ">=", $date_from->setTimeZone(new DateTimeZone('UTC'))->format('Y-m-d H:i:s'));
			$query->and_where(DB::expr('DATE(skp_oi.validity_date)'), "<=", $date_to->setTimeZone(new DateTimeZone('UTC'))->format('Y-m-d H:i:s'));
		}
		if ($only_promocode != NULL)
		{
			$query->and_where('oi.promocode_id', 'IS NOT', NULL);
		}
		if ($promocode_active != NULL OR $promocode_id != NULL)
		{
			$query->join('promocode')
				->on('oi.promocode_id', "=", 'promocode.id');
		}
		if ($promocode_active != NULL)
		{
			if ($promocode_active == 'active')
			{
				$query->and_where('promocode.is_active', "=", 'Y');
			}
			else if ($promocode_active == 'inactive')
			{
				$query->and_where('promocode.is_active', "=", 'N');
			}
		}
		if ($promocode_id != NULL)
		{
			if ($promocode_id != 'ALL')
			{
				$query->and_where('promocode.id', "=", $promocode_id);
			}
		}
		$query->group_by('o.id');
		$query->group_by('p.producttype_id');
		$query->param(':merchant_id', $merchant->pk());
		$query->param(':date_from', $date_from->format('Y-m-d'));
		$query->param(':date_to', $date_to->format('Y-m-d'));
		$query->param(':m_tz', $m_tz->getName());
		
		Log::instance()->add(LOG_DEBUG,'QUESTA E LA QUERY :'.print_r($query->compile(), TRUE));
		$rows = $query->as_object()->execute();
		$results = array();
		
		$count_order = array();
		$count_order['all_type'] = array();
		$results['product_type_sales'] = array();
		$results['orders_sales']['count_orders'] = 0;
		$results['orders_sales']['count_products'] = 0;
		$results['orders_sales']['amount'] = 0;
		
		foreach ($rows as $row)
		{
			$product_type = $row->producttype_id;
			$order_id = $row->id;
			$product_type==NULL AND $product_type='-';
			if (!array_key_exists($product_type, $results['product_type_sales']))
			{
				$results['product_type_sales'][$product_type] = array();
				$results['product_type_sales'][$product_type]['count'] = 0;
				$results['product_type_sales'][$product_type]['amount'] = 0;
				$results['product_type_sales'][$product_type]['count_orders'] = 0;
				$count_order[$product_type] = array();
			}
			if(!in_array($order_id,$count_order[$product_type]))
			{
				$count_order[$product_type][] = $order_id;
				$results['product_type_sales'][$product_type]['count_orders']++;
			}
			$results['product_type_sales'][$product_type]['count'] += floatval($row->qty);
			$results['product_type_sales'][$product_type]['amount'] += json_decode($row->amount);
			
			if(!in_array($order_id,$count_order['all_type']))
			{
				$count_order['all_type'][] = $order_id;
				$results['orders_sales']['count_orders']++;
			}
			$results['orders_sales']['count_products'] += floatval($row->qty);
			$results['orders_sales']['amount'] += json_decode($row->amount);
		}
		return $results;
	}

	/**
	 * @param integer $order_item_id the id of the order item
	 * @param boolean $add_one if TRUE add one more if FALSE remove one
	 *
	 * @return boolean
	 */

	public function changeItemQty($order_item_id, $add_one, $item_qty = 1)
	{
		$order_item = $this->getOrderItem($order_item_id);
		$added_orderitem_ids = array();
		$deleted_orderitem_ids = array();
		if ($add_one == TRUE)
		{
			// If $add_one == TRUE we need to add 1 order item with the same sku and same validity of the $order_item passed (also the eventually present suborderitems should be addeded)
			for ($i = 0; $i < $item_qty; $i++)
			{
				$added_orderitem_ids = array_merge($added_orderitem_ids, $order_item->cloneOrderItem());
			}
			return $added_orderitem_ids;
		}else{
			// If $add_one == FALSE we need to delete the order item using
			if ($order_item->get('sku')->isAggregate() == TRUE) {
				$old_qty = $order_item->get('qty');
				$new_qty = $old_qty - $item_qty;
				if($new_qty <= 0)
				{
					$deleted_orderitem_ids = $this->removeOrderItem($order_item);
				} else {
					$deleted_orderitem_ids = $this->changeOrderItemQty($order_item, $new_qty);
				}
			} else {
				$oitems_to_delete = $this->getLastOrderItemsOfSameSku($order_item, $item_qty);
				foreach ($oitems_to_delete as $oitem)
				{
					$deleted_orderitem_ids = array_merge($deleted_orderitem_ids, $this->removeOrderItem($oitem));
				}
			}
			return $deleted_orderitem_ids;
		}
	}

	/**
	 * @param OrderItem $order_item the order item object
	 * @param integer $new_qty the new quantity of the item
	 */

	public function changeOrderItemQty(OrderItem $order_item, $new_qty)
	{
		$deleted_orderitem_ids = array();
		$oi_sku = $order_item->get('sku');
		if ($oi_sku->isPackage() == TRUE AND $oi_sku->isAggregate() == 1)
		{
			$sub_sku = $this->get('items')->where('sub_orderitem_of_id','=',$order_item->pk())->find_all();
			$to_delete = array();
			foreach ($sub_sku as $ssku)
			{
				if ($ssku->get('sku')->isAggregate())
				{
					$old_qty_ssku = $ssku->get('qty');
					$new_qty_ssku = $old_qty_ssku - ($old_qty_ssku / $order_item->get('qty'));
					$ssku->set('qty', $new_qty_ssku);
					$ssku->save();
				} else {
					$to_delete[$ssku->get('sku_id')][] = $ssku;
				}
			}
			foreach ($to_delete as $key => $values) {
				$sku_assoc =  ORM::factory('Skp_ProductAssociation')->where('main_sku_id', '=', $order_item->get('sku_id'))->and_where('sub_sku_id', '=', $key)->find();

				$original_qty = $sku_assoc->get('qty');
				for ($i=0; $i < $original_qty; $i++) {
					$deleted_orderitem_ids = $this->removeOrderItem($to_delete[$key][$i], FALSE, FALSE, TRUE);
				}
			}
			$order_item->set('qty', $new_qty);
		} else {
			$order_item->set('qty', $new_qty);
		}
		$order_item->save();
		$this->resetPrecheck();
		return $deleted_orderitem_ids;
	}

	/**
	 * @param Person $person the person object
	 * Update Basket OrderItem person saved data
	 */
	
	public function updateOrderItemPersonData(Person $person = NULL)
	{
		if ($this->get('type') == static::TYPE_BASKET)
		{
			$items_to_be_updated = $this->get('items');
			$person != NULL AND $items_to_be_updated->where('person_id', '=', $person->pk());
			foreach ($items_to_be_updated->find_all() as $oi)
			{
				if ($oi->get('person_id') != NULL)
				{
					$oi->setPerson($oi->get('person'));
					$oi->save();
				}
			}
		}
	}

	public function sendReceipt($email = NULL, $r_lang = NULL)
	{
		$_config = Kohana::$config->load('skiperf');
		$messageChannel = $this->get('merchant')->getMessageChannel();

		$msg_lang = ($r_lang != NULL) ? $r_lang: $this->user->last_language;
		$original_lang = I18n::lang();
		I18n::lang($msg_lang);

		$msg_params = array(
			'merchant.merchant' 	=> $this->merchant,
			'subject' 				=> __('create_message.receipt_email_title',  array(
				':dest_name'		=> $this->merchant->name,
				':ref_num'			=> $this->reference_num)
			),
			'body' 					=> $this->getReceipt($msg_lang),
			'lang' 					=> $msg_lang,
			'order.order' 			=> $this,
			'cc' 						=> $messageChannel->mail_from,
			);
		if ($email != NULL)
		{
			$msg_params['to'] = $email;
			$msg_params['user.user'] = NULL;
			$msg_params['cc'] = NULL;
		}else{
			$msg_params['user.user'] = $this->user;
			$msg_params['bcc'] = $_config->get('email_skiperformance_orders');
		}

		$message = $messageChannel->call('createMessage', $msg_params);
		$message->save();
		$attachments = NULL;
		if ($this->needsPdfReceipt() == TRUE) {
			$pdf_receipt_type = Arr::get($this->get('merchant')->get('merchant_extra'), 'pdf_receipt');
			if ($pdf_receipt_type == Merchant::PDF_RECEIPT_RECEIPT_AND_PRODUCTS_AND_EMAIL_ALL)
			{
				// add orderitem pdf Attachments
				foreach ($this->getMainItems() as $mainitem) {
					if (Skp_StaticFileManager::existsFile($this->getReceiptPdfFilename(TRUE, $mainitem->id), static::$filemanager_profile_pdf_order_tickets) == TRUE)
					{
						$attachment = array(
							'full_filename' => $this->getReceiptPdfFilename(TRUE, $mainitem->id),
							'filename' => __('email.pdf.product_receipt').'-'.$this->getReceiptPdfFilename(FALSE, $mainitem->id),
							'filemanager_profile' => static::$filemanager_profile_pdf_order_tickets
						);
						is_array($attachments) OR  $attachments = array();
						$attachments[] = $attachment;
					}
				}
			}
			if (Skp_StaticFileManager::existsFile($this->getReceiptPdfFilename(TRUE), static::$filemanager_profile_pdf_order_tickets) == TRUE)
			{
				$attachment = array(
					'full_filename' => $this->getReceiptPdfFilename(TRUE),
					'filename' => __('email.pdf.global_receipt').'-'.$this->getReceiptPdfFilename(FALSE),
					'filemanager_profile' => static::$filemanager_profile_pdf_order_tickets
				);
				is_array($attachments) OR  $attachments = array();
				$attachments[] = $attachment;
			}
		}

		foreach ($this->get('invoice_receipt_orders')->find_all() as $invoice_receipt)
		{
			if ($invoice_receipt->loaded() AND $pathname = $invoice_receipt->getContabPdfFilename(TRUE) AND Skp_StaticFileManager::existsFile($pathname, InvoiceReceiptOrder::$filemanager_profile_pdf))
			{
				$attachment = array(
					'full_filename' => $pathname,
					'filename' => __('email.pdf.invoice_receipt').'-'.$invoice_receipt->getContabPdfJustFilename(),
					'filemanager_profile' => InvoiceReceiptOrder::$filemanager_profile_pdf
				);
				is_array($attachments) OR  $attachments = array();
				$attachments[] = $attachment;
			}
		}

		$message->messageChannel->call('sendMessage', array('message.message' => $message, 'attachments' => $attachments));

		I18n::lang($original_lang);
	}
	
	public function sendErrorReceipt($params = array())
	{
		$email = Arr::get($params, 'email');
		$r_lang = Arr::get($params, 'r_lang');
		$payment_cancelled = Arr::get($params, 'payment_cancelled');
		$_config = Kohana::$config->load('skiperf');
		$messageChannel = $this->get('merchant')->getMessageChannel();

		$msg_lang = ($r_lang != NULL) ? $r_lang: $this->user->last_language;
		$original_lang = I18n::lang();
		I18n::lang($msg_lang);

		$msg_params = array(
			'merchant.merchant' 	=> $this->merchant,
			'subject' 				=> __('create_message.error_receipt_email_title',  array(
				':dest_name'		=> $this->merchant->name,
				':ref_num'			=> $this->reference_num)
			),
			'body' 					=> $this->getReceipt($msg_lang, array('email_type'=>'error', 'payment_cancelled'=>$payment_cancelled)),
			'lang' 					=> $msg_lang,
			'order.order' 			=> $this,
			'cc' 						=> $messageChannel->mail_from,
			);
		if ($email != NULL)
		{
			$msg_params['to'] = $email;
			$msg_params['user.user'] = NULL;
			$msg_params['cc'] = NULL;
		}else{
			$msg_params['user.user'] = $this->user;
			$msg_params['bcc'] = $_config->get('email_skiperformance_orders');
		}

		$message = $messageChannel->call('createMessage', $msg_params);
		$message->save();

		$result = $message->messageChannel->call('sendMessage', array('message.message' => $message));
		I18n::lang($original_lang);
		return $result;
	}
	
	public function sendReminder($email = NULL, $r_lang = NULL)
	{
		$_config = Kohana::$config->load('skiperf');
		$messageChannel = $this->get('merchant')->getMessageChannel();
		$msg_lang = ($r_lang != NULL) ? $r_lang: $this->user->last_language;
		$original_lang = I18n::lang();
		I18n::lang($msg_lang);

		$msg_params = array(
			'merchant.merchant' 	=> $this->merchant,
			'subject' 				=> __('create_message.reminder_email_title',  array(
				':dest_name'		=> $this->merchant->name,
				':ref_num'			=> $this->reference_num)
			),
			'body' 					=> $this->getReceipt($msg_lang, array('email_type'=>'reminder')),
			'lang' 					=> $msg_lang,
			'order.order' 			=> $this,
			'cc' 						=> $messageChannel->mail_from,
			);
		if ($email != NULL)
		{
			$msg_params['to'] = $email;
			$msg_params['user.user'] = NULL;
			$msg_params['cc'] = NULL;
		}else{
			$msg_params['user.user'] = $this->user;
			$msg_params['bcc'] = $_config->get('email_skiperformance_orders');
		}

		$message = $messageChannel->call('createMessage', $msg_params);
		$message->save();

		$message->messageChannel->call('sendMessage', array('message.message' => $message));

		I18n::lang($original_lang);
	}

	public function filteredPaymentChannels($params = array())
	{
		$results = array();
		$paychannel_id = Arr::get($params, 'payment_channel_id');
		$pcl_orm = $this->get('merchant')->get('paymentChannels');
		if ($paychannel_id != NULL)
		{
			$pcl_orm = $pcl_orm->and_where('id', '=', $paychannel_id);
		}
		$b2b_company = $this->get('user')->getB2BCompany($this->get('merchant'));
		foreach ($pcl_orm->order_by('sort')->find_all() as $pc)
		{
			if ($this->isNewB2B() == FALSE AND $pc->get('b2c_enabled') == 1)
			{
				$results[] = $pc;
			}else if($this->isNewB2B() == TRUE AND $b2b_company->loaded() == TRUE)
			{
				$b2b_payment_channels = $b2b_company->get('payment_channels')->and_where('payment_channel_id', '=', $pc->pk())->find();
				if ($b2b_payment_channels->loaded() == TRUE)
				{
					$results[] = $pc;
				}
			}
		}
		return $results;
	}

	public function getAvailablePaymentChannels($params = array())
	{
		$payment_channels = array();
		if ($this->get('amount') > 0 )
		{
			$pcl_all = $this->filteredPaymentChannels($params);
			$b2b_company = $this->get('b2b_company');
			foreach($pcl_all as $pc)
			{
				if (($pc->getMinAmount() == NULL OR $pc->getMinAmount() <= $this->get('amount'))
					AND ($pc->getMaxAmount() == NULL OR $pc->getMaxAmount() >= $this->get('amount'))
					AND (!$b2b_company->loaded() OR ($b2b_company->loaded() AND !$b2b_company->isDebitOverThreshold(100, $pc, $this->get('amount')))))
				{
					$payment_channels[] = $pc;
				}
				if ($b2b_company->loaded() AND $b2b_company->isDebitOverThreshold(100, $pc, $this->get('amount'), TRUE))
				{
					try
					{
						if ($b2b_company->processAlert($this->user->last_language, $pc, B2BCompany::ALERT_TYPE_CREDIT_LIMIT, $this->get('amount'), TRUE, $this))
						{
							Log::instance()->add(LOG_DEBUG,"Sent Mail Alert to B2B company ( " . $b2b_company->get('email') . " )\n");
						} else
						{
							Log::instance()->add(LOG_ERR,"NOT Sent Mail Alert to B2B company ( " . $b2b_company->get('email') . " )\n");
						}
					} catch (Exception $e)
					{
						Log::instance()->add(LOG_ERR,"NOT Sent Mail Alert to B2B company ( " . $b2b_company->get('email') . " )\n");
						Log::instance()->add(LOG_ERR, $e->getMessage()."\n");
					}
				}
			}
		}
		return $payment_channels;
	}

	public function getUnavailablePaymentChannelsWarnings($params = array())
	{
		$payment_channels = array();
		if ($this->get('amount') > 0 )
		{
			$pcl_all = $this->filteredPaymentChannels($params);
			$b2b_company = $this->get('b2b_company');
			foreach($pcl_all as $pc)
			{
				if ($b2b_company->loaded() AND $b2b_company->isDebitOverThreshold(100, $pc, $this->get('amount'), TRUE))
				{
					$payment_channels[] = $pc;
				}
			}
		}
		return $payment_channels;
	}

	public function getPaymentChannelIfAvailable($paychannel_id)
	{
		$pcl = $this->getAvailablePaymentChannels(array('payment_channel_id'=>$paychannel_id));
		return Arr::get($pcl, 0, NULL);
	}

	/**
	 * Get Expiration detetime.
	 *
	 *     // Usage
	 *     $order->getExpirationDateTime(array());
	 *
	 * @param   array	$params	parameters: use_default_offset, offset_seconds
	 * @return  StdClass	contains: never_expire, expired, expirationDateTime, expirationIntervalSecs
	 * @uses    Sku::needsDateTimeBooking
	 */
	public function getExpirationDateTime($params = array())
	{
		$booking_result						= $this->getBookingExpirationDateTime($params); // If there's a booking -> never_expire=FALSE, expired=TRUE/FALSE. Else never_expire=TRUE, expired=FALSE
		$promo_result							= $this->getPromoExpirationDateTime($params); // If there's a promo -> never_expire=FALSE, expired=TRUE/FALSE. Else never_expire=TRUE, expired=FALSE
		$result = new StdClass;
		$result->never_expire				= ($booking_result->never_expire AND $promo_result->never_expire);
		$result->expired						= ($booking_result->expired OR $promo_result->expired);
		$result->expirationDateTime 		= $booking_result->expirationDateTime;
		$result->expirationIntervalSecs	= $booking_result->expirationIntervalSecs;
		if ($promo_result->expirationDateTime !== NULL)
		{
			$result->expirationDateTime = $result->expirationDateTime === NULL ? $promo_result->expirationDateTime : min($result->expirationDateTime, $promo_result->expirationDateTime);
		}
		if ($promo_result->expirationIntervalSecs !== NULL)
		{
			$result->expirationIntervalSecs = $result->expirationIntervalSecs === NULL ? $promo_result->expirationIntervalSecs : min($result->expirationIntervalSecs, $promo_result->expirationIntervalSecs);
		}
		return $result;
	}

	/**
	 * Get Booking Expiration detetime.
	 *
	 *     // Usage
	 *     $order->getBookingExpirationDateTime(array());
	 *
	 * @param   array	$params	parameters: use_default_offset, offset_seconds
	 * @return  StdClass	contains: never_expire, expired, expirationDateTime, expirationIntervalSecs
	 * @uses    Sku::needsDateTimeBooking
	 */
	public function getBookingExpirationDateTime($params = array())
	{
		$now = new DateTime('now', new DateTimeZone(date_default_timezone_get()));
		$result = new StdClass;
		$result->never_expire = TRUE;
		$result->expired = FALSE;
		$result->expirationDateTime = NULL;
		$result->expirationIntervalSecs = NULL;
		$offset_seconds = Arr::get($params, 'use_default_offset') != NULL ? $this->merchant->getSkpParamConfig(array('booking', 'booking_reservation_default_offset_secs')): Arr::get($params, 'offset_seconds', 0);
		foreach($this->get('items')->find_all() as $item)
		{
			if ($item->get('sku')->needsBooking() == TRUE)
			{
				$result->never_expire = FALSE;
				$txt_date = $item->get('booking')->get('time_out');
				if ($txt_date != NULL)
				{
					$item_checkout_expired = new DateTime($txt_date, new DateTimeZone(date_default_timezone_get()));
					$item_checkout_expired->sub(new DateInterval('PT' . $offset_seconds . 'S'));
					if ($item_checkout_expired < $now){
						$result->expired = TRUE;
						return $result;
					}
					if ($result->expirationDateTime > $item_checkout_expired OR $result->expirationDateTime == NULL)
					{
						$result->expirationDateTime = $item_checkout_expired;
					}
				}else{
					$result->expired = TRUE;
					return $result;
				}
			}
		}
		if ($result->expirationDateTime != NULL)
		{
			$result->expirationIntervalSecs = ($result->expirationDateTime->getTimestamp() - $now->getTimestamp());
		}
		return $result;
	}

	/**
	 * This function force all the the order item booking not expired to be valid again strating from the last_change param value
	 * 
	 * @param string $last_change value in Y-m-d H:i:s to be used for the temporary bookings not expired
	 * @return integer number of affected bookings
	 * 
	 */
	public function resetBookingsTimeout()
	{
		return Booking::resetOrderBookingsTimeout($this);
	}
	
	public function getPromoExpirationDateTime($params = array())
	{
		$now = new DateTime('now', new DateTimeZone(date_default_timezone_get()));
		$result = new StdClass;
		$result->never_expire = TRUE;
		$result->expired = FALSE;
		$result->expirationDateTime = NULL;
		$result->expirationIntervalSecs = NULL;
		$offset_seconds = Arr::get($params, 'use_default_offset') != NULL ? $this->merchant->getSkpParamConfig(array('booking', 'booking_reservation_default_offset_secs')): Arr::get($params, 'offset_seconds', 0);
		$timeout_promo_secs = self::BASKET_TTL - $offset_seconds;
		
		foreach($this->get('items')->find_all() as $item)
		{
			if ($item->get('promocode')->loaded() == TRUE)
			{
				$result->never_expire = FALSE;
				$lastupdate_date = $this->get('lastupdate_date');
				if ($lastupdate_date != NULL)
				{
					$item_checkout_expired = new DateTime($lastupdate_date);
					$item_checkout_expired->add(new DateInterval('PT' . $timeout_promo_secs . 'S'));
					if ($item_checkout_expired < $now){
						$result->expired = TRUE;
						return $result;
					}
					if ($result->expirationDateTime > $item_checkout_expired OR $result->expirationDateTime == NULL)
					{
						$result->expirationDateTime = $item_checkout_expired;
					}
				}else{
					$result->expired = TRUE;
					return $result;
				}
			}
		}
		if ($result->expirationDateTime != NULL)
		{
			$result->expirationIntervalSecs = ($result->expirationDateTime->getTimestamp() - $now->getTimestamp());
		}
		return $result;
	}
	
	public function getLastOrderItemsOfSameSku($order_item, $item_qty)
	{
		$similar_items = array_merge(array($order_item), $order_item->getSimilarOrderItems());
		$last_similar_items = array_slice($similar_items, count($similar_items) - $item_qty);
		return array_reverse($last_similar_items);
	}

	public static function getOrdersSumCount ($merchant, $date_type, $date_from, $date_to, $b2b_company_ids = array(), $b2b_user_ids = array())
	{
		$fields = array(array(DB::expr('skp_order.b2b_company_id'), 'b2b_company_id'), array(DB::expr('skp_b2b_company.name'), 'b2b_company_name'), array(DB::expr('SUM(`skp_order`.`amount`)'), 'total_order_amount'), array(DB::expr('COUNT(`skp_order`.`id`)'), 'order_count'));
		$query = DB::select_array($fields)
			->from('order')
			->join('b2b_company')
			->on('order.b2b_company_id', '=', 'b2b_company.id')
			->where('order.merchant_id', '=', $merchant->pk())->and_where('type', '=', static::TYPE_ORDER)
			->and_where('order.status', '=', static::STATUS_BOOKED);
		if ($date_type == 'purchase_date' AND ($date_from != NULL OR $date_to != NULL)) //date_type == purchase_date
		{
			$query->where_open();
			$date_from != NULL AND $query->where('order_datetime', '>=', $date_from->setTimeZone(new DateTimeZone('UTC'))->format('Y-m-d H:i:s'));
			$date_to != NULL AND $query->where('order_datetime', '<=', $date_to->setTimeZone(new DateTimeZone('UTC'))->format('Y-m-d H:i:s'));
			$query->where_close();
		}
		$b2b_company_ids == NULL AND $b2b_company_ids = array();
		$b2b_company_ids != NULL AND !is_array($b2b_company_ids) AND $b2b_company_ids = array($b2b_company_ids);
		if (sizeof($b2b_company_ids) > 0 )
		{
			$query->and_where('order.b2b_company_id', 'IN', $b2b_company_ids);
		}else{
			// If b2b_company_ids is an empty array it means all the b2b_orders (b2b_company not NULL) no matter the b2b_company_id
			$query->and_where('order.b2b_company_id', '!=', NULL);
		}
		$b2b_user_ids == NULL AND $b2b_user_ids = array();
		$b2b_user_ids != NULL AND !is_array($b2b_user_ids) AND $b2b_user_ids = array($b2b_user_ids);
		if (sizeof($b2b_user_ids) > 0)
		{
			$query->and_where('order.user_id', 'IN', $b2b_user_ids);
		}
		$query->group_by('order.b2b_company_id');
		
		return $query->distinct(TRUE)->execute();
	}
	
	public function getOrderExternalByType($order_type = 'OnlineBooking')
	{
		return $this->get('order_externals')->where('external_order_type', '=', $order_type)->find_all();
	}
	
	public function isInvoiceMandatory()
	{
		$invoice_receipt_config = $this->merchant->get('invoice_receipt_config');
		if ($this->merchant->hasAddOn('Invoice/Receipt') AND $invoice_receipt_config->getExtra('invoice_request')=='Y') {
			if ($this->merchant->hasAddOn('B2B'))
			{
				$b2b_addon = $this->get('merchant')->get('addons')->where('name', '=', 'B2B')->find();
				if ($this->isNewB2B() AND ($b2b_addon->getExtra('mandatory_invoice') == 'Y' OR $this->get('b2b_company')->getExtra('mandatory_invoice') == 'Y'))
				{
					return TRUE;
				}
			}
			foreach ($this->get('items')->find_all() as $item)
			{
				if ($item->isInvoiceMandatory())
				{
					return TRUE;
				}
			}
		}
		return FALSE;
	}
	
	public function getInvoiceReceiptReferenceNumber()
	{
		$inv_rec_ord = $this->get('invoice_receipt_orders')->find();
		if ($inv_rec_ord->loaded() AND $inv_rec_ord->getStatus() == InvoiceReceiptOrder::COMPLETED)
		{
			return $inv_rec_ord->get('reference_num');
		}
		return NULL;
	}
	
	public function getLatestPaymentChannelName($status = NULL)
	{
		$pm = $this->getLatestPaymentByStatus(NULL);
		return $pm->get('payment_channel')->get('payment_class');
	}

	public function createInvoiceReceiptOrder($profile, $model = InvoiceReceiptProfile::MODEL_INVOICE)
	{
		if ($profile->isComplete($this->get('merchant'), $model))
		{
			$invoice_receipt_order = $this->get('invoice_receipt_orders')->find();
			$invoice_receipt_order->set('merchant', $this->get('merchant'));
			$invoice_receipt_order->set('order', $this);
			$invoice_receipt_order->set('user', $this->get('user'));
			$invoice_receipt_order->set('model', $model);

			// Here we remove from profile data the fields not Mandatory/Optional for the given merchant/(invoice or Receipt)
			$merchant_fields = $this->get('merchant')->get('invoice_receipt_config');
			$merchant_fields = ($model == InvoiceReceiptProfile::MODEL_INVOICE) ? $merchant_fields->get('invoice_fields') : $merchant_fields->get('receipt_fields');
			if ($merchant_fields != NULL)
			{
				foreach ($profile->get('extra') as $field_name => $field_value)
				{
					if (!array_key_exists($field_name, $merchant_fields))
					{
						$profile->setExtra($field_name, NULL);
					}
				}
			}

			$invoice_receipt_order->set('header', $profile->get('extra'));
			$header = ($model == InvoiceReceiptOrder::MODEL_INVOICE ? $this->get('merchant')->get('invoice_receipt_config')->getExtra('invoice_header') : $this->get('merchant')->get('invoice_receipt_config')->getExtra('receipt_header'));
			$invoice_receipt_order->setExtra('config_header',$header);
			$invoice_receipt_order->setExtra('profile_id', $profile->pk());
			$invoice_receipt_order->save();
		}else{
			throw new Exception('Profile data not complete');
		}
		return $invoice_receipt_order;
	}
	
	public function countOrderItems()
	{
		$count = 0;
		foreach ($this->get('items')->find_all() as $item)
		{
			$count += $item->get('qty');
		}
		return $count;
	}
	
	public function getCurrentAvailabilities()
	{
		$order_items = $this->getMainItems('all_pools', TRUE, TRUE);
		$order_availabilities = array();
		foreach ($order_items as $oitem)
		{
			if ($oitem->get('sku')->needsBooking())
			{
				$order_availabilities[$oitem->pk()] = $oitem->getAvailabilities(TRUE, TRUE);
			}
		}
		return $order_availabilities;
	}
	
	public function anonymizeOrderData($anon_user, $person_replacement = array(), $card_replacement = array())
	{
		if ($anon_user->loaded())
		{
			$this->set('user', $anon_user);
			$this->get('note')->loaded() AND $this->get('note')->delete();
			$this->save();
			
			foreach ($this->get('items')->find_all() as $item)
			{
				if ($item->get('person_id') != NULL)
				{
					// The person has not been anonymized yet
					if (!isset($person_replacement[$item->get('person_id')]))
					{
						$anon_person = $item->get('person')->anonymizePersonData($anon_user);
						$person_replacement[$item->get('person_id')] = $anon_person;
					}
					$item->anonymizeOrderItemPerson($person_replacement[$item->get('person_id')]);
				}
				if ($item->get('card_id') != NULL)
				{
					// The card has not been anonymized yet
					if (!isset($card_replacement[$item->get('card_id')]))
					{
						$anon_card = $item->get('card')->anonymizeCardData($anon_user);
						$card_replacement[$item->get('card_id')] = $anon_card;
					}
					$item->setCard($card_replacement[$item->get('card_id')]);
					$item->save();
				}
				$item->getExtra('extra_info') AND $item->setExtra('extra_info', NULL) AND $item->save();
			}
			$this->deleteOrderFiles();
			return array('p_replace'=>$person_replacement, 'c_replace'=>$card_replacement);
		}
	}
	
	public function deleteOrderFiles()
	{
		$order_pdf_receipt = $this->getReceiptPdfFilename();
		if (Skp_StaticFileManager::existsFile($order_pdf_receipt, static::$filemanager_profile_pdf_order_tickets))
		{
			Skp_StaticFileManager::deleteFile($order_pdf_receipt, static::$filemanager_profile_pdf_order_tickets);
		}
		foreach ($this->get('items')->find_all() as $item)
		{
			$item_pdf_receipt = $item->getReceiptPdfFilename();
			if (Skp_StaticFileManager::existsFile($item_pdf_receipt, static::$filemanager_profile_pdf_order_tickets))
			{
				Skp_StaticFileManager::deleteFile($item_pdf_receipt, static::$filemanager_profile_pdf_order_tickets);
			}
		}
	}

	public function skuBelongsToPromotion($sku, $promocode_id)
	{
		$promo_skugroups = $sku->get('skugroups')->where('active', '=', 1)->and_where('web_active', '=', 0)->and_where('skp_skugroup.deleted', '=', '0')->find_all();
		foreach ($promo_skugroups as $sg)
		{
			$promocode = $sg->join('promocode_skugroup')->on('skp_skugroup.id', '=', 'promocode_skugroup.skugroup_id')->where('promocode_skugroup.promocode_id', '=', $promocode_id);
			if ($promocode->loaded())
			{
				return TRUE;
			}
		}
		return FALSE;
	}
	
	public function getProcessCode()
	{
		// We run this at DB level to havoid caching problems
		$process_code = mt_rand(10000, 99999);
		try{
			$db = Database::instance();
			// Transaction Start
			$db->begin();
			// We get an exclusive lock for the record (the row) of the table skp_order, no one can access it until the transaction is finished
			$db->query(Database::SELECT, 'SELECT id FROM skp_order WHERE id='.$this->pk().' FOR UPDATE');
			$sql_query = "UPDATE skp_order SET process_code=:process_code WHERE id=:id AND process_code IS NULL";
			$query = DB::query(Database::UPDATE, $sql_query);
			$query->param(':process_code', $process_code);
			$query->param(':id', $this->pk());
			$ok = ($query->execute() == 1);
			if ($ok)
			{
				$db->commit();
				$this->_object['process_code'] = $process_code; 
				return $process_code;
			}else{
				$db->rollback();
			}
		}catch(Exception $e)
		{
			return FALSE;
		}
		return FALSE;
	}
	
	public function releaseProcessCode($process_code)
	{
		try{
			$db = Database::instance();
			// Transaction Start
			$db->begin();
			// We get an exclusive lock for the record (the row) of the table skp_order, no one can access it until the transaction is finished
			$db->query(Database::SELECT, 'SELECT id FROM skp_order WHERE id='.$this->pk().' FOR UPDATE');
			$sql_query = "UPDATE skp_order SET process_code=NULL WHERE id=:id AND process_code = :process_code";
			$query = DB::query(Database::UPDATE, $sql_query);
			$query->param(':process_code', $process_code);
			$query->param(':id', $this->pk());
			$ok = ($query->execute() == 1);
			if ($ok)
			{
				$db->commit();
				$this->_object['process_code'] = NULL; 
				return $process_code;
			}else{
				$db->rollback();
			}
		}catch(Exception $e)
		{
			return FALSE;
		}
		return FALSE;
	}
	
	public static function getMerchantUsersOldBasketOrders($params = array())
	{
		$merchant_id = Arr::get($params, 'merchant_id', NULL);
		$timeout_user_inactivity_secs = Arr::get($params, 'timeout_user_inactivity_secs'); 	// After this amount of time we think the user is inactive on shop 
		$row_limit =  Arr::get($params, 'row_limit', 100);
		$sql_query = 'SELECT o.id, MAX(umv.visit_end) AS last_activity FROM skp_order o JOIN skp_order_item oi ON o.id=oi.order_id LEFT JOIN skp_user_merchant_visit AS umv ON umv.user_id=o.user_id AND umv.merchant_id=o.merchant_id WHERE o.type=:type '; // not LEFT JOIN
		if ($merchant_id != NULL)
		{
			$sql_query .= 'AND o.merchant_id=:merchant_id ';
		}
		$sql_query .= 'AND oi.id IS NOT NULL GROUP BY o.id HAVING MAX(umv.visit_end)<DATE_SUB(NOW(), INTERVAL :timeout_user_inactivity_secs SECOND) ORDER BY o.id';
		$row_limit !== NULL AND $sql_query .= ' LIMIT :row_limit';
		$query = DB::query(Database::SELECT, $sql_query);
		$query->param(':type', Order::TYPE_BASKET);
		$query->param(':timeout_user_inactivity_secs', $timeout_user_inactivity_secs);
		$query->param(':row_limit', $row_limit);
		if ($merchant_id != NULL)
		{
			$query->param(':merchant_id', $merchant_id);
		}
		
		$order_rows = $query->as_object()->execute();
		return $order_rows;
	}
	
	public static function deleteVirtualUsersOldBasketOrders($params = array())
	{
		$merchant_id = Arr::get($params, 'merchant_id', NULL);
		$delete_row_limit =  Arr::get($params, 'delete_row_limit', 10000);
		$timeout_interval_basket_delete_secs = Arr::get($params, 'timeout_interval_basket_delete_secs');	// After this amount of seconds can delete the basket for inactivity
		
		$sql_del_query = 'DELETE FROM skp_order WHERE id IN (SELECT * FROM (SELECT o.id FROM skp_order as o JOIN skp_user u on u.id=o.user_id AND u.status=:user_status LEFT JOIN skp_user_merchant_visit AS umv on umv.user_id=o.user_id AND umv.merchant_id=o.merchant_id WHERE o.type=:type AND lastupdate_date<DATE_SUB(NOW(), INTERVAL :timeout_interval_basket_delete_secs SECOND)';
		if ($merchant_id != NULL)
		{
			$sql_del_query .= 'AND o.merchant_id=:merchant_id ';
		}
		$sql_del_query .= ' GROUP BY o.id,o.user_id,o.merchant_id HAVING MAX(umv.visit_end)<DATE_SUB(NOW(), INTERVAL :timeout_interval_basket_delete_secs SECOND) OR MAX(umv.visit_end) IS NULL) AS t ) LIMIT :delete_row_limit';

		$del_query = DB::query(Database::DELETE, $sql_del_query);
		$del_query->param(':type', Order::TYPE_BASKET);
		$del_query->param(':timeout_interval_basket_delete_secs', $timeout_interval_basket_delete_secs);
		$del_query->param(':user_status', 'virtual');
		$del_query->param(':delete_row_limit', $delete_row_limit);
		if ($merchant_id != NULL)
		{
			$del_query->param(':merchant_id', $merchant_id);
		}
		
		$deleted_basket = $del_query->execute();
		return $deleted_basket;
	}
	
	public function getOrderItemsId()
	{
		$ids_array = array();
		foreach ($this->get('items')->find_all() as $o_item)
		{
			$ids_array[] = $o_item->pk();
		}
		return $ids_array;
	}
	
	public function getCurrentProductQtys($product_id, $validity_start)
	{
		$validity_start_server = ($validity_start != NULL ? clone $validity_start : new DateTime('now'));
		$validity_start_server->setTimeZone($this->merchant->getTimeZone())->setTime(0, 0, 0)->setTimeZone(new DateTimeZone(date_default_timezone_get()));
		
		$sql_query = 'SELECT s.id AS sku_id, SUM(oi.qty) AS item_qty FROM skp_order o JOIN skp_order_item oi ON o.id=oi.order_id JOIN skp_sku s ON oi.sku_id=s.id WHERE o.id=:order_id AND s.product_id=:product_id AND oi.validity_date=:validity_start GROUP BY sku_id';
		
		$select_query = DB::query(Database::SELECT, $sql_query);
		$select_query->param(':order_id', $this->pk());
		$select_query->param(':product_id', $product_id);
		$select_query->param(':validity_start', $validity_start_server->format('Y-m-d H:i:s'));
		
		$rows = $select_query->as_object()->execute();
		$current_qtys = array('product'=>0,'sku'=>array());
		foreach ($rows as $row)
		{
			$current_qtys['product'] += $row->item_qty;
			isset($current_qtys['sku'][$row->sku_id]) OR $current_qtys['sku'][$row->sku_id] = 0;
			$current_qtys['sku'][$row->sku_id] += $row->item_qty;
		}
		return $current_qtys;
	}
	
	public function getRangeValidityDates($params = array())
	{
		$only_export_connector_id = Arr::get($params, 'export_connector_id');
		$range = array();
		foreach($this->get('items')->find_all() as $item)
		{
			if ($only_export_connector_id === NULL OR $item->get('sku')->get('product')->has('export_connectors', $only_export_connector_id))
			{
				if ($item->isVirtualOrderItem() == FALSE)
				{
					$start_validity = $item->getValidityDate();
					$end_validity = $item->getEndValidityDate($start_validity);
					if (!isset($range['first']) OR $start_validity < $range['first'])
					{
						$range['first'] = $start_validity;
					}
					if (!isset($range['last']) OR $end_validity > $range['last'])
					{
						$range['last'] = $end_validity;
					}
				}
			}
		}
		return $range;
	}
	
	public function deliverOrderExternally($params = array())
	{
		$result = TRUE;
		foreach ($this->getOrderExternalByType('OnlineBooking') as $order_external)
		{
			$connector = $order_external->get('connector');
			$result = ($connector->deliverOrderExternally($order_external, $params) AND $result);
		}
		return $result;
	}
	
	public function getOffers($params = array())
	{
		// Get Offers params
		$show_in = Arr::get($params, 'show_in');
		$audience = Arr::get($params, 'audience');
		$offer_type = Arr::get($params, 'offer_type');
		$merchant = $this->get('merchant');
		
		$offer_params = array(
			'merchant' => $merchant,
			'offer_type' => $offer_type,
			'show_in' => $show_in,
			'audience' => $audience,
			'product_ids' => array(),
		);
		
		foreach ($this->get('items')->find_all() as $oitem)
		{
			$product = $oitem->get('sku')->get('product');
			!in_array($product->pk(), $offer_params['product_ids']) AND $offer_params['product_ids'][] = $product->pk();
		}
		$active_offers = Offer::getOffersContents($offer_params);
		
		return $active_offers;
	}
}
