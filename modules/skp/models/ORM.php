<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class ORM extends Ordit {
	protected $_tags = NULL;

	public function call()
	{
		if (! isset($this->connector) AND isset ($this->catalog))
		{
			$connector = $this->catalog->connector;
		}
		else if (isset($this->connector))
		{
			$connector = $this->connector;
		}
		else
		{
			$connector = $this;
		}	
		if (! isset($this->connector_class))
		{
			$class_name = $connector->connector_class.'_Connector';
		}
		else
		{
			$class_name = $this->connector_class.'_Connector';
		}

		$args = func_get_args();
		$method = array_shift($args); // The method is the first parameter
		$params = array_shift($args);
		if (!method_exists($class_name, $method))
		{
			throw new Skp_Exception_Missing('Class: '.$class_name.' Method: '.$method.' does not exists');
		}
		return $class_name::$method($this, $params);
	}

	public function setArray($array_name, $key, $new_value = NULL)
	{
		$_arr = $this->get($array_name);
		if ($new_value != NULL)
		{
			$_arr[$key] = $new_value;
		}else{
			unset($_arr[$key]);
		}
		$this->set($array_name, $_arr);
	}


	/**
	 * Check if an object has extra.
	 *
	 * @return boolean
	 */
	public function hasExtra()
	{
		return array_key_exists('extra', $this->list_columns());
	}

	/**
	 * Set an extra in extra field.
	 *
	 * @return boolean True on success otherwise false.
	 */
	public function setExtra($extra_name, $extra_value, $save_empty_value = FALSE)
	{
		if ($this->hasExtra())
		{
			$ue = $this->get('extra');
			if ($extra_value != NULL OR ($save_empty_value AND $extra_value !== NULL))
			{
				$ue[$extra_name] = $extra_value;
			}else{
				unset($ue[$extra_name]);
			}
			$this->set('extra', $ue);
			return TRUE;
		} else
		{
			return FALSE;
		}
	}

	/**
	 * Get an extra from extra field.
	 *
	 * @return mixed Value found otherwise default.
	 */
	public function getExtra($key, $default_value = NULL)
	{
		if ($this->hasExtra())
		{
			if ($key != NULL)
			{
				return Arr::get($this->get('extra'), $key, $default_value);
			}
		}
	}


	// This is for Association/deassociation of Tags

	public function getTags()
	{
		if ($this->_tags === NULL)
		{
			$this->_tags = TagAssociation::getAssociatedTags($this);
		}
		return $this->_tags;
	}
	
	public function getTagIds()
	{
		return array_keys($this->getTags());
	}
	
	public function getTagValues()
	{
		$values = array();
		foreach ($this->getTags() as $tag_id => $tag)
		{
			$values[$tag_id] = $tag->get('value');
		}
		return $values;
	}
	
	public function setTags($new_ids)
	{
		$this->_tags = TagAssociation::setAssociation(get_class($this), $this->pk(), $this, $new_ids);
	}

	// - END: This is for Association/deassociation of Tags
	
	public function lazyUnserialize($data)
	{
		// Initialize model
		$this->_initialize();

		foreach (unserialize($data) as $name => $var)
		{
		  $this->{$name} = $var;
		}
	}

}

