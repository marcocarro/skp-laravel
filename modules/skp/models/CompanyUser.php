<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class CompanyUser extends Model {

	protected $table = 'b2b_company_user';
	public $timestamps = false;

	public function b2b_company() {
		$this->belongsTo(B2BCompany::class, 'b2b_company_id');
	}

	public function user() {
		$this->belongsTo(User::class);
	}
	
}