<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class PriceListPricecategory extends Model {

	protected $table = 'price_list_price_category';

	protected $_belongs_to = array(
		'price_list' => array(
			'model' => 'Skp_PriceList',
			'foreign_key' => 'price_list_id'
		),
		'price_category' => array(
			'model' => 'Skp_PriceCategory',
			'foreign_key' => 'price_category_id'
		)
	);

	public function rules()
	{
		return array(
		);
	}

}
