<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class B2BOrder extends Model {

	protected $table = 'b2b_order';
	public $timestamps = false;

	public function merchant() {
		$this->belongsTo(Merchant::class);
   }
   
   public function user() {
		$this->belongsTo(User::class);
   }
   
   public function b2bcompany() {
		$this->belongsTo(B2BCompany::class, 'b2b_company_id');
   }
   
   public function related_order() {
		$this->belongsTo(B2BOrder::class, 'related_order_id');
   }
   
   public function note() {
		$this->belongsTo(Note::class, 'note_id');
	}
	
}