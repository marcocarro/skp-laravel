<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class AvailabilityProductSkuCalendarPricelist extends Model {

	protected $table = 'availability_product_sku_calendar_pricelist';
	public $timestamps = false;

	public function availability() {
		$this->belongsTo(Availability::class);
	}

	public function product() {
		$this->belongsTo(Product::class);
	}

	public function sku() {
		$this->belongsTo(Sku::class);
	}

	public function calendar() {
		$this->belongsTo(Calendar::class);
	}

	public function price_list() {
		$this->belongsTo(PriceList::class);
	}

}
