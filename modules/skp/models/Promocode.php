<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Promocode extends ModelNameDescription {

	protected $table = 'promocode';

	protected $_table_columns = array(
		'id' => array(), 
		'merchant_id' => array (),
		'code' => array(), 
		'is_active' => array (),
		'archived' => array (),
		'deleted' => array(),
		'name' => array(),
		'type' => array(),
		'description' => array (),
		'valid_from' => array(), 
		'valid_to' => array (),
		'extra' => array(), 
		'show_to_admins' => array (),
		'valid_for_everyone' => array (),
		'valid_for_every_b2b' => array (),
		'user_list_limit' => array (),
		'company_list_limit' => array (),
	);
	
	protected $_serialize_columns = array('extra', 'name', 'description');

	protected $_belongs_to = array(
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
	);
	
	protected $_has_many = array(
		'skugroups' => array(
			'model' => 'Skp_SkuGroup',
			'foreign_key' => 'promocode_id',
			'through' => 'promocode_skugroup',
			'far_key' => 'skugroup_id',
		),
		'users' => array(
			'model' => 'Skp_User',
			'foreign_key' => 'promocode_id',
			'through' => 'promocode_user',
			'far_key' => 'user_id',
		),
		'b2b_companies' => array(
			'model' => 'Skp_B2BCompany',
			'foreign_key' => 'promocode_id',
			'through' => 'promocode_b2b_company',
			'far_key' => 'b2b_company_id',
		),
		'order_item' => array(
			'model' => 'Skp_OrderItem',
			'foreign_key' => 'promocode_id',
		),
	);
	
	const TYPE_CODE		= 'code';
	const TYPE_OFFER		= 'offer';
	
	const LIMIT_MAX		= 'max';
	const LIMIT_TARGET	= 'target';
	
	public static $types_list = array(self::TYPE_CODE, self::TYPE_OFFER);
	public static $limits_list = array(self::LIMIT_MAX, self::LIMIT_TARGET);
	
	public function rules()
	{
		return array(
			'valid_to'	=>	array(
								array(
									array(
										$this, 'date_check'
									),
								),
							),
		);
	}
	
	public function date_check()
	{
		return $this->get('valid_from')<=$this->get('valid_to');
	}


	public function getSkuGroups($params = array())
	{
		$active_only = Arr::get($params, 'active_only', TRUE);
		$publish_limit = Arr::get($params, 'publish_limit', TRUE);
		$return_name_array = Arr::get($params, 'return_name_array', FALSE);
		$skugroups = $this->get('skugroups')->where('web_active', '=', 0);
		if ($active_only == TRUE)
		{
			$skugroups->and_where('active', '=', 1);
		}
		$now = (new DateTime('now', $this->get('merchant')->getTimeZone()))->format('Y-m-d H:i:s');
		if ($publish_limit == TRUE)
		{
			$skugroups->and_where_open()->where('publish_from', 'is', NULL)->or_where('publish_from', '<=', $now)->and_where_close()->and_where_open()->where('publish_to', 'is', NULL)->or_where('publish_to', '>=', $now)->and_where_close();
		}
		$skugroups->and_where('skp_skugroup.deleted', '=', '0');
		if ($return_name_array == TRUE)
		{
			$skugroups_array = array();
			foreach ($skugroups->find_all() as $sg)
			{
				$skugroups_array[$sg->pk()] = $sg->getName(Request::$lang);
			}
			return $skugroups_array;
		}
		return $skugroups;
	}

	public function isValid($user = NULL, $b2b_company = NULL, $in_buy_page = FALSE)
	{
		if ($this->loaded() == FALSE) return FALSE;
		
		$now = (new DateTime('now', $this->get('merchant')->getTimeZone()))->format('Y-m-d H:i:s');
		// Promo must satisfy these conditions:
		// - Not Deleted and Active
		// - has Valid From not set or less than now
		// - has Valid To not set or greater than now
		// - has no limit of Max Products set or if it is set:
		//   - no products must be sold or less than Max Products
		if ($this->get('deleted') == 0 AND $this->get('is_active') == 'Y' AND ($this->get('valid_from') == NULL OR $this->get('valid_from') <= $now) AND ($this->get('valid_to') == NULL OR $this->get('valid_to') >= $now) AND ($this->getExtra('max_products') == NULL OR ($this->getExtra('max_products') > 0 AND ($this->getExtra('sold_products') == NULL OR $this->getExtra('sold_products') < $this->getExtra('max_products')))))
		{
			if ( !(	(!$b2b_company->loaded() AND ($this->get('valid_for_everyone') == 'Y' OR ($this->get('user_list_limit') == 'Y' AND $this->get('users')->where('user_id', '=', $user->pk())->count_all() > 0 )))
						OR 
						($b2b_company->loaded() AND ($this->get('valid_for_every_b2b') == 'Y' OR ($this->get('company_list_limit') == 'Y' AND $this->get('b2b_companies')->where('b2b_company_id', '=', $b2b_company->pk())->count_all() > 0 )))
					)
				)
			{
				return FALSE;
			}
			$this->cleanPromoItems();
			$bought_promo_items_qty = $this->countBoughtPromoItems($user, $b2b_company);
			$basket_promo_items_qty = $this->countBasketPromoItems($user, $b2b_company);
			$promo_items_in_purchase_qty = $this->countPromoItemsInOtherBaskets($user, $b2b_company) + $basket_promo_items_qty;
			if ($this->getExtra('max_products') > 0 AND (($in_buy_page AND ($this->getExtra('sold_products',0) + $promo_items_in_purchase_qty) >= $this->getExtra('max_products')) OR ($this->getExtra('sold_products',0) + $promo_items_in_purchase_qty) > $this->getExtra('max_products')))
			{
				return FALSE;
			}
			
			// if the promotion has a limit by user we now check how much the user has bought
			if ($this->getExtra('max_products_per_user') != NULL)
			{
				if (($in_buy_page AND $bought_promo_items_qty >= $this->getExtra('max_products_per_user')) OR $bought_promo_items_qty > $this->getExtra('max_products_per_user'))
				{
					return FALSE;
				}
			}
			return TRUE;
		}
		return FALSE;
	}

	public function showToAdmins($user = NULL)
	{
		return ($user != NULL AND $this->get('show_to_admins') == 'Y' AND $this->get('archived') == 0 AND $user->isMerchantAdmin($this->get('merchant')));
	}
	
	public function getPromocodeData($lang = 'en-en')
	{
		$ois = $this->get('order_item')->with('order')->where('order.status', '=', Order::STATUS_BOOKED)->and_where('order.type', '=', Order::TYPE_ORDER)->order_by('order.id')->find_all();
		$orders = array();
		$previous_order_id = NULL;
		foreach($ois as $oi)
		{
			if ($previous_order_id != $oi->get('order_id'))
			{
				$o_data = array();
				$previous_order_id = $oi->get('order_id');
			}
			$oi_data = array();
			$oi_data['qty'] = $oi->get('qty');
			if(!isset($o_data['qty_show']) AND $oi_data['qty'] > '')
			{
				$o_data['qty_show'] =  TRUE;
			}
			$oi_data['product_name'] = $oi->getName(Request::$lang);
			if(!isset($o_data['product_name_show']) AND $oi_data['product_name'] > '')
			{
				$o_data['product_name_show'] =  TRUE;
			}
			$oi_data['validity_date'] = $oi->get('validity_date');
			if(!isset($o_data['validity_date_show']) AND $oi_data['validity_date'] > '')
			{
				$o_data['validity_date_show'] =  TRUE;
			}
			$oi_data['card_serial'] = $oi->get('card_serial');
			if(!isset($o_data['card_serial_show']) AND $oi_data['card_serial'] > '')
			{
				$o_data['card_serial_show'] =  TRUE;
			}
			$oi_data['card_type'] = $oi->get('card_type');
			if(!isset($o_data['card_type_show']) AND $oi_data['card_type'] > '')
			{
				$o_data['card_type_show'] =  TRUE;
			}
			$oi_data['single_amount'] = $oi->getSinglePrice();
			if(!isset($o_data['single_amount_show']) AND $oi_data['single_amount'] > '')
			{
				$o_data['single_amount_show'] =  TRUE;
			}
			$oi_data['total_amount'] = $oi->getTotalPrice();
			if(!isset($o_data['total_amount_show']) AND $oi_data['total_amount'] > '')
			{
				$o_data['total_amount_show'] =  TRUE;
			}
			$oi_data['reference'] = $oi->getExternalReference(TRUE);
			if(!isset($o_data['reference_show']) AND $oi_data['reference'] > '')
			{
				$o_data['reference_show'] =  TRUE;
			}
			$oi_data['status'] = $oi->get('status');
			if(!isset($o_data['status_show']) AND $oi_data['status'] > '')
			{
				$o_data['status_show'] =  TRUE;
			}
			$oi_data['orderitem_extra'] = Arr::get($oi->get('orderitem_extra'), 'extra_info');
			if(!isset($o_data['orderitem_extra_show']) AND $oi_data['orderitem_extra'] > '')
			{
				$o_data['orderitem_extra_show'] =  TRUE;
			}
			$p = $oi->get('person');
			$oi_data['person_name'] = ($p->loaded() ? $p->getFullName() : '');
			if(!isset($o_data['person_name_show']) AND $oi_data['person_name'] > '')
			{
				$o_data['person_name_show'] =  TRUE;
			}
			$oi_data['person_birthdate'] = ($p->loaded() ? $p->get('birth_date') : '');
			if(!isset($o_data['person_birthdate_show']) AND $oi_data['person_birthdate'] > '')
			{
				$o_data['person_birthdate_show'] =  TRUE;
			}
			if(!array_key_exists( $oi->get('order_id') , $orders))
			{
				$o = $oi->get('order');
				$o_data['reference_num'] = $o->get('reference_num');
				$o_data['qty'] = 0;
				$u = $o->get('user');
				$o_data['name'] = $u->getFullName();
				$o_data['phone'] = $u->get('phone');
				$o_data['email'] = $u->get('email');
				$o_data['items'] = array();
				$o_data['items'][$oi->pk()] = $oi_data;
				$orders[$o->pk()] = $o_data;
			}else{
				$orders[$o->pk()]['items'][$oi->pk()] = $oi_data;
			}
			$orders[$o->pk()]['qty'] += $oi->get('qty');
		}
		return $orders;
	}
	
	public function getPromoEndDateIntervalSecs($params = array())
	{
		if ($this->get('valid_to') != NULL)
		{
			$now = new DateTime('now', $this->get('merchant')->getTimeZone());
			$end_date = new DateTime($this->get('valid_to'), $this->get('merchant')->getTimeZone());
			if ($now < $end_date)
			{
				return $end_date->getTimestamp() - $now->getTimestamp();
			} else {
				return 0;
			}
		} else {
			return NULL;
		}
	}
	
	public function getTimeBanner($lang)
	{
		$time_banner_in_lang = '';
		$lang_array = $this->getExtra('time_banner');
		if ($lang_array != NULL)
		{
			if ($lang == NULL OR !isset($lang_array[$lang]))
			{
				$lang = key($lang_array); // Return first language found
			}
			if (isset($lang_array[$lang]))
			{
				$time_banner_in_lang = $lang_array[$lang];
			}
		}
		return $time_banner_in_lang;
	}
	
	public function getLimitBanner($lang)
	{
		$limit_banner_in_lang = '';
		$lang_array = $this->getExtra('limit_banner');
		if ($lang_array != NULL)
		{
			if ($lang == NULL OR !isset($lang_array[$lang]))
			{
				$lang = key($lang_array); // Return first language found
			}
			if (isset($lang_array[$lang]))
			{
				$limit_banner_in_lang = $lang_array[$lang];
			}
			$limit_number = '';
			if ($this->getExtra('show_limit') == self::LIMIT_MAX)
			{
				$limit_value = $this->getExtra('max_products');
				$products_available = max(0, ($this->getExtra('max_products') - $this->getExtra('sold_products', 0)));
			} else if ($this->getExtra('show_limit') == self::LIMIT_TARGET)
			{
				$limit_value = $this->getExtra('target_number');
				$products_available = max(0, ($this->getExtra('target_number') - $this->getExtra('sold_products', 0)));
			}
			$limit_banner_in_lang = str_replace(':limit', $limit_value, $limit_banner_in_lang);
			$limit_banner_in_lang = str_replace(':products_available', $products_available, $limit_banner_in_lang);
		}
		
		return $limit_banner_in_lang;
	}

	public function extra_action_publish()
	{
		if ($this->get('is_active') == 'N')
		{
			$this->set('is_active', 'Y');
			$this->save();
		}
	}

	public function extra_action_unpublish()
	{
		if ($this->get('is_active') == 'Y')
		{
			$this->set('is_active', 'N');
			$this->save();
		}
	}

	static public function countIsActive($merchant_id, $is_active)
	{
		if (!isset($merchant_id) OR $merchant_id == NULL OR !is_numeric($merchant_id))
		{
			throw new Exception("Incorrect merchant_id");
		}
		if (!isset($is_active) OR $is_active == NULL OR ($is_active != 'Y' AND $is_active != 'N'))
		{
			throw new Exception("Incorrect is_active");
		}
		$query = ORM::factory('Skp_Promocode')
			->where('merchant_id', '=', $merchant_id)
			->and_where('is_active', '=', $is_active);
		return $query->count_all();
	}
	
	// Counts promo items in all Authorized Orders, To be payed Orders with age < 20 min , Baskets with age < 15 min except user's ones
	public function countBasketPromoItems($user = NULL, $b2b_company = NULL)
	{
		if ($user == NULL) return FALSE;
		$sql_query = "SELECT SUM(soi.qty) AS bought_qty FROM skp_order so JOIN skp_order_item soi ON soi.order_id=so.id WHERE so.merchant_id=:merchant_id AND ";
		$sql_query .= $b2b_company->loaded() ? "so.b2b_company_id=:b2b_company_id" : "so.user_id=:user_id";
		$sql_query .= " AND so.type=:order_type_basket AND soi.promocode_id=:promocode_id";
		$query = DB::query(Database::SELECT, $sql_query);
		$query->param(':merchant_id', $this->get('merchant_id'));
		$b2b_company->loaded() ? $query->param(':b2b_company_id', $b2b_company->pk()) : $query->param(':user_id', $user->pk());
		$query->param(':order_type_order', Order::TYPE_ORDER);
		$query->param(':order_status_booked', Order::STATUS_BOOKED);
		$query->param(':order_status_authorized', Order::STATUS_PAYMENT_AUTHORIZED);
		$query->param(':order_status_tobepayed', Order::STATUS_TO_BE_PAYED);
		$query->param(':order_type_basket', Order::TYPE_BASKET);
		$query->param(':promocode_id', $this->pk());
		$rows = $query->as_object()->execute();
		$bought_qty = 0;
		foreach ($rows as $row)
		{
			$bought_qty += $row->bought_qty;
		}
		return $bought_qty;
	}
	
	// Counts promo items in all Authorized Orders, To be payed Orders with age < 20 min , Baskets with age < 15 min except user's ones
	public function countBoughtPromoItems($user = NULL, $b2b_company = NULL)
	{
		if ($user == NULL) return FALSE;
		$sql_query = "SELECT SUM(soi.qty) AS bought_qty FROM skp_order so JOIN skp_order_item soi ON soi.order_id=so.id WHERE so.merchant_id=:merchant_id AND ";
		$sql_query .= $b2b_company->loaded() ? "so.b2b_company_id=:b2b_company_id" : "so.user_id=:user_id";
		$sql_query .= " AND ((so.type=:order_type_order AND so.status IN (:order_status_booked, :order_status_authorized, :order_status_tobepayed)) OR so.type=:order_type_basket) AND soi.promocode_id=:promocode_id";
		$query = DB::query(Database::SELECT, $sql_query);
		$query->param(':merchant_id', $this->get('merchant_id'));
		$b2b_company->loaded() ? $query->param(':b2b_company_id', $b2b_company->pk()) : $query->param(':user_id', $user->pk());
		$query->param(':order_type_order', Order::TYPE_ORDER);
		$query->param(':order_status_booked', Order::STATUS_BOOKED);
		$query->param(':order_status_authorized', Order::STATUS_PAYMENT_AUTHORIZED);
		$query->param(':order_status_tobepayed', Order::STATUS_TO_BE_PAYED);
		$query->param(':order_type_basket', Order::TYPE_BASKET);
		$query->param(':promocode_id', $this->pk());
		$rows = $query->as_object()->execute();
		$bought_qty = 0;
		foreach ($rows as $row)
		{
			$bought_qty += $row->bought_qty;
		}
		return $bought_qty;
	}
	
	// Counts promo items in all Authorized and To be payed Orders , Baskets with age < 10 min except user's ones
	public function countPromoItemsInOtherBaskets($user = NULL, $b2b_company = NULL)
	{
		$sql_query = "SELECT SUM(soi.qty) AS used_qty FROM skp_order so JOIN skp_order_item soi ON soi.order_id=so.id WHERE so.merchant_id=:merchant_id AND soi.promocode_id=:promocode_id AND ";
		($b2b_company != NULL AND $b2b_company->loaded()) AND $sql_query .= "so.b2b_company_id<>:b2b_company_id AND ";
		($user != NULL AND $user->loaded()) AND $sql_query .= "so.user_id<>:user_id AND ";
		$sql_query .= "((so.type=:order_type_order AND so.status IN (:order_status_authorized, :order_status_tobepayed)) OR (so.type=:order_type_basket AND so.lastupdate_date>=DATE_SUB(NOW(), INTERVAL :max_basket_age_limit SECOND)))";
		$query = DB::query(Database::SELECT, $sql_query);
		$query->param(':merchant_id', $this->get('merchant_id'));
		$query->param(':promocode_id', $this->pk());
		($b2b_company != NULL AND $b2b_company->loaded()) AND $query->param(':b2b_company_id', $b2b_company->pk());
		($user != NULL AND $user->loaded()) AND $query->param(':user_id', $user->pk());
		$query->param(':order_type_order', Order::TYPE_ORDER);
		$query->param(':order_status_tobepayed', Order::STATUS_TO_BE_PAYED);
		$query->param(':order_status_authorized', Order::STATUS_PAYMENT_AUTHORIZED);
		$query->param(':order_type_basket', Order::TYPE_BASKET);
		$query->param(':max_basket_age_limit', Order::BASKET_TTL);
		$rows = $query->as_object()->execute();
		$qty = 0;
		foreach ($rows as $row)
		{
			$qty += $row->used_qty;
		}
		return $qty;
	}

	public function cleanPromoItems($params = array())
	{
		$merchant_id = $this->get('merchant_id');
		$promo_id = $this->pk();
		
		$sql_query = "SELECT soi.id AS orderitem_id, soi.order_id AS order_id FROM skp_order so JOIN skp_order_item soi ON soi.order_id=so.id WHERE so.merchant_id=:merchant_id AND soi.promocode_id=:promocode_id AND so.type=:order_type_basket AND so.lastupdate_date<DATE_SUB(NOW(), INTERVAL :max_basket_age_limit SECOND)";
		
		$query = DB::query(Database::SELECT, $sql_query);
		$query->param(':merchant_id', $merchant_id);
		$query->param(':promocode_id', $this->pk());
		$query->param(':order_type_basket', Order::TYPE_BASKET);
		$query->param(':max_basket_age_limit', Order::BASKET_TTL);
		
		$oitems_to_be_deleted = $query->as_object()->execute();
		$orders_array = array();
		foreach ($oitems_to_be_deleted as $oitd)
		{
			$order = isset($orders_array[$oitd->order_id]) ? $orders_array[$oitd->order_id] : ORM::factory('Skp_Order', $oitd->order_id);
			$orders_array[$oitd->order_id] = $order;
			$order_item = ORM::factory('Skp_OrderItem', $oitd->orderitem_id);
			$order->removeOrderItem($order_item);
			$order->resetPrecheck();
		}
	}
	
	public static function getPromoLimits($promo_id, $user = NULL, $b2b_company = NULL)
	{
		$promo = ORM::factory('Skp_Promocode', $promo_id);
		$promo_limits = array();
		if ($promo->loaded())
		{
			$bought_promo_items_qty = $promo->countBoughtPromoItems($user, $b2b_company);
			$promo_items_in_purchase_qty = $promo->countPromoItemsInOtherBaskets($user, $b2b_company) + $promo->countBasketPromoItems($user, $b2b_company);
			$total_available = NULL;
			$max_products = $promo->getExtra('max_products') ?: NULL;
			$max_products_per_user = $promo->getExtra('max_products_per_user') ?: NULL;
			$promo->getExtra('max_products') > 0 AND $total_available = $promo->getExtra('max_products') - $promo->getExtra('sold_products',0) - $promo_items_in_purchase_qty;
			$promo_limits['max_products'] = $max_products;
			$promo_limits['max_per_user'] = $max_products == NULL ? $max_products_per_user : min($max_products_per_user, $max_products);
			$promo_limits['total_available'] = $total_available;
			$promo_limits['current_qty'] = $bought_promo_items_qty;
		}
		return $promo_limits;
	}

}
