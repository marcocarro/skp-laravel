<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class InvoiceReceiptConfig extends Model {

	protected $table = 'invoice_receipt_config';
	protected $primaryKey = 'merchant_id';
	protected $casts = [
		'invoice_fields' => 'array',
		'receipt_fields' => 'array',
		'extra' => 'array'
	];

	public function merchant() {
		$this->belongsTo(Merchant::class);
	}
	
}