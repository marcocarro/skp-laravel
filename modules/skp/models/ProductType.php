<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class ProductType extends ModelNameDescription {

	protected $table = 'product_type';
	protected $_table_columns = Array(
		'id' => Array(),
		'merchant_id' => Array(),
		'type' => Array(),
		'name' => Array(),
		'preselect' => Array(),
		'aggregate' => Array(),
		'store_show' => Array(),
		'extra' => Array(),
	);
	protected $_serialize_columns = array('name','extra');
	
	protected $_has_many = array(
		'products' => array(
			'model' => 'Skp_Product',
			'foreign_key' => 'producttype_id',
		),
		'product_type_merchant' => array(
			'model' => 'Skp_ProductTypeMerchant',
			'foreign_key' => 'product_type_id',
		),
	);
	
	protected $_belongs_to = array(
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
	);
	
	const TYPE_TICKET						= 'Ticket';
	const TYPE_MERCHANDISE				= 'Merchandise';
	const TYPE_IDENTIFICATION_CARD	= 'IdentificationCard';
	const TYPE_ACTIVITY					= 'Activity';
	const TYPE_PARKING					= 'Parking';
	const TYPE_LOCKER						= 'Locker';
	const TYPE_RENTAL						= 'Rental';
	const TYPE_LESSON						= 'Lesson';
	
	public static $type_list = array(self::TYPE_TICKET, self::TYPE_MERCHANDISE, self::TYPE_IDENTIFICATION_CARD, self::TYPE_ACTIVITY, self::TYPE_PARKING, self::TYPE_LOCKER, self::TYPE_RENTAL, self::TYPE_LESSON);

	public function rules()
	{
		return array(
			'preselect' => array(
				array('not_empty'),
				array('in_array',array(':value', array(0,1))),
			),
		);
	}
}
