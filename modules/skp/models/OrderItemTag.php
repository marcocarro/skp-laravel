<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class OrderItemTag extends Model {

	protected $table = 'order_item_tags';
	
	protected $_table_columns = array(
		'id' => array(), 
		'order_id' => array (),
		'order_item_id' => array (),
		'value' => array(),
	);
	
	protected $_serialize_columns = array();

	protected $_belongs_to = array(
		'order' => array(
			'model' => 'Skp_Order',
			'foreign_key' => 'order_id',
		),
		'orderitem' => array(
			'model' => 'Skp_OrderItem',
			'foreign_key' => 'orderitem_id',
		),
	);
	
	protected $_has_many = array();
	
	public function rules()
	{
		return array(
			'order_id' => array(
				array('not_empty'),
			),
			'order_item_id' => array(
				array('not_empty'),
			),
			'value' => array(
				array('not_empty'),
			),
		);
	}

}
