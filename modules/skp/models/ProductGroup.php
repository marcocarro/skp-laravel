<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class ProductGroup extends ModelNameDescription implements Interfaces_VisibleProduct
{
	protected $table = 'productgroup';
	protected $_serialize_columns = array('extra', 'name', 'short_description', 'description', 'practical_info');
	protected $_table_columns = Array(
		'id' => Array(),
		'merchant_id' => Array(),
		'name' => Array(),
		'short_description' => Array(),
		'description' => Array(),
		'practical_info' => Array(),
		'venue_id' => Array(),
		'extra' => Array(),
		'active' => Array(),
		'archived' => Array(),
		'deleted' => Array(),
	);
	
	protected $_belongs_to = array(
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
		'venue' => array(
			'model' => 'Skp_Venue',
			'foreign_key' => 'venue_id',
		),
	);
	
	protected $_has_many = array(
		'productgroup_products' => array(
			'model' => 'Skp_ProductGroupProduct',
			'foreign_key' => 'productgroup_id',
		),
		'products' => array(
			'model' => 'Skp_Product',
			'through' => 'productgroup_product',
			'foreign_key' => 'productgroup_id',
			'far_key' => 'product_id',
		),
		'productgroup_images' => array(
			'model' => 'Skp_ProductGroupImage',
			'foreign_key' => 'productgroup_id',
		),
	);

	public static $display_on_list = array(self::DESCRIPTION_DISPLAY_PDF, self::DESCRIPTION_DISPLAY_SHOP);
	protected $_current_products = array();

	public function rules()
	{
		return array(
			'merchant_id' => array(
				array('not_empty'),
			),
			'name' => array(
				array('not_empty'),
			),
			'active' => array(
				array('not_empty'),
				array('in_array',array(':value', array(0,1))),
			),
			'archived' => array(
				array('not_empty'),
				array('in_array',array(':value', array(0,1))),
			),
			'deleted' => array(
				array('not_empty'),
				array('in_array',array(':value', array(0,1))),
			),
		);
	}

	public function save(Validation $validation = NULL)
	{
		parent::save($validation);
	}

	public function getProductGroupImages($only_first = FALSE)
	{
		$images = $this->get('productgroup_images')->order_by('sort');
		if ($only_first)
		{
			return $images->find();
		} else
		{
			return $images->find_all();
		}
	}
	
	public function getImage($relative = TRUE, $version = 'original')
	{
		$first_productgroup_image = $this->getProductGroupImages(TRUE);
		return $first_productgroup_image->get('image')->getImageUrl($relative, $version);
	}
	
	public function getImageFile($return_filename_if_not_exists = FALSE, $version = 'original')
	{
		$first_productgroup_image = $this->getProductGroupImages(TRUE);
		return $first_productgroup_image->get('image')->getImageFile($return_filename_if_not_exists, $version);
	}

	// interface
	public function printInShop ($fieldname)
	{
		switch ($fieldname)
		{
			case 'product_info.shortDescr':
				$diplay_on = $this->getExtra('short_description_display_on', self::$display_on_list);
				break;
			case 'product_info.description':
				$diplay_on = $this->getExtra('description_display_on', array(self::DESCRIPTION_DISPLAY_SHOP));
				break;
			case 'product_info.practicalInfo':
				$diplay_on = $this->getExtra('practical_info_display_on', self::$display_on_list);
				break;
			default:
				$display_on = NULL;
		}
		if ($diplay_on !== NULL)
		{
			return in_array(self::DESCRIPTION_DISPLAY_SHOP, $diplay_on);
		}
		return TRUE;
	}
	
	public function getMinValidityDate()
	{
		$merchant_min_validity_date = $this->get('merchant')->getMinPurchaseDate();
		$productgroup_min_validity_date = NULL;
		foreach ($this->_current_products as $product)
		{
			$product_min_validity_date = $product->getMinValidityDate();
			$productgroup_min_validity_date > $product_min_validity_date AND $productgroup_min_validity_date = $product_min_validity_date;
		}
		if ($productgroup_min_validity_date == NULL OR $merchant_min_validity_date != NULL AND $productgroup_min_validity_date < $merchant_min_validity_date)
		{
			$productgroup_min_validity_date = $merchant_min_validity_date;
		}
		return $productgroup_min_validity_date;
	}
	
	public function getMaxValidityDate()
	{
		$merchant_max_validity_date = $this->get('merchant')->getMaxPurchaseDate();
		$productgroup_max_validity_date = NULL;
		foreach ($this->_current_products as $product)
		{
			$product_max_validity_date = $product->getMaxValidityDate();
			$productgroup_max_validity_date < $product_max_validity_date AND $productgroup_max_validity_date = $product_max_validity_date;
		}
		if ($productgroup_max_validity_date == NULL OR $merchant_max_validity_date != NULL AND $productgroup_max_validity_date > $merchant_max_validity_date)
		{
			$productgroup_max_validity_date = $merchant_max_validity_date;
		}
		return $productgroup_max_validity_date;
	}
	
	public function getCalendarMinMax()
	{
		return NULL;
	}

	public function getValidityCalendarMinMax()
	{
		return NULL;
	}
	
	public function getCalendarLabel($lang)
	{
		$calendar_label = ARR::get($this->getExtra('calendar_label', array()), $lang, NULL);
		if ($calendar_label == NULL)
		{
			foreach ($this->_current_products as $product)
			{
				$calendar_label = $product->getCalendarLabel($lang);
				if ($calendar_label == __('fe.accordion.valid_from'))
				{
					break;
				}
			}
		}
		return $calendar_label;
	}
	
	public function getSelectPlaceholder($lang)
	{
		$select_placeholder = ARR::get($this->getExtra('select_placeholder', array()), $lang, NULL);
		if ($select_placeholder == NULL)
		{
			$select_placeholder = reset($this->_current_products)->getSelectPlaceholder($lang);
		}
		return $select_placeholder;
	}
	
	public function getPriceCategoryType($lang)
	{
		$price_category_type = Arr::get($this->getExtra('price_category_type', array()), $lang, NULL);
		if ($price_category_type == NULL)
		{
			$price_category_type = reset($this->_current_products)->getPriceCategoryType($lang);
		}
		return $price_category_type;
	}

	public function getFEValidityCalendarDates()
	{
		$result = array();
		foreach ($this->_current_products as $product)
		{
			$product_dates = $product->getFEValidityCalendarDates();
			if($product_dates === NULL)
			{
				$result = NULL;
				break;
			} else
			{
				sizeof($product_dates) > 0 AND $result = array_merge($result, $product_dates);
			}
		}
		if (is_array($result) AND sizeof($result) > 0)
		{
			$result = array_values(array_unique($result));
		}
		// When no dates are associated to the current products it must return NULL
		// When all dates associated to the current products are null it must return a empty array
		return $result;
	}

	public function getFEKey($prefix = NULL)
	{
		$prefix != NULL OR $prefix = 'pg_';
		return $prefix.$this->pk();
	}
	
	public function getFEType()
	{
		return str_replace('', '', get_class());
	}
	
	public function getFEProductListIDs()
	{
		$result = array();
		foreach ($this->_current_products as $product)
		{
			foreach ($product->getFEProductListIDs() as $p_id)
			{
				$result[] = $p_id;
			}
		}
		return $result;
	}
	
	public function isOpenDate()
	{
		$is_not_open_date = FALSE;
		foreach ($this->_current_products as $product)
		{
			if ($is_not_open_date = ($product->getExtra('is_open_date') != 'Y') AND $is_not_open_date)
			{
				break;
			}
		}
		return !$is_not_open_date;
	}
	
	public function getCurrentProducts()
	{
		return $this->_current_products;
	}
	
	public function needsValidityStartTime()
	{
		$result = FALSE;
		foreach ($this->_current_products as $product)
		{
			$product_needs_validity_start_time = $product->needsValidityStartTime();
			if ($product_needs_validity_start_time)
			{
				$result = $product_needs_validity_start_time;
				break;
			}
		}
		return $result;
	}
	
	public function getFEValidityTimes($params = array())
	{
		$result = array();
		foreach ($this->_current_products as $product)
		{
			$product_validity_times = $product->getFEValidityTimes($params);
			foreach ($product_validity_times as $hour => $minutes)
			{
				if (!isset($result[$hour]))
				{
					$result[$hour] = $minutes;
				} else {
					$result[$hour] = array_merge($result[$hour], $minutes);
				}
			}
		}
		return $result;
	}
	
	public function needsDateWithPrice()
	{
		$result = FALSE;
		foreach ($this->_current_products as $product)
		{

			$product_value = $product->needsDateWithPrice();
			if ($product_value)
			{
				$result = $product_value;
				break;
			}
		}
		return $result;
	}
	
	public function needsDateWithPriceDatepicker()
	{
		$result = FALSE;
		foreach ($this->_current_products as $product)
		{
			$product_value = $product->needsDateWithPriceDatepicker();
			if ($product_value)
			{
				$result = $product_value;
				break;
			}
		}
		return $result;
	}
	
	public function needsDateWithPriceGraph()
	{
		$result = FALSE;
		foreach ($this->_current_products as $product)
		{
			$product_value = $product->needsDateWithPriceGraph();
			if ($product_value)
			{
				$result = $product_value;
				break;
			}
		}
		return $result;
	}

	public function getUrl ($skugroup_id = 0)
	{
		return '#' . URL::site(Route::get('shop.welcome', Request::$lang)->uri()) .
			'?skugroup_id=' . $skugroup_id .
			'&product_id=' . $this->_current_products[0]->pk() .
			'&bookable_y_n_a=a';
	}
	
	// END interface

	public function addCurrentProduct(Product $p)
	{
		$this->_current_products[] = $p;
	}
	
	public static function filterVisibleProducts($products)
	{
		$results = array();
		$used_productgroup = array();
		foreach ($products as $product)
		{
			$productgroups = $product->get('productgroups')->where('skp_productgroup.deleted', '=', '0')->find_all();
			if ($productgroups->count() > 0)
			{
				foreach ($productgroups as $productgroup)
				{
					if ($productgroup->get('active'))
					{
						if (in_array($productgroup->pk(), $used_productgroup))
						{
							$used_productgroup[$productgroup->pk()]->addCurrentProduct($product);
						} else
						{
							$productgroup->addCurrentProduct($product);
							$results[] = $productgroup;
							$used_productgroup[$productgroup->pk()] = $productgroup;
						}
					} else
					{
						$results[] = $product;
					}
				}
			} else
			{
				$results[] = $product;
			}
		}
		return $results; // array(VisibleProducts)
	}

}
