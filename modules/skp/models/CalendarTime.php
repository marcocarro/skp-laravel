<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class CalendarTime extends Model {

	protected $table = 'calendar_time';
	public $timestamps = false;

	public function calendar() {
		$this->belongsTo(Calendar::class, 'calendar_id');
	}

	public function calendar_date_time() {
		return $this->hasMAny(
			CalendarDateTime::class,
			'time_id'
		);
	}

}
