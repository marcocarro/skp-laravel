<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class OrderExternal extends Model {

	protected $table = 'order_external';
	protected $_serialize_columns = array('external_extra');
	
	protected $_belongs_to = array(
		'order' => array(
			'model' => 'Skp_Order',
			'foreign_key' => 'order_id',
		),
		'connector' => array(
			'model' => 'Skp_Connector',
			'foreign_key' => 'connector_id',
		),
	);
	const STATUS_BOOKED	= 'BOOKED';
	const STATUS_ERROR	= 'ERROR';
	const STATUS_PENDING	= 'PENDING';

	public function isB2B()
	{
		return FALSE;
	}
	
	public function setExtra($extra_name, $extra_value)
	{
		$ue = $this->get('external_extra');

		if ($extra_value !== NULL)
		{
			$ue[$extra_name] = $extra_value;
		}else{
			unset($ue[$extra_name]);
		}
		$this->set('external_extra', $ue);
	}
	
	public function getExtra($key, $default_value = NULL)
	{
		if ($key != NULL)
		{
			return Arr::get($this->get('external_extra'), $key, $default_value);
		}
	}

	public function updateStatus()
	{
		$connectorClass = $this->get('connector')->get('connector_class').'_Connector';
		try{
			$result = $connectorClass::updateStatus($this);
			if ($result != FALSE)
			{
				if (property_exists($result, 'external_extra'))
				{
					$this->set('external_extra', $result->external_extra);
				}
				if (property_exists($result, 'status'))
				{
					$this->set('status', $result->status);
				}
				$this->save();
			}
			// the Order status is changed
		}catch (Exception $e){
		}
	}
	
	public function isBarcoded()
	{
		return $this->get('connector')->isBarcoded();
	}

	public function getBarcodeFormat()
	{
		return $this->get('connector')->getBarcodeFormat();
	}
	
	public function getExternalReference($only_data = FALSE)
	{
		$result = 'KO';
		$connector = $this->get('connector');
		if ($connector->loaded())
		{
			$connectorClass = $connector->get('connector_class'). '_Connector';
			$connector_id = $connector->pk();
			try{
				// We loop throw all the different order external of this connector (there can be more than one)
				$params = array('order_external' => $this, 'only_data' => $only_data);
				$result = $connectorClass::getExternalReference($params);
			}catch (Exception $e){
				echo $e;
			}
		}
		return $result;
		//return $this->get('external_reference');
	}
	
	public function syncOrderItems($params)
	{
		$now = new DateTime('now');
		$resync_interval = Arr::get($params, 'resync_interval', 1);
		$max_sync_tries = Arr::get($params, 'max_sync_tries');
		$error_resync_interval = Arr::get($params, 'error_resync_interval', 1);
		$tmp_import_cards = Arr::get($params, 'tmp_import', TRUE);
		$save_import_cards = Arr::get($params, 'save_import', FALSE);
		$connector = $this->get('connector');
		if ($connector->loaded())
		{
			$connectorClass = $connector->get('connector_class').'_Connector';
			try{
				/*		sync_result = array(
				 * 		'next_sync' = date,				// The minimum validity date of the order items not completed
				 * 		'card_type' = string,			// The type of the new cards (Skidata Keycard or TeamAxess WTP)
				 * 		'item_cards' = array(			// The array of synced order items
				 * 			id => card_serial,			// 1...n
				 * 		),
				 * 		'external_extra' = array()		// The array with external extra updated
				 * 	)
				 */
				$sync_result = $connectorClass::syncOrderItems($this);				// ToDo: syncOrderItems($order_external) function in each connector
				if ($sync_result == -1)
				{
					// In this case the Order External data are not enough this order cannot be synced at all
					$this->set('next_sync', NULL);
					$this->save();
					return FALSE;
				}
				if (is_array($sync_result))
				{
					if (isset($sync_result['error']))
					{
						$import_log = ORM::factory('Skp_TmpCardImport');
						$import_log->set('card_id', NULL);
						$import_log->set('user_id', NULL);
						$import_log->set('order_external_id', $this->pk());
						$import_log->set('card_type', NULL);
						$import_log->set('card_serial', NULL);
						$import_log->set('external_extra', $sync_result['error']);
						$import_log->save();
						if ($save_import_cards)
						{
							$this->set('next_sync', (clone $now)->add(new DateInterval('P'.$error_resync_interval.'D'))->format('Y-m-d'));
							$this->set('sync_tries', $this->get('sync_tries')+1);
							$this->save();
						}
						return FALSE;
					}
					foreach ($sync_result['item_cards'] as $item_external_id => $card_serial)
					{
						$user = $this->get('order')->get('user');
						$new_card = $user->get('cards')->where('card_serial', '=', $card_serial)->find();
						if (!$new_card->loaded() AND $card_serial != NULL)
						{
							$new_card->set('card_type', $sync_result['card_type']);
							$new_card->set('card_serial', $card_serial);
							$new_card->set('user', $user);
							$new_card->set('nick', $card_serial);
							if ($save_import_cards)
							{
								$new_card->save();
							}
							
							$import_log = ORM::factory('Skp_TmpCardImport');
							$import_log->set('card_id', $new_card->pk());
							$import_log->set('user_id', $user->pk());
							$import_log->set('order_external_id', $this->pk());
							$import_log->set('card_type', $sync_result['card_type']);
							$import_log->set('card_serial', $card_serial);
							$import_log->set('external_extra', $sync_result['external_extra']);
							if ($tmp_import_cards)
							{
								$import_log->save();
							}
						}
					}
					
					$this->set('external_extra', $sync_result['external_extra']);
					
					// If at least an order item is not complete we have to increment the days for the next sync.
					$next_sync_date = $this->get('next_sync') == NULL ? new DateTime : DateTime::createFromFormat('Y-m-d H:i:s', $this->get('next_sync'));
					
					/*		TA-WTPSI returns YES if the order is still open and needs to be synced again
					 *		SKIDATA and TA-DCI4WTP returns the minimum validity date of the item still open in the order.
					 */
					if ($sync_result['next_sync'] != NULL)
					{
						if ($sync_result['next_sync'] == 'YES' OR $next_sync_date >= $sync_result['next_sync'])
						{
							($max_sync_tries !== NULL AND $this->get('sync_tries') > $max_sync_tries*0.5) AND $resync_interval *= 2;
							($max_sync_tries !== NULL AND $this->get('sync_tries') > $max_sync_tries*0.75) AND $resync_interval *= 2;
							$sync_result['next_sync'] = $next_sync_date->add(new DateInterval('P'.$resync_interval.'D'));
							if ($sync_result['next_sync'] < $now)
							{
								$sync_result['next_sync'] = (clone $now)->add(new DateInterval('P1D'));
							}
						}
						$sync_result['next_sync'] = $sync_result['next_sync']->format('Y-m-d');
						$this->get('sync_tries') == NULL AND $this->set('sync_tries', 0);
						$this->set('sync_tries', $this->get('sync_tries')+1);
					}
					
					$this->set('next_sync', $sync_result['next_sync']);
					
					if ($save_import_cards)
					{
						$this->save();
					}
					return ($sync_result['next_sync'] == NULL);
				}
			}catch (Exception $e){
				return FALSE;
			}
		}else{
			// Since the connector is not loaded this order cannot be synced
			$this->set('next_sync', NULL)->save();
		}
	}

	public function getExternalDeliveryLink()
	{
		$connector = $this->get('connector');
		try{
			return $connector->callConnectorMethod('getExternalDeliveryLink', $this);
		} catch (Skp_Exception_Missing $e)
		{}
		return FALSE;
	}

}
