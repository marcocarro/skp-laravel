<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class SkuGroup extends ModelNameDescription {

	protected $table = 'skugroup';
	protected $_serialize_columns = array('name', 'layout_options', 'skugroup_extra');
	protected $_table_columns = Array(
		'id' => Array(),
		'merchant_id' => Array(),
		'sort' => Array(),
		'name' => Array(),
		'active' => Array(),
		'web_active' => Array(),
		'publish_from' => Array(),
		'publish_to' => Array(),
		'layout_id' => Array(),
		'layout_options' => Array(),
		'skugroup_extra' => Array(),
		'archived' => Array(),
		'deleted' => Array(),
	);
	
	protected $_belongs_to = array(
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
		'layout' => array(
			'model' => 'Skp_Layout',
			'foreign_key' => 'layout_id',
		),
	);
	protected $_has_many = array(
		'skus' => array(
			'model' => 'Skp_Sku',
			'foreign_key' => 'skugroup_id',
			'through' => 'skugroup_sku',
			'far_key' => 'sku_id',
		),
		'promocodes' => array(
			'model' => 'Skp_Promocode',
			'foreign_key' => 'skugroup_id',
			'through' => 'promocode_skugroup',
			'far_key' => 'promocode_id',
		),
	);

	public function rules()
	{
		return array(
			'sort' => array(
				array('not_empty'),
				array('numeric'),
			)
		);
	}

	public function getExtra($key, $default_value = NULL)
	{
		if ($key != NULL)
		{
			return Arr::get($this->get('skugroup_extra'), $key, $default_value);
		}
	}

	public function setExtra($extra_name, $extra_value, $save_empty_value = FALSE)
	{
		$ue = $this->get('skugroup_extra');

		if ($extra_value != NULL)
		{
			$ue[$extra_name] = $extra_value;
		}else{
			unset($ue[$extra_name]);
		}
		$this->set('skugroup_extra', $ue);
	}

	static public function getDefault($payment_class = NULL)
	{
		$default_config = Kohana::$config->load('default_setting_new_merchant');
		$default = ORM::factory('Skp_SkuGroup');
		$def = $default_config['SkuGroup'];
		foreach ($def as $field => $value)
		{
			$default->set($field, $value);
		}
		return $default;
	}
	
	public function getLayoutOption($key)
	{
		if ($key != NULL)
		{
			return Arr::get($this->get('layout_options'), $key);
		}
	}

	/*
	 * This function is to add a SKU to this SkuGroup
	 * @param $sku the Sku object to add
	 */
	public function addSku($sku)
	{
		
	}
	
	/*
	 * This function is to remove a SKU from this SkuGroup
	 * @param $sku the Sku object to remove
	 */
	public function removeSku($sku)
	{
		
	}
	
	public function isInPromo()
	{
		return $this->get('promocodes')->count_all() > 0;
	}
	
	public function getFilterDataProfile()
	{
		$data_profile_id = $this->getExtra('data_profile');
		if ($data_profile_id == NULL)
		{
			$data_profile = ORM::factory('Skp_DataProfile');
		} else {
			$data_profile = ORM::factory('Skp_DataProfile')->where('id', '=', $data_profile_id)->and_where('merchant_id', '=', $this->get('merchant_id'))->find();
		}
		return $data_profile;
	}
	
	public function hasShopTabFilter()
	{
		return ($this->getFilterDataProfile()->loaded() OR $this->getExtra('date_filter',0) == 1 OR $this->getExtra('pool_filter',0) == 1);
	}
	
	public function getAvailableProductsDates($is_promo = FALSE, $params = array())
	{
		$pool_id_selected = Arr::get($params, 'pool_id');
		if ($is_promo)
		{
			$products = Product::getProducts(array('merchant'=>$this->merchant, 'group_id'=>$this->pk(), 'only_web_active' => FALSE, 'only_active'=>1, 'pool_id'=>$pool_id_selected))->find_all();
		} else {
			$products = Product::getProducts(array('merchant'=>$this->merchant, 'group_id'=>$this->pk(), 'only_active'=>1, 'only_web_active'=>TRUE, 'pool_id'=>$pool_id_selected))->find_all();
		}
		$now = (new DateTime('now', $this->get('merchant')->getTimeZone()));
		$available_dates = array();
		$min_date = NULL;
		foreach ($products as $product)
		{
			if ($product->add_prod_only == 0)
			{
				$product_calendar_dates = $product->get('validity_calendar')->getDates();
				$product_min_validity_date = $product->getMinValidityDate();
				$product_max_validity_date = $product->getMaxValidityDate();
				if ($product_max_validity_date == NULL OR ($product_max_validity_date  >= $now AND $product_max_validity_date >= $product_min_validity_date))
				{
					if (sizeof($product_calendar_dates) == 0 AND $product_min_validity_date != NULL AND $product_max_validity_date == NULL)
					{
						$available_dates = 'all';
						$min_date = $now;
						break;
					} else if (sizeof($product_calendar_dates) > 0)
					{
						foreach($product_calendar_dates as $date)
						{
							//~ if (!in_array($date, $available_dates))
							//~ {
								$date_obj = DateTime::createFromFormat('Y-m-d', $date);
								if ($product->get('merchant')->isOpen($date) == TRUE
									AND ($product_max_validity_date == NULL OR $product_max_validity_date >= $date_obj)
									AND ($product_min_validity_date == NULL OR $product_min_validity_date <= $date_obj))
								{
									$available_dates[$date] = $date;
								}
							//~ }
						}
					} else if ($product_max_validity_date != NULL AND $now <= $product_max_validity_date)
					{
						$now > $product_min_validity_date AND $product_min_validity_date = $now;
						$period = new DatePeriod($product_min_validity_date, new DateInterval('P1D'), $product_max_validity_date);
						foreach ($period as $key => $date_obj)
						{
							$date = $date_obj->format('Y-m-d');
							if (!in_array($date, $available_dates))
							{
								if ($product->get('merchant')->isOpen($date) == TRUE)
								{
									$available_dates[$date] = $date;
								}
							}
						}
					} else if ($product_min_validity_date > $now)
					{
						($product_min_validity_date < $min_date OR $min_date == NULL) AND $min_date = $product_min_validity_date;
					}
				}
			}
		}
		$min_date != NULL AND $min_date = $min_date->format('Y-m-d');
		return array('dates' => $available_dates, 'min_date'=>$min_date);
	}
	
	public function getOffers($params = array())
	{
		// Get Offers params
		$show_in = Arr::get($params, 'show_in');
		$audience = Arr::get($params, 'audience');
		$offer_type = Arr::get($params, 'offer_type');
		$merchant = $this->get('merchant');
		$skugroup_id = $this->pk();
		
		// If cache
		
		// else
		$offer_params = array(
			'merchant' => $merchant,
			'offer_type' => $offer_type,
			'show_in' => $show_in,
			'skugroup_id' => $skugroup_id,
			'audience' => $audience,
		);
		$active_offers = ($skugroup_id != NULL ? Offer::getOffersContents($offer_params) : array());
		
		return $active_offers;
	}

	public function getUrl ()
	{
		return '#' . URL::site(Route::get('shop.welcome', Request::$lang)->uri()) .
			'?skugroup_id=' . $this->pk();
	}

}


