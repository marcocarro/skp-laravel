<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Offer extends ModelNameDescription {

	protected $table = 'offer';
	protected $_table_columns = Array(
		'id' => Array(),
		'merchant_id' => Array(),
		'offer_type' => Array(),
		'active' => Array(),
		'publish_from_datetime' => Array(),
		'publish_to_datetime' => Array(),
		'image_id' => Array(),
		'extra' => Array(),
		'rules' => Array(),
		'archived' => Array(),
		'deleted' => Array(),
	);

	protected $_serialize_columns = array('extra', 'rules');

	protected $_belongs_to = array(
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
		'image' => array(
			'model' => 'Skp_Image',
			'foreign_key' => 'image_id',
		),
	);

	protected $_has_many = array(
		'offer_product_suggested' => array(
			'model' => 'Skp_OfferProductSuggested',
			'foreign_key' => 'offer_id',
		),
		'products_suggested' => array(
			'model' => 'Skp_Product',
			'foreign_key' => 'offer_id',
			'through' => 'offer_product_suggested',
			'far_key' => 'product_id',
		),
		'products_related' => array(
			'model' => 'Skp_Product',
			'foreign_key' => 'offer_id',
			'through' => 'offer_product_related',
			'far_key' => 'product_id',
		),
	);

	const OFFER_CUSTOM			= 'Custom';
	const OFFER_SELLING_CROSS	= 'Cross Selling';
	const OFFER_SELLING_UP		= 'Up Selling';

	const OFFER_DISPLAY_OPTION_NONE		= 'None';
	const OFFER_DISPLAY_OPTION_LIST		= 'List';
	const OFFER_DISPLAY_OPTION_ALL		= 'All';

	const OFFER_SHOW_IN_SKUGROUP = 'show_in_skugroup';
	const OFFER_SHOW_IN_PRODUCT = 'show_in_product';
	const OFFER_SHOW_IN_ADDTOCART = 'show_in_addToCart_modal';
	const OFFER_SHOW_IN_CART = 'show_in_cart';

	const OFFER_CACHE_TTL = 180;

	public function getFieldInLang($field_name, $lang = NULL,$separator = ',')
	{
		$result = '';
		if (! is_array($this->getExtra($field_name)))
		{
			return $result;
		}
		$lang_array = $this->getExtra($field_name);
		if ($lang == NULL)
		{
			foreach ($lang_array as $lang=>$text)
			{
				$result .= "$lang=>'$text'".$separator;
			}
		}
		else
		{
			if (! isset($lang_array[$lang]))
			{
				$lang = key($lang_array); // Return first language found
			}
			if (isset($lang_array[$lang]))
			{
				$result = $lang_array[$lang];
			}
		}
		return $result;
	}

	function __construct($id = NULL)
	{
		parent::__construct($id);
		if ($this->loaded() == FALSE)
		{
			$this->set('active', 0);
			$this->set('publish_from_datetime', NULL);
			$this->set('publish_to_datetime', NULL);
			$this->set('offer_type', self::OFFER_CUSTOM);
			$this->set('rules', array(
				'product_suggested_list' => array(),
				'audience' => 'All',
				'hide_if_in_cart' => 'N',
				'show_in_skugroup' => self::OFFER_DISPLAY_OPTION_NONE,
				'skugroup_list' => array(),
				'show_in_product' => self::OFFER_DISPLAY_OPTION_NONE,
				'product_related_list' => array(),
				'show_in_addToCart_modal' => self::OFFER_DISPLAY_OPTION_NONE,
				'show_in_cart' => self::OFFER_DISPLAY_OPTION_NONE
			));
		} else
		{
			$rules = $this->get('rules');
			if (!isset($rules) OR !is_array($rules)) $rules = array();
			if (!isset($rules['product_suggested_list'])) $rules['product_suggested_list'] = array();
			if (!isset($rules['audience'])) $rules['audience'] = 'All';
			if (!isset($rules['hide_if_in_cart'])) $rules['hide_if_in_cart'] = 'N';
			if (!isset($rules['show_in_skugroup'])) $rules['show_in_skugroup'] = self::OFFER_DISPLAY_OPTION_ALL;
			if (!isset($rules['skugroup_list'])) $rules['skugroup_list'] = array();
			if (!isset($rules['show_in_product'])) $rules['show_in_product'] = self::OFFER_DISPLAY_OPTION_ALL;
			if (!isset($rules['product_related_list'])) $rules['product_related_list'] = array();
			if (!isset($rules['show_in_addToCart_modal'])) $rules['show_in_addToCart_modal'] = self::OFFER_DISPLAY_OPTION_NONE;
			if (!isset($rules['show_in_cart'])) $rules['show_in_cart'] = self::OFFER_DISPLAY_OPTION_ALL;
			$this->set('rules', $rules);
		}
	}

	public function getBoName($lang = NULL)
	{
		return ($this->getExtra('bo_name') != NULL ? $this->getExtra('bo_name') : $this->getName(($lang != NULL ? $lang : $this->get('merchant')->getMerchantExtra('default_language'))));
	}
	
	public function initDateTimes($merchant)
	{
		if ($merchant->loaded() == TRUE AND $this->loaded() == FALSE)
		{
			$this->set('merchant', $merchant);
			$this->set('publish_from_datetime', (new DateTime('now', $this->get('merchant')->getTimeZone()))->format('Y-m-d H:i:s'));
			$this->set('publish_to_datetime', (new DateTime('now', $this->get('merchant')->getTimeZone()))->format('Y-m-d H:i:s'));
		}
	}

	public function getOfferCacheKey()
	{
		return ($this->loaded() ? 'offer_' . $this->pk() : NULL);
	}

	public function getWhereIsPublished()
	{
		$rules = $this->get('rules', array());
		$rule_section = array('show_in_skugroup' => 'skugroups', 'show_in_product' => 'product_detail', 'show_in_addToCart_modal' => 'cart_modal', 'show_in_cart' => 'basket');
		$where_is_published = '';
		foreach ($rule_section as $rule => $section)
		{
			if (Arr::get($rules, $rule) != NULL AND Arr::get($rules, $rule) != self::OFFER_DISPLAY_OPTION_NONE)
			{
				$where_is_published != '' AND $where_is_published .= '<br>';
				$where_is_published .= __('offer.bo.'.$section) . ' ['.Arr::get($rules, $rule).']';
			}
		}
		return $where_is_published;
	}

	public static function getOffers($params = array())
	{
		$merchant = Arr::get($params, 'merchant', NULL);
		$offer_type = Arr::get($params, 'offer_type', NULL);
		$ignore_offer_type = Arr::get($params, 'ignore_offer_type', FALSE);
		$from = Arr::get($params, 'from', NULL);
		$to = Arr::get($params, 'to', NULL);
		$all = Arr::get($params, 'all', FALSE);

		$result = ORM::factory('Skp_Offer');
		if ($offer_type != NULL AND !$ignore_offer_type)
		{
			!is_array($offer_type) AND $result->and_where('offer_type', '=', $offer_type);
			is_array($offer_type) AND $result->and_where('offer_type', 'IN', $offer_type);
		}
		if ($merchant != NULL)
		{
			$result->and_where('merchant_id', '=', $merchant->pk());
		}
		if ($from != NULL)
		{
			$result->and_where('publish_from_datetime', '<=', $to);
		}
		if ($to != NULL)
		{
			$result->and_where('publish_to_datetime', '>=', $from);
		}
		if ($all != TRUE)
		{
			$result->and_where('active', '=', 1);
		}
		$result->and_where('deleted','=',0);
		return $result;
	}

	public static function getActiveOffers($params = array())
	{
		$merchant = Arr::get($params, 'merchant', NULL);
		$params['from'] = Arr::get($params, 'from', (new DateTime('now', $merchant->getTimeZone()))->format('Y-m-d H:i:s'));
		$params['to'] = Arr::get($params, 'to', $params['from']);
		$offers = static::getOffers($params);
		$offers = $offers->find_all()->as_array();
		
		return $offers;
	}

	public static function cacheOfferRelations($offers = array(), $merchant)
	{
		$offer_ids_list = array(
			'skugroup' => array('all'=>array()),
			'product_related' => array('all'=>array()),
		);
		foreach ($offers as $offer)
		{
			$rules = $offer->get('rules');
			$related_list_set = array();
			foreach (array('show_in_skugroup'=>'skugroup', 'show_in_product'=>'product_related', 'show_in_addToCart_modal'=>'product_related', 'show_in_cart'=>'product_related') as $show_in=>$related_list)
			{
				!isset($related_list_set[$related_list]) AND $related_list_set[$related_list] = FALSE;
				if ($rules[$show_in] == self::OFFER_DISPLAY_OPTION_ALL)
				{
					!in_array($offer->pk(), $offer_ids_list[$related_list]['all']) AND $offer_ids_list[$related_list]['all'][] = $offer->pk();
				}
				else if ($rules[$show_in] == self::OFFER_DISPLAY_OPTION_LIST AND !$related_list_set[$related_list])
				{
					foreach ($rules[$related_list.'_list'] as $related_id)
					{
						!isset($offer_ids_list[$related_list][$related_id]) AND $offer_ids_list[$related_list][$related_id] = array();
						!in_array($offer->pk(), $offer_ids_list[$related_list][$related_id]) AND $offer_ids_list[$related_list][$related_id][] = $offer->pk();
					}
					$related_list_set[$related_list] = TRUE;
				}
			}
		}
		// Cycle through all active Skugroups and if $offer_ids_list['skugroup'][skugroup_id] is not set, add empty array
		$now = (new DateTime('now', $merchant->getTimeZone()))->format('Y-m-d H:i:s');
		$skugroups_without_offers = $merchant->get('skugroups')->where('id','NOT IN',array_keys($offer_ids_list['skugroup']))->and_where('active','=',1)->and_where('deleted','=',0)->and_where_open()->where('publish_from', 'is', NULL)->or_where('publish_from', '<=', $now)->and_where_close()->and_where_open()->where('publish_to', 'is', NULL)->or_where('publish_to', '>=', $now)->and_where_close()->find_all();
		foreach ($skugroups_without_offers as $skugroup)
		{
			$offer_ids_list['skugroup'][$skugroup->pk()] = array();
		}
		// Cycle through all active Products and if $offer_ids_list['product_related'][product_id] is not set, add empty array
		$products_without_offers = $merchant->get('products')
			->join('sku')
				->on('sku.product_id', '=', 'skp_product.id')
			->join('skugroup_sku')
				->on('skugroup_sku.sku_id','=','sku.id')
			->join('skugroup')
				->on('skugroup_sku.skugroup_id','=','skugroup.id')
			->where('skp_product.active','=',1)->and_where('skp_product.deleted','=',0)
			->and_where('skp_product.id','NOT IN',array_keys($offer_ids_list['product_related']))
			->and_where('skugroup.id', 'IN', array_keys($offer_ids_list['skugroup']))->distinct(TRUE)->find_all();
		foreach ($products_without_offers as $product)
		{
			$offer_ids_list['product_related'][$product->pk()] = array();
		}

		$cache_key = $merchant->getOfferRelationsCacheKey();
		$cache_inst = Skp_Cache::instance();
		$cache_inst->set($cache_key, $offer_ids_list, self::OFFER_CACHE_TTL);

		return $offer_ids_list;
	}

	public static function cacheOffersData($offers = array())
	{
		$cached_offers_data = array();
		foreach ($offers as $offer)
		{
			$offer_type = $offer->get('offer_type');
			$obj = new StdClass();
			$obj->rules = $offer->get('rules');
			$obj->from = $offer->get('publish_from_datetime');
			$obj->to = $offer->get('publish_to_datetime');
			$obj->type = $offer_type;
			$obj->contents = array();
			if ($offer_type == Offer::OFFER_CUSTOM)
			{
				$obj->contents[] = array(
					'object_id' => 'offer_'.$offer->pk(),
					'sort' => $offer->getExtra('sort'),
					'image_url' => $offer->get('image')->getImageUrl(TRUE, ($offer->getExtra('image_aspect_ratio') != 'original' ? 'large-'.$offer->getExtra('image_aspect_ratio') : 'original')),
					'offer_name' => $offer->getExtra('name'),
					'offer_description' => $offer->getExtra('description'),
					'offer_link' => $offer->getExtra('link'),
					'new_window_link' => $offer->getExtra('new_window_link'),
				);
			} else if ($offer_type == Offer::OFFER_SELLING_CROSS OR $offer_type == Offer::OFFER_SELLING_UP)
			{
				foreach ($offer->get('offer_product_suggested')->order_by('sort','ASC')->find_all() as $offer_product_suggested)
				{
					$product_suggested = $offer_product_suggested->get('product');
					$obj->contents[] = array(
						'object_id' => 'product_'.$product_suggested->pk(),
						'sort' => $offer_product_suggested->get('sort'),
						'image_url' => $product_suggested->getImage(TRUE, 'large-5_3'),
						'offer_name' => $product_suggested->get('name'),
						'offer_description' => $product_suggested->get('short_description'),
						'offer_link' => $offer_product_suggested->getI18nLink(),
						'new_window_link' => $offer_product_suggested->getExtra('new_window_link'),
					);
				}
			}
			Skp_Cache::instance()->set($offer->getOfferCacheKey(), $obj);
			$cached_offers_data[$offer->pk()] = $obj;
		}
		return $cached_offers_data;
	}

	public static function filterOffersData($offers_data, $params = array())
	{
		$skugroup_id = Arr::get($params, 'skugroup_id');
		$product_ids = Arr::get($params, 'product_ids');
		$audience = Arr::get($params, 'audience');
		$offer_type = Arr::get($params, 'offer_type');
		$show_in = Arr::get($params, 'show_in');
		$merchant = Arr::get($params, 'merchant');
		$now = (new DateTime('now', $merchant->getTimeZone()))->format('Y-m-d H:i:s');

		$offers_contents = array();
		$skugroup_id != NULL AND $obj_key = 'skugroup' AND $obj_ids = array($skugroup_id);
		$product_ids !== NULL AND $obj_key = 'product_related' AND $obj_ids = $product_ids;
		if (isset($obj_key))
		{
			foreach ($offers_data as $offer_data)
			{
				$from = $offer_data->from;
				$to = $offer_data->to;
				$type = $offer_data->type;
				if ($now >= $from AND $now < $to AND in_array($type, $offer_type))
				{
					$rules = $offer_data->rules;
					$rule_show_in = Arr::get($rules, $show_in);
					$related_obj_list = Arr::get($rules, $obj_key.'_list');
					$intersect_obj_ids_related_obj_list = $obj_ids != array() ? array_intersect($obj_ids, array_values($related_obj_list)) : NULL;
					if ($rule_show_in == self::OFFER_DISPLAY_OPTION_ALL OR ($rule_show_in == self::OFFER_DISPLAY_OPTION_LIST AND $obj_ids !== NULL AND count($intersect_obj_ids_related_obj_list) > 0))
					{
						$rule_audience = Arr::get($rules, 'audience');
						if (in_array($rule_audience, array('All', $audience)))
						{
							$contents = $offer_data->contents;
							foreach ($contents as $content)
							{
								!isset($offers_contents[$content['object_id']]) AND $offers_contents[$content['object_id']] = $content;
							}
						}
					}
				}
			}
		}
		if (sizeof($offers_contents) > 0)
		{
			uasort($offers_contents, function($a, $b) {
				if ($a['sort'] == $b['sort']) return 0;
				return ($a['sort'] < $b['sort']) ? -1 : 1;
			});
		}
		return $offers_contents;
	}

	public static function reduceOffersData($offer_relations, $offers_data, $params = array())
	{
		$skugroup_id = Arr::get($params, 'skugroup_id');
		$product_ids = Arr::get($params, 'product_ids');

		$skugroup_id != NULL AND $obj_key = 'skugroup' AND $obj_ids = array($skugroup_id);
		$product_ids !== NULL AND $obj_key = 'product_related' AND $obj_ids = $product_ids;
		$distinct_offer_ids = $offer_relations[$obj_key]['all'];
		foreach ($obj_ids as $obj_id)
		{
			$obj_offer_ids = Arr::get($offer_relations[$obj_key], $obj_id);
			$obj_offer_ids != NULL AND is_array($obj_offer_ids) AND $distinct_offer_ids = array_unique(array_merge($distinct_offer_ids, $obj_offer_ids));
		}
		$reduced_offers_data = array();
		foreach ($distinct_offer_ids as $offer_id)
		{
			$reduced_offers_data[] = Arr::get($offers_data, $offer_id);
		}
		return $reduced_offers_data;
	}

	public static function getCachedOfferRelations($params = array())
	{
		$merchant = Arr::get($params, 'merchant');

		$cache_inst = Skp_Cache::instance();
		$offer_relations_cache_key = $merchant->getOfferRelationsCacheKey();
		return $cache_inst->get($offer_relations_cache_key);
	}

	public static function getCachedOfferIds($params = array())
	{
		$skugroup_id = Arr::get($params, 'skugroup_id');
		$product_ids = Arr::get($params, 'product_ids');
		$merchant = Arr::get($params, 'merchant');

		$offer_ids = NULL;
		$offer_relations = Offer::getCachedOfferRelations($params);
		if ($offer_relations !== NULL)
		{
			$skugroup_id != NULL AND $obj_key = 'skugroup' AND $obj_ids = array($skugroup_id);
			$product_ids !== NULL AND $obj_key = 'product_related' AND $obj_ids = $product_ids;
			if (isset($obj_key))
			{
				$offer_ids_list = Arr::get($offer_relations, $obj_key);
				if ($offer_ids_list !== NULL)
				{
					$offer_ids_list_all = Arr::get($offer_ids_list, 'all');
					$missing_offer_ids_list_obj_ids = FALSE;
					$offer_ids_list_obj_ids = array();
					foreach ($obj_ids as $obj_id)
					{
						$offer_ids_list_obj_id = Arr::get($offer_ids_list, $obj_id);
						if ($offer_ids_list_obj_id !== NULL)
						{
							$offer_ids_list_obj_ids = array_unique(array_merge($offer_ids_list_obj_ids, $offer_ids_list_obj_id));
						} else {
							$missing_offer_ids_list_obj_ids = TRUE;
							break;
						}
					}
					if ($offer_ids_list_all !== NULL AND !$missing_offer_ids_list_obj_ids)
					{
						$offer_ids = array_unique(array_merge($offer_ids_list_all, $offer_ids_list_obj_ids));
					}
				}
			}
		}
		return $offer_ids;
	}

	public static function getCachedOffersData($cached_offer_ids = array())
	{
		$offers_data = array();
		if (!empty($cached_offer_ids))
		{
			$cache_inst = Skp_Cache::instance();
			$offers_cache_keys = array_map(function($val) { return 'offer_' . $val; }, $cached_offer_ids);
			// Get offers data from cache
			$cached_offers_data = $cache_inst->getMulti($offers_cache_keys);
			// Check which offers_data is missing in the cache
			$offers_cache_keys_not_found = array_diff($offers_cache_keys, array_keys($cached_offers_data));
			// Put found offers_data in the returning array
			$offers_data = array_values($cached_offers_data);
			$missing_orm_offers = array();
			// Foreach missing offers_data reloads orm_offer
			foreach ($offers_cache_keys_not_found as $offer_cache_key)
			{
				$offer_id = str_replace('offer_', '', $offer_cache_key);
				$offer = ORM::factory('Skp_Offer', $offer_id);
				$offer->loaded() AND $missing_orm_offers[] = $offer;
			}
			// If there were missing offers_data cache orm_offers reloaded in the previous step adding offers_data to the returning array
			if (sizeof($missing_orm_offers) > 0)
			{
				$offers_data = array_merge($offers_data, array_values(Offer::cacheOffersData($missing_orm_offers)));
			}
		}
		return $offers_data;
	}

	public static function cacheActiveOffers($params = array())
	{
		$merchant = Arr::get($params, 'merchant');
		$now = new DateTime('now', $merchant->getTimeZone());
		$params['from'] = Arr::get($params, 'from',($now)->format('Y-m-d H:i:s'));
		$params['to'] = Arr::get($params, 'to', (clone $now)->add(new DateInterval('PT'.(self::OFFER_CACHE_TTL+1).'S'))->format('Y-m-d H:i:s'));
		$params['ignore_offer_type'] = TRUE;
		// Get active ORM offers
		$active_offers = static::getActiveOffers($params);
		// Set in cache offer_relations
		$offer_relations = Offer::cacheOfferRelations($active_offers, $merchant);
		// Set in cache offers_data
		$offers_data = Offer::cacheOffersData($active_offers);
		
		return array('relations' => $offer_relations, 'data' => $offers_data);
	}
	
	public static function getOffersContents($params = array())
	{
		$skugroup_id = Arr::get($params, 'skugroup_id');
		$product_ids = Arr::get($params, 'product_ids');
		$merchant = Arr::get($params, 'merchant');
		$offers_contents = array();
		
		if ($skugroup_id != NULL OR $product_ids !== NULL)
		{
			// If in cache offers_[merchant_id]
			$cached_offer_ids = Offer::getCachedOfferIds($params);
			if ($cached_offer_ids !== NULL)
			{
				// Get offers_data from cache (if any offers_data is missing in the cache it will be reloaded and added in cache)
				$offers_data = Offer::getCachedOffersData($cached_offer_ids);
			}
			// else
			else
			{
				
				$now = new DateTime('now', $merchant->getTimeZone());
				$params['from'] = Arr::get($params, 'from',($now)->format('Y-m-d H:i:s'));
				$params['to'] = Arr::get($params, 'to', (clone $now)->add(new DateInterval('PT'.(self::OFFER_CACHE_TTL+1).'S'))->format('Y-m-d H:i:s'));
				// Get active ORM offers, set in cache offer_relations, set in cache offers_data
				$active_offers = Offer::cacheActiveOffers($params);
				$offer_relations = Arr::get($active_offers, 'relations', array());
				$offers_data = Arr::get($active_offers, 'data', array());
				//	Reduce offer_relations and offers_data based on product_id/skugroup_id
				$offers_data = Offer::reduceOffersData($offer_relations, $offers_data, $params);
			}
			// Filter offers_data
			$offers_contents = Offer::filterOffersData($offers_data, $params);
		}

		return $offers_contents;
	}
}
