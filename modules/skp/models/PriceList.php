<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class PriceList extends Model {

	protected $table = 'price_list';
	
	protected $_table_columns = array(
		'id' => array(),
		'merchant_id' => array (),
		'catalog_id' => array(),
		'name' => array(),
		'priority' => array(),
		'archived' => array(),
		'deleted' => array(),
	);
	
	protected $_belongs_to = array(
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
		'catalog' => array(
			'model' => 'Skp_Catalog',
			'foreign_key' => 'catalog_id',
		),
	);
	
	protected $_has_many = array(
		'calendars' => array(
			'model' => 'Skp_Calendar',
			'through' => 'price_list_calendar',
			'foreign_key' => 'calendar_id',
			'far_key' => 'price_list_id'
		),
		'availability_product_sku_calendar_pricelist' => array(
			'model' => 'Skp_AvailabilityProductSkuCalendarPricelist',
			'foreign_key' => 'price_list_id'
		),
		'price_list_price_category' => array(
			'model' => 'Skp_PriceListPricecategory',
			'foreign_key' => 'price_list_id',
		),
	);

	public function rules()
	{
		return array(
			'name' => array(
				array('not_empty'),
			),
			'merchant_id' => array(
				array('not_empty'),
			),
			'catalog_id' => array(
				array('not_empty'),
			),
		);
	}

}
