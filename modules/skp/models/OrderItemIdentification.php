<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class OrderItemIdentification extends Model {

	protected $table = 'order_item_identification';
	protected $_table_columns = Array(
		'id' => Array(),
		'orderitem_id' => Array(),
		'type' => Array(),
		'subtype' => Array(),
		'value' => Array(),
		'extra' => Array(),
	);
	
protected $casts = [
		'extra' => 'array',
	];

	const TYPE_BARCODE		= 'Barcode';
	const TYPE_CARD			= 'Card';
	const TYPE_LICENSEPLATE	= 'LicensePlate';

	protected $_belongs_to = array(
		'merchant' => array(
			'model' => 'Skp_OrderItem',
			'foreign_key' => 'orderitem_id',
		),
	);
}
