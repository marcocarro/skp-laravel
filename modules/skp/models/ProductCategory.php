<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class ProductCategory extends ModelNameDescription {

	protected $table = 'product_category';
	
	protected $_table_columns = array(
		'id' => array(), 
		'merchant_id' => array (),
		'name' => array (),
		'description' => array (),
		'extra' => array (),
		'active' => array (),
		'archived' => array (),
		'deleted' => array (),
	);
	
	protected $_serialize_columns = array('extra', 'name', 'description');

	protected $_belongs_to = array(
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
	);
	protected $_has_many = array(
		'products' => array(
			'model' => 'Skp_Product',
			'foreign_key' => 'productcategory_id',
		),
		'product_category_externals' => array(
			'model' => 'Skp_ProductCategoryExternal',
			'foreign_key' => 'product_category_id',
		),
	);

	public function rules()
	{
		return array(
			'merchant_id' => array(
				array('not_empty'),
			),
			'name' => array(
				array('not_empty'),
			),
		);
	}
}
