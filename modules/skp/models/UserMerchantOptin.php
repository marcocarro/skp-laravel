<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class UserMerchantOptin extends Model {

	protected $table = 'user_merchant_optin';

	protected $_table_columns = Array(
		'id' => Array(), 
		'merchant_id' => Array (),
		'user_id' => Array(),
		'optin_id' => Array(),
		'accepted' => Array(),
		'last_change' => Array(),
		);

	protected $_belongs_to = array(
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
		'user' => array(
			'model' => 'Skp_User',
			'foreign_key' => 'user_id',
		),
		'merchant_optin' => array(
			'model' => 'Skp_MerchantOptin',
			'foreign_key' => 'optin_id',
		),
	);
	
	public function getLastChangeInFormat($time_zone = NULL, $date_format = NULL)
	{
		$last_change = $this->get('last_change');
		if ($last_change != NULL AND $date_format != NULL AND $time_zone != NULL)
		{
			$date_time = new DateTime($last_change);
			$date_time->setTimezone($time_zone);
			return $date_time->format($date_format);
		}else
		{
			return '';
		}
	}
	
	public function getOptinName($lang)
	{
		return $this->get('merchant_optin')->getName($lang);
	}
	
}

