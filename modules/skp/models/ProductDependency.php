<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class ProductDependency extends Model {

	protected $table = 'product_dependency';
	protected $_belongs_to = array(
		'main_product' => array(
			'model' => 'Skp_Product',
			'foreign_key' => 'main_product_id',
		),
		'main_pricecategory' => array(
			'model' => 'Skp_PriceCategory',
			'foreign_key' => 'main_pricecategory_id',
		),
		'sub_product' => array(
			'model' => 'Skp_Product',
			'foreign_key' => 'sub_product_id',
		),
		'sub_pricecategory' => array(
			'model' => 'Skp_PriceCategory',
			'foreign_key' => 'sub_pricecategory_id',
		),
	);
	
	public function getMerchantId()
	{
		if ($this->get('main_product_id') != NULL)
		{
			return $this->get('main_product')->get('merchant')->pk();
		}
		else if ($this->get('main_pricecategory_id') != NULL)
		{
			return $this->get('main_pricecategory')->get('merchant')->pk();
		}
		else
			return NULL;
	}
}

