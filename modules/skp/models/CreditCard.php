<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class CreditCard extends Model {

	protected $table = 'credit_card';
	public $timestamps = false;

	public function user() {
		$this->belongsTo(User::class);
	}

	public function payment_channel() {
		$this->belongsTo(PaymentChannel::class, 'payment_channel_id');
	}

}