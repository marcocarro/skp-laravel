<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class ORMNameDescription extends Model {

	protected $_serialize_columns = array('name', 'short_description', 'description');

	public function getFieldInLang($field_name, $lang = NULL, $separator = ',')
	{
		$result = '';
		if (! is_array($this->get($field_name)))
		{
			$this->get($field_name) == '' OR $result = $this->get($field_name);
			return $result;
		}
		$lang_array = $this->get($field_name);
		if ($lang == NULL)
		{
			foreach ($lang_array as $lang=>$text)
			{
				$result .= "$lang=>'$text'".$separator;
			}
		}
		else
		{
			if (! isset($lang_array[$lang]))
			{
				$lang = key($lang_array); // Return first language found
			}
			if (isset($lang_array[$lang]))
			{
				$result = $lang_array[$lang];
			}
		}
		return $result;
	}

	public function getName($lang = NULL,$separator = ',')
	{
		return $this->getFieldInLang('name', $lang, $separator);
	}

	public function getShortDescription($lang = NULL,$separator = ',')
	{
		return $this->getFieldInLang('short_description', $lang, $separator);
	}

	public function getDescription($lang = NULL,$separator = ',')
	{
		return $this->getFieldInLang('description', $lang, $separator);
	}

	public function getPracticalInfo($lang = NULL,$separator = ',')
	{
		return $this->getFieldInLang('practical_info', $lang, $separator);
	}
	
	public function getLink($lang = NULL,$separator = ',')
	{
		return $this->getFieldInLang('link', $lang, $separator);
	}
}
