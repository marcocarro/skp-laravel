<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Tag extends Model {

	protected $table = 'tag';
	
	protected $_table_columns = array(
		'id' => array(), 
		'merchant_id' => array (),
		'value' => array(),
	);
	
	protected $_serialize_columns = array();

	protected $_belongs_to = array(
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
	);
	
	protected $_has_many = array(
		'tag_associations' => array(
			'model' => 'Skp_TagAssociation',
			'foreign_key' => 'tag_id',
		),
	);
	
	public function rules()
	{
		return array(
			'merchant_id' => array(
				array('not_empty'),
			),
			'value' => array(
				array('not_empty'),
			),
		);
	}

	public function getNewPK ()
	{
		$query = DB::select('AUTO_INCREMENT')->from( DB::expr('INFORMATION_SCHEMA.TABLES'))->where('TABLE_NAME', '=', Database::instance()->table_prefix().$this->table_name());
		$results = $query->execute();
		if (count($results) == 1)
		{
			$result = intval($results[0]['AUTO_INCREMENT']);
		} else
		{
			$result = 1;
		}
		return $result;
	}
}
