<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class VirtualCardRealCard extends Model {

	protected $table = "virtual_card_real_card";

	protected $_table_columns = array(
		'id'							=> array(),
		'virtual_card_id'			=> array(),
		'card_type'					=> array(),
		'card_serial'				=> array(),
		'extra'						=> array(),
		'deleted'					=> array(),
	);

	
protected $casts = [
		'extra' => 'array',
	];

	protected $_belongs_to = array(
		
	);
	
	protected $_has_many =	array(
		
	);
	
	protected $_has_one = array(
		'virtual_card' => array(
			'model' => 'Skp_VirtualCardType',
			'foreign_key' => 'virtual_card_id',
		),
	);
	
	public function rules()
	{
		return array(
			'card_serial' => array(
				array('not_empty'),
			),
			'card_type' => array(
				array('not_empty'),
			),
			'virtual_card_id' => array(
				array('not_empty'),
			),
		);
	}

	public function __toString()
	{
		return (String)$this->card_serial;
	}
	
	public function getExtra($key, $default = NULL)
	{
		if ($key != NULL)
		{
			return Arr::get($this->get('extra'), $key, $default);
		}
	}
	
	public function setExtra($extra_name, $extra_value)
	{
		$extras = $this->get('extra');

		if ($extra_value != NULL)
		{
			$extras[$extra_name] = $extra_value;
		}else{
			unset($extras[$extra_name]);
		}
		$this->set('extra', $extras);
	}
	
	public function getCardTypeList()
	{
		$card_type_list = Card::$card_types;
		if(($key = array_search(Card::CARDTYPE_VIRTUAL, $card_type_list)) !== false) {
			 unset($card_type_list[$key]);
		}
		return array_combine($card_type_list, $card_type_list);
	}
	
	public function delete ()
	{
		$this->set('deleted', 1);
		$this->save();
	}
}

