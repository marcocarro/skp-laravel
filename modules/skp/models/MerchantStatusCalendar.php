<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class MerchantStatusCalendar extends Model {

	protected $table = 'merchant_status_calendar';
	public $timestamps = false;

	public function merchant() {
		$this->belongsTo(Merchant::class);
	}

	public function calendar() {
		$this->belongsTo(Calendar::class);
	}

}