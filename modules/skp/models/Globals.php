<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;

class Globals extends Model {

	protected $table = 'global';
	public $timestamps = false;
	protected $casts = [
		'config_value' => 'array',
		'extra' => 'array'
	];
	
}