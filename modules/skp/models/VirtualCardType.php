<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class VirtualCardType extends Model {

	protected $table = "virtual_card_type";

	
protected $casts = [
		'extra' => 'array',
	];

	protected $_belongs_to = array(
		'merchant' => array(
			'merchant' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
	);
	
	protected $_has_many =	array(
		'merchants' => array(
			'model' => 'Skp_Merchant',
			'through' => 'virtual_card_type_merchants',
			'foreign_key' => 'virtual_card_type_id',
			'far_key' => 'merchant_id',
		),
	);
	
	protected $_has_one = array(
		
	);
	
	public function rules()
	{
		return array(
			'name' => array(
				array('not_empty'),
			),
			'merchant_id' => array(
				array('not_empty'),
			),
		);
	}

	public function __toString()
	{
		return (String)$this->name;
	}
	
	public function getExtra($key, $default = NULL)
	{
		if ($key != NULL)
		{
			return Arr::get($this->get('extra'), $key, $default);
		}
	}
	
	public function setExtra($extra_name, $extra_value)
	{
		$extras = $this->get('extra');

		if ($extra_value != NULL)
		{
			$extras[$extra_name] = $extra_value;
		}else{
			unset($extras[$extra_name]);
		}
		$this->set('extra', $extras);
	}
	
	public function getCardTypeList()
	{
		$card_type_list = Card::$card_types;
		if(($key = array_search(Card::CARDTYPE_VIRTUAL, $card_type_list)) !== false) {
			 unset($card_type_list[$key]);
		}
		return array_combine($card_type_list, $card_type_list);
	}

	public function delete ()
	{
		$this->set('deleted', 1);
		$this->save();
	}
}

