<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class TrackingCode extends ModelNameDescription {

	protected $table = 'tracking_code';
	
	protected $_table_columns = Array(
		'id' => Array(), 
		'merchant_id' => Array (),
		'name' => Array(), 
		'type' => Array (),
		'code' => Array(), 
		'is_active' => Array (),
	);

	protected $_belongs_to = array(
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
	);
	
	public static $type_list = array('Google Analytics'=>'Google Analytics', 'Google Tag Manager' => 'Google Tag Manager', 'Facebook Pixel'=>'Facebook Pixel');
}
