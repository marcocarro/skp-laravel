<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class UserDownload extends Model {

	protected $table = 'user_download';
	
protected $casts = [
		'extra' => 'array',
	];
	protected $_table_columns = Array(
		'id' => Array(),
		'user_id' => Array(),
		'merchant_id' => Array(),
		'download_type' => Array(),
		'download_status' => Array(),
		'download_datetime' => Array(),
		'extra' => Array(),
	);
	
	protected $_belongs_to = array(
		'user' => array(
			'model' => 'Skp_User',
			'foreign_key' => 'user_id',
		),
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
	);
	
	const TYPE_USER_DATA				= 'USER_DATA';
	const STATUS_REQUESTED			= 'REQUESTED';
	const STATUS_DELIVERED			= 'DELIVERED';
	const USER_DATA_PERSONAL		= 'personal';
	const USER_DATA_TEAM				= 'team';
	const USER_DATA_CARD				= 'card';
	const USER_DATA_CREDIT_CARD	= 'credit-card';
	const USER_DATA_INVOICE			= 'invoice';
	const USER_DATA_RECEIPT			= 'receipt';
	const USER_DATA_POSITION		= 'position';
	const USER_DATA_OPT_IN			= 'opt-in';
	const USER_DATA_PHOTO			= 'photo';
	const USERDATA_TYPE_LIST = array(self::USER_DATA_PERSONAL, self::USER_DATA_TEAM, self::USER_DATA_CARD, self::USER_DATA_INVOICE, self::USER_DATA_RECEIPT, self::USER_DATA_POSITION, self::USER_DATA_OPT_IN, self::USER_DATA_PHOTO);

	public static $filemanager_profile = 'user_tmp_data';
	
	public function setExtra($extra_name, $extra_value)
	{
		$ue = $this->get('extra');

		if ($extra_value !== NULL)
		{
			$ue[$extra_name] = $extra_value;
		}else{
			unset($ue[$extra_name]);
		}
		$this->set('extra', $ue);
	}
	
	public function getExtra($key, $default_value = NULL)
	{
		if ($key != NULL)
		{
			return Arr::get($this->get('extra'), $key, $default_value);
		}
	}
	
	public function getCreationDate()
	{
		return DateTime::createFromFormat('Y-m-d H:i:s', $this->get('download_datetime'));
	}
	
	public function downloadData()
	{
		$download_type = $this->get('download_type');
		switch ($download_type)
		{
			case self::TYPE_USER_DATA:
				$complete = $this->downloadData_UserData(self::USERDATA_TYPE_LIST);
				break;
		}
		if ($complete)
		{
			$this->set('download_status', self::STATUS_DELIVERED);
			$this->save();
			return TRUE;
		}
		return FALSE;
	}
	
	public function downloadData_UserData($type_list = array())
	{
		$zip_file = $this->getZippedDatas($type_list);
		$attachments = array(
								array(
									'full_filename' => $zip_file,
									'filename' => 'user_data.zip',
									'filemanager_profile' => UserDownload::$filemanager_profile
								),
							);
		$this->sendDownloadEmail(array('attachments'=> $attachments));
		Skp_StaticFileManager::deleteFile($zip_file, UserDownload::$filemanager_profile);
		//unlink($zip_file);
		return TRUE;
	}
	
	public function getZippedDatas($type_list = array())
	{
		$user = $this->get('user');
		$user_data = $this->getUserData($type_list);
		$download_folder_path = DIRECTORY_SEPARATOR . 'tmp' ; // we use the system tmp directory
		$tmp_folder_name = 'user_data_'.$user->pk();
		$tmp_folder_path = $download_folder_path . DIRECTORY_SEPARATOR . $tmp_folder_name;
		$zip_filename = $tmp_folder_path . '.zip';
		
		if (file_exists($tmp_folder_path) OR mkdir($tmp_folder_path, 0777, TRUE))
		{
			foreach ($user_data as $data_type => $datas)
			{
				$num_datas = count($datas);
				if (($data_type == 'photo' AND $num_datas > 0) OR $num_datas > 1)
				{
					$data_type_folder = $tmp_folder_path . DIRECTORY_SEPARATOR . $data_type;
					if (file_exists($data_type_folder) OR mkdir($data_type_folder, 0777, TRUE))
					{
						if ($data_type == 'photo')
						{
							foreach ($datas as $photo_filename => $raw_photo)
							{
								$filename = $data_type_folder . DIRECTORY_SEPARATOR . $photo_filename;
								$base64_photo = base64_decode($raw_photo);
								file_put_contents($filename, $base64_photo);
							}
						} else {
							$filename = $data_type_folder . DIRECTORY_SEPARATOR . $data_type . '_data.csv';
							$fp = fopen($filename, 'w');
							foreach ($datas as $data)
							{
								fputcsv($fp, $data, ';');
							}
							fclose($fp);
						}
					}
				}
			}
			$zip_pwd = $this->getExtra('zip_password') != NULL ? '--password ' . $this->getExtra('zip_password') : '';
			$compress_data = exec("zip $zip_pwd -rm $zip_filename $tmp_folder_path");
		}
		return $zip_filename;
	}
	
	/* Returns an array of user's datas.
	 * The key of each element of the array represent data type, the value is an array in which the first element is always the array of fields name, 
	 * and the other elements are array of values (in same order as first element) 
	 * The only exception is Photo data that is an array of filename.jpg => raw_data */
	public function getUserData($type_list = array())
	{
		$user = $this->get('user');
		$merchant = $this->get('merchant');
		$user_data = array();
		foreach ($type_list as $type)
		{
			$user_data[$type] = array();
		}
		
		if (in_array(self::USER_DATA_PERSONAL, $type_list))
		{
			/* Personal Datas
			* Load all contact profiles used by the merchant then return array of user's datas based
			* on which fields are mandatory/optional in at least one contact profile */
			$merchant_contact_profiles = ORM::factory('Skp_ContactProfile')->where('merchant_id', '=', $merchant->pk())->find_all();
			$user_columns = $user->list_columns();
			$contact_profiles_fields = ContactProfile::$list_info;
			$personal_datas = array();
			$user_data[self::USER_DATA_PERSONAL][] = $contact_profiles_fields;
			foreach ($contact_profiles_fields as $field)
			{
				if (!isset($personal_datas[$field]))
				{
					if ($field == 'photo' AND $user->get($field) != NULL)
					{
						$photo_filename = $user->getMyPerson()->pk() . '.jpg';
						$user_data[self::USER_DATA_PHOTO][$photo_filename] = $user->get($field);
						$personal_datas[] = $photo_filename;
					} else {
						$personal_datas[] = (isset($user_columns[$field]) ? $user->get($field) : $user->getExtra($field));
					}
				}
			}
			$user_data[self::USER_DATA_PERSONAL][] = $personal_datas;
		}
		
		if (in_array(self::USER_DATA_TEAM, $type_list))
		{
			/* Team Datas
			 * Load all user's team members then return array of their datas based on contact_profiles_fields */
			$user_data[self::USER_DATA_TEAM][] = $contact_profiles_fields;
			foreach ($user->get('persons')->find_all() as $t_person)
			{
				$team_person_data = array();
				if ($t_person->get('relation') != Person::RELATION_SELF)
				{
					isset($person_columns) OR $person_columns = $t_person->list_columns();
					foreach ($contact_profiles_fields as $field)
					{
						if ($field == 'photo' AND $t_person->get($field) != NULL)
						{
							$photo_filename = $t_person->pk() . '.jpg';
							$user_data[self::USER_DATA_PHOTO][$photo_filename] = $t_person->get($field);
							$team_person_data[] = $photo_filename;
						} else {
							$team_person_data[] = isset($person_columns[$field]) ? $t_person->get($field) : $t_person->getExtra($field);
						}
					}
					$user_data[self::USER_DATA_TEAM][] = $team_person_data;
				}
			}
		}
		
		if (in_array(self::USER_DATA_CARD, $type_list))
		{
			/* Cards Datas
			 * Returns array of user's cards */
			$user_data[self::USER_DATA_CARD][] = array('nick','card_type','card_serial');
			foreach ($user->get('cards')->find_all() as $card)
			{
				$user_data[self::USER_DATA_CARD][] = array($card->get('nick'), $card->get('card_type'), $card->get('card_serial'));
			}
		}
		
		if (in_array(self::USER_DATA_CREDIT_CARD, $type_list))
		{		
			//~ /* Credit Card Datas
			 //~ * */
			$user_data[self::USER_DATA_CREDIT_CARD][] = array('payment_channel', 'issuer', 'issuer_country', 'masked_pan', 'expiry_dateYYMM', 'payment_method');
			foreach ($user->get('credit_cards')->find_all() as $c_card)
			{
				$user_data[self::USER_DATA_CREDIT_CARD][] = array($c_card->get('payment_channel')->get('name'), $c_card->get('issuer'), $c_card->get('issuer_country'), $c_card->get('masked_pan'), $c_card->get('expiry_dateYYMM'), $c_card->get('payment_method'));
			}
		}
		
		if (in_array(self::USER_DATA_INVOICE, $type_list) OR in_array(self::USER_DATA_RECEIPT, $type_list))
		{
			/* Invoice Receipt Datas
			 * Returns array of all Invoice/Receipt profiles of the user */
			$inv_rec_config = $merchant->get('invoice_receipt_config');
			$user_data[self::USER_DATA_INVOICE][] = InvoiceReceiptConfig::$invoice_field_list; //$inv_rec_config->getInvoiceFields();
			$user_data[self::USER_DATA_RECEIPT][] = InvoiceReceiptConfig::$receipt_field_list; //$inv_rec_config->getReceiptFields();
			foreach ($user->get('invoice_receipt_profiles')->where('merchant_id', '=', $merchant->pk())->find_all() as $profile)
			{
				$profile_data = array();
				$model = $profile->get('model') == InvoiceReceiptProfile::MODEL_INVOICE ? self::USER_DATA_INVOICE : self::USER_DATA_RECEIPT;
				foreach ($user_data[$model][0] as $field)
				{
					$profile_data[] = $profile->getExtra($field);
				}
				$user_data[$model][] = $profile_data;
			}
		}
		
		if (in_array(self::USER_DATA_POSITION, $type_list))
		{
			/* Position Datas
			 * */
			$user_data[self::USER_DATA_POSITION][] =  array('when', 'device_type', 'latitude', 'longitude','formatted_address' );
			foreach ($user->get('user_merchant_positions')->where('merchant_id', '=', $merchant->pk())->find_all() as $position)
			{
				$user_data[self::USER_DATA_POSITION][] = array($position->get('when'), $position->get('device_type'), $position->get('latitude'), $position->get('longitude'), $position->get('formatted_address'));
			}
		}
		
		if (in_array(self::USER_DATA_OPT_IN, $type_list))
		{
			/* OptIn Datas
			 * */
			$user_data[self::USER_DATA_OPT_IN][] =  array('name', 'accepted', 'last_update');
			foreach ($user->get('user_merchant_optins')->where('merchant_id', '=', $merchant->pk())->find_all() as $optin)
			{
				$user_data[self::USER_DATA_OPT_IN][] = array(
													$optin->getOptinName($user->get('last_language')),
													($optin->get('accepted') == 1 ? 'Yes' : 'No'), 
													$optin->getLastChangeInFormat($merchant->getTimeZone(), 'Y-m-d H:i:s'),
				);
			}
		}
		
		return $user_data;
	}
	
	public function sendDownloadEmail($params = array(), $r_lang = NULL)
	{
		$user = $this->get('user');
		$merchant = $this->get('merchant');
		$download_type = $this->get('download_type');
		
		$email = $user->get('email');
		$attachments = Arr::get($params, 'attachments', NULL);
		
		$_config = Kohana::$config->load('skiperf');
		$messageChannel = $merchant->getMessageChannel();

		$msg_lang = ($r_lang != NULL) ? $r_lang: $user->last_language;
		$original_lang = I18n::lang();
		I18n::lang($msg_lang);

		$default_email_receipt_file = 'shop' . DIRECTORY_SEPARATOR . 'merchants' . DIRECTORY_SEPARATOR . 'default' . DIRECTORY_SEPARATOR . 'receipts' . DIRECTORY_SEPARATOR .'email_user_download';
		$merchant_email_receipt_file = 'shop' . DIRECTORY_SEPARATOR . 'merchants' . DIRECTORY_SEPARATOR . $this->get('merchant')->get('domain') . DIRECTORY_SEPARATOR . 'receipts' . DIRECTORY_SEPARATOR .'email_user_download';
		if (is_file(MODPATH . 'skiperformance' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . $merchant_email_receipt_file . '.php') == TRUE)
		{
			$receipt = View::factory($merchant_email_receipt_file);
		} else {
			$receipt = View::factory($default_email_receipt_file);
		}
		$receipt->body = __('email.user_download.body',  array(
									':merchant_name'	=> $merchant->get('name'),
									':user_name'		=> $user->getFullName(),
									':request_date'	=> $this->get('download_datetime'))
								);
		$receipt->lang = $msg_lang;
		$receipt->base_url = Skp_Helper::getProtocol() . Skp_Helper::getDomainEnv($this->get('merchant')->get('domain')) . Route::url($msg_lang.'.shop');
		$receipt->base_ajax_url = Skp_Helper::getProtocol() . Skp_Helper::getDomainEnv($this->get('merchant')->get('domain'));
		$receipt->base_img_url = Skp_Helper::getProtocol() . Skp_Helper::getDomainEnv($this->get('merchant')->get('domain'));
		$receipt->merchant = $this->get('merchant');
		$domain_array = explode('.' , $this->get('merchant')->get('domain'));
		$receipt->merchant_name = $domain_array[0];
		$receipt->default_receipt_colors = MODPATH . 'skiperformance' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'shop' . DIRECTORY_SEPARATOR . 'merchants' . DIRECTORY_SEPARATOR . 'default' . DIRECTORY_SEPARATOR . 'receipts' . DIRECTORY_SEPARATOR .'order_email_receipt_colors.php';
		$receipt->merchant_receipt_colors = MODPATH . 'skiperformance' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'shop' . DIRECTORY_SEPARATOR . 'merchants' . DIRECTORY_SEPARATOR . $this->get('merchant')->get('domain') . DIRECTORY_SEPARATOR . 'receipts' . DIRECTORY_SEPARATOR .'order_email_receipt_colors.php';

		$msg_params = array(
			'merchant.merchant' 	=> $merchant,
			'subject' 				=> __('email.user_download.subject',  array(
												':merchant_name'	=> $merchant->get('name'))
											),
			'body' 					=> $receipt,
			'lang' 					=> $msg_lang,
			// 'bcc' 					=> $messageChannel->mail_from,
			);
		if ($email != NULL)
		{
			$msg_params['to'] = $email;
			$msg_params['user.user'] = $user;
		}else{
			$msg_params['user.user'] = $user;
			$msg_params['bcc'] = $_config->get('email_skiperformance_orders');
		}
		if (!$user->isAnonymousUser($merchant))
		{
			$message = $messageChannel->call('createMessage', $msg_params);
			$message->save();

			$message->messageChannel->call('sendMessage', array('message.message' => $message, 'attachments' => $attachments));
		}
		I18n::lang($original_lang);
	}
}
