<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class CalendarDateTime extends Model {

	protected $table = 'calendar_date_time';
	public $timestamps = false;

	public function calendar() {
		$this->belongsTo(Calendar::class);
	}

	public function date() {
		$this->belongsTo(Skp_CalendarDate::class, 'date_id');
	}

	public function time() {
		$this->belongsTo(CalendarTime::class, 'time_id');
	}
	
}