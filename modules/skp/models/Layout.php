<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Layout extends Model {

	protected $table = 'layout';
	public $timestamps = false;
	protected $casts = [
		'options' => 'array',
	];

	public function merchant() {
		return $this->hasOne(Merchant::class);
   }

}