<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class DataProfile extends Model {

	protected $table = 'data_profile';
	public $timestamps = false;
	protected $casts = [
		'infos' => 'array',
	];

	public function merchant() {
		$this->belongsTo(Merchant::class);
	}

	public function product_data_profiles() {
		return $this->hasMAny(
			ProductDataProfile::class,
			'profile_id'
		);
	}

	public function data_profile_fields() {
		return $this->hasMAny(
			DataProfileField::class,
			'data_profile_id'
		);
	}
	
}