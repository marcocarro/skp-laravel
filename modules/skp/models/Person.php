<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Person extends Model {

	protected $table = 'person';
	
protected $casts = [
		'extra' => 'array',
	];
	
	protected $_table_columns = Array(
		'id' => Array(), 
		'user_id' => Array (),
		'relation' => Array(),
		'email' => Array(),
		'title' => Array(),
		'first_name' => Array(),
		'middle_name' => Array(),
		'last_name' => Array(),
		'birth_date' => Array(),
		'gender' => Array(),
		'street_num' => Array(),
		'street' => Array(),
		'city' => Array(),
		'state_prov' => Array(),
		'zip' => Array(),
		'country' => Array(),
		'phone' => Array(),
		'photo' => Array(),
		'image_url_thumb' => Array(),
		'deleted' => Array(),
		'extra' => Array(),
		);
		
	protected $_belongs_to = array(
		'user' => array(
			'model' => 'Skp_User',
			'foreign_key' => 'user_id',
		),
	);
	protected $_has_many = array(
		'cards' => array(
				'model' => 'Skp_Card',
				'foreign_key' => 'person_id',
				),
		'connector_person' => array(
			'model' => 'Skp_PersonConnector',
			'foreign_key' => 'person_id',
		),
		'order_items' => array(
			'model' => 'Skp_OrderItem',
			'foreign_key' => 'person_id',
		),
	);
	
	const RELATION_SELF 				= 'Self';
	const RELATION_WIFE 				= 'Wife';
	const RELATION_GIRLFRIEND 		= 'Girlfriend';
	const RELATION_HUSBAND 			= 'Husband';
	const RELATION_BOYFRIEND 		= 'Boyfriend';
	const RELATION_DAUGHTER 		= 'Daughter';
	const RELATION_SON 				= 'Son';
	const RELATION_OTHER_RELATIVE	= 'Other relative';
	const RELATION_FRIEND 			= 'Friend';
	
	public static $relations = array(NULL, self::RELATION_SELF, self::RELATION_WIFE, self::RELATION_GIRLFRIEND, self::RELATION_HUSBAND, self::RELATION_BOYFRIEND, self::RELATION_DAUGHTER, self::RELATION_SON, self::RELATION_OTHER_RELATIVE, self::RELATION_FRIEND);

	public function rules()
	{
		return array(
			'email' => array(
				array('email'),
				),
			);
	}
	
	public function setExtra($extra_name, $extra_value)
	{
		$ue = $this->get('extra');

		if ($extra_value !== NULL)
		{
			$ue[$extra_name] = $extra_value;
		}else{
			unset($ue[$extra_name]);
		}
		$this->set('extra', $ue);
	}
	
	public function getExtra($key, $default_value = NULL)
	{
		if ($key != NULL)
		{
			return Arr::get($this->get('extra'), $key, $default_value);
		}
	}

	public function createForUser($params = array())
	{
		if (isset($params['user_id']))
		{
			$this->user_id = $params['user_id'];
		}
		if (isset($params['id']))
		{
			$this->id = $params['id'];
		}
		if (is_object ($params['copy_userdata']) AND get_class($params['copy_userdata']) == 'User')
		{
			$user = $params['copy_userdata'];
			$this->relation			= 'Self';
			$this->email				= $user->email;
			$this->title				= $user->title;
			$this->first_name			= $user->first_name;
			$this->middle_name		= $user->middle_name;
			$this->last_name			= $user->last_name;
			$this->birth_date			= $user->birth_date;
			$this->gender				= $user->gender;
			$this->street_num			= $user->street_num;
			$this->street				= $user->street;
			$this->city					= $user->city;
			$this->state_prov			= $user->state_prov;
			$this->zip					= $user->zip;
			$this->country				= $user->country;
			$this->phone				= $user->phone;
			$this->photo				= $user->photo;
			$this->image_url_thumb	= $user->image_url_thumb;
			$this->extra				= $user->user_extra;
		}
	}
	
	public function updateUserFromSelf()
	{
		if ($this->relation == 'Self')
		{
			$this->user->first_name = $this->first_name;
			$this->user->middle_name = $this->middle_name;
			$this->user->last_name = $this->last_name;
			$this->user->email = $this->email;
			$this->user->birth_date = $this->birth_date;
			$this->user->save();
		}
	}
	
	public function getBirthDate()
	{
		return DateTime::createFromFormat('Y-m-d H:i:s', $this->birth_date.' 00:00:00');
	}
	
	public function getAge($at_day = NULL)
	{
		$at_day = $at_day == NULL?new DateTime('today'):$at_day;
		$at_day = (!is_object($at_day) || get_class($at_day) != 'DateTime') ? DateTime::createFromFormat('Y-m-d', $at_day) : $at_day;
		return $this->getBirthDate() != NULL ? $this->getBirthDate()->diff($at_day)->y : NULL;
	}
	
	public function minRequirements()
	{
		if ($this->first_name != NULL AND strlen(trim($this->first_name))>1)
		{
			if ($this->last_name != NULL AND strlen(trim($this->last_name))>1)
			{
				if ($this->birth_date != NULL)
				{
					if ($this->relation != NULL)
					{
						if ($this->relation != 'Self' OR $this->email != NULL)
						{
							return TRUE;
						}
					}
				}
			}
		}
		return FALSE;
	}
	
	public function getFullName()
	{
		return $this->first_name.' '.($this->middle_name != NULL?$this->middle_name.' ':'').$this->last_name;
	}
	
	public function getFullAddress()
	{
		$fields = array('street', 'street_num', 'city', 'zip', 'state_prov', 'country');
		$full_address = '';
		foreach ($fields as $field)
		{
			$full_address .= $this->get($field) != NULL ? $this->get($field).' ' : '';
		}
		return $full_address;
	}
	
	public function isProfileComplete ($params = array()) {
		$isProfileComplete = TRUE;
		$contact_profile = NULL;
		if(Arr::get($params, 'contact_profile_id', NULL) != NULL){
			$contact_profile = ORM::factory('Skp_ContactProfile')->where('id', '=', Arr::get($params, 'contact_profile_id', NULL))->find();
		}
		if(Arr::get($params, 'contact_profile', NULL) != NULL){
			$contact_profile = Arr::get($params, 'contact_profile', NULL);
		}
		if ($contact_profile != NULL AND is_array($contact_profile->get('infos')))
		{
			$mandatory_fields = array_keys($contact_profile->get('infos'), "M", TRUE);
			$extras = $this->get('extra');
			$columns = $this->list_columns();
			foreach ($mandatory_fields as $mandatory_field){
				$is_column = isset($columns[$mandatory_field]);
				if (($is_column AND $this->get($mandatory_field) == NULL) OR (!$is_column AND Arr::get($extras, $mandatory_field) == NULL)){
					return FALSE;
				}
			}
		}else{
			return FALSE;
		}
		return $isProfileComplete;
	}
	
	// Anonymize person datas
	public function anonymizePersonData($anon_user, $replace_orig_person = FALSE)
	{
		$anon_person = $replace_orig_person ? $this : ORM::factory('Skp_Person');
		
		$person_columns = $this->list_columns();
		$gdpr_fields = ContactProfile::$gdpr_fields;
		foreach (ContactProfile::$list_info as $profile_field)
		{
			$field_value = isset($person_columns[$profile_field]) ? $this->get($profile_field) : $this->getExtra($profile_field);
			
			$anon_value = in_array($profile_field, $gdpr_fields) ? Skp_Helper::anonymizeValue($field_value, $profile_field) : $field_value;
			(isset($person_columns[$profile_field])) AND $anon_person->set($profile_field, $anon_value) OR $anon_person->setExtra($profile_field, $anon_value);
		}
		$anon_person->set('user', $anon_user);
		$anon_person->save();
		return $anon_person;
	}
	
	// Anonymize all Person datas for the given merchant (if merchant NULL it will be anonymized for all merchant replacing original person)
	public function anonymizeForMerchant($merchant = NULL)
	{
		$db = Database::instance();
		// Transaction Start
		$db->begin();
		try
		{
			// Anonymize Person datas
			$anon_person = $this->anonymizePersonData($this->get('user'), ($merchant == NULL));
			$anon_person->set('deleted', 1);
			$anon_person->save();
			
			$person_order_items = $this->get('order_items');
			$merchant != NULL AND $person_order_items->join('order')->on('skp_orderitem.order_id', '=', 'order.id')->where('order.merchant_id', '=', $merchant->pk());
			$person_order_items = $person_order_items->find_all();
			foreach ($person_order_items as $o_item)
			{
				$o_item->anonymizeOrderItemPerson($anon_person);
			}
			
			$db->commit();
		} catch (Exception $e)
		{
			$db->rollback();
			throw $e;
			return FALSE;
		}
	}

	public function createExportRecords($merchant, $params = array())
	{
		$now_utc = gmdate('Y-m-d H:i:s');
		$export_connectors = Arr::get($params, 'active_export_connectors', $merchant->getPersonExportConnectors());
		foreach ($export_connectors as $exp_c)
		{
			$user_export_record = $exp_c->get('export_records')->where('object_class', '=', Arr::get(ExportConnector::$record_types, ExportConnector::RECORD_TYPE_PERSON))->and_where('object_id', '=', $this->pk())->find();
			if (!$user_export_record->loaded())
			{
				$user_export_record->set('merchant_id', $merchant->pk());
				$user_export_record->set('export_connector_id', $exp_c->pk());
				$user_export_record->set('object_class', Arr::get(ExportConnector::$record_types, ExportConnector::RECORD_TYPE_PERSON));
				$user_export_record->set('object_id', $this->pk());
			}
			$user_export_record->set('export_status', ExportConnectorRecord::STATUS_NEW);
			$user_export_record->set('record_datetime', $now_utc);
			$user_export_record->save();
		}
	}
}
