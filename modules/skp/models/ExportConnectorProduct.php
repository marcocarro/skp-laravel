<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;

class ExportConnectorProduct extends Model {

	protected $table = 'export_connector_product';
	public $timestamps = false;

}