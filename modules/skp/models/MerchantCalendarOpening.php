<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class MerchantCalendarOpening extends Model
{

	protected $table = 'merchant_calendar_opening';
   public $timestamps = false;

}