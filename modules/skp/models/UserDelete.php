<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class UserDelete extends Model {

	protected $table = 'user_delete';
	
protected $casts = [
		'extra' => 'array',
	];
	protected $_table_columns = Array(
		'id' => Array(),
		'merchant_id' => Array(),
		'user_id' => Array(),
		'delete_status' => Array(),
		'request_datetime' => Array(),
		'delete_datetime' => Array(),
		'extra' => Array(),
	);
	
	protected $_belongs_to = array(
		'user' => array(
			'model' => 'Skp_User',
			'foreign_key' => 'user_id',
		),
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
	);
	
	const STATUS_REQUESTED 	= 'REQUESTED';
	const STATUS_DELETED 	= 'DELETED';
	const SUGGESTION_OK 		= 'OK';
	const SUGGESTION_KO 		= 'KO';
	const SUGGESTION_CHECK 	= 'CHECK';
	
	public function getDeleteRequestDate()
	{
		return DateTime::createFromFormat('Y-m-d H:i:s', $this->get('request_datetime'));
	}
	
	public function getDeleteDate()
	{
		return DateTime::createFromFormat('Y-m-d H:i:s', $this->get('delete_datetime'));
	}
	
	public function deleteUser()
	{
		$user = $this->get('user');
		$merchant = $this->get('merchant');
		$user->anonymizeForMerchant($merchant);
		return TRUE;
	}
	
	public function getDeleteSuggestion()
	{
		$user = $this->get('user');
		$merchant = $this->get('merchant');
		return $user->getDeletionSuggestion($merchant);
	}
}
