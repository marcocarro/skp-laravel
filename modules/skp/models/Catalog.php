<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Catalog extends Model {

	protected $table = 'catalog';
	public $timestamps = false;

	public function merchant() {
		$this->belongsTo(Merchant::class);
	}

	public function connector() {
		$this->belongsTo(Connector::class);
	}

	public function product_category_externals() {
		return $this->hasMAny(
			ProductCategoryExternal::class,
			'catalog_id'
		);
	}

	public function products() {
		return $this->hasMAny(
			Product::class,
			'catalog_id'
		);
	}

	public function price_categories() {
		return $this->hasMAny(
			PriceCategory::class,
			'availability_id'
		);
	}

	public function skus() {
		return $this->hasMAny(
			Sku::class,
			'catalog_id'
		);
	}

}