<?php

class ProductGroupImage extends Model {

	protected $table = 'productgroup_image';
	protected $_table_columns = Array(
		'id' => Array(),
		'productgroup_id' => Array(),
		'image_id' => Array(),
		'sort' => Array(),
	);
	protected $_serialize_columns = array();
	
	protected $_belongs_to = array(
		'productgroup' => array(
			'model' => 'Skp_ProductGroup',
			'foreign_key' => 'productgroup_id',
		),
		'image' => array(
			'model' => 'Skp_Image',
			'foreign_key' => 'image_id',
		),
	);
}
