<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Product extends ModelNameDescription implements Interfaces_ConnectorExtra, Interfaces_ValidityDate, Interfaces_VisibleProduct
{
	protected $table = 'product';
	protected $_serialize_columns = array('product_extra', 'name', 'short_description', 'description', 'practical_info');
	protected $_table_columns = Array(
		'id' => Array(),
		'merchant_id' => Array(),
		'catalog_id' => Array(),
		'producttype_id' => Array(),
		'productcategory_id' => Array(),
		'vat_id' => Array(),
		'add_prod_only' => Array(),
		'name' => Array(),
		'short_description' => Array(),
		'description' => Array(),
		'practical_info' => Array(),
		'external_id' => Array(),
		'preselected' => Array(),
		'sort' => Array(),
		'validity_unit' => Array(),
		'validity_value' => Array(),
		'validity_min_value' => Array(),
		'validity_max_value' => Array(),
		'buy_interval_before_validity' => Array(),
		'ext_need_contact' => Array(),
		'product_extra' => Array(),
		'venue_id' => Array(),
		'onsale_calendar_id' => Array(),
		'validity_calendar_id' => Array(),		// Validity calendar is the calendar of dates in which it make sense that the product is valid
		'active' => Array(),
		'archived' => Array(),
		'deleted' => Array(),
	);
	
	protected $_belongs_to = array(
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
		'catalog' => array(
			'model' => 'Skp_Catalog',
			'foreign_key' => 'catalog_id',
		),
		'productcategory' => array(
			'model' => 'Skp_ProductCategory',
			'foreign_key' => 'productcategory_id',
		),
		'producttype' => array(
			'model' => 'Skp_ProductType',
			'foreign_key' => 'producttype_id',
		),
		'mainproducts'=> array(
			'model' => 'Skp_Product',
			'through' => 'product_association',
			'foreign_key' => 'sub_product_id',
			'far_key' => 'main_product_id',
		),
		'vat' => array(
			'model' => 'Skp_Vat',
			'foreign_key' => 'vat_id',
		),
		'venue' => array(
			'model' => 'Skp_Venue',
			'foreign_key' => 'venue_id',
		),
		'onsale_calendar' => array(
			'model' => 'Skp_Calendar',
			'foreign_key' => 'onsale_calendar_id'
		),
		'validity_calendar' => array(
			'model' => 'Skp_Calendar',
			'foreign_key' => 'validity_calendar_id'
		)
	);
	
	protected $_has_many = array(
		'export_connectors' => array(
			'model' => 'Skp_ExportConnector',
			'foreign_key' => 'product_id',
			'through' => 'export_connector_product',
			'far_key' => 'export_connector_id',
		),
		'subproducts' => array(
			'model' => 'Skp_Product',
			'through' => 'product_association',
			'foreign_key' => 'main_product_id',
			'far_key' => 'sub_product_id',
		),
		'subskus' => array(
			'model' => 'Skp_Sku',
			'through' => 'product_association',
			'foreign_key' => 'main_product_id',
			'far_key' => 'sub_sku_id',
		),
		'skus' => array(
			'model' => 'Skp_Sku',
			'foreign_key' => 'product_id',
		),
		'pools' => array(
			'model' => 'Skp_Pool',
			'through' => 'pool_product',
			'foreign_key' => 'product_id',
			'far_key' => 'pool_id',
		),
		'product_images' => array(
			'model' => 'Skp_ProductImage',
			'foreign_key' => 'product_id',
		),
		'product_association' => array(
			'model' => 'Skp_ProductAssociation',
			'foreign_key' => 'main_product_id',
		),
		'acp' => array(
			'model' => 'Skp_AvailabilityProductSkuCalendarPricelist',
			'foreign_key' => 'product_id',
		),
		'calendars' => array(
			'through' => 'availability_product_sku_calendar_pricelist',
			'model' => 'Skp_Calendar',
			'foreign_key' => 'product_id',
			'far_key' => 'calendar_id',
		),
		'product_data_profiles' => array(
			'model' => 'Skp_ProductDataProfile',
			'foreign_key' => 'product_id',
		),
		'product_contact_profiles' => array(
			'model' => 'Skp_ProductContactProfile',
			'foreign_key' => 'product_id',
		),
		'product_productgroups' => array(
			'model' => 'Skp_ProductGroupProduct',
			'foreign_key' => 'product_id',
		),
		'productgroups' => array(
			'model' => 'Skp_ProductGroup',
			'through' => 'productgroup_product',
			'foreign_key' => 'product_id',
			'far_key' => 'productgroup_id',
		),
		'product_attributes' => array(
			'model' => 'Skp_ProductAttribute',
			'foreign_key' => 'product_id',
		),
	);

	const VALIDITY_TYPE_YEAR			= 'Year';
	const VALIDITY_TYPE_SEASON			= 'Season';
	const VALIDITY_TYPE_MONTH			= 'Month';
	const VALIDITY_TYPE_WEEK			= 'Week';
	const VALIDITY_TYPE_WEEKEND		= 'Weekend';
	const VALIDITY_TYPE_DAY				= 'Day';
	const VALIDITY_TYPE_MORNING		= 'Morning';
	const VALIDITY_TYPE_AFTERNOON		= 'Afternoon';
	const VALIDITY_TYPE_EVENING		= 'Evening';
	const VALIDITY_TYPE_HOUR			= 'Hour';
	const VALIDITY_TYPE_MINUTE			= 'Minute';
	const VALIDITY_TYPE_ONEWAY			= 'OneWay';
	const VALIDITY_TYPE_ROUNDTRIP		= 'RoundTrip';
	const VALIDITY_TYPE_POINT			= 'Point';
	const VALIDITY_TYPE_VALUEPOINT	= 'ValuePoint';
	const VALIDITY_TYPE_USAGE			= 'Usage';


	const DURATION_UNIT_MINUTE			= 'Minute';
	const DURATION_UNIT_HOUR			= 'Hour';
	const DURATION_UNIT_DAY				= 'Day';
	const DURATION_UNIT_WEEK			= 'Week';
	const DURATION_UNIT_MONTH			= 'Month';
	const DURATION_UNIT_SEASON			= 'Season';
	const DURATION_UNIT_YEAR			= 'Year';
	const DURATION_UNIT_VALUEPOINT	= 'ValuePoint';
	const DURATION_UNIT_POINT			= 'Point';
	const DURATION_UNIT_USAGE			= 'Usage';

	public static $display_on_list = array(self::DESCRIPTION_DISPLAY_PDF, self::DESCRIPTION_DISPLAY_SHOP);

	public static $duration_units = array(self::DURATION_UNIT_MINUTE, self::DURATION_UNIT_HOUR, self::DURATION_UNIT_DAY, self::DURATION_UNIT_WEEK, self::DURATION_UNIT_MONTH, self::DURATION_UNIT_SEASON, self::DURATION_UNIT_YEAR, self::DURATION_UNIT_VALUEPOINT, self::DURATION_UNIT_POINT, self::DURATION_UNIT_USAGE);

	public static $validity_types = array(NULL, self::VALIDITY_TYPE_ONEWAY, self::VALIDITY_TYPE_ROUNDTRIP, self::VALIDITY_TYPE_USAGE, self::VALIDITY_TYPE_POINT, self::VALIDITY_TYPE_VALUEPOINT, self::VALIDITY_TYPE_YEAR, self::VALIDITY_TYPE_MONTH, self::VALIDITY_TYPE_WEEK,self::VALIDITY_TYPE_DAY, self::VALIDITY_TYPE_HOUR, self::VALIDITY_TYPE_MINUTE, self::VALIDITY_TYPE_SEASON, self::VALIDITY_TYPE_MORNING, self::VALIDITY_TYPE_AFTERNOON, self::VALIDITY_TYPE_WEEKEND, self::VALIDITY_TYPE_EVENING);

	public static $validity_types_daily = array(NULL, self::VALIDITY_TYPE_ONEWAY, self::VALIDITY_TYPE_ROUNDTRIP, self::VALIDITY_TYPE_USAGE, self::VALIDITY_TYPE_POINT, self::VALIDITY_TYPE_VALUEPOINT, self::VALIDITY_TYPE_HOUR, self::VALIDITY_TYPE_MINUTE, self::VALIDITY_TYPE_MORNING, self::VALIDITY_TYPE_AFTERNOON, self::VALIDITY_TYPE_EVENING);
	
	public static $validity_time_units = array(self::DURATION_UNIT_MINUTE, self::DURATION_UNIT_HOUR);
	
	public static $barcode_formats = array('I25','C128','QRCODE,H', 'DATAMATRIX');
	
	public function rules()
	{
		return array(
			'validity_value' => array(
				array('numeric'),
			),
			'validity_min_value' => array(
				array('numeric'),
			),
			'validity_max_value' => array(
				array('numeric'),
			),
			'validity_unit' => array(
				array('in_array', array(':value', self::$validity_types)),
			),
			'extra_info'=>array(
				array(function($m, $obj)
				{
					$min_length = Arr::get($m->get('product_extra'), 'extra_info_min_length');
					if ($min_length != NULL AND Arr::get($m->get('product_extra'), 'extra_info_trkey') == NULL)
					{
						$obj->error('product_extra', 'extra info min length cannot be set if extra info trkey is null');
					}
					if ($min_length != NULL AND (is_numeric($min_length) == FALSE OR $min_length != intval($min_length) OR $min_length < 0))
					{
						$obj->error('product_extra', 'extra info min length can be only empty or integer numbers');
					}
				}, array(':model', ':validation')),
			),
		);
	}

	public function save(Validation $validation = NULL)
	{
		parent::save($validation);
	}
	
	public function isFullyConfigured()
	{
		return $this->get('skus')->count_all() >= 1;
	}

	public function getTimeZone()
	{
		return $this->get('merchant')->getTimeZone();
	}

	public function getPools($only_higher_priority = TRUE)
	{
		if ($only_higher_priority)
			$result = array($this->pools->order_by('priority','DESC')->find());
		else
			$result =  $this->pools->order_by('priority','DESC')->find_all();
		return $result;
	}
	
	public function addCreateExternalPool($ext_pool_id, $ext_pool_names)
	{
		$pool = ORM::factory('Skp_Pool')->where('merchant_id', '=', $this->merchant->pk())->and_where('external_id', '=', $ext_pool_id)->find();
		if ($pool->loaded() == FALSE)
		{
			$pool->set('merchant', $this->merchant);
			$pool->set('name', $ext_pool_names);
			$pool->set('external_id', $ext_pool_id);
			$pool->save();
		}
		if ($this->has_any('pools', $pool) == FALSE)
		{
			$this->add('pools', $pool);
		}
	}
	
	public function isBarcoded()
	{
		$product_extra = $this->get('product_extra');
		return (isset($product_extra['barcode']));
	}

	public function getBarcodeFormat()
	{
		$product_extra = $this->get('product_extra');
		return ((isset($product_extra['barcode']) AND isset($product_extra['barcode']['format'])) ? $product_extra['barcode']['format'] : NULL);
	}

	public function withDataFieldName($data_field_name)
	{
		return $this->getProductDataProfile()->get('profile')->get('data_profile_fields')->join('data_field')->on('skp_dataprofilefield.data_field_id', '=', 'data_field.id')->where('data_field.name', '=', $data_field_name)->count_all() > 0;
	}

	public function withDataFieldType($data_field_type)
	{
		return $this->getProductDataProfile()->get('profile')->get('data_profile_fields')->join('data_field')->on('skp_dataprofilefield.data_field_id', '=', 'data_field.id')->where('data_field.field_type', '=', $data_field_type)->count_all() > 0;
	}
	
	public function doNotPrintBarcode()
	{
		$product_extra = $this->get('product_extra');
		return isset($product_extra['do_not_print_barcode']) AND $product_extra['do_not_print_barcode'] == TRUE;
	}

	public function setConnectorExtra($extra)
	{
		$current_extra = $this->get('product_extra');
		$current_extra['connector_object'] = $extra;
		$this->set('product_extra', $current_extra);
	}

	private function getFirstValidityDate()
	{
		$first_validity_date = NULL;
		$first_validity_date_string = Arr::get($this->get('product_extra'), 'first_validity_date');
		if ($first_validity_date_string != NULL)
		{
			$first_validity_date = DateTime::createFromFormat('Y-m-d H:i:s', $first_validity_date_string.' 00:00:00', $this->getTimeZone());
		}
		return $first_validity_date;
	}
	
	private function getFinalValidityDate()
	{
		$final_validity_date = NULL;
		$final_validity_date_string = Arr::get($this->get('product_extra'), 'final_validity_date');
		if ($final_validity_date_string != NULL)
		{
			$final_validity_date = DateTime::createFromFormat('Y-m-d H:i:s', $final_validity_date_string.' 23:59:59', $this->getTimeZone());
		}
		return $final_validity_date;
	}
	
	public function setExtra($extra_name, $extra_value, $save_empty_value = FALSE)
	{
		$ue = $this->get('product_extra');

		if ($extra_value !== NULL)
		{
			$ue[$extra_name] = $extra_value;
		}else{
			unset($ue[$extra_name]);
		}
		$this->set('product_extra', $ue);
	}
	
	public function getExtra($key, $default_value = NULL)
	{
		if ($key != NULL)
		{
			return Arr::get($this->get('product_extra'), $key, $default_value);
		}
	}

	// Returns an array with extra_product datas specified by extra_name
	public function getExtraInfoLangArray()
	{
		$extra_datas = Arr::get($this->get('product_extra'), 'extra_info_trkey');
		if (is_array($extra_datas))
		{
			return $extra_datas;
		}else if (is_string($extra_datas))
		{
			$ed = array();
			foreach ($this->get('merchant')->getLanguages() as $ml => $val)
			{
				I18n::$lang = $ml;
				$ed[$ml] = __($extra_datas, array());
			}
			I18n::$lang = Request::$lang;
			return $ed;
		}
		return array();
	}

	public function getExtraInfo($lang)
	{
		$texts = $this->getExtraInfoLangArray();

		if (Arr::get($texts, $lang) != NULL)
		{
			return Arr::get($texts, $lang);
		}

		// Ne Extra Info text is found for the given language return the first one found
		foreach ($texts as $lang => $value)
		{
			if ($value != NULL)
			{
				return $value;
			}
		}
	}
	
	public function getConnectorExtra()
	{
		$current_extra = $this->get('product_extra');
		return Arr::get($current_extra, 'connector_object', NULL);
   }

	public function getAvailabilities($date_time = NULL)
	{
		return Availability::getProductAvailabilities($this);
	}

	public function onSaleNow()
	{
		if ($this->get('onsale_calendar_id') != NULL)
		{
			$now = new DateTime('now');
			$now->setTimeZone($this->getTimeZone());
			$cal = $this->get('onsale_calendar');
			if ($cal->hasDate($now) == FALSE)
			{
				return FALSE;
			}
		}
		return TRUE;
	}

	public function getMinValidityDate()
	{
		$merchant_min_validity_date = $this->get('merchant')->getMinPurchaseDate();
		$product_min_validity_date = $this->getFirstValidityDate();
		if ($product_min_validity_date == NULL OR $merchant_min_validity_date != NULL AND $product_min_validity_date < $merchant_min_validity_date)
		{
			$product_min_validity_date = $merchant_min_validity_date;
		}
		$bibv = $this->get('buy_interval_before_validity');
		$ignore_time = FALSE;
		if ($bibv != NULL)
		{
			$valid_date_before_interval = new DateTime('now', $this->getTimeZone());
			if (preg_match("/^(\d{1,})(D|H|m){1}$/", $bibv, $match))
			{
				list($original, $value, $type) = $match;
				
				if (($type == 'H' OR $type == 'm') AND $ignore_time) return TRUE;
				
				if ($type == 'D')
				{
					$valid_date_before_interval->add(new DateInterval('P'.$value.'D'));
					// for this difference we need to take in considerations only the difference from the current day (no hours, no minutes or seconds taken in the calculus)
					$valid_date_before_interval->setTime(0, 0, 0);
				}
				if ($type == 'H' OR $type == 'm')
				{
					$valid_date_before_interval->add(new DateInterval('PT'.$value.strtoupper($type)));
					// for this difference we need to take in considerations only the difference from the current time (no seconds taken in the calculus)
					$valid_date_before_interval->setTime($valid_date_before_interval->format('H'), $valid_date_before_interval->format('i'), 0);
				}
				$product_min_validity_date = max($product_min_validity_date, $valid_date_before_interval);
			}
		}
		if ($this->needsValidityStartTime())
		{
			$product_min_validity_date = max($product_min_validity_date, new DateTime('now', $this->getTimeZone()));
		}
		return $product_min_validity_date;
	}

	public function getMaxValidityDate()
	{
		$merchant_max_validity_date = $this->get('merchant')->getMaxPurchaseDate();
		$product_max_validity_date = $this->getFinalValidityDate();
		if ($product_max_validity_date == NULL OR $merchant_max_validity_date != NULL AND $product_max_validity_date > $merchant_max_validity_date)
		{
			$product_max_validity_date = $merchant_max_validity_date;
		}
		return $product_max_validity_date;
	}

	public function canBeBoughtBasedOnBuyInterval($params)
	{
		$validity_start = Arr::get($params, 'validity_date') AND $validity_start != NULL AND $validity_start = clone $validity_start;
		$now = Arr::get($params, 'now');
		if ($validity_start == NULL)
		{
			return TRUE;
		}
		$now = $now == NULL ? new DateTime('now') : $now;
		// We always compare day and time in the merchant local time zone
		$now->setTimeZone($this->getTimeZone());
		
		$validity_start->setTimeZone($this->getTimeZone());
		$today_buy_only_before_time = $this->getExtra('today_buy_only_before_time', $this->get('merchant')->getMerchantExtra('today_buy_only_before_time')); // If valid for today on sale only before the time
		
		$now->setTime($now->format('H'), $now->format('i'), 0);
		if ($this->isOpenDate() == FALSE AND $today_buy_only_before_time != NULL AND $validity_start->format('Y-m-d') == $now->format('Y-m-d'))
		{
			// it is the same date
			if ($now->format('H:i') > $today_buy_only_before_time) return FALSE; 
		}

		$bibv = $this->get('buy_interval_before_validity');
		if ($bibv != NULL)
		{
			if (preg_match("/^(\d{1,})(D|H|m){1}$/", $bibv, $match))
			{
				list($original, $value, $type) = $match;
				
				if (($type == 'H' OR $type == 'm') AND Arr::get($params, 'ignore_time', FALSE)) return TRUE;
				
				if ($type == 'D')
				{
					// for this difference we need to take in considerations only the difference from the current day (no hours, no minutes or seconds taken in the calculus)
					$now->setTime(0, 0, 0);
				}
				if ($type == 'H' OR $type == 'm')
				{
					// for this difference we need to take in considerations only the difference from the current time (no seconds taken in the calculus)
					$now->setTime($now->format('H'), $now->format('i'), 0);
				}
				$diff = $validity_start->getTimestamp()-$now->getTimeStamp();
				$result = array();
				$result['D'] = floor($diff/86400);
				$result['H'] = floor($diff/3600);
				$result['m'] = floor($diff/60);
				$ok = ($result[$type] >= $value);
			}
			if ($ok == FALSE)
			{
				return FALSE;
			}
		}
		return TRUE;
	}
	
	/*
	 * This method check is there was a buy interval and in case if the validity start is beyond that
	 */
	 
	public function canBeBought($params)
	{
		$result = FALSE;
		// Here we check the onsale calendar (when the product can be sold, not to be confused with the validity date)
		
		if ($this->onSaleNow() != TRUE)
		{
			return FALSE;
		}

		$validity_start = Arr::get($params, 'validity_date');

		if ($validity_start != NULL)
		{
			$validity_start = clone $validity_start;
			$validity_start->setTimeZone($this->getTimeZone());
			$merchant_max_purchase_date = $this->get('merchant')->getMaxPurchaseDate();
			$merchant_min_purchase_date = $this->get('merchant')->getMinPurchaseDate();
			if ($this->get('merchant')->isOpen($validity_start->format('Y-m-d')) == FALSE)
			{
				return FALSE;
			}

/* Remove by Issue #812 it has to be removed otherwise the calendar will always limit if the product is on sale but sometimes the calendar is used to only define if there is a special price (instead then the price take from External Connector) for a specific date
 * Suggestion: Use a Validity Calendar instead
		// Here we check if the product is connected to any calendar
		if ($this->get('calendars')->count_all() > 0)
		{
			$ok = FALSE;
			// We loop for all the calendars found
			foreach ($this->get('calendars')->find_all() as $cal)
			{
				// here we check if the calendar contains the validity date
				if ($cal->hasDate($validity_start))
				{
					$ok = TRUE;
					break;
				}
			}
			if ($ok == FALSE)
			{
				return FALSE;
			}
		}
*/

			if ($this->getMaxValidityDate() != NULL AND $validity_start > $this->getMaxValidityDate())
			{
				return FALSE;
			}
			if ($this->getMinValidityDate() != NULL AND $validity_start < $this->getMinValidityDate())
			{	
				return FALSE;
			}
			if ($this->get('validity_calendar_id') != NULL)
			{
				$cal = $this->get('validity_calendar');
				if ($cal->hasDate($validity_start) == FALSE)
				{
					return FALSE;
				}
			}
		}
		$result = $this->canBeBoughtBasedOnBuyInterval($params);
		if ($result == FALSE) return FALSE;

		$params['validity_date_end'] = $this->getEndValidityDate($validity_start);

		// here we ask the Connector (for example in case of ExternalContingentManagment)
		try{
			$result = $this->call('canBeBought', $params);
		}catch(Skp_Exception_Missing $e)
		{
			// nothing to do beacuse method is not implemented by the Connector
		}
		return $result;
	}
	
	public function getProductImages()
	{
		return $this->get('product_images')->order_by('sort')->find_all();
	}
	
	public function getImage($relative = TRUE, $version = 'original')
	{
		$first_product_image = $this->get('product_images')->order_by('sort')->find();
		return $first_product_image->get('image')->getImageUrl($relative, $version);
	}
	
	public function getImageFile($return_filename_if_not_exists = FALSE, $version = 'original')
	{
		$first_product_image = $this->get('product_images')->order_by('sort')->find();
		return $first_product_image->get('image')->getImageFile($return_filename_if_not_exists, $version);
	}
	
	public function getAvailable($params = array())
	{
		$params['product'] = $this;
		$validity_start = Arr::get($params, 'from_datetime');
		$params_clean = array();
		$validity_start != NULL AND $params_clean['day'] = $validity_start->format('Y-m-d');
		$params_clean['merchant'] = $this->get('merchant');
		Booking::cleanBookings($params_clean);
		return Availability::getAvailable($params);
	}
	
	public function getAvailableSkusOfAvailability($params = array())
	{
		$params['product'] = $this;
		$availability_id = Arr::get($params, 'availability_id');
		$skugroup_id = Arr::get($params, 'skugroup_id');

		$skus = $this->get('skus')
			->join('price_list_price_category')
				->on('price_list_price_category.price_category_id', '=', 'skp_sku.pricecategory_id')
			->join('availability_product_sku_calendar_pricelist')
				->on('availability_product_sku_calendar_pricelist.price_list_id', '=', 'price_list_price_category.price_list_id')
				->on('availability_product_sku_calendar_pricelist.availability_id', '=', DB::expr($availability_id))
				->on('availability_product_sku_calendar_pricelist.product_id', '=', 'skp_sku.product_id');
		$skugroup_id != NULL AND $skus->join('skugroup_sku')->on('skugroup_sku.sku_id', '=', 'skp_sku.id')->and_where('skugroup_sku.skugroup_id', '=', $skugroup_id);
		$skus->and_where('skp_sku.deleted', '=', '0');
		$skus = $skus->distinct(TRUE)->find_all();
		return $skus;
	}
	
	public function getUrl ($skugroup_id = 0)
	{
		return '#' . URL::site(Route::get('shop.welcome', Request::$lang)->uri()) .
			'?skugroup_id=' . $skugroup_id .
			'&product_id=' . $this->pk() .
			($this->needsDateTimeBooking()?'&bookable_y_n_a=y':'');
	}

	public static function getProducts($params = array())
	{
		$merchant = Arr::get($params, 'merchant', ORM::factory('Skp_Merchant'));
		$merchant_id = $merchant->pk();
		$product_id = Arr::get($params, 'product_id');
		$group_id = Arr::get($params,'group_id');
		$producttype_id = Arr::get($params,'producttype_id');
		$only_active = Arr::get($params,'only_active', TRUE);
		$pricecategory_id = Arr::get($params,'pricecategory_id');
		$pricecategory_ids = Arr::get($params,'pricecategory_ids');
		$pool_id = Arr::get($params,'pool_id');

		if (array_key_exists('group_id', $params) == TRUE AND $group_id == NULL)
		{
			return ORM::factory('Skp_Product')->where('id','=',-1);
		}

		$r = ORM::factory('Skp_Product');
		$r->and_where('skp_product.deleted', '=', '0');
		if ($merchant_id !== NULL)
		{
			$r = $r->where('skp_product.merchant_id','=',$merchant_id);
		}else{
			$r = $r->where(DB::expr('1'), '=', DB::expr('1'));
		}
		
		if ($group_id !== NULL OR $only_active OR $product_id !== NULL OR $pricecategory_id !== NULL OR $pricecategory_ids !== NULL OR $pool_id != NULL)
		{
			$r = $r->distinct(TRUE)
				->join('sku')
					->on('sku.product_id', '=', 'skp_product.id')
				->join('skugroup_sku')
					->on('skugroup_sku.sku_id','=','sku.id')
				->join('skugroup')
					->on('skugroup_sku.skugroup_id','=','skugroup.id')
				->join('price_category')
					->on('price_category.id','=','sku.pricecategory_id')
				->join('product_category', 'LEFT')
					->on('product_category.id', '=', 'skp_product.productcategory_id')
				->join('pool_product', 'LEFT')
					->on('pool_product.product_id','=','skp_product.id')
				->join('product_type', 'LEFT')
					->on('product_type.id','=','skp_product.producttype_id');
			$r->and_where_open()->where('product_category.active', '=', 1)->or_where('product_category.active', '=', NULL)->and_where_close();
			$r->and_where('skp_product.active', '=', 1);
			$r->and_where('sku.deleted', '=', 0);
			if ($product_id !== NULL)
			{
				$r->and_where('skp_product.id', '=', $product_id);
			}
			$r->and_where('price_category.active', '=', 1);
			if ($group_id !== NULL)
			{
				$r = $r->and_where('skugroup.id','=',$group_id);
			}
			if ($producttype_id !== NULL)
			{
				$r = $r->and_where('skp_product.producttype_id', '=', $producttype_id);
			}
			if ($pricecategory_id !== NULL)
			{
				$r = $r->and_where('price_category.id','=',$pricecategory_id);
			}
			if ($pricecategory_ids !== NULL AND sizeof($pricecategory_ids) > 0)
			{
				$pc_id_list = '(';
				foreach ($pricecategory_ids as $pc_id)
				{
					$pc_id_list .= ($pc_id_list != '('?',':'').$pc_id;
				}
				$pc_id_list .= ')';
				$r = $r->and_where('price_category.id','in',$pricecategory_ids);
			}
			if ($pool_id !== NULL)
			{
				$r = $r->and_where('pool_product.pool_id', '=', $pool_id);
			}
			if ($product_id == NULL OR TRUE) $r->order_by('skp_product.sort');
		}
		return $r;
	}

	public function needsBooking()
	{
		return sizeof($this->getAvailabilities()) > 0;
	}

	public function needsDateTimeBooking()
	{
		$avps = $this->getAvailabilities();
		foreach ($avps as $avs)
		{
			if ($avs->get('availability')->get('calendars')->count_all() > 0)
			{
				return TRUE;
			}
		}
		return FALSE;
	}
	
	public function hasDateTimeAvailabilityAfterDateTime($datetime_in_merchant_tz)
	{
		$avps = $this->getAvailabilities();
		if (count($avps) == 0 )
		{
			// Products without Availability are always available
			return TRUE;
		}
		foreach ($avps as $avs)
		{
			$cal = $avs->get('calendar');
			if ($cal->loaded() == FALSE OR ($cal->loaded() == TRUE AND $cal->getDateTime(array('from_datetime' => $datetime_in_merchant_tz))->count() > 0))
			{
				// Products with Availability without Calendar are always available
				// Products with Availability with a Calendar wich contains at least 1 datetime from now (in merchant TZ) are available
				return TRUE;
			}
		}
		return FALSE;
	}
	
	public function getOriginalName($separator = ',')
	{
		$original_name = "";
		foreach (Arr::get($this->get('product_extra'), 'original_name', array()) as $l => $n)
		{
			$original_name .= $l.': '.$n.$separator;
		}
		return $original_name;
	}

	// Check if this products has an extra info text configuration option
	public function needsExtraInfo()
	{
		return (Arr::get($this->get('product_extra'), 'extra_info_trkey') != NULL OR $this->getProductDataProfile()->loaded());
	}

	// Check if this products has an terms and conditions option
	public function needsTermsConditions()
	{
		return (Arr::get($this->get('product_extra'), 'enable_terms_and_conditions') != NULL);
	}

	public function getCardTypes()
	{
		$card_types = Arr::get($this->get('product_extra'), 'card_types');
		if ($card_types === NULL)
		{
			// No Specific Card Types defined for this product, take those from the merchant to be the default one. (for legacy reasons)
			$card_types = $this->get('merchant')->getCardTypes();
		}else{
			$card_types = array_intersect($card_types, $this->get('merchant')->getCardTypes());
			
		}
		return $card_types;
	}
	
	public function getVirtualCardTypeIds()
	{
		if ($this->merchant->hasAddOn('Virtual Card')) {
			$virtual_card_types = $this->getExtra('virtual_card_type', array());
		}else
		{
			$virtual_card_types = array();
		}
		return $virtual_card_types;
	}

	public function getPurchaseQtyLimit($limit_type)
	{
		if(!in_array($limit_type, array('min_qty', 'max_qty', 'mod_qty')))
		{
         throw new Exception('Purchase Quantity Limit type: '.$limit_type.' does not exists');
      }
		return Arr::get($this->get('product_extra'), $limit_type, NULL);
	}
	
	public function getValidOnInfo()
	{
		$info = ($this->validity_value != NULL ? Skp_Utils::roundTSD($this->validity_value) : $this->validity_min_value . ' - ' . $this->validity_max_value) . '  ' . __($this->validity_unit);
		return $info;
	}

	public function getStartValidityDate()
	{
		$start_date = $this->getExtra('first_validity_date');
		$start_date == NULL OR ($start_date = DateTime::createFromFormat('Y-m-d', $start_date));
		return $start_date;
	}

	public function getEndValidityDate($start_date = NULL)
	{
		$final_date = $this->getMaxValidityDate(); //DateTime::createFromFormat('Y-m-d', $this->getExtra('final_validity_date'));
		if ($this->isOpenDate())
		{
			if ($final_date != NULL)
			{
				return $final_date;
			}
			else if ($this->get('validity_calendar')->loaded() AND ($max_date_time = $this->get('validity_calendar')->getMaxDateTime()) AND $max_date_time != NULL)
			{
				$start_date = DateTime::createFromFormat('Y-m-d', $max_date_time->day);
			}
			else
			{
				return NULL;
			}
		}
		$end_date = $start_date != NULL ? clone $start_date : NULL;
		$validity_unit = $this->get('validity_unit');
		$validity_value = $this->get('validity_value');
		if ($validity_unit != NULL AND $validity_value !== NULL AND $end_date != NULL)
		{
			$map = array(
				'Year' => 'Y',
				'Month' => 'M',
				'Week' => 'W',
				'Day' => 'D',
				'Hour' => 'H',
				'Minute' => 'M'
			);
			$interval_spec = (array_key_exists($validity_unit, $map) AND $validity_value !== NULL) ? 'P'.(in_array($validity_unit, array('Hour', 'Minute')) ? 'T' : '').Skp_Utils::roundTSD($validity_value).$map[$validity_unit] : NULL;
			if ($interval_spec != NULL)
			{
				$end_date->add(new DateInterval($interval_spec));
				if (in_array($validity_unit, array('Day', 'Week', 'Month', 'Year')))
				{
					$end_date->sub(new DateInterval('P1D'))->setTime(23,59,59);
				}
			}
			!in_array($validity_unit, array_merge(self::$validity_types_daily, array('Day'))) AND $end_date == $start_date AND $end_date = NULL;
		}
		$final_date != NULL AND ($final_date < $end_date OR $end_date == NULL) AND $end_date = $final_date;
		return $end_date;
	}

	public function cleanPriceCache()
	{
		foreach ($this->get('skus')->find_all() as $sku)
		{
			$sku->cleanPriceCache();
		}
	}

	public function getPrintPriceFormat($is_b2b = FALSE)
	{
		$print_price_key = $is_b2b? 'print_price_b2b': 'print_price';
		$print_price = Arr::get($this->get('product_extra'), $print_price_key);
		$print_price = (($print_price === NULL OR $print_price == Merchant::PRICE_PARENT_SET) ? $this->get('merchant')->getPrintPriceFormat($is_b2b) : $print_price);
		return $print_price;
	}
	
	public function getVirtualCardTypes()
	{
		$virtual_card_types = array();
		if ($this->merchant->hasAddOn('Virtual Card')) {
			$virtual_card_type_ids = $this->getExtra('virtual_card_type', array());
			foreach ($virtual_card_type_ids as $virtual_card_type_id)
			{
				$virtual_card_types[] = ORM::factory('Skp_VirtualCardType', $virtual_card_type_id);
			}
		}
		return $virtual_card_types;
	}
	
	public function getCalendarMinMax()
	{
		$now = new DateTime();
		/*
		 * This query search in product > config calendar if setted
		 */
		$query_union_config = DB::select_array(array(
				array(DB::expr("`skp_calendar_date_time`.`day`"), 'date_min'),
				array(DB::expr("`skp_calendar_date_time`.`day`"), 'date_max'),
				array(DB::expr("`skp_calendar_date_time`.`time_start`"), 'time_start'),
				array(DB::expr("`skp_calendar_date_time`.`time_end`"), 'time_end')
			))
			->from('product')
			->join('availability_product_sku_calendar_pricelist')->on('availability_product_sku_calendar_pricelist.product_id', '=', 'product.id')
			->join('calendar_date_time')->on('calendar_date_time.calendar_id', '=', 'availability_product_sku_calendar_pricelist.calendar_id')
			->where('product.id', '=', $this->pk())
			->and_where('calendar_date_time.deleted' ,'=', 'CN')
			->and_where_open()
				->or_where_open()
					->and_where('calendar_date_time.day', '=', $now->format('Y-m-d'))
					->or_where_open()
						->and_where('calendar_date_time.time_start', '=', NULL)
						->and_where('calendar_date_time.time_start', '>', $now->format('H:i:s'))
					->or_where_close()
				->or_where_close()
				->or_where('calendar_date_time.day', '>', $now->format('Y-m-d'))
			->and_where_close();
		/*
		 * This query search in product > ticket_info validity calendar if setted
		 */
		$query_union_validity = DB::select_array(array(
				array(DB::expr("`skp_calendar_date_time`.`day`"), 'date_min'),
				array(DB::expr("`skp_calendar_date_time`.`day`"), 'date_max'),
				array(DB::expr("`skp_calendar_date_time`.`time_start`"), 'time_start'),
				array(DB::expr("`skp_calendar_date_time`.`time_end`"), 'time_end')
			))
			->from('product')
			->join('calendar_date_time')->on('calendar_date_time.calendar_id', '=', 'product.validity_calendar_id')
			->where('product.id', '=', $this->pk())
			->and_where('calendar_date_time.deleted' ,'=', 'CN')
			->and_where_open()
				->or_where_open()
					->and_where('calendar_date_time.day', '=', $now->format('Y-m-d'))
					->or_where_open()
						->and_where('calendar_date_time.time_start', '=', NULL)
						->and_where('calendar_date_time.time_start', '>', $now->format('H:i:s'))
					->or_where_close()
				->or_where_close()
			->or_where('calendar_date_time.day', '>', $now->format('Y-m-d'))
			->and_where_close();
		$query_union_config->union($query_union_validity, TRUE);
		
		//~ $query_all_time = DB::select_array(array(
				//~ array('time_start','time_start'),
				//~ array('time_end','time_end')
			//~ ))
			//~ ->from(array($query_union_config, 'date_union'))
			//~ ->where('time_start', 'IS NOT', NULL)
			//~ ->group_by('time_start');
		
		$query = DB::select_array(array(
				array(DB::expr("max(`skp_date_union`.`date_min`)"), 'date_max'),
				array(DB::expr("min(`skp_date_union`.`date_max`)"), 'date_min'),
				array(DB::expr("max(`skp_date_union`.`time_start`)"), 'time_start_max'),
				array(DB::expr("min(`skp_date_union`.`time_start`)"), 'time_start_min')
			))
			->from(array($query_union_config, 'date_union'));
		$date_range = new stdClass();
		foreach ($query->execute() as $row)
		{
			$date_range->date_min = ($row['date_min'] != NULL ? new DateTime($row['date_min']) : NULL);
			$date_range->date_max = ($row['date_max'] != NULL ? new DateTime($row['date_max']) : NULL);
			$date_range->time_start_max = ($row['time_start_max'] != NULL ? new DateTime('0000/00/00 '.$row['time_start_max']) : NULL);
			$date_range->time_start_min = ($row['time_start_min'] != NULL ? new DateTime('0000/00/00 '.$row['time_start_min']) : NULL);
		}
		return $date_range;
	}

	public function getValidityCalendarMinMax()
	{
		$now = new DateTime();
		/*
		 * This query search in product > ticket_info validity calendar if setted
		 */
		$query_validity = DB::select_array(array(
				array(DB::expr("`skp_calendar_date_time`.`day`"), 'date_min'),
				array(DB::expr("`skp_calendar_date_time`.`day`"), 'date_max'),
				array(DB::expr("`skp_calendar_date_time`.`time_start`"), 'time_start'),
				array(DB::expr("`skp_calendar_date_time`.`time_end`"), 'time_end')
			))
			->from('product')
			->join('calendar_date_time')->on('calendar_date_time.calendar_id', '=', 'product.validity_calendar_id')
			->where('product.id', '=', $this->pk())
			->and_where('calendar_date_time.deleted' ,'=', 'CN')
			->and_where_open()
				->or_where_open()
					->and_where('calendar_date_time.day', '=', $now->format('Y-m-d'))
					->or_where_open()
						->and_where('calendar_date_time.time_start', '=', NULL)
						->and_where('calendar_date_time.time_start', '>', $now->format('H:i:s'))
					->or_where_close()
				->or_where_close()
			->or_where('calendar_date_time.day', '>', $now->format('Y-m-d'))
			->and_where_close();

		$query = DB::select_array(array(
				array(DB::expr("max(`skp_date_validity`.`date_min`)"), 'date_max'),
				array(DB::expr("min(`skp_date_validity`.`date_max`)"), 'date_min'),
				array(DB::expr("max(`skp_date_validity`.`time_start`)"), 'time_start_max'),
				array(DB::expr("min(`skp_date_validity`.`time_start`)"), 'time_start_min')
			))
			->from(array($query_validity, 'date_validity'));
		$date_range = new stdClass();
		foreach ($query->execute() as $row)
		{
			$date_range->date_min = ($row['date_min'] != NULL ? new DateTime($row['date_min']) : NULL);
			$date_range->date_max = ($row['date_max'] != NULL ? new DateTime($row['date_max']) : NULL);
			$date_range->time_start_max = ($row['time_start_max'] != NULL ? new DateTime('0000/00/00 '.$row['time_start_max']) : NULL);
			$date_range->time_start_min = ($row['time_start_min'] != NULL ? new DateTime('0000/00/00 '.$row['time_start_min']) : NULL);
		}
		return $date_range;
	}
	
	public function getPriceMinMax()
	{
		$price_details = '';
		$method = 'getPriceMinMax';
		$params = array();
		$params['product'] = $this;
		$priceMinMax = $this->get('catalog')->get('connector')->callConnectorMethod($method, $params);
		$price_min = Arr::get($priceMinMax, 'min');
		$price_max = Arr::get($priceMinMax, 'max');
		$currency = Arr::get($priceMinMax, 'currency');
		if ($price_min != NULL)
		{
			$price_min = new Skp_Money($price_min, $currency);
			$price_details = $price_min . ' ';
		}
		if ($price_max != NULL)
		{
			$price_max = new Skp_Money($price_max, $currency);
			if ($price_min != NULL)
			{
				$price_details .= ' - ';
			}
			$price_details .= $price_max . ' ';
		}
		return $price_details;
	}

	public function getDateFormat()
	{
		return $this->get('merchant')->get('date_format');
	}
	
	public function isOpenDate()
	{
		return $this->getExtra('is_open_date') == 'Y';
	}
	
	public function getValidityUnit()
	{
		return $this->get('validity_unit');
	}
	
	public function getValidityValue()
	{
		return $this->get('validity_value');
	}
	
	public function getCustomValidityText($lang = NULL)
	{
		$result = NULL;
		if ($this->getExtra('use_custom_validity_text') == 'Y')
		{
			if (!is_array($this->getExtra('custom_validity_text')))
			{
				return $result;
			}
			$lang_array = $this->getExtra('custom_validity_text');
			if ($lang == NULL OR !isset($lang_array[$lang]))
			{
				$lang = key($lang_array); // Return first language found
			}
			if (isset($lang_array[$lang]))
			{
				$result = $lang_array[$lang];
			}
		}
		return $result;
	}

	public function getMonthList ($params)
	{
		$month_list = array();
		$from_datetime = Arr::get($params, 'from_datetime', new DateTime('now', $this->get('merchant')->getTimeZone()));
		if ($this->needsDateTimeBooking())
		{
			$query = $this->get('acp')
				->select(array(DB::expr("MONTH(`skp_calendar_date_time`.`day`)"), 'month'))
				->join('calendar_date_time', 'LEFT')
					->on('skp_availabilityproductskucalendarpricelist.calendar_id', '=', 'calendar_date_time.calendar_id')
				->where('calendar_date_time.day', '>=', $from_datetime->format('Y-m-d'))
				->group_by('month');
			foreach ($query->find_all() as $row)
			{
				$month_list[] = (intval($row->month)<10 ? '0' : '').intval($row->month);
			}
		} else
		{
			$currentMonth = intval($from_datetime->format('m'));
			for ($i = $currentMonth; $i <= 12; $i++)
			{
				$month_list[] = (intval($i)<10 ? '0' : '').intval($i);
			}
		}
		return $month_list;
	}
	
	public function copyProduct()
	{
		$copied_product = ORM::factory('Skp_Product');
		
		$copied_product->set('merchant_id', $this->get('merchant_id'));
		$copied_names = $this->get('name');
		foreach ($copied_names as $lang => $name)
		{
			$copied_names[$lang] = $name.' (copy)';
		}
		$copied_product->set('name',  $copied_names);
		$copied_product->set('active',$this->get('active'));
		$copied_product->set('catalog_id', $this->get('catalog_id'));
		$copied_product->set('external_id', $this->get('external_id'));
		$copied_product->set('producttype_id', $this->get('producttype_id'));
		$copied_product->set('productcategory_id', $this->get('productcategory_id'));

		$copied_product->set('add_prod_only', $this->get('add_prod_only'));
		$copied_product->set('vat_id', $this->get('vat_id'));
		$copied_product->set('sort', $this->get('sort'));
		$copied_product->save();
		$tags = array();
		foreach ($this->getTags() as $tag)
		{
			$tag->loaded() AND $tags[] = $tag->pk();
		}
		$copied_product->setTags($tags);
		$copied_product->set('ext_need_contact', $this->get('ext_need_contact'));
		$copied_product->set('short_description',  $this->get('short_description'));
		$copied_product->set('description',  $this->get('description'));
		$copied_product->set('practical_info',  $this->get('practical_info'));
		$copied_product->set('productcategory_id', $this->get('productcategory_id'));
		foreach ($this->getProductImages() as $p_image)
		{
			$new_p_image = ORM::factory('Skp_ProductImage');
			$new_p_image->set('product',$copied_product);
			$new_p_image->set('image_id',$p_image->get('image_id'));
			$new_p_image->set('sort', $p_image->get('sort'));
			$new_p_image->save();
		}
		$copied_product->set('venue_id', $this->get('venue_id'));

		foreach ($this->get('acp')->find_all() as $acp)
		{
			$new_acp = ORM::factory('Skp_AvailabilityProductSkuCalendarPricelist');
			$new_acp->set('product_id', $copied_product->pk());
			$new_acp->set('calendar_id', $acp->get('calendar_id'));
			$new_acp->set('availability_id', $acp->get('availability_id'));
			$new_acp->set('price_list_id', $acp->get('price_list_id'));
			$new_acp->save();
		}
		
		$copied_product->set('onsale_calendar_id', $this->get('onsale_calendar_id'));
		$copied_product->set('buy_interval_before_validity', $this->get('buy_interval_before_validity'));
		
		$copied_product->set('validity_unit', $this->get('validity_unit'));
		$copied_product->set('validity_value', $this->get('validity_value'));
		$copied_product->set('validity_min_value', $this->get('validity_min_value'));
		$copied_product->set('validity_max_value', $this->get('validity_max_value'));
		$copied_product->set('validity_calendar_id', $this->get('validity_calendar_id'));
		
		foreach ($this->get('pools')->find_all() as $pool)
		{
			$copied_product->add('pools', $pool);
		}
		
		$copied_product->set('product_extra', $this->get('product_extra'));
		$copied_product->set('preselected', $this->get('preselected'));
		
		$product_data_profile = $this->getProductDataProfile();
		if ($product_data_profile->loaded())
		{
			$original_pdp = $product_data_profile->as_array();
			array_shift($original_pdp);
			$new_pdp = ORM::factory('Skp_ProductDataProfile')->values($original_pdp);
			$new_pdp->set('product_id', $copied_product->pk());
			$new_pdp->save();
		}
		
		foreach ($this->get('product_attributes')->find_all() as $attribute)
		{
			$original_attribute = $attribute->as_array();
			array_shift($original_attribute);
			$new_attribute = ORM::factory('SKP_ProductAttribute')->values($original_attribute);
			$new_attribute->set('product_id', $copied_product->pk());
			$new_attribute->save();
		}
		$copied_product->save();
		return $copied_product;
	}
	
	public function getProductDataProfile()
	{
		return $this->get('product_data_profiles')->where('profile_type', '=', ProductDataProfile::PROFILE_TYPE_PRODUCT)->find();
	}
	
	public function printInReceiptPDF ($fieldname)
	{
		switch ($fieldname)
		{
			case 'product_info.shortDescr':
				$diplay_on = $this->getExtra('short_description_display_on', self::$display_on_list);
				break;
			case 'product_info.description':
				$diplay_on = $this->getExtra('description_display_on', array(self::DESCRIPTION_DISPLAY_SHOP));
				break;
			case 'product_info.practicalInfo':
				$diplay_on = $this->getExtra('practical_info_display_on', self::$display_on_list);
				break;
			default:
				$display_on = NULL;
		}
		if ($diplay_on !== NULL)
		{
			return in_array(self::DESCRIPTION_DISPLAY_PDF, $diplay_on);
		}
		return $this->get('merchant')->printInReceiptPDF($fieldname);
	}
	
	public function printInShop ($fieldname)
	{
		switch ($fieldname)
		{
			case 'product_info.shortDescr':
				$diplay_on = $this->getExtra('short_description_display_on', self::$display_on_list);
				break;
			case 'product_info.description':
				$diplay_on = $this->getExtra('description_display_on', array(self::DESCRIPTION_DISPLAY_SHOP));
				break;
			case 'product_info.practicalInfo':
				$diplay_on = $this->getExtra('practical_info_display_on', self::$display_on_list);
				break;
			default:
				$display_on = NULL;
		}
		if ($diplay_on !== NULL)
		{
			return in_array(self::DESCRIPTION_DISPLAY_SHOP, $diplay_on);
		}
		return TRUE;
	}

	public function setConnectorProductExtra($param, $value)
	{
		$connector = $this->get('catalog')->get('connector');
		if ($connector->loaded() == FALSE)
		{
			throw new Skp_Exception('Cannot set Product\'s ConnectorExtra if Connector is not assigned to product');
		}
		$connector_params_key = 'connector_'.$connector->pk();
		$connectors_params = $this->getExtra('connectors_params', array());
		$connector_params = Arr::get($connectors_params, $connector_params_key, array());
		$connector_params[$param] = $value;
		$connectors_params[$connector_params_key] = $connector_params;
		$this->setExtra('connectors_params', $connectors_params);
	}

	public function getConnectorProductExtra($param, $default_value = NULL)
	{
		$connector = $this->get('catalog')->get('connector');
		if ($connector->loaded() == FALSE)
		{
			throw new Skp_Exception('Cannot get Product\'s Connector Product Extra if Connector is not assigned to product');
		}
		$connector_params_key = 'connector_'.$connector->pk();
		$connectors_params = $this->getExtra('connectors_params', array());
		return Arr::get(Arr::get($connectors_params, $connector_params_key, array()), $param, $default_value);
	}

	public function getConnectorProperties()
	{
		$connector = $this->get('catalog')->get('connector');
		if ($connector->loaded() == FALSE)
		{
			throw new Skp_Exception('Cannot get Product\'s Connector Properties  if Connector is not assigned to product');
		}
		$connector_class_name = $connector->get('connector_class').'_Connector';
		return $connector_class_name::getProductConnectorProperties($this);
	}

	public function getConnectorProperty($property)
	{
		$result = $this->getConnectorProperties();
		if (isset($result[$property]))
		{
			return $result[$property];
		}
		return NULL;
	}

	public function getExportConnectorIds()
	{
		$return_array = array();
		foreach ($this->get('export_connectors')->find_all() as $ec)
		{
			$return_array[] = $ec->pk();
		}
		return $return_array;
	}

	public function getCalendarLabel($lang = NULL)
	{
		if (in_array($this->get('validity_unit'), Product::$validity_types_daily) OR ($this->get('validity_unit') == 'Day' AND $this->get('validity_value') <= 1)) {
			return __('global.valid');
		} else {
			return __('fe.accordion.valid_from');
		}
	}

	public function getPriceCategoryType($lang)
	{
		return $this->getExtra('price_category_description', 'product.price_category_type');
	}
	
	public static function getPriceCategoryDescriptionList()
	{
		return $price_category_description_list = array(
			'product.price_category_type'=>__('product.price_category_type').' (default)',
			'description'=>__('description'),
		);
	}

	public function getFEValidityCalendarDates()
	{
		$result = array();
		$min_validity_date = $this->getMinValidityDate();
		$max_validity_date = $this->getMaxValidityDate();
		$now = new DateTime('now', $this->getTimeZone());
		$params = array(
			'from_date' => $now,
			'only_daily_range' => TRUE,
			'include_partial_timeslots' => !$this->needsDateTimeBooking(),
		);
		if ($this->get('validity_calendar_id') != NULL)
		{
			foreach($this->get('validity_calendar')->getDatesAndTimes($params) as $date)
			{
				$null_if_empty = TRUE;
				$date_obj = DateTime::createFromFormat('Y-m-d', $date['day'], $this->getTimeZone());
				$start_date_obj = NULL;
				if($date['time_start'] != NULL)
				{
					list ($start_H, $start_i, $start_s) = explode(':', $date['time_start']);
					$start_date_obj = (clone $date_obj)->setTime($start_H, $start_i, $start_s);
				}
				$end_date_obj = NULL;
				if($date['time_end'] != NULL)
				{
					list ($end_H, $end_i, $end_s) = explode(':', $date['time_end']);
					$end_date_obj = (clone $date_obj)->setTime($end_H, $end_i, $end_s);
				}
				
				$valid_date = FALSE;
				if ($this->needsBooking())
				{
					// If the product needs booking it could have time slots (with time_start - eventually also time_end) or not (no start and end time)
					// If the time slot has no start time, it is a full day availability
					$start_date_obj == NULL AND $start_date_obj = (clone $date_obj)->setTime(0,0,0) AND $end_date_obj = (clone $date_obj)->setTime(23,59,59);
					$end_date_obj == NULL AND $end_date_obj = clone $start_date_obj;
					$valid_date = (($min_validity_date == NULL OR $min_validity_date <= $start_date_obj) AND ($max_validity_date == NULL OR $max_validity_date >= $end_date_obj));
				}
				else if ($this->needsValidityStartTime())
				{
					// If the product needs validity start time the product is valid within time_start and time_end
					$start_date_obj == NULL AND $start_date_obj = (clone $date_obj)->setTime(0,0,0) AND $end_date_obj = (clone $date_obj)->setTime(23,59,59);
					$valid_date = (($end_date_obj == NULL OR $min_validity_date == NULL OR $min_validity_date <= $end_date_obj) AND ($max_validity_date == NULL OR $max_validity_date >= $start_date_obj));
				}
				else
				{
					$valid_date = (($max_validity_date == NULL OR $max_validity_date >= $date_obj) AND ($min_validity_date == NULL OR $min_validity_date <= $date_obj));
				}
				
				if ($this->get('merchant')->isOpen($date['day']) == TRUE AND $valid_date)
				{
					!in_array($date['day'], $result) AND $result[] = $date['day'];
				}
			}
		} else
		{
			$result = NULL;
		}
		// When no dates are associated to the products it must return NULL
		// When dates are associated to the products but the no min/max condition is match it must return an empty array

		return $result;
	}

	public function getSelectPlaceholder($lang)
	{
		return '';
	}

	public function getFEKey($prefix = NULL)
	{
		$prefix != NULL OR $prefix = 'p_';
		return $prefix.$this->pk();
	}

	public function getFEType()
	{
		return str_replace('', '', get_class());
	}
	
	public function getFEProductListIDs()
	{
		return array($this->pk());
	}

	public function getCurrentProducts()
	{
		return array($this);
	}

	public function needsValidityStartTime()
	{
		return ($this->getExtra('use_validity_time') == 'Y');
	}

	public function needsDateWithPrice()
	{
		return ($this->get('merchant')->hasAddOn('Price Comparison') AND $this->getExtra('price_comparison_module') == TRUE AND $this->getExtra('default_sku') != NULL);
	}

	public function needsDateWithPriceDatepicker()
	{
		$result = FALSE;
		if ($this->needsDateWithPrice())
		{
			$result = ($this->get('merchant')->hasAddOn('Price Comparison') AND $this->getExtra('price_comparison_module_datepicker') == TRUE);
		}
		return $result;
	}

	public function needsDateWithPriceGraph()
	{
		$result = FALSE;
		if ($this->needsDateWithPrice())
		{
			$result = ($this->get('merchant')->hasAddOn('Price Comparison') AND $this->getExtra('price_comparison_module_graph') == TRUE);
		}
		return $result;
	}
	
	public function getFEValidityTimes($params = array())
	{
		set_time_limit(10);
		$times = array();
		if ($this->getExtra('use_validity_time') == 'Y')
		{
			$validity_date = Arr::get($params, 'validity_start') != NULL ? (clone Arr::get($params, 'validity_start')) : NULL;
			$validity_time_step_unit = $this->getExtra('validity_time_step_unit') ?: Product::VALIDITY_TYPE_MINUTE;
			$validity_time_step_value = $this->getExtra('validity_time_step_value') ?: 1;
			$min_validity_date = $this->getMinValidityDate(); // new DateTime('now', $this->getTimeZone()); //
			$calendar_date_times = $this->get('validity_calendar')->get('calendar_date_times')->where('deleted', 'IN', DB::expr("('CN', 'DN')"));
			$validity_date != NULL AND $calendar_date_times = $calendar_date_times->and_where('day', '=', $validity_date->format('Y-m-d'));
			$calendar_date_times = $this->get('validity_calendar')->loaded() ? $calendar_date_times->find_all() : array(ORM::factory('Skp_CalendarDateTime'));
			$date_interval = new DateInterval('PT'.$validity_time_step_value.($validity_time_step_unit==Product::VALIDITY_TYPE_HOUR?'H':'M'));
			$tz_utc = new DateTimeZone('UTC');
			foreach ($calendar_date_times as $cdt)
			{
				$time_start = $cdt->get('time_start') ?? '00:00:00';
				list($time_start_h, $time_start_m, $time_start_s) = explode(':', $time_start);
				$time_end = $cdt->get('time_end') ?? '23:59:59';
				list($time_end_h, $time_end_m, $time_end_s) = explode(':', $time_end);
				$start_range_date = (clone $validity_date)->setTime($time_start_h, $time_start_m, $time_start_s);
				$end_range_date = (clone $validity_date)->setTime($time_end_h, $time_end_m, $time_end_s);
				$original_time_zone = $start_range_date->getTimeZone();
				while ($start_range_date <= $end_range_date)
				{
					if ($start_range_date > $min_validity_date)
					{
						$hour = $start_range_date->format('G');
						$minute = $start_range_date->format('i');
						isset($times[$hour]) OR $times[$hour] = array();
						in_array($minute, $times[$hour]) OR $times[$hour][] = intval($minute);
					}
					$start_range_date = $start_range_date->setTimeZone($tz_utc)->add($date_interval)->setTimeZone($original_time_zone); // We need to work with UTC because when changing from day saving to solar time the add create problem
				}
			}
		}
		return $times;
	}

	// ProductGroup Function
	public function getNameInProductGroup($productgroup_id)
	{
		return $this->get('product_productgroups')->where('productgroup_id', '=', $productgroup_id)->find()->get('name');
	}
	
	public function hasProductGroup()
	{
		return $this->get('product_productgroups')->count_all() > 0;
	}
	// END ProductGroup Function

	public function getProductBookingOption($params)
	{
		if ($this->needsBooking())
		{
			$validity_start = Arr::get($params, 'validity_start');
			// for get skus
			$promo_id = Arr::get($params, 'promo_id');
			$skugroup_id = Arr::get($params, 'skugroup_id');
			$pool_id = Arr::get($params, 'pool_id');
			$bookable_y_n_a = Arr::get($params, 'bookable_y_n_a');
			$skus = Arr::get($params, 'sku_list');
			// END for get skus
			$avail_prices = array();
			$availabilities = $this->getAvailable(array('from_datetime' => $validity_start, 'daily_only' => FALSE));
			$max_time_visible = 12;
			$time_ranges = array();
			$time_range_key = 1;
			$count_avail_elem = array();
			$insert_start_date_time = TRUE;
			$choose_availability = TRUE;
			$last_end = NULL;

			foreach ($availabilities as $key=>$avail)
			{
				if ($avail->calendar_date_time_day == $validity_start->format('Y-m-d') OR $avail->calendar_date_time_day == NULL)
				{
					if ($avail->calendar_date_time_id > 0) {
						$choose_availability = FALSE;
					}
					$avail->skus = array();
					foreach ($skus as $sku)
					{
						$params = array();
						$sd = new StdClass;
						$sd->id = $sku->pk();
						$sd->name = $sku->getPriceCategoryExtendedName(Request::$lang);
						if ($avail->calendar_date_time_day != NULL)
						{
							if ($avail->calendar_date_time_time_start != NULL)
							{
								$validity_date = new DateTime($avail->calendar_date_time_day . ' ' . $avail->calendar_date_time_time_start, $this->get('merchant')->getTimeZone());
							}else{
								$validity_date = new DateTime($avail->calendar_date_time_day.' 00:00:00', $this->get('merchant')->getTimeZone());
							}
							$params['validity_date'] = $validity_date;
						}else{
							$params['validity_date'] = $validity_start;
						}
						$params['ignore_time'] = FALSE;
						$sd->price = $sku->price($params);
						if ($sd->price->amount !== NULL)
						{
							$avail->skus[] = $sd;
						}
					}
					if (sizeof($avail->skus) > 0 )
					{
						$avail->unlimited = false;
						if ($avail->availability_qty === NULL)
						{
							$avail->unlimited = true;
						}
						$avail_prices[] = $avail;
					}
					// Create time range
					if (!isset($time_ranges[$time_range_key-1]->end) OR (isset($time_ranges[$time_range_key-1]->end) AND $time_ranges[$time_range_key-1]->end < $avail->calendar_date_time_time_start))
					{
						if ($insert_start_date_time)
						{
							$time_ranges[$time_range_key] = new stdClass;
							$time_ranges[$time_range_key]->start = ($avail->calendar_date_time_time_start != NULL ? $avail->calendar_date_time_time_start : '00:00:00');
							$insert_start_date_time = FALSE;
						}
						if ( !isset($count_avail_elem[$avail->availability_id]) )
						{
							$count_avail_elem[$avail->availability_id] = 1;
						} else {
							if ( $count_avail_elem[$avail->availability_id] >= $max_time_visible-1 )
							{
								$time_ranges[$time_range_key]->end = $avail->calendar_date_time_time_start;
								$time_range_key++;
								$insert_start_date_time = TRUE;
								$count_avail_elem = array();
							} else {
								$count_avail_elem[$avail->availability_id]++;
							}
						}
					}
					$last_end = $avail->calendar_date_time_time_start;
				}
			}
			
			if (isset($time_ranges[$time_range_key])) {
				if ((!isset($time_ranges[$time_range_key]->end) OR $time_ranges[$time_range_key]->end === NULL)) {
					$time_ranges[$time_range_key]->end = ($last_end != NULL ? $last_end : '23:59:59');
				}
			}
			$booking_options = new StdClass();
			$booking_options->pk = $this->pk();
			$booking_options->avail_prices = $avail_prices;
			$booking_options->needs_booking = ($this->needsBooking() OR count($avail_prices) > 0);
			$booking_options->time_ranges = $time_ranges;
			$booking_options->choose_availability = $choose_availability;
			return $booking_options;
		} else
		{
			return NULL;
		}
	}
	
	public function getAttributes($params = array())
	{
		$catalog = $this->get('catalog');
		if ($catalog->loaded() == FALSE)
		{
			throw new Skp_Exception('Cannot get Product\'s Attributes  if product has no Catalog assigned to it');
		}
		$attributes = $this->get('product_attributes');
		if (Arr::get($params, 'ext_send') === TRUE)
		{
			$attributes = $attributes->where('ext_send', '=', 1);
		}
		if (Arr::get($params, 'status'))
		{
			$attributes = $attributes->where('status', '=', Arr::get($params, 'status'));
		}
		$attributes = $attributes->find_all();
		return $attributes;
	}
	
	public function getActiveAttributes()
	{
		$params = array('ext_send'=>TRUE, 'status'=>ProductAttribute::STATUS_OK);
		return $this->getAttributes($params);
	}

	public function hasDynamicDuration(): bool
	{
		return $this->getExtra('DynamicDuration', FALSE) != FALSE;
	}

	public function hasExternalAvailability()
	{
		try{
			return $this->call('hasExternalAvailability');
		}catch (Skp_Exception_Missing $e){}
		return FALSE;
	}
	
	public function getConnectorValidityUnitValue($call_connector_method = FALSE)
	{
		$connector_validity = $this->getExtra('connector_validity');
		if ($connector_validity == NULL AND $this->get('external_id') != NULL AND $call_connector_method)
		{
			try{
				$connector_validity = $this->call('getValidityUnitValue'); // array('unit'=>.., 'value'=>..)
				$this->setExtra('connector_validity', $connector_validity);
				$this->save();
			}
			catch(Skp_Exception_Missing $e){
				return NULL;
			}
		}
		return $connector_validity;
	}
	
	public function getOffers($params = array())
	{
		// Get Offers params
		$show_in = Arr::get($params, 'show_in');
		$audience = Arr::get($params, 'audience');
		$offer_type = Arr::get($params, 'offer_type');
		$merchant = $this->get('merchant');
		$product_id = $this->pk();
		
		// If cache
		
		// else
		$offer_params = array(
			'merchant' => $merchant,
			'offer_type' => $offer_type,
			'show_in' => $show_in,
			'product_ids' => array($product_id),
			'audience' => $audience,
		);
		$active_offers = ($product_id != NULL ? Offer::getOffersContents($offer_params) : array());
		
		return $active_offers;
	}

	public function getExportConnectors()
	{
		$thread_cache_key = get_called_class().'_'.$this->pk().'_export_connectors';
		if (Skp_ThreadCache::get($thread_cache_key) !== NULL) return Skp_ThreadCache::get($thread_cache_key);
		$specific_export_connectors = $this->get('export_connectors')->where('active', '=', 1)->find_all()->as_array('id');
		$global_export_connectors = $this->get('merchant')->getExportConnectorsForAllProducts();
		Skp_ThreadCache::set($thread_cache_key, array_merge($specific_export_connectors, $global_export_connectors));
		return Skp_ThreadCache::get($thread_cache_key);
	}
}
