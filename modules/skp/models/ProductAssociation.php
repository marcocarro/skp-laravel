<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class ProductAssociation extends Model {

	protected $table = 'product_association';
	protected $_table_columns = Array(
		'id' => Array(), 
		'main_product_id' => Array (),
		'main_sku_id' => Array(),
		'sub_product_id' => Array(),
		'sub_sku_id' => Array(),
		'relation' => Array(),
		'qty' => Array(),
		'archived' => Array(),
		'deleted' => Array(),
	);
	
	protected $_belongs_to = array(
		'main_product' => array(
			'model' => 'Skp_Product',
			'foreign_key' => 'main_product_id',
		),
		'main_sku' => array(
			'model' => 'Skp_Sku',
			'foreign_key' => 'main_sku_id',
		),
		'sub_product' => array(
			'model' => 'Skp_Product',
			'foreign_key' => 'sub_product_id',
		),
		'sub_sku' => array(
			'model' => 'Skp_Sku',
			'foreign_key' => 'sub_sku_id',
		),
	);
	
	const RELATION_OPTIONAL			= 'optional';
	const RELATION_MANDATORY		= 'mandatory';
	const RELATION_RECOMMENDED		= 'recommended';
	const RELATION_PACKAGE			= 'package';
	
	public static $relations = array(
		self::RELATION_OPTIONAL => 'backoffice.product_association.optional',
		self::RELATION_MANDATORY => 'backoffice.product_association.mandatory', 
		self::RELATION_RECOMMENDED => 'backoffice.product_association.recommended', 
		self::RELATION_PACKAGE => 'backoffice.product_association.package'
	);
	
	public function getMerchantId()
	{
		if ($this->get('main_product_id') != NULL)
		{
			return $this->get('main_product')->get('merchant')->pk();
		}
		else if ($this->get('main_sku_id') != NULL)
		{
			return $this->get('main_sku')->get('merchant')->pk();
		}
		else
			return NULL;
	}
}

