<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class LogDb extends Model {

	protected $table = 'log';
	public $timestamps = false;

	public function merchant() {
		$this->belongsTo(Merchant::class);
	}

	public function order() {
		$this->belongsTo(Order::class);
	}

}