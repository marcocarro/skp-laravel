<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class DataField extends Model {

	protected $table = 'data_field';
	public $timestamps = false;
	protected $casts = [
		'label' => 'array',
		'extra' => 'array',
		'notes' => 'array'
	];

	public function merchant() {
		$this->belongsTo(Merchant::class);
	}

	public function data_profile_fields() {
		return $this->hasMAny(
			DataProfileField::class,
			'data_field_id'
		);
	}
	
}