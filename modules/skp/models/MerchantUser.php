<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class MerchantUser extends Model {

	protected $table = 'merchant_user';
	public $timestamps = false;

	public function merchant() {
		$this->belongsTo(Merchant::class);
	}

	public function user() {
		$this->belongsTo(User::class);
	}
	
}