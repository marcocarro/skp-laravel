<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class VirtualCardTypeMerchant extends Model
{

	protected $table = 'virtual_card_type_merchant';
   public $timestamps = false;

}