<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Note extends Model {

	protected $table = 'note';
	
	protected $_belongs_to = array(
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		)
	);

	public function rules()
	{
		return array(
		);
	}

}
