<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class ProductTypeMerchant extends Model {

	protected $table = 'product_type_merchant';
	
protected $casts = [
		'extra' => 'array',
	];
	
	protected $_belongs_to = array(
	'merchant' => array(
		'model' => 'Skp_Merchant',
		'foreign_key' => 'merchant_id',
		),
	'product_type' => array(
		'model' => 'Skp_ProductType',
		'foreign_key' => 'product_type_id',
		),
	);


}
