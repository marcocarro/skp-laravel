<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class TagAssociation extends Model {

	protected $table = 'tag_association';
	
	protected $_table_columns = array(
		'id' => array(), 
		'tag_id' => array (),
		'element_id' => array (),
		'class_name' => array(),
	);
	
	protected $_serialize_columns = array();

	protected $_belongs_to = array(
		'tag' => array(
			'model' => 'Skp_Tag',
			'foreign_key' => 'tag_id',
		),
	);
	
	protected $_has_many = array();
	
	public function rules()
	{
		return array(
			'tag_id' => array(
				array('not_empty'),
			),
			'element_id' => array(
				array('not_empty'),
			),
			'class_name' => array(
				array('not_empty'),
			),
		);
	}

	public static function getAssociatedTags (&$obj)
	{
		$class_name = get_class($obj);
		$element_id = $obj->pk();
		$tags = array();
		foreach(ORM::factory('Skp_TagAssociation')->with('tag')->where('class_name', '=', $class_name)->and_where('element_id', '=', $element_id)->find_all() as $tag_association)
		{
			$tags[$tag_association->get('tag')->pk()] = $tag_association->get('tag');
		}
		return $tags;
	}
	
	public static function setAssociation ($class_name, $element_id, &$obj, $tag_ids_new)
	{
		$tags = $obj->getTags();
		$tag_ids_new == NULL AND $tag_ids_new = array();
		$tag_ids_old = $obj->getTagIds();
		$to_remove = array_diff($tag_ids_old, $tag_ids_new);
		$to_add = array_diff($tag_ids_new, array_intersect($tag_ids_old, $tag_ids_new));
		if(count($to_remove) > 0)
		{
			foreach (ORM::factory('Skp_TagAssociation')->where('class_name', '=', $class_name)->and_where('element_id', '=', $element_id)->and_where('tag_id', 'IN', $to_remove)->find_all() as $tag_association)
			{
				unset($tags[$tag_association->get('tag_id')]);
				$tag_association->delete();
			}
		}
		foreach ($to_add as $tag_id)
		{
			$new_tag_association = ORM::factory('Skp_TagAssociation');
			$new_tag_association->set('class_name', $class_name);
			$new_tag_association->set('element_id', $element_id);
			$new_tag_association->set('tag_id', $tag_id);
			$new_tag_association->save();
			$tags[$tag_id] = ORM::factory('Skp_Tag', $tag_id);
		}
		return $tags;
	}
}
