<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class ProductAttribute extends Model {

	protected $table = 'product_attribute';
	
protected $casts = [
		'extra' => 'array',
	];
	protected $_table_columns = array(
		'id' => array(),
		'product_id' => array(),
		'name' => array(),
		'type' => array(),
		'ext_key' => array(),
		'ext_default' => array(),
		'ext_type_id' => array(),
		'ext_send' => array(),
		'variable' => array(),
		'data_field_id' => array(),
		'fixed_value' => array(),
		'status' => array(),
		'extra' => array(),
	);
	
	protected $_belongs_to = array(
		'product' => array(
			'model' => 'Skp_Product',
			'foreign_key' => 'product_id',
		),
		'data_field' => array(
			'model' => 'Skp_DataField',
			'foreign_key' => 'data_field_id',
		),
	);
	
	const STATUS_OK				= 'OK';
	const STATUS_INCOMPLETE		= 'INCOMPLETE';
	const STATUS_EXT_NEW			= 'EXT_NEW';
	const STATUS_EXT_CHANGED	= 'EXT_CHANGED';
	const STATUS_EXT_MISSING	= 'EXT_MISSING';
	
	const TYPE_BOOLEAN	= 'boolean';
	const TYPE_STRING		= 'string';
	const TYPE_INTEGER	= 'integer';
	const TYPE_DECIMAL	= 'decimal';
	const TYPE_DATE		= 'date';
	const TYPE_DATE_TIME	= 'date_time';
	const TYPE_IMAGE		= 'image';
	public static $type_list = array(self::TYPE_BOOLEAN, self::TYPE_STRING, self::TYPE_INTEGER, self::TYPE_DECIMAL, self::TYPE_DATE, self::TYPE_DATE_TIME, self::TYPE_IMAGE);
}
