<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Lock extends Model {

	protected $table = 'lock';
	public $timestamps = false;
	
}