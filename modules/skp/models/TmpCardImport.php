<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class TmpCardImport extends Model {

	protected $table = 'tmp_card_import';

	protected $_table_columns = array(
		'id' => array(),
		'card_id' => array(),
		'user_id' => array(),
		'order_external_id' => array(),
		'card_type' => array(),
		'card_serial' => array(),
		'external_extra' => array(),
	);
	
	protected $_serialize_columns = array('external_extra');

}
