<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class UserMerchantMessage extends Model {

	protected $table = 'user_merchant_message';
	
	
protected $casts = [
		'extra' => 'array',
	];
	protected $_table_columns = Array(
		'id' => Array(),
		'user_id' => Array(),
		'merchant_id' => Array(),
		'message_type' => Array(),
		'message_text' => Array(),
		'new' => Array(),
		'extra' => Array(),
	);

	public static function getMessages(User $user, Merchant $merchant, $message_type)
	{
		
	}
	
	public static function setMessage(User $user, Merchant $merchant, $message_type, $message_text)
	{
		
	}
}
