<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Country extends Model {

	protected $table = 'country';
	public $timestamps = false;
	protected $casts = [
		'extra' => 'array',
	];

}