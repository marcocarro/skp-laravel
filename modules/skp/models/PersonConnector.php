<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class PersonConnector extends Model {

	protected $table = 'person_connector';
	protected $_serialize_columns = array('external');
	protected $_belongs_to = array(
		'person' => array(
			'model' => 'Skp_Person',
			'foreign_key' => 'person_id',
		),
		'connector' => array(
			'model' => 'Skp_Connector',
			'foreign_key' => 'connector_id',
		),
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
	);
}
