<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Calendar extends Model {

	protected $table = 'booking_manual';
	public $timestamps = false;

	public function merchant() {
		$this->belongsTo(Merchant::class);
	}

	public function dates() {
		return $this->hasMAny(
			CalendarDate::class,
			'calendar_id'
		);
	}

	public function times() {
		return $this->hasMAny(
			CalendarTime::class,
			'calendar_id'
		);
	}

	public function calendar_date_times() {
		return $this->hasMAny(
			CalendarDateTime::class,
			'calendar_id'
		);
	}

	public function availabilities() {
		return $this->hasManyThrough(
			Availability::class,
			AvailabilityProductSkuCalendarPricelist::class,
			'availability_id',
			'calendar_id'
		);
	}

	public function price_lists() {
		return $this->hasManyThrough(
			PriceList::class,
			PriceListCalendar::class,
			'price_list_id',
			'calendar_id'
		);
	}

	public function availability_product_sku_calendar_pricelist() {
		return $this->hasMAny(
			AvailabilityProductSkuCalendarPricelist::class,
			'calendar_id'
		);
	}
	
}