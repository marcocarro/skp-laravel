<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class UserMerchant extends Model {

	protected $table = 'user_merchant';
	protected $_table_columns = Array(
		'id' => Array(),
		'merchant_id' => Array(),
		'user_id' => Array(),
		'user_type' => Array(),
		'registration_date' => Array(),
		'last_visit_date' => Array(),
		'num_visit' => Array(),
		'gdpr_status' => Array(),
	);
	
	protected $_belongs_to = array(
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
		'user' => array(
			'model' => 'Skp_User',
			'foreign_key' => 'user_id',
		),
	);

	protected $_has_many = array(
		'user_merchant_auths' => array(
			'model' => 'Skp_UserMerchantAuth',
			'foreign_key' => 'user_merchant_id'
		),
	);

	const TYPE_LEAD		= 'LEAD';
	const TYPE_CUSTOMER	= 'CUSTOMER';
	const TYPE_ADMIN 		= 'ADMIN';

	const STATUS_REGISTERED = 'REGISTERED';
	const STATUS_CONSENT_NEEDED = 'CONSENT_NEEDED';
	const STATUS_DELETED 	= 'DELETED';

	public static $types_list = array(self::TYPE_LEAD, self::TYPE_CUSTOMER, self::TYPE_ADMIN);
	public static $status_list = array(self::STATUS_REGISTERED, self::STATUS_CONSENT_NEEDED, self::STATUS_DELETED);

	public static function addConsentGDPR($user, $merchant)
	{
		$um = $user->get('user_merchants')->where('merchant_id', '=', $merchant->pk())->find();
		if ($um->get('gdpr_status') != self::STATUS_REGISTERED)
		{
			if ($um->loaded() == FALSE)
			{
				$tot_visits = $user->get('user_merchant_visits')->where('merchant_id', '=', $merchant->pk())->count_all();
			}else{
				$tot_visits = $um->get('num_visit');
			}
			$now_utc = gmdate('Y-m-d H:i:s');
			$um->set('user', $user);
			$um->set('merchant', $merchant);
			$um->set('user_type', $um->calculateGDPRType());
			$um->set('registration_date', $now_utc);
			$um->set('last_visit_date', $now_utc);
			$um->set('num_visit', $tot_visits);
			$um->set('gdpr_status', self::STATUS_REGISTERED);
			$um->save();
		}
		foreach ($merchant->getAuths() as $auth_id => $auth_data)
		{
			$um->get('user_merchant_auths')->where('auth_id', '=', $auth_id)->find()->set('user_merchant_id', $um->pk())->set('auth_id', $auth_id)->set('accepted', 1)->set('changed', gmdate('Y-m-d H:i:s'))->save();
		}
		foreach ($merchant->getOptins() as $optin)
		{
			$optin->isImplicit() AND $user->setOptinStatus($optin, 1);
		}
	}

	public static function calculateGDPRTypeForUserMerchant($user, $merchant)
	{
		if ($user->get('merchants')->where('merchant_id', '=', $merchant->pk())->count_all() > 0)
		{
			return self::TYPE_ADMIN;
		}
		if ($user->get('orders')->where('merchant_id', '=', $merchant->pk())->and_where('type', '=', Order::TYPE_ORDER)->and_where('status', '=', Order::STATUS_BOOKED)->count_all() > 0)
		{
			return self::TYPE_CUSTOMER;
		}else{
			return self::TYPE_LEAD;
		}
	}

	public function calculateGDPRType()
	{
		return self::calculateGDPRTypeForUserMerchant($this->user, $this->merchant);
	}

	public static function updateUserType($user, $merchant)
	{
		$um = $user->get('user_merchants')->where('merchant_id', '=', $merchant->pk())->find();
		$um->set('user_type', $um->calculateGDPRType());
		$um->save();
	}
}

