<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;

class B2BCompanyPaymentChannel extends Model
{
    protected $table = 'b2b_company_payment_channel';
    protected $timestamp = false;
}