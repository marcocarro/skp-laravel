<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class UserConnector extends Model {

	protected $table = 'user_connector';
	protected $_belongs_to = array(
		'user' => array(
			'model' => 'Skp_User',
			'foreign_key' => 'user_id',
		),
		'connector' => array(
			'model' => 'Skp_Connector',
			'foreign_key' => 'connector_id',
		),
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
	);
}
