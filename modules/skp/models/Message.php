<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Message extends Model {

	protected $table = 'message';
	public $timestamps = false;

	public function message_channel() {
		$this->belongsTo(MessageChannel::class, 'message_channel_id');
	}

	public function merchant() {
		$this->belongsTo(Merchant::class);
	}

	public function user() {
		$this->belongsTo(User::class);
	}

	public function order() {
		$this->belongsTo(Order::class);
	}

	public function b2b_order() {
		$this->belongsTo(B2BOrder::class);
	}
	
}