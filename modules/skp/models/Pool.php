<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Pool extends ModelNameDescription {
	protected $table = 'pool';
		
	protected $_belongs_to = array(
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
	);

	protected $_has_many = array(
		'products' => array(
			'model' => 'Skp_Product',
			'foreign_key' => 'pool_id',
			'through' => 'pool_product',
			'far_key' => 'product_id',
		),
	);
	
	public static function getPools($merchant_id = NULL, $producttype_id = NULL)
	{
		$r = ORM::factory('Skp_Pool');

		if ($producttype_id != NULL)
			$r->join('pool_product')->on('pool_product.pool_id','=','skp_pool.id')->join('product')->on('product.id','=','pool_product.product_id')->join('product_type')->on('product_type.id','=','product.producttype_id')->where('product_type.id','=', $producttype_id);
		if ($merchant_id != NULL)
			$r->where('skp_pool.merchant_id', '=', $merchant_id);
		$r->group_by('skp_pool.id')->order_by('sort');
		return $r;
	}
	
	public static function getSkuGroupPools($merchant_id = NULL, $skugroup_id = NULL)
	{
		$r = ORM::factory('Skp_Pool');

		if ($skugroup_id != NULL)
			$r->join('pool_product')->on('pool_product.pool_id','=','skp_pool.id')->join('product')->on('product.id','=','pool_product.product_id')->join('sku')->on('sku.product_id','=','product.id')->join('skugroup_sku')->on('skugroup_sku.sku_id', '=', 'sku.id')->where('skugroup_sku.skugroup_id','=', $skugroup_id);
		if ($merchant_id != NULL)
			$r->where('skp_pool.merchant_id', '=', $merchant_id);
		$r->group_by('skp_pool.id')->order_by('sort');
		return $r;
	}
}
