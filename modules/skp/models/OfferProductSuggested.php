<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class OfferProductSuggested extends ModelNameDescription {

	protected $table = 'offer_product_suggested';
	protected $_table_columns = Array(
		'id' => Array(),
		'merchant_id' => Array(),
		'offer_id' => Array(),
		'product_id' => Array(),
		'skugroup_id' => Array(),
		'sort' => Array(),
		'extra' => Array(),
	);

	
protected $casts = [
		'extra' => 'array',
	];

	protected $_belongs_to = array(
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
		'offer' => array(
			'model' => 'Skp_Offer',
			'foreign_key' => 'offer_id',
		),
		'product' => array(
			'model' => 'Skp_Product',
			'foreign_key' => 'product_id',
		),
		'skugroup' => array(
			'model' => 'Skp_SkuGroup',
			'foreign_key' => 'skugroup_id',
		),
	);

	public function getI18nLink()
	{
		$merchant = $this->get('merchant');
		$linkI18n = array();
		$product = ORM::factory('Skp_Product', $this->get('product_id'));
		$skugroup = ORM::factory('Skp_SkuGroup', $this->get('skugroup_id'));
		$bookable = $product->needsDateTimeBooking() ? 'y' : 'n';
		$has_bookable_filter = $skugroup->getLayoutOption('booking_product_filter') === TRUE;
		foreach ($merchant->getLanguages() as $lang)
		{
			$linkI18n[$lang] = Skp_Helper::getProtocol().Skp_Helper::getDomainEnv($merchant->get('domain')) . URL::site(Route::get('shop', $lang)->uri(), FALSE) . '?skugroup_id=' . $this->get('skugroup_id').'&product_id='.$this->get('product_id').($has_bookable_filter?'&bookable_y_n_a='.$bookable : '');
		}
		return $linkI18n;
	}

}
