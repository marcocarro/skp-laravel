<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class PaymentChannel extends Model {

	protected $table = 'payment_channel';
	protected $_serialize_columns = array('parameters', 'extra');
	protected $_table_columns = Array(
		'id' => Array(),
		'merchant_id' => Array(),
		'name' => Array(),
		'payment_class' => Array(),
		'capture_on_process' => Array(),
		'b2c_enabled' => Array(),
		'test_env' => Array(),
		'username' => Array(),
		'password' => Array(),
		'connection_status' => Array(),
		'sort' => Array(),
		'parameters' => Array(),
		'extra' => Array(),
	);
	
	protected $_belongs_to = array(
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
	);

	protected $_has_many = array(
		'payments' => array(
			'model' => 'Skp_Payment',
			'foreign_key' => 'payment_channel_id',
		),
	);

	public static $payment_classes = array('Skp_PaymentChannel_Nets_V1', 'Skp_PaymentChannel_Sella_V1', 'Skp_PaymentChannel_Sella_V2', 'Skp_PaymentChannel_DotPay_V1', 'Skp_PaymentChannel_MonetaWeb_V1', 'Skp_PaymentChannel_Manual');

	public function rules()
	{
		return array(
			'payment_class' => array(
				array('not_empty')
			)
		);
	}

	static public function getDefault($payment_class = NULL)
	{
		$default_config = Kohana::$config->load('default_setting_new_merchant');
		$default = ORM::factory('Skp_PaymentChannel');
		$payment_class != NULL OR $payment_class = $default_config['PaymentChannel']['class'];
		if ($payment_class != NULL)
		{
			$default->set('payment_class', $payment_class);
			$def = $default_config['PaymentChannel'][$payment_class];
			foreach ($def as $field => $value)
			{
				$default->set($field, $value);
			}
		}
		return $default;
	}

	public function checkConnection($save = TRUE)
	{
		try
		{
			$this->call('checkConnection');
		}
		catch(Exception $e)
		{
			$this->connection_status = 'KO';
			if ($save)
			{
				$this->save();
			}
			throw $e;
			Log::instance()->add(LOG_DEBUG, 'exception 1 catched:');
		}
		$this->connection_status = 'OK';
		if ($save)
		{
			$this->save();
		}
	}
	
	public function call()
	{
		$class_name = $this->payment_class.'_Connector';
		$args = func_get_args();
		$method = array_shift($args); // The method is the first parameter
		$params = array_shift($args);
		//Log::instance()->add(LOG_DEBUG,' PaymentChannel class: '.$class_name.' calls: '.$method.' with params: '.print_r($params,TRUE));
		return $class_name::$method($this, $params);
	}

	public function hasCards()
	{
		$class_name = $this->payment_class.'_Connector';
		return $class_name::$has_cards;
	}

	public function recurrencyEnabled()
	{
		$class_name = $this->payment_class.'_Connector';
		return $class_name::recurrencyEnabled($this);
	}

	public function getMinAmount()
	{
		return Arr::get($this->get('parameters'), 'min_amount');
	}

	public function getMaxAmount()
	{
		return Arr::get($this->get('parameters'), 'max_amount');
	}

	public function getExpireDateData()
	{
		$expire_date_data = new stdClass();
		$expire_date_data->b2b_payment_manager_num_day_for_pay = $this->getExtra('b2b_payment_manager_num_day_for_pay');
		$expire_date_data->b2b_payment_manager_num_day_for_pay_month_end = $this->getExtra('b2b_payment_manager_num_day_for_pay_month_end');
		$expire_date_data->b2b_payment_manager_num_day_for_pay_month_day = $this->getExtra('b2b_payment_manager_num_day_for_pay_month_day');
		return $expire_date_data;
	}
}
