<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Booking extends Model {

	protected $table = 'booking';
	
	protected $casts = [
		'extra' => 'array',
	];

	public function calendar_date_time() {
		$this->belongsTo(CalendarDateTime::class);
	}

	public function availability() {
		$this->belongsTo(Availability::class);
	}

	public function order_item() {
		$this->belongsTo(OrderItem::class);
	}

	public function booking_manual() {
		$this->belongsTo(BookingManual::class);
	}
	
}