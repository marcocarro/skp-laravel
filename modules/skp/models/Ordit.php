<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Ordit extends Ordit{
	
	protected function get_username()
    {
        return 'chris';
    }
    
    public function update(Validation $validation = NULL)
	{
		if (!empty($this->_changed))
		{
			$log = $this->get_ordit_log("update");
		}
		ORM::update($validation);
		if (isset($log))
		{
			$log->save();
		}
	}
	
    /*
    @returns DateTime object of the latest change
    */
    
    public function get_last_change()
    {
		$db = Database::instance();
		$query = DB::query(Database::SELECT, "select timestamp_created from " . $db->table_prefix() .  "ordit_logs" . " where model = '" . $this->_object_name . ' :: ' . $this->pk()."' order by timestamp_created desc limit 1");
		$res = $query->as_object()->execute();
		if ($res[0] != NULL)
		{
			return DateTime::createFromFormat('Y-m-d H:i:s', $res[0]->timestamp_created, new DateTimeZone('UTC'));
		}else{
			return DateTime::createFromFormat('Y-m-d H:i:s', '2000-01-01 00:00:00', new DateTimeZone('UTC'));
		}
	}

}
