<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class MerchantOptin extends Model {

	protected $table = 'merchant_optin';
	public $timestamps = false;
	protected $casts = [
		'extra' => 'array',
		'name' => 'array',
	];

	public function merchant() {
		$this->belongsTo(Merchant::class, 'merchant_id');
	}

}