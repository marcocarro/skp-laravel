<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class ContactProfile extends Model {

	protected $table = 'contact_profile';
	public $timestamps = false;
	protected $casts = [
		'infos' => 'array',
	];

	public function merchant() {
		$this->belongsTo(Merchant::class);
	}

	public function product_contact_profiles() {
		return $this->hasMAny(
			Skp_ProductContactProfile::class,
			'profile_id'
		);
	}
	
}