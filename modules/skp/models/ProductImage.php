<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class ProductImage extends Model {

	protected $table = 'product_image';
	protected $_table_columns = Array(
		'id' => Array(),
		'product_id' => Array(),
		'image_id' => Array(),
		'sort' => Array(),
	);
	protected $_serialize_columns = array();
	
	protected $_belongs_to = array(
		'product' => array(
			'model' => 'Skp_Product',
			'foreign_key' => 'product_id',
		),
		'image' => array(
			'model' => 'Skp_Image',
			'foreign_key' => 'image_id',
		),
	);
}
