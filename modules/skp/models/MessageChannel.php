<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class MessageChannel extends Model {

	protected $table = 'message_channel';
	public $timestamps = false;
	protected $casts = [
		'extra' => 'array'
	];

	public function merchant() {
		$this->belongsTo(Merchant::class);
	}

	protected $_has_many = array(
		'messages' => array(
			'model' => 'Skp_Message',
			'foreign_key' => 'message_channel_id',
		),
	);

}