<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class User extends Model {

	protected $table = 'user';
	protected $_serialize_columns = array('user_extra');
	protected $_table_columns = Array(
		'id' => Array(),
		'email' => Array(),
		'verified' => Array(),
		'status' => Array(),
		'deleted' => Array(),
		'password' => Array(),
		'title' => Array(),
		'first_name' => Array(),
		'middle_name' => Array(),
		'last_name' => Array(),
		'birth_date' => Array(),
		'gender' => Array(),
		'street_num' => Array(),
		'street' => Array(),
		'city' => Array(),
		'state_prov' => Array(),
		'zip' => Array(),
		'country' => Array(),
		'phone' => Array(),
		'photo' => Array(),
		'image_url_thumb' => Array(),
		'last_language' => Array(),
		'is_admin' => Array(),
		'autologin_cookie' => Array(),
		'user_extra' => Array(),
	);
	
	protected $_has_many = array(
		'authorizations' => array(
			'model' => 'Skp_UserAuth',
			'foreign_key' => 'user_id',
		),
		'persons' => array(
			'model' => 'Skp_Person',
			'foreign_key' => 'user_id',
		),
		'providers' => array(
			'model' => 'Skp_UserProvider',
			'foreign_key' => 'user_id',
		),
		'merchants' => array(
			'model' => 'Skp_Merchant',
			'through' =>'merchant_user',
			'foreign_key' => 'user_id',
		),
		'user_merchants' => array(
			'model' => 'Skp_UserMerchant',
			'foreign_key' => 'user_id',
		),
		'logs' => array(
			'model' => 'Skp_UserLog',
			'foreign_key' => 'user_id',
		),
		'sessions' => array(
			'model' => 'Skp_Session',
			'through'=>'user_log',
			'foreign_key' => 'user_id',
		),
		'cards' => array(
			'model' => 'Skp_Card',
			'foreign_key' => 'user_id',
		),
		'orders' => array(
			'model' => 'Skp_Order',
			'foreign_key' => 'user_id',
		),
		'user_merchant_positions' => array(
			'model' => 'Skp_UserMerchantPosition',
			'foreign_key' => 'user_id',
		),
		'credit_cards' => array(
			'model' => 'Skp_CreditCard',
			'foreign_key' => 'user_id',
		),
		'b2b_orders' => array(
			'model' => 'Skp_B2BOrder',
			'foreign_key' => 'user_id',
		),
		'companyusers' => array(
			'model' => 'Skp_CompanyUser',
			'foreign_key' => 'user_id',
		),
		'b2b_companies' => array(
			'model' => 'Skp_B2BCompany',
			'through' => 'b2b_company_user',
			'far_key' => 'b2b_company_id',
			'foreign_key' => 'user_id',
		),
		'merchantusers' => array(
			'model' => 'Skp_MerchantUser',
			'foreign_key' => 'user_id',
		),
		'user_merchant_optins' => array(
			'model' => 'Skp_UserMerchantOptin',
			'foreign_key' => 'user_id',
		),
		'connector_users' => array(
			'model' => 'Skp_UserConnector',
			'foreign_key' => 'user_id',
		),
		'promocodes' => array(
			'model' => 'Skp_Promocode',
			'foreign_key' => 'user_id',
			'through' => 'promocode_user',
			'far_key' => 'promocode_id',
		),
		'invoice_receipt_profiles' => array(
			'model' => 'Skp_InvoiceReceiptProfile',
			'foreign_key' => 'user_id',
		),
		'invoice_receipt_orders' => array(
			'model' => 'Skp_InvoiceReceiptOrder',
			'foreign_key' => 'user_id',
		),
		'user_downloads' => array(
			'model' => 'Skp_UserDownload',
			'foreign_key' => 'user_id',
		),
		'user_deletes' => array(
			'model' => 'Skp_UserDelete',
			'foreign_key' => 'user_id',
		),
		'messages' => array(
			'model' => 'Skp_Message',
			'foreign_key' => 'user_id',
		),
		'user_merchant_visits' => array(
			'model' => 'Skp_UserMerchantVisit',
			'foreign_key' => 'user_id',
		),
	);

	const PASSWORD_MIN_LENGTH	= 3;
	const PASSWORD_MAX_LENGTH	= 64;

	const MAX_LOGIN_ATTEMPTS	= 4;
	const LOGIN_RETRY_INTERVAL_MIN = 10;
	
	const REMINDER_ORDER_TO_BE_PAYED = 'order_to_be_payed';
	const REMINDERS_LIST = array(User::REMINDER_ORDER_TO_BE_PAYED);

	public function rules()
	{
		return array(
/*
			'country' =>array(
				array('in_array',array(':value',array_keys(Kohana::$config->load('lists')->get('country_list')))),
			),
			'gender' =>array(
				array('in_array',array(':value',array_keys(Kohana::$config->load('lists')->get('gender_list')))),
			),
*/
		);
	}

	public function __construct($id = NULL)
	{
		parent::__construct($id);
		if ($id != NULL)
		{
			// this stores the last user_merchant_positions
			$cached = UserMerchantPosition::getFromCache('last_user_merchant_positions_userid_'.$this->pk());
			is_array($cached) OR $this->setIntoCache('last_user_merchant_positions_userid_'.$this->pk(), array());
		}
	}

	public function set($column , $value)
	{
		if ($column == 'password')
		{
			if ($value !== NULL AND (strlen($value) < self::PASSWORD_MIN_LENGTH OR strlen($value) > self::PASSWORD_MAX_LENGTH))
			{
				throw new Skp_Exception(__('Invalid password length (min :1, max :2)', array(':1'=>self::PASSWORD_MIN_LENGTH, ':2'=>self::PASSWORD_MAX_LENGTH)));
			}
		}
		parent::set($column, $value);
	}

	public function __initialize()
	{
		$this->gender = 'U';
		$this->status = 'virtual';
		$this->deleted = 0;
		$this->is_admin = 'N';
		parent::__initialize();
	}

	private function setStatus()
	{
		if ($this->email == NULL AND ($this->password == NULL OR $this->providers->count_all() == 0))
		{
			$this->status = 'virtual';
		}
		else
		{
			if ($this->first_name == NULL OR $this->last_name == NULL OR $this->birth_date == NULL OR $this->phone == NULL OR $this->addressComplete() == FALSE)
			{
				$this->status = 'missing_data';
			}
			else
			{
				$this->status = 'complete';
			}
		}
	}
	
	public function setUserExtra($extra_name, $extra_value)
	{
		$ue = $this->get('user_extra');
		if ($extra_value != NULL)
		{
			$ue[$extra_name] = $extra_value;
		}else{
			unset($ue[$extra_name] );
		}
		$this->set('user_extra', $ue);
	}
	
	public function getExtra($key, $default_value = NULL)
	{
		if ($key != NULL)
		{
			return Arr::get($this->get('user_extra'), $key, $default_value);
		}
	}

	public function getMerchantUserAuth($merchant, $auth_id)
	{
		$um = $this->get('user_merchants')->where('merchant_id', "=", $merchant->pk())->find();
		if ($um->loaded() == FALSE)
		{
			throw new Exception ('UserMerchant not found for UserId: '.$this->pk().' MerchantID: '.$merchant->pk());
		}
		$uma = $um->get('user_merchant_auths')->where('auth_id' ,'=', $auth_id)->find();
		$uma->set('user_merchant', $um);
		$uma->set('auth_id', $auth_id);
		return $uma;
	}
	
	public function setToken($token)
	{
		$this->setUserExtra('token', $token);
	}
	
	public function getToken()
	{
		return Arr::get($this->get('user_extra'), 'token');
	}

	public function isLogged()
	{
		return $this->get('status') != 'virtual';
	}

	public function isLoggedGDPR($merchant)
	{
		if (gettype($merchant) == 'array') $merchant = array_pop($merchant);
		return ($this->isLogged() AND $this->isRegisteredUser($merchant)) AND ($this->hasDeleteRequest($merchant) == FALSE);
	}

	public function isComplete($params = array())
	{
		$isProfileComplete = TRUE;
		$param_merchant = Arr::get($params, 'merchant', ORM::factory('Skp_Merchant'));
		$contact_profile = $param_merchant->get('contact_profile');
		if ($contact_profile != NULL AND is_array($contact_profile->get('infos')))
		{
			$mandatory_fields = array_keys($contact_profile->get('infos'), "M", TRUE);
			$key = array_search('relation', $mandatory_fields);
			if ($key) unset($mandatory_fields[$key]);
			foreach ($mandatory_fields as $mandatory_field){
				if((array_key_exists($mandatory_field, $this->list_columns()) AND $this->get($mandatory_field) === NULL) OR (!array_key_exists($mandatory_field, $this->list_columns()) AND $this->getExtra($mandatory_field) == NULL))
				{
					$isProfileComplete = FALSE;
					break 1;
				}
			}
		}else{
			$isProfileComplete = FALSE;
		}
		foreach ($param_merchant->getAuths() as $auth_id=>$auth_data)
		{
			try{
				if($this->getMerchantUserAuth($param_merchant, $auth_id)->get('accepted') != 1){
					$isProfileComplete = FALSE;
					break;
				}
			}catch (Exception $e)
			{
				$isProfileComplete = FALSE;
				break;
			}
		}
		return $isProfileComplete;
	}

	public function hasOrder()
	{
		return false;
	}

	public function allowedTo($controller, $action, $merchant)
	{
		if ($merchant->allowedTo($controller, $action) == FALSE)
		{
			return FALSE;
		}

		$config = Kohana::$config->load('skiperf');
		$boAuth = $config->BO_authorizations;
		//$auth_type = $this->authorizations->where('merchant_id', '=', $merchant->pk())->find();
		$auth_types = $this->merchantusers->where('merchant_id', '=', $merchant->pk())->find_all();
		$extra_auth = $this->authorizations->where('merchant_id', '=', $merchant->pk());
		

		if ($controller == "User" AND ($action == "login" OR $action == "logged")) return TRUE;
		if ($this->is_admin == 'N') return FALSE;
		if ($controller == "Dashboard") return TRUE;
		
		$extra_auth_controller_action = $extra_auth->and_where('controller', '=', $controller)->and_where('action', '=', $action)->find();
		if ($extra_auth_controller_action->loaded()) return $extra_auth_controller_action->get('permitted') == 'Y' ? TRUE: FALSE;
		
		$extra_auth_controller = $extra_auth->and_where('controller', '=', $controller)->and_where('action', 'IS', NULL)->find();
		if ($extra_auth_controller->loaded()) return $extra_auth_controller->get('permitted') == 'Y' ? TRUE: FALSE;
		
		$is_allowed = FALSE;
		
		foreach ($auth_types as $auth_type)
		{
			// uMA contains the array with the user authoriztions for this merchant
			$uMA = $boAuth[$auth_type->get('user_type')];
			if ($auth_type->get('user_type') == MerchantUser::USER_TYPE_SUPERADMIN) return TRUE;
		
			if (Arr::get($uMA['NotAllowed'], 'all', FALSE) OR (Arr::get($uMA['NotAllowed'], $controller, FALSE) != FALSE AND (Arr::get($uMA['NotAllowed'][$controller]['actions'], 'all', FALSE) != FALSE OR Arr::get($uMA['NotAllowed'][$controller]['actions'], $action, FALSE))))
			{
					$is_allowed = FALSE;
			}
			if (Arr::get($uMA['Allowed'], 'all', FALSE) OR (Arr::get($uMA['Allowed'], $controller, FALSE) != FALSE AND (Arr::get($uMA['Allowed'][$controller]['actions'], 'all', FALSE) != FALSE OR Arr::get($uMA['Allowed'][$controller]['actions'], $action, FALSE))))
			{
					$is_allowed = TRUE;
					break;
			}
		}
		return $is_allowed; // To be changed based on the user level
	}
	
	public function getFullName()
	{
		return $this->first_name.' '.($this->middle_name != NULL?$this->middle_name.' ':'').$this->last_name;
	}

	public static function getUserFromEmail($email = NULL)
	{
		if ($email != NULL)
		{
			$user = ORM::factory('Skp_User')->where('email','=',$email)->find();
			return $user;
		}
		return ORM::factory('Skp_User');
	}

	public static function getUserFromEmailProviderData($email, $provider_name, $provider_id)
	{
		return ORM::factory('Skp_User')->join('user_provider', 'LEFT')->on('skp_user.id', '=', 'user_provider.user_id')->on('user_provider.provider', '=', DB::expr("'".$provider_name."'"))->on('user_provider.provider_id', '=', DB::expr("'".$provider_id."'"))->where('skp_user.email','=',$email)->order_by('user_provider.provider', 'DESC')->limit(1)->find();
	}

	public static function login($email, $password)
	{
		$max_ip_email_failures	= self::MAX_LOGIN_ATTEMPTS;	// Attempts relative to the sameIP + Email
		$max_email_failures		= 2 * $max_ip_email_failures;	//	Attempts relative to the same Email
		$max_ip_failures			= 4 * $max_email_failures;		//	Attempts relative to the same IP

		$retry_interval_seconds = self::LOGIN_RETRY_INTERVAL_MIN * 60;

		$cache			= Skp_Cache::instance();
		$key_prefix		= 'login_failed_attemps_';
		$key_ip_email	= $key_prefix.sha1(strtoupper($_SERVER['REMOTE_ADDR'].$email));
		$key_email		= $key_prefix.sha1(strtoupper($email));
		$key_ip			= $key_prefix.sha1(strtoupper($_SERVER['REMOTE_ADDR']));

		$counter_failed_attemps_ip_email	= $cache->get($key_ip_email, 0);
		$counter_failed_attemps_email		= $cache->get($key_email, 0);
		$counter_failed_attemps_ip			= $cache->get($key_ip, 0);

		if ($counter_failed_attemps_ip_email >= $max_ip_email_failures 
			OR $counter_failed_attemps_email >= $max_email_failures 
			OR $counter_failed_attemps_ip >= $max_ip_failures)
		{ 
				throw new Skp_Exception(__('g.login.maximum_attempts_reached', array(':minutes' => floor($retry_interval_seconds/60))));
		}
		$user = ORM::factory('Skp_User')->where('email', '=', $email)->and_where('password', '=', $password)->find();
		if ($user->loaded() === FALSE)
		{
			$cache->set($key_ip_email, ++$counter_failed_attemps_ip_email, $retry_interval_seconds);
			$cache->set($key_email, ++$counter_failed_attemps_email, $retry_interval_seconds);
			$cache->set($key_ip, ++$counter_failed_attemps_ip, $retry_interval_seconds);
			(max($counter_failed_attemps_ip_email, $counter_failed_attemps_email, $counter_failed_attemps_ip) > 2) AND sleep(3);
			usleep(rand(0,1000000));
		}else{
			$cache->delete($key_ip_email);
			$cache->delete($key_email);
		}
		return $user;
	}

	/* Old code removed with GDPR because "logs" will been removed
	public function getLastLog($at_datetime = NULL)
	{
		isset($this->_cache['lastlog']) OR $this->_cache['lastlog'] = array();
		if ($at_datetime != NULL)
		{
			if (!isset($this->_cache['lastlog'][$at_datetime->format('Y-m-d H:i:s')]))
			{
				$this->_cache['lastlog'][$at_datetime->format('Y-m-d H:i:s')] = $this->logs->where('login','<=', $at_datetime->format('Y-m-d H:i:s'))->order_by('id','DESC')->find();
			}
			return $this->_cache['lastlog'][$at_datetime->format('Y-m-d H:i:s')];
		}else{
			if (!isset($this->_cache['lastlog']['lastlog']))
			{
				$this->_cache['lastlog']['lastlog'] = $this->logs->order_by('id','DESC')->find();
			}
			return $this->_cache['lastlog']['lastlog'];
		}
	}
	*/

	public function getLastActivity($merchant_id = NULL)
	{
		$um = $this->get('user_merchants');
		$merchant_id === NULL OR $um->where('merchant_id', '=', $merchant_id);
		$um = $um->order_by('last_visit_date', 'DESC')->limit(1)->find();
		$last_visit = $um->get('last_visit_date');
		if ($last_visit != NULL)
		{
			return DateTime::createFromFormat('Y-m-d H:i:s', $last_visit, new DateTimeZone('UTC'));
		}
		return NULL;
	}

	public function logout()
	{
		/* Old code removed with GDPR because "logs" will been removed
		$lastlog = $this->getLastLog();
		if ($lastlog->loaded())
		{
			$lastlog->logout = gmdate('Y-m-d H:i:s');
			$lastlog->save();
		} */
	}

	public function updateLogin()
	{
		$this->save();
	}

	public function getMyPerson()
	{
		return $this->get('persons')->where('relation','=','Self')->find();
	}
	
	public function isMerchantAdmin($merchant)
	{
		return $this->isMerchantType($merchant, array(MerchantUser::USER_TYPE_ADMIN, MerchantUser::USER_TYPE_SUPERADMIN));
	}

	public function isMerchantSuperAdmin($merchant)
	{
		return $this->isMerchantType($merchant, MerchantUser::USER_TYPE_SUPERADMIN);
	}

	public function isMerchantType($merchant, $types = NULL)
	{
		if ($types == NULL) return FALSE;
		!is_array($types) AND $types = array($types);
		$role = $this->get('merchantusers')->getUserType($merchant->pk());
		if ($this->get('is_admin') == 'Y' AND in_array($role, $types))
		{
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function save(Validation $validation = NULL)
	{
		$this->setStatus();
		$person = $this->getMyPerson();
		$save_person = (sizeof($this->changed()) > 0 OR $person->loaded() == FALSE);
		parent::save($validation);
		if ($save_person)
		{	
			$person->createForUser(array('id'=>$person->id, 'user_id'=>$this->pk(), 'copy_userdata'=>$this));
			$person->save();
		}
		$basket_items_of_myself = ORM::factory('Skp_OrderItem')->join('order')->on('order.id', '=', 'skp_orderitem.order_id')->where('order.user_id', '=', $this->pk())->and_where('order.type', '=', Order::TYPE_BASKET)->and_where('skp_orderitem.person_id' , '=', $person->pk())->find_all();
		foreach ($basket_items_of_myself as $oi)
		{
			$oi->first_name = $person->first_name;
			$oi->middle_name = $person->middle_name;
			$oi->last_name = $person->last_name;
			$oi->birth_date = $person->birth_date;
			$oi->save();
		}
		
	}

	public function addProvider($provider_name, $provider_id, $provider_profile_name = NULL)
	{
		$found = false;
		foreach ($this->providers->find_all() as $provider)
		{
			if ($provider->provider == $provider_name && $provider->provider_id == $provider_id)
			{
				$found = true;
				Log::instance()->add(LOG_DEBUG,'UserProvider found in our DB: (UserProvider->id = '.$provider->id.' )');
				break;
			}
		}
		if ($found == true AND $provider->provider_profile_name != $provider_profile_name)
		{
			$provider->provider_profile_name = $provider_profile_name;
			$provider->save();
		}
		if (! $found)
		{
			$new_provider = ORM::factory('Skp_UserProvider');
			$new_provider->user_id = $this->id;
			$new_provider->provider = $provider_name;
			$new_provider->provider_id = $provider_id;
			$new_provider->provider_profile_name = $provider_profile_name;
			$new_provider->save();
		}
	}

	public function addressComplete()
	{
		if ($this->street_num == NULL) return FALSE;
		if ($this->street == NULL) return FALSE;
		if ($this->city == NULL) return FALSE;
		if ($this->zip == NULL) return FALSE;
		if ($this->country == NULL) return FALSE;
		if ($this->state_prov == NULL) return FALSE;
		return TRUE;
		$stateField = UserMerchantPosition::getStateField($this->country);
		if ($stateField != NULL AND $this->$stateField == NULL) return FALSE;
	}
	
	public function mergeData($vuid)
	{
		$vuser = ORM::factory('Skp_User',$vuid);
		if ($vuser->loaded() === TRUE AND $vuser->pk() != $this->pk())
		{
			Log::instance()->add(LOG_DEBUG,'User found:'.$vuser->id);
			foreach ($vuser->get('user_merchant_positions')->find_all() as $pos)
			{
				$pos->set('user', $this);
				$pos->save();
			}
			foreach ($vuser->persons->find_all() as $pers)
			{
				if ($pers->relation != 'Self' AND TRUE)   // Do not overwrite duplicates
				{
					$pers->set('user', $this);
					$pers->save();
				}
			}
			foreach ($vuser->cards->find_all() as $card)
			{
				if (TRUE)	// Do not overwrite duplicates
				{
					$card->set('user', $this);
					$card->save();
				}
			}
			
			LOG::instance()->add(LOG_DEBUG,'I am userid: '.$this->pk());
			foreach ($vuser->orders->find_all() as $vord)
			{
				$current_baskets = $this->orders->where('type', '=', Order::TYPE_BASKET)->and_where('merchant_id','=',$vord->merchant_id)->find_all();
				foreach($current_baskets as $cb)
				{
					Log::instance()->add(LOG_DEBUG,'OLD BASKET for user:'.$vord->user->pk().' found:'.$vord->id.' ('.$vord->amount.'  '.$vord->lastupdate_date.' > '.$cb->lastupdate_date.')');
					Log::instance()->add(LOG_DEBUG,'CURRENT BASKET for user:'.$cb->user->pk().' found:'.$cb->id.' ('.$cb->amount.' - '.$cb->lastupdate_date.')');
					if ($vord->get('type') == Order::TYPE_BASKET)
					{
						if ($vord->items->count_all() > 0)
						{
							Log::instance()->add(LOG_DEBUG,'CHANGING ORDER OWNER to:'.$this->pk());
							$vord->set('user', $this);
							$vord->save();
							$cb->delete();
						}
						else
						{
							Log::instance()->add(LOG_DEBUG,'DELETING OLD BASKET:'.$this->pk());
							$vord->delete();
						}
					}
				}
				
				if ($vord->loaded() AND $vord->get('status') != Order::TYPE_BASKET)
				{
					Log::instance()->add(LOG_DEBUG,'CHANGING ORDER OF ORDER: '.$vord->pk().' TO USER:'.$this->pk());
					$vord->set('user', $this);
					$vord->save();
				}
			}
			/* Old code removed with GDPR because "logs" will been removed
			foreach ($vuser->logs->find_all() as $log)
			{
				$log->set('user', $this);
				$log->save();
			} */
			foreach ($vuser->get('user_merchant_visits')->find_all() as $umv)
			{
				$umv->set('user', $this);
				$umv->save();
				// here we update the user_merchant record for this user/merchant
				$um = $this->get('user_merchants')->where('merchant_id', '=', $umv->get('merchant_id'))->find();
				if ($um->loaded())
				{
					$um->set('last_visit_date', max($umv->get('visit_end'), $um->get('last_visit_date')));
					$um->set('num_visit', $um->get('num_visit')+1);
					$um->save();
				}
			}
			$vuser->delete();
		}
	}
	
	public function getAgeRange()
	{
		$age = $this->getAge();
		if ($age == NULL)
		{
			$age_range = 'not set';
		}
		else if ($age < 20) 
		{
			$age_range = '<20';
		} 
		else if ($age < 30) 
		{
			$age_range = '20-30';
		} 
		else if ($age < 40) 
		{
			$age_range = '30-40';
		} 
		else if ($age < 50) 
		{
			$age_range = '40-50';
		}
		else $age_range = '>50';
		return $age_range;
	}
	
	public function getAge($at_day = NULL)
	{
		$at_day = $at_day == NULL ? new DateTime('today') : $at_day;
		$at_day = (!is_object($at_day) || get_class($at_day) != 'DateTime') ? DateTime::createFromFormat('Y-m-d', $at_day) : $at_day;
		return $this->getBirthDate() != NULL ? $this->getBirthDate()->diff($at_day)->y : NULL;
	}
	
	public function getBirthDate()
	{
		if ($this->birth_date == NULL)
		{
			return NULL;
		} else {
			return DateTime::createFromFormat('Y-m-d H:i:s', $this->birth_date.' 00:00:00');
		}
	}
	
	public function getCreditCard($paychannel, $credit_card_id = NULL)
	{
		return $this->credit_cards->where('payment_channel_id','=', $paychannel->pk())->and_where('id', '=', $credit_card_id)->find();
	}
	
	public function getMerchantCreditCards($merchant)
	{
		return $this->get('credit_cards')->join('payment_channel')->on('skp_creditcard.payment_channel_id', '=', 'payment_channel.id')->where('payment_channel.merchant_id','=',$merchant->pk())->find_all();
	}
	
	public function getLastUserMerchantPosition(Merchant $merchant = NULL, Session $session = NULL)
	{
		$last_user_positions = UserMerchantPosition::getFromCache('last_user_merchant_positions_userid_'.$this->pk());
//die(print_r($last_user_positions,TRUE));
		if (isset($last_user_positions[$merchant->pk()])) return $last_user_positions[$merchant->pk()];
		$result = $this->get('user_merchant_visits');
		$merchant != NULL AND $result->where('merchant_id', '=', $merchant->pk());
		$session != NULL AND $result->where('session_id', '=', $session->id());
		$umv = $result->order_by('visit_end','DESC')->find();
		$last_user_positions[$merchant->pk()] = $umv->get('user_merchant_positions')->order_by('when','DESC')->find();
		$last_user_positions[$merchant->pk()]->loaded() AND UserMerchantPosition::setIntoCache('last_user_merchant_positions_userid_'.$this->pk(), $last_user_positions);
		return $last_user_positions[$merchant->pk()];
	}
	
	public function __toString()
	{
		return $this->first_name.($this->middle_name!=NULL?' '.$this->middle_name:'').' '.$this->last_name;
	}
	
	public static function restorePassword($email, $token, $new_password, $new_password_confirm)
	{
		$user = ORM::factory('Skp_User')->where('email', '=', $email)->find();
		if ($user->loaded() == FALSE OR $token == NULL OR $user->getToken() != $token) 
		{
			throw new Skp_Exception('User_Not_Found_Or_Invalid_Token', 10); 
		}
		if ($new_password == $new_password_confirm AND $new_password != NULL AND strlen($new_password) >= 3)
		{
			$user->set('password', $new_password);
			$user->setToken(NULL);
			$user->save();
		} else {
			throw new Skp_Exception('Password_Mismatch_Or_Too_Short', 20); 
		}
	}
	
	public function get_profile_list($model = InvoiceReceiptProfile::MODEL_INVOICE)
	{
		$invoice_receipt_profiles = $this->get('invoice_receipt_profiles')->where('model', '=', $model)->find_all();
		$profile_list = array();
		foreach ($invoice_receipt_profiles as $irp)
		{
			$profile_list[$irp->pk()] = $irp->getExtra('profile_name');			
		}
		return $profile_list;
	}
	
	public function check_forwarded_email_limit($order_item_id, $merchant) {
		$time_out_sec = $merchant->getSkpParamConfig(array('forwarded_tickets', 'order_item_send_email_period'));
		$max_messages = $merchant->getSkpParamConfig(array('forwarded_tickets', 'order_item_send_email_messages_limit_per_period'));
		$email_forwarded = Arr::get($this->get('user_extra'), 'email_forwarded', array());
		if (isset($email_forwarded[$order_item_id])) {
			if ($email_forwarded[$order_item_id]['count_sent'] < $max_messages) {
				$email_forwarded[$order_item_id]['count_sent']++;
				$this->setUserExtra('email_forwarded', $email_forwarded);
				$this->save();
				return TRUE;
			} else {
				$datetime_first_sent = DateTime::createfromformat('Y-m-d H:i:s', $email_forwarded[$order_item_id]['datetime_first_sent']);
				$now = new DateTime('now');
				if (($now->getTimestamp() - $datetime_first_sent->getTimestamp()) > $time_out_sec) {
					$email_forwarded[$order_item_id]['count_sent'] = 1;
					$email_forwarded[$order_item_id]['datetime_first_sent'] = $now->format('Y-m-d H:i:s');
					$this->setUserExtra('email_forwarded', $email_forwarded);
					$this->save();
					return TRUE;
				} else {
					$excep = new Skp_Exception('max number of email sent per order_item limit reached');
					$excep_data = new stdClass;
					$excep_data->minutes = ceil(abs($time_out_sec-($now->getTimestamp()-$datetime_first_sent->getTimestamp()))/60);
					$excep->setData($excep_data);
					throw $excep;
				}
			}
		} else {
			$new_email_forwarded = new stdClass;
			$new_email_forwarded->count_sent = 1;
			$now = new DateTime('now');
			$new_email_forwarded->datetime_first_sent = $now->format('Y-m-d H:i:s');
			$email_forwarded[$order_item_id] = $new_email_forwarded;
			$this->setUserExtra('email_forwarded', $email_forwarded);
			$this->save();
			return TRUE;
		}
	}

	public function getB2BUserRole($b2b_company_id)
	{
		$cu = $this->get('companyusers')->where('b2b_company_id', '=', $b2b_company_id)->find();
		return $cu->get('user_role');
	}

	public function getB2BCompany($merchant, $also_not_active = FALSE)
	{
		$b2bc_result = $this->get('b2b_companies')->where('merchant_id', '=', $merchant->pk());
		if ($also_not_active == FALSE) $b2bc_result->and_where('active', '=', 'Y');
		$b2bc_result = $b2bc_result->find_all();
		$b2bc_list = $b2bc_result->as_array('id');
		$active_b2b_company_id = $this->getActiveB2BCompanyId($merchant->pk());
		$b2bc = Arr::get($b2bc_list, $active_b2b_company_id);
		if ($active_b2b_company_id == NULL OR $b2bc == NULL)
		{
			$b2bc = $b2bc_result->current();
		}
		if ($b2bc != NULL AND $b2bc->get('active') == 'Y' OR $also_not_active == TRUE)
		{
			return $b2bc;
		}else{
			return ORM::factory('Skp_B2BCompany');
		}
	}
	
	public function getB2BCompaniesByRole($merchant_id, $roles = array())
	{
		$b2b_companies = $this->get('b2b_companies')
			->where('merchant_id', '=', $merchant_id)
			->and_where('active' , '=', 'Y');
		if (count($roles) > 0)
		{
			$b2b_companies->and_where('user_role' , 'IN', $roles);
		}
		return $b2b_companies->find_all();
	}
	
	public function initUser($merchant)
	{
		// Add Card from Cookie
		if (isset($_COOKIE['new_card']) AND $_COOKIE['new_card'] != NULL)
		{
			$card_cookie_array = json_decode(Arr::get($_COOKIE, 'new_card'), TRUE);
			unset($_COOKIE['new_card']);
			setcookie('new_card', '', time()-3600, '/', $merchant->get('domain'));
			Card::createNewCard($merchant, $this, $card_cookie_array);
		}
		// Reset Basket reminder
		$this->getExtra('m_'.$merchant->pk().'-basket_reminded') == 'Y' AND $this->resetReminder($merchant, 'basket_reminded');
	}
	
	public function resetReminder($merchant, $reminder_type = User::REMINDER_ORDER_TO_BE_PAYED)
	{
		$this->setUserExtra('m_'.$merchant->pk().'-'.$reminder_type, NULL);
		$this->save();
	}

	public function sendReminders($merchant, $reminders_array = User::REMINDERS_LIST, $params = array())
	{
		$result = FALSE;
		// Send Reminder about pending payments
		if (in_array(User::REMINDER_ORDER_TO_BE_PAYED, $reminders_array) AND $this->getExtra('m_'.$merchant->pk().'-'.User::REMINDER_ORDER_TO_BE_PAYED) == 'Y')
		{
			$order_tbp = $this->getOrderToBePayed($merchant, $params);
			if ($order_tbp->loaded())
			{
				$result = $order_tbp->sendErrorReceipt();
			}
			if ($result)
			{
				$this->resetReminder($merchant, User::REMINDER_ORDER_TO_BE_PAYED);
			}
		}
		return $result;
	}
	
	public function getOrderToBePayed($merchant, $params = array())
	{
		$order_id = Arr::get($params, 'order_id');
		$order_tbp = $this->get('orders')->where('merchant_id','=',$merchant->pk())->and_where('status', '=', Order::STATUS_TO_BE_PAYED);
		if ($order_id != NULL)
		{
			$order_tbp = $order_tbp->and_where('id', '=', $order_id);
		} else {
			$order_tbp = $order_tbp->order_by('id','DESC');
		}
		return $order_tbp->find();
	}
	
	// Return an array of notification messages that will be displayed in different areas of frontend
	public function getNotificationMessages($merchant)
	{
		$messages = array();
		// If the user has an order TO BE PAYED we add the order to messages list
		if ($this->getExtra('m_'.$merchant->pk().'-'.User::REMINDER_ORDER_TO_BE_PAYED) == 'Y')
		{
			$order_tbp = $this->getOrderToBePayed($merchant);
			$this->resetReminder($merchant, User::REMINDER_ORDER_TO_BE_PAYED);
			$order_tbp->loaded() AND $messages[User::REMINDER_ORDER_TO_BE_PAYED] = $order_tbp;
		}
		/* For each $this->get('user_merchant_messages')->where('merchant_id', '=', $merchant->pk())->find_all() as $message
		*  we add in array $messages['user_merchant_messages'][$message->pk()] = $message->get('text'); 
		**/
		return $messages;
	}
	
	// Check if the user is registered to the current merchant
	public function isRegisteredUser($merchant)
	{
		return ($this->get('user_merchants')->where('merchant_id', '=', $merchant->pk())->and_where('gdpr_status', '=', UserMerchant::STATUS_REGISTERED)->count_all() == 1);
	}
	
	// Check if the user is an anonymous user for the current merchant
	public function isAnonymousUser($merchant)
	{
		return ($this->get('user_merchants')->where('merchant_id', '=', $merchant->pk())->and_where('gdpr_status', '=', UserMerchant::STATUS_DELETED)->count_all() == 1);
	}
	
	// Check if the user is new for any merchant
	public function isNewUser()
	{
		return ($this->get('user_merchants')->count_all() == 0); 
	}
	
	public function canBeAnonymized($merchant, &$return_message)
	{
		if ($this->get('b2b_companies')->where('merchant_id', '=', $merchant->pk())->count_all() > 0)
		{
			$return_message = __('status.delete_user_error.b2b_user');
			return FALSE;
		} else if ($this->isBackofficeUser())
		{
			$return_message = __('status.delete_user_error.backoffice_user');
			return FALSE;
		} else if ($this->isAnonymousUser($merchant))
		{
			$return_message = __('status.delete_user_error.anonymous_user');
			return FALSE;
		}
		return TRUE;
	}
	
	// Anonymize all User datas for the given merchant
	public function anonymizeForMerchant($merchant)
	{
		$db = Database::instance();
		// Transaction Start
		$db->begin();
		try
		{
			if ($this->isRegisteredUser($merchant))
			{
				$last_merchant = $this->get('user_merchants')->count_all() == 1;
				
				// Anonymize User profile datas
				$anon_user = $this->anonymizeUserProfileData();
				$orig_user_person = $this->getMyPerson();
				
				// Anonymize Order list + Persons Data + Cards Data
				$orders = $this->get('orders')->where('merchant_id', '=', $merchant->pk())->find_all();
				$person_replacement = array();
				$card_replacement = array();
				
				$person_replacement[$orig_user_person->pk()] = $anon_user->getMyPerson();
				foreach ($orders as $ord)
				{
					// We anonymize the order only if the order was booked, otherwise we delete it from db
					if ($ord->get('type') == Order::TYPE_ORDER AND $ord->get('status') == Order::STATUS_BOOKED)
					{
						$result_anonymization = $ord->anonymizeOrderData($anon_user, $person_replacement, $card_replacement);
						$person_replacement = $result_anonymization['p_replace'];
						$card_replacement = $result_anonymization['c_replace'];
					} else {
						$ord->delete();
					}
				}
				
				// Anonymize Credit Cards Data
				foreach ($this->getMerchantCreditCards($merchant) as $c_card)
				{
					$c_card->delete();
				}
				
				// Anonymize Invoice/Receipt Profiles Data
				foreach ($this->get('invoice_receipt_profiles')->where('merchant_id', '=', $merchant->pk())->find_all() as $ir_profile)
				{
					$ir_profile->anonymizeInvoiceReceiptProfile($anon_user);
				}
				
				// Anonymize Invoice/Receipt Orders Data
				foreach ($this->get('invoice_receipt_orders')->where('merchant_id', '=', $merchant->pk())->find_all() as $ir_order)
				{
					$ir_order->anonymizeInvoiceReceiptOrder($anon_user);
				}
				
				// Anonymize User Messages Data
				foreach ($this->get('messages')->where('merchant_id', '=', $merchant->pk())->find_all() as $msg)
				{
					$msg->anonymizeMessageData($anon_user);
				}
				
				// Anonymize User Downloads Data
				foreach ($this->get('user_downloads')->where('merchant_id', '=', $merchant->pk())->find_all() as $u_download)
				{
					$u_download->set('user_id', $anon_user->pk());
					$u_download->save();
				}
				
				// Anonymize User Positions Data
				foreach ($this->get('user_merchant_positions')->where('merchant_id', '=', $merchant->pk())->find_all() as $position)
				{
					$position->anonymizeUserPosition($anon_user);
				}
				
				// Anonymize User Merchant Visit Data
				foreach ($this->get('user_merchant_visits')->where('merchant_id', '=', $merchant->pk())->find_all() as $visit)
				{
					$visit->set('user_id', $anon_user->pk());
					$visit->save();
				}
				
				// Anonymize User Merchant Optins Data
				foreach ($this->get('user_merchant_optins')->where('merchant_id', '=', $merchant->pk())->find_all() as $optin)
				{
					//~ $optin->delete();
				}
				
				// Anonymize User Delete Data
				$u_delete = $this->get('user_deletes')->where('merchant_id', '=', $merchant->pk())->find();
				$u_delete->set('user_id', $anon_user->pk());
				$u_delete->set('delete_status', UserDelete::STATUS_DELETED);
				$u_delete->set('delete_datetime', gmdate('Y-m-d H:i:s'));
				$u_delete->save();
				
				// Anonymize User Merchant relation
				$user_merchant = $this->get('user_merchants')->where('merchant_id', '=', $merchant->pk())->find();
				$user_merchant->set('user_id', $anon_user->pk());
				$user_merchant->set('gdpr_status', UserMerchant::STATUS_DELETED);
				$user_merchant->save();
				
				// Actions if anonymizing for last merchant
				if ($last_merchant)
				{
					// If we are anonymizing the user for the last merchant we DELETE the original user and all records related to the user will be deleted in CASCADE.
					foreach ($this->get('cards')->find_all() as $card)
					{
						$card->delete_forced();
					}
					$this->delete_forced();
				}
				
				$db->commit();
			} else {
				return TRUE;
			}
		} catch (Exception $e)
		{
			$db->rollback();
			throw $e;
			return FALSE;
		}
	}
	
	// Anonymize user profile datas
	public function anonymizeUserProfileData()
	{
		$anon_user = ORM::factory('Skp_User');
		$anon_user->save();
		
		$user_columns = $this->list_columns();
		$gdpr_fields = ContactProfile::$gdpr_fields;
		foreach (ContactProfile::$list_info as $profile_field)
		{
			$field_value = isset($user_columns[$profile_field]) ? $this->get($profile_field) : $this->getExtra($profile_field);
			
			$anon_value = in_array($profile_field, $gdpr_fields) ? Skp_Helper::anonymizeValue($field_value, $profile_field) : $field_value;
			(isset($user_columns[$profile_field])) AND $anon_user->set($profile_field, $anon_value) OR $anon_user->setUserExtra($profile_field, $anon_value);
		}
		$anon_user->set('last_language', $this->get('last_language'));
		$anon_user->save();
		return $anon_user;
	}

	public function isBackofficeUser()
	{
		return $this->get('merchants')->count_all() >0;
	}

	public function getGDPRType(Merchant $merchant)
	{
		return $this->get('user_merchants')->where('merchant_id', '=', $merchant->pk())->find()->get('user_type');
	}

	public function getOptin($optin_id, $merchant = NULL)
	{
		$user_merchant_optin = $this->get('user_merchant_optins')->where('optin_id', '=', $optin_id);
		($merchant != NULL) AND $user_merchant_optin->and_where('merchant_id', '=', $merchant->pk());
		return $user_merchant_optin->find();
	}

	public function getOptinStatus($optin_id, $merchant = NULL)
	{
		$optin = $this->getOptin($optin_id, $merchant);
		if ($optin->get('accepted') === NULL) return NULL;
		if ($optin->get('accepted') == 1) return 'Y';
		if ($optin->get('accepted') == 0) return 'N';
	}

	public function setOptinStatus($optin, $accepted)
	{
		$user_merchant_optin = $this->get('user_merchant_optins')->where('optin_id', '=', $optin->pk())->find();
		$user_merchant_optin->set('user', $this);
		$user_merchant_optin->set('merchant', $optin->get('merchant'));
		$user_merchant_optin->set('merchant_optin', $optin);
		if ($user_merchant_optin->get('accepted') != 1 AND $accepted)
		{
			$user_merchant_optin->set('accepted', 1);
			$user_merchant_optin->set('last_change', gmdate('Y-m-d H:i:s'));
			$user_merchant_optin->save();
		}else if ($user_merchant_optin->get('accepted') !== 0 AND $accepted == FALSE){
			$user_merchant_optin->set('accepted', 0);
			$user_merchant_optin->set('last_change', gmdate('Y-m-d H:i:s'));
			$user_merchant_optin->save();
		}
	}

	public function getAllOptinsStatus($merchant)
	{
		$thread_cache_key = 'merchant_'.$merchant->pk().'_optins';
		if (Skp_ThreadCache::get($thread_cache_key) === NULL)
		{
			Skp_ThreadCache::set($thread_cache_key, $merchant->getOptins());
		}
		$merchant_optins = Skp_ThreadCache::get($thread_cache_key);
		$optins_status_array = array();
		foreach ($merchant_optins as $optin)
		{
			$optin_data = new stdClass;
			$user_optin = $this->getOptin($optin->pk(), $merchant);

			$optin_data->optin_id = $optin->pk();
			$optin_data->optin_name = $optin->get('name');
			$optin_data->status = $user_optin->get('accepted');
			$optin_data->last_changed = $user_optin->get('last_change');

			$optins_status_array[] = $optin_data;
		}
		return $optins_status_array;
	}

	public function hasDeleteRequest($merchant)
	{
		return $this->get('user_deletes')->where('merchant_id', '=', $merchant->pk())->and_where('delete_status', '=', UserDelete::STATUS_REQUESTED)->count_all() > 0;
	}

	public function getLastProcessedDownloadRequest($merchant)
	{
		return $this->get('user_downloads')->where('merchant_id', '=', $merchant->pk())->and_where('download_status', '=', UserDownload::STATUS_DELIVERED)->order_by('id', 'DESC')->find();
	}

	public function hasDownloadRequestPending($merchant)
	{
		return $this->get('user_downloads')->where('merchant_id', '=', $merchant->pk())->and_where('download_status', '=', UserDownload::STATUS_REQUESTED)->find()->loaded();
	}

	public function deleteDeleteRequest($merchant)
	{
		$user_delete = $this->get('user_deletes')->where('merchant_id', '=', $merchant->pk())->and_where('delete_status', '=', UserDelete::STATUS_REQUESTED)->find();
		$user_delete->loaded() AND $user_delete->delete();
	}
	
	public function getLastVisit(Merchant $merchant)
	{
		return $this->get('user_merchant_visits')->where('merchant_id', '=', $merchant->pk())->order_by('id','DESC')->limit(1)->find();
	}

	public function getCurrentVisit(Session $session, Merchant $merchant)
	{
		return $this->get('user_merchant_visits')->where('session_id','=', $session->id())->and_where('merchant_id', '=', $merchant->pk())->limit(1)->find();
	}

	public function logVisit(Session $session, Merchant $merchant, Request $request_obj, StdClass $device)
	{
		$now_utc = gmdate('Y-m-d H:i:s');
		$device_type = NULL;
		$device_platform = NULL;
		// here we convert the device to 'D','T' or 'M'
		if ($device != NULL)
		{
			if (isset($device->device_type))
			{
				$device_type = $device->device_type;
			}
			isset($device->device_platform) AND $device_platform = $device->device_platform;
		}
		$current_visit = $this->getCurrentVisit($session, $merchant);
		$user_merchant = $this->get('user_merchants')->where('merchant_id', '=', $merchant->pk())->find();
		if ($current_visit->loaded() == FALSE)
		{
			// This is a new visit
			$current_visit->set('merchant', $merchant);
			$current_visit->set('user', $this);
			$current_visit->set('session_id', $session->id());
			$current_visit->set('visit_start', $now_utc);
			// if the user is registered for the merchant we update the number of visits
			$user_merchant->loaded() AND $user_merchant->set('num_visit', $user_merchant->get('num_visit')+1);
		}
		// if the user is registered for the merchant we update the last visit date
		$user_merchant->loaded() AND $user_merchant->set('last_visit_date', $now_utc)->save();
		$current_visit->set('visit_end', $now_utc);
		$current_visit->setDeviceType($device_type);
		$current_visit->set('device_platform', $device_platform);
		$current_visit->save();
	}

	public function geoLocalizationEnabled(Merchant $merchant)
	{
		// in the future we could use this function to disable geoloc if the merchant or the user or the user for this merchant doesn't want
		return TRUE;
	}

	public function shouldGetPosition(Merchant $merchant, Session $session = NULL)
	{
		if ($this->geoLocalizationEnabled($merchant) == FALSE) return FALSE;
		$last_position = $this->getLastUserMerchantPosition($merchant, $session);
		if ($last_position->loaded())
		{
			$last_tracked = DateTime::createFromFormat('Y-m-d H:i:s', $last_position->when);
			$now = new DateTime();
			return ($now->getTimestamp()-$last_tracked->getTimestamp())> Kohana::$config->load('skiperf')->get('position_timeout_secs')? TRUE: FALSE;
		}else{
			return TRUE;
		}
	}
	
	public function getMerchantMainOrderItems(Merchant $merchant)
	{
		$merchant_orders = $this->get('orders')->where('merchant_id', '=', $merchant->pk())->and_where('status', '=', Order::STATUS_BOOKED)->and_where('related_order_id', 'IS', NULL)->find_all();
		$merchant_orderitems = array();
		foreach ($merchant_orders as $order)
		{
			if (TRUE OR !$order->isCancelledOrder())
			{
				foreach ($order->get('items')->where('sub_orderitem_of_id','is',NULL)->find_all() as $oi)
				{
					$merchant_orderitems[] = $oi;
				}
			}
		}
		return $merchant_orderitems;
	}
	
	public function getDeletionSuggestion(Merchant $merchant)
	{
		$query = DB::select('oi.id','oi.validity_date', DB::expr('IF (skp_oi.validity_date>=CURDATE(),"FUTURE","PAST") AS vdate'))->from(array('order_item','oi'))->join(array('order','o'))->on('oi.order_id', '=', 'o.id')->where('o.merchant_id','=', $merchant->pk())->and_where('o.user_id', '=', $this->pk())->and_where('o.type', '=', Order::TYPE_ORDER)->order_by('validity_date', 'DESC')->limit(1);
		$order_items = $query->as_assoc()->execute();
		if (count($order_items) == 0)
		{
			return UserDelete::SUGGESTION_OK;
		} else if ($order_items[0]['vdate'] == 'FUTURE') {
			return UserDelete::SUGGESTION_KO;
		} else {
			return UserDelete::SUGGESTION_CHECK;
		}
	}

	public function getActiveB2BCompanyId($merchant_id)
	{
		if ($merchant_id != NULL)
		{
			return Arr::get($this->getExtra('active_b2b_company_ids', array()), $merchant_id);
		} else
		{
			return NULL;
		}
	}

	public function setActiveB2BCompanyId($merchant_id, $b2b_company_id)
	{
		if ($merchant_id != NULL AND $b2b_company_id != NULL)
		{
			$active_b2b_company_ids = $this->getExtra('active_b2b_company_ids', array());
			$active_b2b_company_ids[$merchant_id] = $b2b_company_id;
			$this->setUserExtra('active_b2b_company_ids', $active_b2b_company_ids);
			$this->save();
		}
	}

	public function createExportRecords($merchant, $params = array())
	{
		$now_utc = gmdate('Y-m-d H:i:s');
		$export_connectors = Arr::get($params, 'active_export_connectors', $merchant->getUserExportConnectors());
		foreach ($export_connectors as $exp_c)
		{
			$user_export_record = $exp_c->get('export_records')->where('object_class', '=', Arr::get(ExportConnector::$record_types, ExportConnector::RECORD_TYPE_USER))->and_where('object_id', '=', $this->pk())->find();
			if (!$user_export_record->loaded())
			{
				$user_export_record->set('merchant_id', $merchant->pk());
				$user_export_record->set('export_connector_id', $exp_c->pk());
				$user_export_record->set('object_class', Arr::get(ExportConnector::$record_types, ExportConnector::RECORD_TYPE_USER));
				$user_export_record->set('object_id', $this->pk());
			}
			$user_export_record->set('export_status', ExportConnectorRecord::STATUS_NEW);
			$user_export_record->set('record_datetime', $now_utc);
			$user_export_record->save();
		}
	}
}

