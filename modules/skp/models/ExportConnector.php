<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;

class ExportConnector extends Model {

	protected $table = 'export_connector';
	public $timestamps = false;
	protected $casts = [
		'connector_extra' => 'array',
	];

	public function merchant() {
		$this->belongsTo(Merchant::class);
	}

	public function products() {
		return $this->hasManyThrough(
			Product::class,
			ExportConnectorProduct::class,
			'export_connector_id',
			'product_id'
		);
	}

	public function export_records() {
		return $this->hasMAny(
			ExportConnectorRecord::class,
			'export_connector_id'
		);
	}

}