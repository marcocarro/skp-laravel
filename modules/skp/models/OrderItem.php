<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class OrderItem extends Model implements Interfaces_ValidityDate{

	protected $table = 'order_item';
	protected $_table_columns = Array(
		'id' => Array(),
		'sub_orderitem_of_id' => Array(),
		'order_id' => Array(),
		'sku_id' => Array(),
		'pool_id' => Array(),
		'card_id' => Array(),
		'person_id' => Array(),
		'promocode_id' => Array(),
		'product_name' => Array(),
		'pricecategory_name' => Array(),
		'original_connector_class' => Array(),
		'original_connector_id' => Array(),
		'card_type' => Array(),
		'card_serial' => Array(),
		'first_name' => Array(),
		'middle_name' => Array(),
		'last_name' => Array(),
		'birth_date' => Array(),
		'validity_date' => Array(),
		'qty' => Array(),
		'single_amount' => Array(),
		'vat_id' => Array(),
		'vat_perc' => Array(),
		'vat_amount' => Array(),
		'total_amount' => Array(),
		'currency' => Array(),
		'status' => Array(),
		'status_extra' => Array(),
		'related_orderitem_id' => Array(),
		'orderitem_extra' => Array(),
		'person_data' => Array(),
		'person_photo' => Array(),
	);
	protected $_serialize_columns = array('product_name','pricecategory_name','status_extra','orderitem_extra','person_data');
	protected $_belongs_to = array(
		'order' => array(
			'model' => 'Skp_Order',
			'foreign_key' => 'order_id',
		),
		'sku' => array(
			'model' => 'Skp_Sku',
			'foreign_key' => 'sku_id',
		),
		'card' => array(
			'model' => 'Skp_Card',
			'foreign_key' => 'card_id',
		),
		'person' => array(
			'model' => 'Skp_Person',
			'foreign_key' => 'person_id',
		),
		'mainorderitem' => array(
			'model' => 'Skp_OrderItem',
			'foreign_key' => 'sub_orderitem_of_id',
		),
		'pool' => array(
			'model' => 'Skp_Pool',
			'foreign_key' => 'pool_id',
		),
		'vat' => array(
			'model' => 'Skp_Vat',
			'foreign_key' => 'vat_id',
		),
		'promocode' => array(
			'model' => 'Skp_Promocode',
			'foreign_key' => 'promocode_id',
		),
	);

	protected $_has_one = array(
		'booking' => array(
			'model' => 'Skp_Booking',
			'foreign_key' => 'order_item_id',
		),
		'related_orderitem' => array(
			'model' => 'Skp_OrderItem',
			'foreign_key' => 'related_orderitem_id',
		),
	);

	protected $_has_many = array(
		'suborderitems' => array(
			'model' => 'Skp_OrderItem',
			'foreign_key' => 'sub_orderitem_of_id',
		),
		'tags' => array(
			'model' => 'Skp_OrderItemTag',
			'foreign_key' => 'order_item_id',
		),
		'export_records' => array(
			'model' => 'Skp_ExportConnectorRecord',
			'foreign_key' => 'orderitem_id'
		),
		'identifications' => array(
			'model' => 'Skp_OrderItemIdentification',
			'foreign_key' => 'orderitem_id'
		),
	);

	const STATUS_OK = 'OK';
	const STATUS_ERROR = 'ERROR';
	const STATUS_PICKED = 'PICKED';

	private $_canBeDeletedFromBasket;

	private $_isVirtualOrderItem;
	
	public function delete()
	{
		$success = ($this->get('booking')->loaded() == FALSE OR $this->get('booking')->delete());
		$success AND ($success = parent::delete());
		return $success;
	}
	public function setStatusExtra($extra_name, $extra_value)
	{
		$e = $this->get('status_extra');
		if ($extra_value != NULL)
		{
			$e[$extra_name] = $extra_value;
		}else{
			unset($e[$extra_name] );
		}
		$this->set('status_extra', $e);
	}

	public function getSinglePrice()
	{
		return new Skp_Money($this->single_amount, $this->currency);
	}

	public function getTotalPrice()
	{
		return new Skp_Money($this->total_amount, $this->currency);
	}

	public function getTimeZone()
	{
		return $this->get('order')->get('merchant')->getTimeZone();
	}

	public function getValidityDate($time_zone = NULL)
	{
		$time_zone == NULL AND $time_zone = $this->getTimeZone();
		// returns the validity datetime in the merchant timezone
		return  $this->get('validity_date') != NULL ? DateTime::createFromFormat('Y-m-d H:i:s', $this->get('validity_date'))->setTimeZone($time_zone) : NULL;
	}
	
	public function getValidityType()
	{
		return floatval($this->get('sku')->get('product')->get('validity_value')) . ' ' . $this->get('sku')->get('product')->get('validity_unit');
	}

	public function getStartValidityDate()
	{
		$date = $this->getExtra('start_validity_date') AND $date != NULL AND $date = DateTime::createFromFormat('Y-m-d H:i:s', substr($date['date'], 0, 19), new DateTimeZone($date['timezone']));
		return $date;
	}

	public function getEndValidityDate($validity_start)
	{
		$date = $this->getExtra('end_validity_date') AND $date != NULL AND $date = DateTime::createFromFormat('Y-m-d H:i:s', substr($date['date'], 0, 19), new DateTimeZone($date['timezone']));
		return $date;
	}

	public function getDateFormat()
	{
		return $this->get('order')->get('merchant')->get('date_format');
	}

	public function getPersonBirthDate()
	{
		return ($this->birth_date != NULL ? DateTime::createFromFormat('Y-m-d', $this->birth_date) : NULL);
	}

	public function setPerson($person)
	{
		if ($person != NULL)
		{
			$this->set('person',$person);
			$this->set('first_name', $person->first_name);
			$this->set('middle_name', $person->middle_name);
			$this->set('last_name', $person->last_name);
			$this->set('birth_date', $person->birth_date);
			$contact_profile = $this->getContactProfile();
			$need_contact_profile = $this->needContactProfile();
			$person_data = NULL;
			$person_photo = NULL;
			if ($need_contact_profile)
			{
				$person_infos = $contact_profile->get('infos');
				$person_data = $person->as_array();
				if (isset($person_data['extra']) AND is_array($person_data['extra']))
				{
					foreach ($person_data['extra'] as $key=>$value)
					{
						$person_data[$key] = $value;
					}
				}
				unset($person_data['extra']);
				foreach ($person_data as $key=>$value)
				{
					if (!array_key_exists($key, $person_infos))
					{
						unset($person_data[$key]);
					}
				}
				if (Arr::get($person_data, 'photo') != NULL)
				{
					$person_photo = $person_data['photo'];
					unset($person_data['photo']);
				}
			}
			$this->set('person_data', $person_data);
			$this->set('person_photo', $person_photo);
		}
		else
		{
			$this->set('person', NULL);
			$this->set('first_name', NULL);
			$this->set('middle_name', NULL);
			$this->set('last_name', NULL);
			$this->set('birth_date', NULL);
			$this->set('person_data', NULL);
			$this->set('person_photo', NULL);
		}
	}

	public function addSubOrderItem($sku, $qty)
	{
		$added_order_items = array();
		if ($sku->isAggregate() == TRUE OR $this->suborderitems->where('sku_id','=',$sku->pk())->count_all() == 0)
		{
			$added_order_items = $this->order->addOrderItem($sku, $qty, $this->getValidityDate(), $this->pool->pk() ,$this, $this->get('promocode_id'));
			// removed $this->get('promocode_id'), a sub order item must not count as a promo product
		}
		return $added_order_items;
	}

	public function delSubOrderItem($subitem)
	{
		$deleted_ids = array();
		foreach ($this->suborderitems->where('id','=',$subitem->pk())->find_all() as $si)
		{
			$deleted_ids = array_merge($deleted_ids, $this->order->removeOrderItem($si));
		}
		return $deleted_ids;
	}

	public function deleteWithAllChildren()
	{
		$deleted_ids = array($this->pk());
		foreach ($this->get('suborderitems')->find_all() as $subitem)
		{
			$deleted_ids = array_merge($deleted_ids, $subitem->deleteWithAllChildren());
		}
		$this->delete();
		return $deleted_ids;
	}

	public function deleteAllSameWithAllChildren()
	{
		$deleted_ids = array();
		if ($this->get('sku')->isPackage() == TRUE OR $this->get('sku')->isAggregate() OR $this->isAggregable() == FALSE)
		{
			$deleted_ids = array_merge($deleted_ids, $this->deleteWithAllChildren());
		} else {
			foreach (array_merge(array($this), $this->getSimilarOrderItems()) as $item)
			{
				$deleted_ids = array_merge($deleted_ids, $item->deleteWithAllChildren());
			}
		}
		return $deleted_ids;
	}

	public function delSubOrderItemSku($sku)
	{
		foreach ($this->get('suborderitems')->where('sku_id','=',$sku->pk())->find_all() as $si)
		{
			if ($si->get('sku')->pk() == $sku->pk())
			{
				$this->order->removeOrderItem($si);
			}
		}
	}
	
	public function checkSameBooking($booking = NULL)
	{
		$same_booking = FALSE;
		$item_booking = $this->get('booking');
		(!$booking->loaded() AND !$item_booking->loaded()) AND $same_booking = TRUE;
		if (($booking->loaded() AND $item_booking->loaded() AND $booking->get('calendar_date_time_id') == $item_booking->get('calendar_date_time_id') AND $booking->get('availability_id') == $item_booking->get('availability_id')) OR (!$booking->loaded() AND !$item_booking->loaded()))
		{
			$same_booking = TRUE;
		}
		return $same_booking;
	}

	public function subOrderItemQty($sku)
	{
		$sub_item = $this->get('suborderitems')->where('sku_id', '=', $sku->pk())->find();

		if ($sub_item->loaded() == FALSE)
		{
			return 0;
		}
		return $sub_item->get('qty');
	}

	public function isWithIdentificationCard()
	{
		return $this->suborderitems->with('sku')->join('product')->on('product.id','=','sku.product_id')->join('product_type')->on('product_type.id','=','product.producttype_id')->where('product_type.type','=','IdentificationCard')->count_all()==1?TRUE:FALSE;
	}

	public function getIdentificationCardSubItem()
	{
		return $this->suborderitems->with('sku')->join('product')->on('product.id','=','sku.product_id')->join('product_type')->on('product_type.id','=','product.producttype_id')->where('product_type.type','=','IdentificationCard')->find();
	}

	public function getI18n($field, $lang = NULL, $separator = ',')
	{
		$result = '';
		$v = $this->get($field);
		if (! is_array($v))
		{
			return $result;
		}
		if ($lang == NULL)
		{
			foreach ($v as $lang=>$text)
			{
				$result .= "$lang=>'$text'".$separator;
			}
		}
		else
		{
			if (! isset($v[$lang]))
			{
				$lang = key($v); // Return first language found
			}
			if (isset($v[$lang]))
			{
				$result = $v[$lang];
			}
		}
		return $result;
	}

	public function getI18nExtra($field, $lang = NULL, $separator = ',')
	{
		$result = '';
		$v = $this->getExtra($field);
		if (! is_array($v))
		{
			return $result;
		}
		if ($lang == NULL)
		{
			foreach ($v as $lang=>$text)
			{
				$result .= "$lang=>'$text'".$separator;
			}
		}
		else
		{
			if (! isset($v[$lang]))
			{
				$lang = key($v); // Return first language found
			}
			if (isset($v[$lang]))
			{
				$result = $v[$lang];
			}
		}
		return $result;
	}

	public function getProductName($lang = NULL)
	{
		return $this->getI18n('product_name', $lang);
	}

	public function getPricecategoryName($lang = NULL)
	{
		return $this->getI18n('pricecategory_name', $lang);
	}

	public function getName($lang = NULL)
	{
		return $this->getProductName($lang).' '.$this->getPricecategoryName($lang);
	}

	public function getArrayNames()
	{
		$product_names = $this->get('product_name');
		$pricecategory_names = $this->get('pricecategory_name');

		$names = array();
		foreach ($product_names as $lang => $name)
		{
			$names[$lang] = $name . ' ' . Arr::get($pricecategory_names, $lang);
		}
		return $names;
	}

	public function filterCompatiblePersons($all_persons = array())
	{
		$filtered_persons = array();
		foreach ($all_persons as $person)
		{
			if ($this->sku->pricecategory->from_date == NULL OR $this->sku->pricecategory->from_date <= $person->birth_date)
				if ($this->sku->pricecategory->to_date == NULL OR $this->sku->pricecategory->to_date >= $person->birth_date)
					if ($this->sku->pricecategory->max_age == NULL OR $this->sku->pricecategory->max_age >= $person->getAge($this->getValidityDate()))
						if ($this->sku->pricecategory->min_age == NULL OR $this->sku->pricecategory->min_age <= $person->getAge($this->getValidityDate()))
							$filtered_persons[] = $person;
		}
		return $filtered_persons;
	}
	
	public function checkCompatibilePerson($person)
	{
		$result = FALSE;
		// if the birthdate is NULL we let the person to be used
		$pc = $this->get('sku')->get('pricecategory');
		if ($pc->get('from_date') === NULL OR $person->get('birth_date') === NULL OR $pc->get('from_date') <= $person->get('birth_date'))
				if ($pc->get('to_date') === NULL OR $person->get('birth_date') === NULL OR $pc->get('to_date') >= $person->get('birth_date'))
				{
					$age_year = $person->getAge($this->getValidityDate());
					if ($pc->get('max_age') === NULL OR $person->getAge($this->getValidityDate()) === NULL OR $pc->get('max_age') >= $age_year)
						if ($pc->get('min_age') === NULL OR $person->getAge($this->getValidityDate()) === NULL OR $pc->get('min_age') <= $age_year)
							$result = TRUE;
				}
		return $result;
	}
	
	public function getAgeDateLimits()
	{
		$result = array();
		$result['date'] = array();
		if ($this->sku->pricecategory->from_date != NULL)
			$result['date']['from'] = Skp_Helper::fdate($this->sku->pricecategory->from_date, $this->sku->merchant->date_format);
		if ($this->sku->pricecategory->to_date != NULL)
			$result['date']['to'] = Skp_Helper::fdate($this->sku->pricecategory->to_date, $this->sku->merchant->date_format);
		$result['age'] = array();
		if ($this->sku->pricecategory->min_age != NULL)
			$result['age']['min'] = $this->sku->pricecategory->min_age;
		if ($this->sku->pricecategory->max_age != NULL)
			$result['age']['max'] = $this->sku->pricecategory->max_age;
		return $result;
	}

	public function setCard($card = NULL)
	{
		if ($card != NULL AND $card->loaded())
		{
			$this->set('card',$card);
			$this->set('card_type',$card->getCardTypeName());
			$this->set('card_serial',$card->card_serial);
		}else{
			$this->set('card', NULL);
			$this->set('card_type', NULL);
			$this->set('card_serial', NULL);
		}
	}

	public function getExternalReference($only_data = FALSE)
	{
		$connector = $this->get('sku')->get('product')->get('catalog')->get('connector');
		$connectorClass = ($connector->get('connector_class') != NULL ? $connector->get('connector_class') : $this->get('original_connector_class') );
		$connector_id = $connector->pk() != NULL ? $connector->pk(): $this->get('original_connector_id');
		$result = 'KO';
		if ($connectorClass == NULL) return $result;
		$connectorClass .= '_Connector';
		try{
			// We loop throw all the different order external of this connector (there can be more than one)
			$order_externals = $this->get('order')->get('order_externals')->where('connector_id', '=', $connector_id)->find_all();
			foreach ($order_externals as $order_external)
			{
				$params = array('order_external' => $order_external, 'item' => $this, 'only_data' => $only_data);
				$result = $connectorClass::getExternalReference($params);
				if ($result != '' AND $result != NULL)
				{
					// The external reference has been found
					break;
				}
			}
		}catch (Exception $e){
			echo $e;
		}
		return $result;
	}

	public function getExternalOrder()
	{
		$connector = $this->get('sku')->get('product')->get('catalog')->get('connector');
		$connectorClass = ($connector->get('connector_class') != NULL ? $connector->get('connector_class') : $this->get('original_connector_class') ). '_Connector';
		$connector_id = $connector->pk() != NULL ? $connector->pk(): $this->get('original_connector_id');
		$order_external = ORM::factory('Skp_OrderExternal');
		try{
			// We loop throw all the different order external of this connector (there can be more than one)
			$order_externals = $this->get('order')->get('order_externals')->where('connector_id', '=', $connector_id)->find_all();
			foreach ($order_externals as $order_external)
			{
				$params = array('order_external' => $order_external, 'item' => $this, 'only_data' => TRUE);
				$result = $connectorClass::getExternalReference($params);
				if ($result != 'KO' AND $result != '' AND $result != NULL)
				{
					// The external reference has been found
					return $order_external;
				}
			}
		}catch (Exception $e){
			echo $e;
		}
		return $order_external;
	}

	public function isBarcoded()
	{
		return $this->get('sku')->get('product')->isBarcoded();
	}

	public function getBarcodeSerial()
	{
		$connector = $this->get('sku')->get('product')->get('catalog')->get('connector');
		$barcode_serial = NULL;
		foreach ($this->get('order')->getOrderExternals($connector->pk()) as $order_external)
		{
			$barcode_serial = $connector->callConnectorMethod('getBarcodeSerial', array('order_external'=>$order_external, 'item'=>$this));
			if ($barcode_serial !== NULL) break;
		}
		return $barcode_serial;
	}

	public function getBarcodeFormat()
	{
		return $this->get('sku')->get('product')->getBarcodeFormat();
	}

	public function isBookingAvailable($availability_id, $calendar_datetime_id = NULL)
	{
		$available = FALSE;

		// Here we clean the temporary bookings
		Booking::cleanBookings(array('merchant'=>$this->get('order')->get('merchant'), 'availability_id'=>$availability_id, 'calendar_date_time_id'=>$calendar_datetime_id, 'day' => $this->getValidityDate() != NULL ? $this->getValidityDate()->format('Y-m-d') : NULL));

		
		$availability = ORM::factory('Skp_Availability', $availability_id);
		if ($availability->loaded())
		{
			if ($availability->get('qty') === NULL)
			{
				$available = TRUE;
			} else {
				$total_already_booked = 0;
				foreach ($availability->get('bookings')->where('calendar_date_time_id', '=', $calendar_datetime_id)->find_all() as $booking)
				{
					$total_already_booked += $booking->get('order_item')->get('qty');
					$booking_manual = $booking->get('booking_manual');
					if ($booking_manual->loaded())
					{
						$total_already_booked += $booking_manual->get('qty');
					}
				}
				$available = (($availability->get('qty') - $total_already_booked) >= $this->get('qty'));
			}
		}
		return $available;
	}
	
	/*
	 *
	 * @param for_all_order_items_same_date , if TRUE show for each given orderitem only the availabilities that can satisfy all order items within the same date
	 * @param daily_only if TRUE shows only availabilities for the same day of validity of the orderitem
	 */

	public function getAvailabilities($for_all_order_items_same_date = TRUE, $daily_only = FALSE)
	{
		$valid_from = $this->getValidityDate() != NULL ? $this->getValidityDate()->setTimezone($this->getTimeZone()) : NULL;
		if ($valid_from != NULL)
			$valid_from->setTime(0,0);

		$now = new DateTime('now', $this->getTimeZone());
		if ($valid_from < $now)
		{
			$valid_from = $now;
			$bibv = $this->get('sku')->get('product')->get('buy_interval_before_validity');
			if ($bibv != NULL)
			{
				if (preg_match("/^(\d{1,})(D|H|m){1}$/", $bibv, $match))
				{
					list($original, $value, $type) = $match;
					$valid_from->add(new DateInterval(($type == 'D' ? 'P' : 'PT').strtoupper($original)));
				}
			}
		}

		// Here we clean the temporary bookings
		Booking::cleanBookings(array('day' => $this->getValidityDate() != NULL ? $this->getValidityDate()->format('Y-m-d') : NULL));

		// Log::instance()->add(LOG_DEBUG,'VALID FROM FOR ORDER ITEM '.$this->pk().' IS:'.print_r($valid_from, TRUE));
		$params = array('from_datetime' => $valid_from, 'daily_only' => $daily_only);
		$readd_item_qty = TRUE;
		$similar_orderitem_ids = $this->getSimilarOrderItems(TRUE, TRUE);
		if ($this->get('order')->isB2B() == TRUE)
		{
			$params['exclude_b2b_orderitem_id'] = $this->pk();
			$params['exclude_similar_b2b_orderitem_ids'] = $similar_orderitem_ids;
			$readd_item_qty = FALSE;
		}else{
			$params['exclude_orderitem_id'] = $this->pk();
			$params['exclude_similar_orderitem_ids'] = $similar_orderitem_ids;
			$readd_item_qty = FALSE;
		}

		$bds = $this->get('sku')->getAvailable($params);

		$needed_availability = array();

		if ($for_all_order_items_same_date == TRUE)
		{
			// we now filter the availability/calendar_datetime that have enough quantity also for all the other orderitems that have the same validity date
			$all_order_items_same_validity_date = $this->get('order')->get('items')->where('validity_date', '=', $this->get('validity_date'))->find_all();
			foreach ($all_order_items_same_validity_date as $aoisvd)
			{
				$availabilities = $aoisvd->get('sku')->getAvailabilities($params);
				foreach ($availabilities as $av)
				{
					if (isset($needed_availability[$av->get('availability_id')]) == FALSE)
					{
						$needed_availability[$av->get('availability_id')] = 0;
					}
					if ( $aoisvd->get('booking')->loaded() == FALSE)
					{
						$needed_availability[$av->get('availability_id')] += $aoisvd->get('qty');
					}
				}
			}
		}else{
			// We only check if our is available
			$availabilities = $this->get('sku')->getAvailabilities($params);
			foreach ($availabilities as $av)
			{
				$needed_availability[$av->get('availability_id')] = $this->get('qty');
			}
		}

		$availabilities = array();
		foreach ($bds as $bd)
		{
			if (Arr::get($needed_availability, $bd->availability_id) !== NULL)
			{
				if ($this->get('booking')->get('calendar_date_time_id') == $bd->calendar_date_time_id AND $this->get('booking')->get('availability_id') == $bd->availability_id)
				{
					$readd_item_qty AND $bd->available_qty += $this->get('qty');
				}
				if ($bd->available_qty === NULL OR $bd->available_qty >= $needed_availability[$bd->availability_id])
				{
					$bd->not_enough = FALSE;
					if ($this->get('booking')->get('calendar_date_time_id') == $bd->calendar_date_time_id AND $this->get('booking')->get('availability_id') == $bd->availability_id)
					{
						$bd->selected = TRUE;
					}
				}else{
					$bd->not_enough = TRUE;
				}
				if (($bd->available_qty-$needed_availability[$bd->availability_id]) < ceil($bd->availability_qty * $this->get('order')->get('merchant')->getSkpParamConfig(array('booking', 'availability_alert_perc'))))
				{
					$bd->availability_alert = TRUE;
				}
				$availabilities[] = $bd;
			}
		}

		return $availabilities;
	}

	public function book($calendar_datetime_id, $availability_id, $cleaned = FALSE)
	{
		$booking = $this->get('booking');
		if ($booking->loaded())
		{
			return $booking->resetBookingTimeout();
		}else{
			return Booking::createBooking($this, $calendar_datetime_id, $availability_id, $cleaned);
		}
	}

	public function getVatPrice()
	{
		return new Skp_Vat($this->get('vat_perc'), $this->get('vat_amount'), $this->get('currency'));
	}

	public function getTotalPriceNetVat()
	{
		return new Skp_Money($this->get('total_amount') - $this->get('vat_amount'), $this->get('currency'));
	}

	public function createCancelationOrderItem($canc_order)
	{
		$original_item_as_array = $this->as_array();
		array_shift($original_item_as_array);
		// Now we remove the unneed only the fields we really need
		$cancel_item = ORM::factory(( $canc_order->isB2B() ? 'Skp_B2BOrderItem' : 'Skp_OrderItem' ));
		$cancel_item->values($original_item_as_array);
		$cancel_item->set('related_orderitem_id', $this->pk());
		$cancel_item->set('order', $canc_order);
		$cancel_item->set('qty', -$original_item_as_array['qty']);
		$cancel_item->set('vat_amount', -$original_item_as_array['vat_amount']);
		$cancel_item->set('total_amount', -$original_item_as_array['total_amount']);
		$cancel_item->save();
		$this->get('booking')->createCancelationBooking($cancel_item);
		foreach ($this->get('suborderitems')->find_all() as $sub_item)
		{
			$cancel_sub_item = $sub_item->createCancelationOrderItem($canc_order);
			$cancel_sub_item->set('sub_orderitem_of_id', $cancel_item);
			$cancel_sub_item->save();
		}
		$cancel_item->createExportRecords(array('cancel'=>TRUE));
		return $cancel_item;
	}

	public function canBeDeletedFromBasket()
	{
		if (!isset($this->_canBeDeletedFromBasket))
		{
			$this->_canBeDeletedFromBasket = TRUE;
			$mainorderitem = $this->get('mainorderitem');
			if ($mainorderitem->loaded() != FALSE)
			{
				$pa = $mainorderitem->get('sku')->get('product_association')->where('sub_sku_id', '=', $this->get('sku')->pk())->find();
				if ($pa->loaded() == TRUE)
				{
					if ($pa->get('relation') == ProductAssociation::RELATION_PACKAGE OR $pa->get('relation') == ProductAssociation::RELATION_MANDATORY)
					{
						$this->_canBeDeletedFromBasket = FALSE;
					}
				}
			}
		}
		return $this->_canBeDeletedFromBasket;
	}

	public function isVirtualOrderItem()
	{
		if (!isset($this->_isVirtualOrderItem))
		{
			if (Arr::get(Arr::get($this->get('orderitem_extra'), 'sku'),'package')=='Y' OR $this->get('sku')->isPackage() == TRUE)
			{
				$this->_isVirtualOrderItem = TRUE;
			}else{
				$this->_isVirtualOrderItem = FALSE;
			}
		}
		return $this->_isVirtualOrderItem;
	}

	public function checkPackageConsistency()
	{
		$sku = $this->get('sku');
		$is_valid = TRUE;
		if ($sku->isPackage() == TRUE)
		{
			$package_pa = $sku->getProductAssociations(ProductAssociation::RELATION_PACKAGE);
			foreach ($package_pa as $pa)
			{
				$total_qty = $pa->get('qty')*$this->get('qty');
				foreach ($this->get('suborderitems')->where('sku_id', '=', $pa->get('sub_sku_id'))->find_all() as $subitem)
				{
					// Here we force that all package subsku are valid for the same date of the Main Package Item, this may be not needed in the future ?
					if ($subitem->getValidityDate()->format('Y-m-d') == $this->getValidityDate()->format('Y-m-d'))
					{
						$total_qty -= $subitem->get('qty');
					}else{
						$is_valid = FALSE;
						break;
					}
				}
				if ($total_qty != 0) $is_valid = FALSE;
				if ($is_valid == FALSE) break;
			}
		}
		return $is_valid;
	}

	// Returns Order Items for all the orders in status BOOKED

	public static function getPickups($params)
	{
		$utc_tz = new DateTimeZone('UTC');
		$search_column_names = array('order.reference_num', 'order.order_datetime');
		$merchant = Arr::get($params, 'merchant');
		$from_date = Arr::get($params, 'from_date');
		$to_date = Arr::get($params, 'to_date');
		$ref_num = Arr::get($params, 'ref_num');
		$item_ref_num = Arr::get($params, 'item_ref_num');
		$with_b2b_company = Arr::get($params, 'with_b2b_company', FALSE);
		$with_order_item_identification = Arr::get($params, 'with_order_item_identification', FALSE);

		$oi = ORM::factory('Skp_OrderItem')
			->with('order')
			->where('order.status', '=', Order::STATUS_BOOKED);
		$with_b2b_company AND $oi->join('b2b_company', 'LEFT')->on('order.b2b_company_id', '=', 'b2b_company.id');
		($with_order_item_identification OR $item_ref_num != NULL) AND $oi->join('order_item_identification', 'LEFT')->on('skp_orderitem.id', '=', 'order_item_identification.orderitem_id');
		if ($merchant != NULL AND $merchant->loaded() == TRUE)
		{
			$oi->and_where('order.merchant_id', '=', $merchant->pk());
		}
		
		if ($ref_num != NULL)
		{
			$oi->and_where('order.reference_num' , 'LIKE' , DB::expr('\'%'.$ref_num.'%\''));
		}else if ($item_ref_num != NULL)
		{
			$oi->and_where('order_item_identification.value' , 'LIKE' , DB::expr('\'%'.$ref_num.'%\''));
		} else {
			if ($from_date != NULL)
			{
				$oi->and_where('order_datetime', '>=', $from_date->setTimeZone($utc_tz)->format('Y-m-d H:i:s'));
			}
			if ($to_date != NULL)
			{
				$oi->and_where('order_datetime', '<=', $to_date->setTimeZone($utc_tz)->format('Y-m-d H:i:s'));
			}
		}
		return $oi;
	}

	// Returns Boolean id the OrderItem has been pickedup
	public function pickedup()
	{
		return ($this->get('status') == static::STATUS_PICKED);
	}

	// Returns Boolean id the OrderItem is pickable
	public function pickable()
	{
		return ($this->loaded() == TRUE AND $this->get('status') == static::STATUS_OK AND $this->get('order')->get('status') == Order::STATUS_BOOKED AND $this->isVirtualOrderItem() == FALSE AND $this->getExternalReference(TRUE) != NULL);
	}

	// Mark an OrderItem as picked
	public function pick($user)
	{
		if ($this->pickable() == TRUE)
		{
			$this->set('status', static::STATUS_PICKED);
			if ($user != NULL)
			{
				$this->setStatusExtra('user_id', $user->pk());
			}
			$this->setStatusExtra('picked_datetime', gmdate('Y-m-d H:i:s'));
			$this->save();
		}else{
			throw new Exception ('OrderItem already picked');
		}
	}

	public function isAggregable($aggregate_in_config = FALSE)
	{
		// First we check if the sku is aggregable
		if ($this->get('sku')->isAggregate() == 1)
		{
			return TRUE;
		}
		// Now we check if any person data were already set
		if ($this->get('person_id') != NULL OR $this->get('first_name') != NULL OR $this->get('middle_name') != NULL OR $this->get('last_name') != NULL OR $this->get('birth_date') != NULL)
		{
			return FALSE;
		}
		// Now we check if any card data was set
		if ($this->get('card_id') != NULL OR $this->get('card_serial') != NULL)
		{
			return FALSE;
		}
		// Now we check if the orderitem has subitems
		if ($this->get('suborderitems')->count_all() > 0)
		{
			return FALSE;
		}
		// Now we check if it has booking or not
		if (FALSE AND $this->get('booking')->loaded() == TRUE)
		{
			return FALSE;
		}
		if ($aggregate_in_config AND ($this->get('sku')->get('need_card') != 'N' OR $this->needContactProfile() OR $this->get('sku')->get('product')->needsExtraInfo() OR count($this->get('sku')->getAllSubSkus()) > 0))
		{
			return FALSE;
		}
		return TRUE;
	}

	public function cloneOrderItem($parent_order_item = NULL)
	{
		$added_orderitems = $this->get('order')->addOrderItem($this->get('sku'), 1, $this->getValidityDate(), $this->get('pool_id'), $parent_order_item, $this->get('promocode_id'));
		$added_orderitem_ids = array();
		foreach ($added_orderitems as $item)
		{
			$added_orderitem_ids[] = $item->pk();
		}
		return $added_orderitem_ids;
	}

	public function getPersonName()
	{
		if ($this->get('first_name') != NULL OR $this->get('middle_name') != NULL OR $this->get('last_name') != NULL)
		{
			$first_name = $this->get('first_name') != NULL ? $this->get('first_name') : '';
			$middle_name = ($this->get('middle_name') != NULL) ? $this->get('middle_name') : '';
			$last_name = ($this->get('last_name') != NULL) ? $this->get('last_name') : '';
			return($first_name.' '.$middle_name.' '.$last_name);
		} else {
			return NULL;
		}
	}

	public function setExtra($extra_name, $extra_value)
	{
		$ue = $this->get('orderitem_extra');

		if ($extra_value != NULL)
		{
			$ue[$extra_name] = $extra_value;
		}else{
			unset($ue[$extra_name]);
		}
		$this->set('orderitem_extra', $ue);
	}
	
	public function getExtra($key, $default_value = NULL)
	{
		if ($key != NULL)
		{
			return Arr::get($this->get('orderitem_extra'), $key, $default_value);
		}
	}

	public function confirmOrderItemPurchase()
	{
		$pc = $this->get('promocode');
		if ($pc->loaded() == TRUE)
		{
			$sold = $pc->getExtra('sold_products');
			$sold != NULL OR $sold = 0;
			$pc->setExtra('sold_products', $sold + $this->get('qty'));
			$pc->save();
		}
		$export_connectors = $this->createExportRecords();
		$person = $this->get('person');
		if ($person->loaded())
		{
			$person->createExportRecords($this->get('order')->get('merchant'), array('active_export_connectors' => $export_connectors));
		}
		$confirm_purchase_result = array('active_export_connectors' => $export_connectors);
		return $confirm_purchase_result;
	}

	public function getPersonData()
	{
		$person_data = $this->get('person_data');
		is_array($person_data) OR $person_data = array();
		$this->get('person_photo') == NULL OR $person_data['photo'] = $this->get('person_photo');
		return $person_data;
	}

	public function getReceiptPdfFilename($withPath = TRUE)
	{
		return $this->get('order')->getReceiptPdfFilename($withPath, $this->pk());
	}
	
	public function createReceiptPdf($lang, $params = array())
	{
		$order = $this->get('order');
		$merchant = $this->get('order')->get('merchant');
		$multilang_config = Kohana::$config->load('multilang');
		// Set the language in I18n mapped from Multilang to I18n (example: en => en_US)
		I18n::lang($multilang_config->languages[$lang]['i18n'].'-'.$merchant->get('domain'));

		$dPath = MODPATH . 'skiperformance' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'shop' . DIRECTORY_SEPARATOR . 'merchants' . DIRECTORY_SEPARATOR.'default' . DIRECTORY_SEPARATOR . 'receipts' . DIRECTORY_SEPARATOR;
		$mainitem = $this; // this is used in the template

		include($order->getReceiptPdfTemplate('single-item-one'));
	}
	
	public function sendReceipt($email = NULL, $params = array())
	{
		$order = $this->get('order');
		$merchant = $order->merchant;
		
		$_config = Kohana::$config->load('skiperf');
		$messageChannel = $merchant->getMessageChannel();

		$msg_lang = (isset($params['r_lang']) AND $params['r_lang'] != NULL) ? $params['r_lang'] : $order->user->last_language;
		$original_lang = I18n::lang();
		I18n::lang($msg_lang);

		$msg_params = array(
			'merchant.merchant' 	=> $merchant,
			'subject' 				=> __('create_message.receipt_email_title_forwarded',
				array(
					':dest_name'		=> $merchant->name,
					':ref_num'			=> $order->reference_num
				)
			),
			'body' 					=> $order->getReceipt($msg_lang, $params),
			'lang' 					=> $msg_lang,
			'order.order' 			=> $order,
			'cc' 						=> $messageChannel->mail_from,
			);
		if ($email != NULL)
		{
			$msg_params['to'] = $email;
			$msg_params['user.user'] = NULL;
			$msg_params['cc'] = NULL;
		}else{
			$msg_params['user.user'] = $order->user;
			$msg_params['bcc'] = $_config->get('email_skiperformance_orders');
		}

		$message = $messageChannel->call('createMessage', $msg_params);
		$message->save();
		$attachments = NULL;
		if ($order->needsPdfReceipt() == TRUE) {
			$pathname = $this->getReceiptPdfFilename(TRUE);
			if (Skp_StaticFileManager::existsFile($pathname, Order::$filemanager_profile_pdf_order_tickets) == TRUE)
			{
				$attachment = array(
					'full_filename' => $pathname,
					'filename' => $this->getReceiptPdfFilename(FALSE),
					'filemanager_profile' => Order::$filemanager_profile_pdf_order_tickets
				);
				is_array($attachments) OR  $attachments = array();
				$attachments[] = $attachment;
			}
		}

		$message->messageChannel->call('sendMessage', array('message.message' => $message, 'attachments' => $attachments));

		I18n::lang($original_lang);
	}

	public function canBeBought()
	{
		$main_orderitem = $this->get('mainorderitem');
		if ($main_orderitem->loaded() == TRUE)
		{
			if ($main_orderitem->get('sku')->isPackage() == TRUE)
			{
				// In case of suborderitem of Package we need to search if the mainorderitem has the subitem in his package list
				$found_sku = $main_orderitem->get('sku')->getPackageSubSkus()->where('skp_sku.id', '=', $this->get('sku_id'))->find();
			}else{
				// In case of suborderitem we need to search if the mainorderitem has the subitem in his list
				$found_sku = Arr::get($main_orderitem->get('sku')->getAllSubSkus(array('sku_id'=>$this->get('sku_id'), 'include_identification_card'=>TRUE)), 0, ORM::factory('Skp_Sku'));
			}
		}else{
			// In case of mainorderitem we need to search if the mainorderitem is available
			$found_sku = Sku::getSkus(array('merchant'=>$this->get('order')->get('merchant'), 'user'=>$this->get('order')->get('user'), 'sku_id'=>$this->get('sku_id')))->find(); // this function find also promo sku items
			if ($found_sku->loaded() == FALSE)
			{
				$pc = $this->get('promocode');
				if ($pc->loaded() AND $pc->get('is_active') == 'Y' AND $pc->isValid($this->get('order')->get('user'), $this->get('order')->get('user')->getB2BCompany($this->get('order')->get('merchant'))))
				{
					foreach($pc->get('skugroups')->and_where('skp_skugroup.deleted', '=', '0')->find_all() as $sg)
					{
						if ($sg->get('active') == 1)
						{
							$found_sku = Sku::getSkus(array('merchant'=>$this->get('order')->get('merchant'), 'group_id'=>$sg->pk(), 'sku_id'=>$this->get('sku_id'), 'only_web_active'=>FALSE, 'user'=>$this->get('order')->get('user'), 'promocode'=>TRUE))->find();
							if ($found_sku->loaded())
							{
								return TRUE;
							}
						}
					}
				}
			} else {
				$pc = $this->get('promocode');
				if ($pc->loaded() AND !$pc->isValid($this->get('order')->get('user'), $this->get('order')->get('user')->getB2BCompany($this->get('order')->get('merchant'))))
				{
					return FALSE;
				}
			}	
		}
		if ($found_sku->loaded() AND $found_sku->canBeBought(array('validity_date'=>$this->getValidityDate()))) return TRUE;
		return FALSE;
	}
	
	// Returns the contact profile based on the properties of the order item (ProductContactProfile conditions)
	public function getContactProfile()
	{
		$sku = $this->get('sku');
		$product = $sku->get('product');
		$properties_string = $this->getOrderItemProperties();
		
		$contact_profile_sku_specific = $sku->get('product_contact_profiles')->where('conditions','=',$properties_string)->find();
		if ($contact_profile_sku_specific->loaded())
			return $contact_profile_sku_specific->get('profile');
			
		$contact_profile_product_specific = $product->get('product_contact_profiles')->where('sku_id','=',NULL)->and_where('conditions','=',$properties_string)->find();
		if ($contact_profile_product_specific->loaded())
			return $contact_profile_product_specific->get('profile');
			
		$contact_profile_sku_default = $sku->get('product_contact_profiles')->where('conditions','=','')->or_where('conditions','=',NULL)->find();
		if ($contact_profile_sku_default->loaded())
			return $contact_profile_sku_default->get('profile');
			
		$contact_profile_product_default = $product->get('product_contact_profiles')->where('conditions','=','')->or_where('conditions','=',NULL)->find();
		if ($contact_profile_product_default->loaded())
			return $contact_profile_product_default->get('profile');
			
		return NULL;
	}
	
	// Returns a string containing the properties of the order item, must be in the same order of ProductContactProfile PROPERTIES
	public function getOrderItemProperties()
	{
		$properties = array();
		if ($this->isWithIdentificationCard() OR $this->getExtra('card_operation') == ProductContactProfile::PROPERTY_BUY_CARD)
		{
			array_push($properties, ProductContactProfile::PROPERTY_BUY_CARD);
		} 
		else if ($this->get('card')->loaded() OR $this->getExtra('card_operation') == ProductContactProfile::PROPERTY_REGISTERED_CARD)
		{
			array_push($properties, ProductContactProfile::PROPERTY_REGISTERED_CARD);
		}
		if ($this->get('sku')->get('product')->needsDateTimeBooking())
		{
			array_push($properties, ProductContactProfile::PROPERTY_HAS_BOOKING);
		}
		if ($this->get('order')->isNewB2B())
		{
			array_push($properties, ProductContactProfile::PROPERTY_IS_B2B);
		}
		return implode('-',$properties);
	}
	
	public function needContactProfile()
	{
		$contact_profile = $this->getContactProfile();
		return ($contact_profile != NULL AND $contact_profile->loaded());
	}
	
	public function printPrice($amount)
	{
		$amount = strval($amount);
		$print_price = $this->getExtra('print_price');
		if ($print_price == Merchant::PRICE_HIDDEN)
		{
			// hide price
			$amount = '';
		} else if ($print_price == Merchant::PRICE_ENCRYPT)
		{
			// encrypt price
			$print_price_encrypt = $this->getExtra('print_price_encrypt', '0123456789');
			if (strlen($print_price_encrypt) === 10)
			{
				$amount = str_replace(str_split('0123456789'), str_split($print_price_encrypt), $amount);
			} else {
				// impossible to encrypt -> nothing to do
			}
		} else {
			//nothing to do
		}
		return $amount;
	}

	public function getPrintPriceFormat()
	{
		$is_b2b = $this->get('order')->isNewB2B();
		$extraAddon_print_price = $this->get('order')->get('merchant')->hasAddOn('Advanced Sales Functionalities', 'Print Price Encrypt');
		if ($extraAddon_print_price)
		{
			$print_price = $this->get('sku')->getPrintPriceFormat($is_b2b);
		}else{
			$print_price = NULL;
		}
		return $print_price;
	}
	
	// Returns TRUE if the related SKU has invoice_mandatory option set to 'YES' or 'AS PARENT' (and parent Product has option set to 'YES'). FALSE otherwise.
	public function isInvoiceMandatory()
	{
		$is_mandatory = FALSE;
		$sku = $this->get('sku');
		$sku_mandatory = $sku->getExtra('mandatory_invoice');
		if ($sku_mandatory === NULL)
		{
			$product = $sku->get('product');
			if ($product->getExtra('mandatory_invoice') == 'Y')
			{
				$is_mandatory = TRUE;
			}
		}
		else if ($sku_mandatory == 'Y')
		{
			$is_mandatory = TRUE;
		}
		return $is_mandatory;
	}
	
	public function addTags()
	{
		$all_added = TRUE;
		try{
			foreach ($this->get('sku')->get('product')->getTagValues() as $tag_value)
			{
				$order_item_tag = ORM::factory('Skp_OrderItemTag');
				$order_item_tag->set('order_id', $this->get('order')->pk());
				$order_item_tag->set('order_item_id', $this->pk());
				$order_item_tag->set('value', $tag_value);
				$order_item_tag->save();
			}
		} catch (Exception $e)
		{
			$all_added = FALSE;
		}
		return $all_added;
	}
	
	public function isOpenDate()
	{
		return $this->get('sku')->get('product')->isOpenDate();
	}
	
	public function needsBooking()
	{
		return $this->get('sku')->get('product')->needsBooking();
	}
	
	public function needsDateTimeBooking()
	{
		return $this->get('sku')->get('product')->needsDateTimeBooking();
	}
	
	public function needsValidityStartTime()
	{
		return $this->get('sku')->get('product')->needsValidityStartTime();
	}
	
	public function getValidityUnit()
	{
		return $this->getExtra('validity_unit');
	}
	
	public function getValidityValue()
	{
		return $this->getExtra('validity_value');
	}
	
	public function getCustomValidityText($lang = NULL)
	{
		return $this->getExtra('custom_validity_text');
	}

	public function updateValidityData($lang)
	{
		$this->setExtra('start_validity_date', $this->get('sku')->get('product')->getStartValidityDate());
		$this->setExtra('end_validity_date', $this->get('sku')->get('product')->getEndValidityDate($this->getValidityDate()));
		$this->setExtra('validity_unit', $this->get('sku')->get('product')->getValidityUnit());
		$this->setExtra('validity_value', $this->get('sku')->get('product')->getValidityValue());
		$this->setExtra('custom_validity_text', $this->get('sku')->get('product')->getCustomValidityText($lang));
	}
	
	// Return an array of orderitems similar to $this, or an array of their ids if $only_ids=TRUE.
	// Two orderitems are similar if they are both Aggregable, have same sku and same validity date and if their booking's time and availability are the same
	public function getSimilarOrderItems($aggregate_in_config = FALSE, $only_ids = FALSE)
	{
		$result = array();
		if ($this->isAggregable($aggregate_in_config))
		{
			$oitems = $this->get('order')->get('items')->where('sub_orderitem_of_id','=',NULL)->and_where('sku_id', '=', $this->get('sku_id'))->and_where('validity_date', '=', $this->get('validity_date'))->and_where('id', '<>', $this->pk())->find_all();
			$check_booking = $this->get('sku')->needsBooking();
			$check_termcond = $this->get('sku')->needsTermsConditions();
			$this_booking = $this->get('booking');
			$this_tc = $this->getExtra('terms_condition') == '1' ? 'Y' : 'N';
			foreach ($oitems as $oi)
			{
				if ($oi->isAggregable($aggregate_in_config))
				{
					$add_item = !$check_booking AND !$check_termcond;
					if ($check_booking)
					{
						$oi_booking = $oi->get('booking');
						if ((!$this_booking->loaded() AND !$oi_booking->loaded()) OR ($this_booking->loaded() AND $oi_booking->loaded() AND $this_booking->get('calendar_date_time_id') == $oi_booking->get('calendar_date_time_id') AND $this_booking->get('availability_id') == $oi_booking->get('availability_id')))
						{
							$add_item = TRUE;
						}
					}
					if ($add_item AND $check_termcond)
					{
						$add_item = FALSE;
						$oi_tc = $oi->getExtra('terms_condition') == '1' ? 'Y' : 'N';
						if ($this_tc == $oi_tc)
						{
							$add_item = TRUE;
						}
					}
					$add_item AND ($only_ids AND $result[] = $oi->pk() OR $result[] = $oi);
				}
			}
		}
		return $result;
	}
	
	public function anonymizeOrderItemPerson($person)
	{
		$this->set('person', $person);
		$this->set('first_name', $person->get('first_name'));
		$this->set('middle_name', $person->get('middle_name'));
		$this->set('last_name', $person->get('last_name'));
		$this->set('person_photo', NULL);
		
		$person_columns = $person->list_columns();
		$person_data = $this->get('person_data');
		if ($person_data != NULL)
		{
			foreach ($person_data as $field => $value)
			{
				$person_data[$field] = isset($person_columns[$field]) ? $person->get($field) : $person->getExtra($field);
			}
			$this->set('person_data', $person_data);
		}
		$this->save();
	}
	
	public function createExportRecords($params = array())
	{
		$order = $this->get('order');
		$product = $this->get('sku')->get('product');
		$is_cancellation = (Arr::get($params, 'cancel', FALSE) == TRUE);

		$export_connectors = $product->getExportConnectors();
		foreach ($export_connectors as $exp_c)
		{
			$order_export_record = $exp_c->get('export_records')->where('object_class', '=', Arr::get(ExportConnector::$record_types, ExportConnector::RECORD_TYPE_ORDER))->and_where('object_id', '=', $order->pk())->find();
			if (!$order_export_record->loaded())
			{
				$order_export_record = ORM::factory('Skp_ExportConnectorRecord');
				$order_export_record->set('merchant_id', $exp_c->get('merchant_id'));
				$order_export_record->set('export_connector_id', $exp_c->pk());
				$order_export_record->set('object_class', Arr::get(ExportConnector::$record_types, ExportConnector::RECORD_TYPE_ORDER));
				$order_export_record->set('object_id', $order->pk());
				$order_export_record->set('export_status', ExportConnectorRecord::STATUS_NEW);
				$order_export_record->set('order_datetime', $order->get('order_datetime'));
				$order_export_record->set('record_datetime', $order->get('order_datetime'));
				$is_cancellation AND $order_export_record->setExtra('cancel_order', TRUE);
				$order_export_record->save();
			}
			$orderitem_export_record = ORM::factory('Skp_ExportConnectorRecord');
			$orderitem_export_record->set('merchant_id', $exp_c->get('merchant_id'));
			$orderitem_export_record->set('export_connector_id', $exp_c->pk());
			$orderitem_export_record->set('object_class', Arr::get(ExportConnector::$record_types, ExportConnector::RECORD_TYPE_ORDERITEM));
			$orderitem_export_record->set('object_id', $this->pk());
			$orderitem_export_record->set('export_status', ExportConnectorRecord::STATUS_NEW);
			$orderitem_export_record->set('order_datetime', $order->get('order_datetime'));
			$orderitem_export_record->set('validity_datetime', $this->get('validity_date'));
			$orderitem_export_record->set('record_datetime', $order->get('order_datetime'));
			$orderitem_export_record->setExtra('order_id', $order->pk());
			$is_cancellation AND $orderitem_export_record->setExtra('cancel_item', TRUE);
			$orderitem_export_record->save();
		}
		return $export_connectors;
	}
	
	public function getBookingTimeRange()
	{
		$validity_unit = $this->getValidityUnit();
		$validity_value = $this->getValidityValue();
		if ($this->get('booking')->loaded())
		{
			return $this->get('booking')->getTimeRange(array('unit'=>$validity_unit, 'value'=>$validity_value));
		}
		return NULL;
	}

	public function getMainItemPerson()
	{
		$this_orderitem_person = $this->get('person');
		$main_orderitem = $this->get('mainorderitem');
		if ($this_orderitem_person->loaded() == FALSE AND $main_orderitem->loaded())
		{
			$this_orderitem_person = $main_orderitem->getMainItemPerson();
		}
		return $this_orderitem_person;
	}
	
	public function getProductAttributeValue(ProductAttribute $product_attribute)
	{
		$value = NULL;
		if ($product_attribute->loaded())
		{
			if ($product_attribute->get('fixed_value') != NULL)
			{
				$value = $product_attribute->get('fixed_value');
			} else if ($product_attribute->get('data_field_id') != NULL)
			{
				$value = Arr::get($this->getExtra('extra_fields', array()), $product_attribute->get('data_field')->get('name'));
			}
		}
		return $value;
	}

	public function getExtraFieldByType($field_type)
	{
		$field_name = array_search($field_type, $this->getExtra('extra_fields_types', array()));
		if ($field_name !== FALSE)
		{
			return Arr::get($this->getExtra('extra_fields', array()), $field_name);
		}
		return NULL;
	}

	public function hasIdentificator()
	{
		$result = $this->get('card')->loaded();
		$product = $this->get('sku')->get('product');
		$result = ($result OR $product->isBarcoded() OR $this->getExtraFieldByType(DataField::FIELD_TYPE_LICENSE_PLATE) !== NULL);
		return $result;
	}

	public function addIdentificator($order_type, $orderitem_identification, $order_external_identification)
	{
		if ($orderitem_identification != NULL)
		{
			$oii = ORM::factory('Skp_OrderItemIdentification')->set('orderitem_id', $this->pk());
			$oii->set('type', Arr::get($orderitem_identification, 'type'));
			$oii->set('subtype', Arr::get($orderitem_identification, 'subtype'));
			$oii->set('value', Arr::get($orderitem_identification, 'value'));
			$oii->set('extra', Arr::get($orderitem_identification, 'extra'));
			$oii->save();
		}

		$order_external = $this->get('order')->getOrderExternal($this->get('sku')->get('product')->get('catalog')->get('connector')->pk(), $order_type);
		$order_external->set('external_order_type', $order_type);
		
		$external_extra = $order_external->get('external_extra');
		$external_extra['items'] = Arr::get($external_extra, 'items', array());
		$external_extra['items'][$this->pk()] = Arr::get($external_extra['items'], $this->pk(), array());
		// We need to check only 1 external identification is set in order_external.extra in order to mantain previous structure
		if (sizeof($external_extra['items'][$this->pk()]) == 0)
		{
			if ($order_external_identification != NULL)
			{
				$external_extra['items'][$this->pk()] = $order_external_identification;
			}
		}
		// We need to save always to associate this orderitem to the external extra
		$order_external->set('external_extra', $external_extra)->save();
	}
}
