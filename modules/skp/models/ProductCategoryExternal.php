<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class ProductCategoryExternal extends ModelNameDescription implements Interfaces_ConnectorExtra {

	protected $table = 'product_category_external';
	
	protected $_table_columns = array(
		'id' => array(), 
		'product_category_id' => array (),
		'catalog_id' => array (),
		'name' => array (),
		'description' => array (),
		'external_id' => array (),
		'extra' => array (),
	);
	
	protected $_serialize_columns = array('extra', 'name', 'description');
	
	protected $_belongs_to = array(
		'product_category' => array(
			'model' => 'Skp_ProductCategory',
			'foreign_key' => 'product_category_id',
		),
		'catalog' => array(
			'model' => 'Skp_Catalog',
			'foreign_key' => 'catalog_id',
		),
	);
	
	protected $_has_many = array();

	public function rules()
	{
		return array(
		);
	}
	
	public function setConnectorExtra($extra)
	{
		$current_extra = $this->get('extra');
		$current_extra['connector_object'] = $extra;
		$this->set('extra', $current_extra);
	}
	
	public function getConnectorExtra()
	{
		$current_extra = $this->get('extra');
		return Arr::get($current_extra, 'connector_object', NULL);
	}

}
