<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class ProductContactProfile extends Model {

	protected $table = 'product_contact_profile';
	protected $_table_columns = Array(
		'id' => Array(),
		'product_id' => Array(),
		'sku_id' => Array(),
		'profile_id' => Array(),
		'conditions' => Array(),
	);
	
	protected $_belongs_to = array(
		'product' => array(
			'model' => 'Skp_Product',
			'foreign_key' => 'product_id',
		),
		'sku' => array(
			'model' => 'Skp_Sku',
			'foreign_key' => 'sku_id',
		),
		'profile' => array(
			'model' => 'Skp_ContactProfile',
			'foreign_key' => 'profile_id',
		),
	);

	// const PROPERTY_NO_CARD = 'no_card';
	const PROPERTY_BUY_CARD = 'buy_card';
	// const PROPERTY_BRING_CARD = 'bring_card';
	const PROPERTY_REGISTERED_CARD = 'registered_card';
	const PROPERTY_HAS_BOOKING = 'has_booking';
	const PROPERTY_IS_B2B = 'is_b2b';
	
	// must be in the same order of $order_item->getOrderItemProperties()'s checks
	public static $properties_list = array(
		ProductContactProfile::PROPERTY_BUY_CARD, 
		ProductContactProfile::PROPERTY_REGISTERED_CARD, 
		ProductContactProfile::PROPERTY_HAS_BOOKING,
		ProductContactProfile::PROPERTY_IS_B2B);
	
	// Returns an array containing all the properties selected for the current ProductContactProfile
	public function getSelectedConditionsArray()
	{
		return array_intersect(ProductContactProfile::$properties_list, explode('-', $this->get('conditions')));
	}
}
