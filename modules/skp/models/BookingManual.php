<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class BookingManual extends Model {

	protected $table = 'booking_manual';
	public $timestamps = false;
	protected $casts = [
		'extra' => 'booking_data',
	];

	public function user() {
		$this->belongsTo(User::class);
	}

}