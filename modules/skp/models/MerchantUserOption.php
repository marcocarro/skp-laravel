<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class MerchantUserOption extends Model {

	protected $table = 'merchant_user_option';
	public $timestamps = false;

	public function merchant() {
		$this->belongsTo(Merchant::class);
	}

	public function user() {
		$this->belongsTo(User::class);
	}
	
}