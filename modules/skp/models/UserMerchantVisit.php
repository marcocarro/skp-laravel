<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class UserMerchantVisit extends Model {

	protected $table = 'user_merchant_visit';

	protected $_table_columns = Array(
		'id' => Array(), 
		'merchant_id' => Array (),
		'user_id' => Array(),
		'session_id' => Array(),
		'visit_start' => Array(),
		'visit_end' => Array(),
		'device_type' => Array(),
		'device_platform' => Array(),
		);

	protected $_belongs_to = array(
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
		'user' => array(
			'model' => 'Skp_User',
			'foreign_key' => 'user_id',
		),
	);

	protected $_has_many = array(
		'user_merchant_positions' => array(
			'model' => 'Skp_UserMerchantPosition',
			'foreign_key' => 'user_merchant_visit_id',
		),
	);

	const TYPE_DESKTOP 	= 'D';
	const TYPE_TABLET		= 'T';
	const TYPE_MOBILE 	= 'M';

	public function setDeviceType ($device_type)
	{
		if ($device_type == 'Desktop')
		{
			$this->set('device_type', self::TYPE_DESKTOP);
		} else if ($device_type == 'Tablet')
		{
			$this->set('device_type', self::TYPE_TABLET);
		} else if ($device_type == 'Mobile')
		{
			$this->set('device_type', self::TYPE_MOBILE);
		} else
		{
			$this->set('device_type', NULL);
		}
	}


}
