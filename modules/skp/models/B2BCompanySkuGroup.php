<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;

class B2BCompanySkuGroup extends Model
{
    protected $table = 'b2b_company_skugroup';
    public $timestamps = false;
}
