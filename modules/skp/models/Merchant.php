<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Merchant extends Model {

	protected $table = 'merchant';
	public $timestamps = false;
	protected $casts = [
		'merchant_extra' => 'array',
	];

	public function contact_profile() {
		$this->belongsTo(ContactProfile::class, 'contact_profile_id');
	}

	public function team_contact_profile() {
		$this->belongsTo(ContactProfile::class, 'team_contact_profile_id');
	}

	public function prodmerchantusersucts() {
		return $this->hasMany(
			MerchantUser::class,
			'merchant_id'
		);
	}

	public function user_merchant_optins() {
		return $this->hasMany(
			UserMerchantOptin::class,
			'merchant_id'
		);
	}

	public function merchant_optins() {
		return $this->hasMany(
			MerchantOptin::class,
			'merchant_id'
		);
	}

	public function users() {
		return $this->hasMany(
			ProductImage::class,
			'merchant_id'
		);
	}

	public function user_merchants() {
		return $this->hasMany(
			UserMerchant::class,
			'merchant_id'
		);
	}

	public function connectors() {
		return $this->hasMany(
			Connector::class,
			'merchant_id'
		);
	}

	public function export_connectors() {
		return $this->hasMany(
			ExportConnector::class,
			'merchant_id'
		);
	}

	public function catalogs() {
		return $this->hasMany(
			Catalog::class,
			'merchant_id'
		);
	}

	public function products() {
		return $this->hasMany(
			Product::class,
			'merchant_id'
		);
	}

	public function productgroups() {
		return $this->hasMany(
			ProductGroup::class,
			'merchant_id'
		);
	}

	public function priceCategories() {
		return $this->hasMany(
			PriceCategory::class,
			'merchant_id'
		);
	}

	public function skugroups() {
		return $this->hasMany(
			SkuGroup::class,
			'merchant_id'
		);
	}

	public function skus() {
		return $this->hasMany(
			Sku::class,
			'merchant_id'
		);
	}

	public function paymentChannels() {
		return $this->hasMany(
			PaymentChannel::class,
			'merchant_id'
		);
	}

	public function messageChannels() {
		return $this->hasMany(
			MessageChannel::class,
			'merchant_id'
		);
	}

	public function orders() {
		return $this->hasMany(
			Order::class,
			'merchant_id'
		);
	}

	public function b2b_orders() {
		return $this->hasMany(
			B2BOrder::class,
			'merchant_id'
		);
	}

	public function b2b_companies() {
		return $this->hasMany(
			B2BOrder::class,
			'merchant_id'
		);
	}

	public function payments() {
		return $this->hasMany(
			Payment::class,
			'merchant_id'
		);
	}

	public function availabilities() {
		return $this->hasMany(
			Availability::class,
			'merchant_id'
		);
	}

	public function product_type_merchant() {
		return $this->hasMany(
			ProductTypeMerchant::class,
			'merchant_id'
		);
	}

	public function product_tvatsype_merchant() {
		return $this->hasMany(
			Skp_Vat::class,
			'merchant_id'
		);
	}

	public function venues() {
		return $this->hasMany(
			Skp_Venue::class,
			'merchant_id'
		);
	}

	public function images() {
		return $this->hasMany(
			Skp_Image::class,
			'merchant_id'
		);
	}

	public function image_categories() {
		return $this->hasMany(
			Skp_ImageCategory::class,
			'merchant_id'
		);
	}

	public function calendars() {
		return $this->belongsToMany(Calendar::class)->using(MerchantCalendarOpening::class);
	}

	public function tr_keys() {
		return $this->hasMany(
			Skp_Trkey::class,
			'merchant_id'
		);
	}

	public function contact_profiles() {
		return $this->hasMany(
			Skp_ContactProfile::class,
			'merchant_id'
		);
	}

	public function promo_codes() {
		return $this->hasMany(
			Skp_Promocode::class,
			'merchant_id'
		);
	}

	public function tracking_codes() {
		return $this->hasMany(
			Skp_TrackingCode::class,
			'merchant_id'
		);
	}

	public function addons() {
		return $this->hasMany(
			Skp_Addon::class,
			'merchant_id'
		);
	}

	public function invoice_receipt_profiles() {
		return $this->hasMany(
			InvoiceReceiptProfile::class,
			'merchant_id'
		);
	}

	public function invoice_receipt_orders() {
		return $this->hasMany(
			Skp_InvoiceReceiptOrder::class,
			'merchant_id'
		);
	}

	public function virtual_cards() {
		return $this->hasMany(
			Skp_VirtualCard::class,
			'merchant_id'
		);
	}

	public function active_virtual_card_types() {
		return $this->belongsToMany(
			VirtualCardType::class,
			'virtual_card_type_merchants',
			'merchant_id',
			'virtual_card_type_id'
		);
	}

	public function tags() {
		return $this->hasMany(
			Tag::class,
			'merchant_id'
		);
	}

	public function product_categories() {
		return $this->hasMany(
			ProductCategory::class,
			'merchant_id'
		);
	}

	public function user_downloads() {
		return $this->hasMany(
			UserDownload::class,
			'merchant_id'
		);
	}

	public function user_deletes() {
		return $this->hasMany(
			Skp_UserDelete::class,
			'merchant_id'
		);
	}

	public function user_merchant_visits() {
		return $this->hasMany(
			UserMerchantVisit::class,
			'merchant_id'
		);
	}

	public function data_fields() {
		return $this->hasMany(
			DataField::class,
			'merchant_id'
		);
	}

	public function data_profiles() {
		return $this->hasMany(
			DataProfile::class,
			'merchant_id'
		);
	}

	public function export_records() {
		return $this->hasMany(
			ExportConnectorRecord::class,
			'merchant_id'
		);
	}

	public function product_types() {
		return $this->hasMany(
			ProductType::class,
			'merchant_id'
		);
	}

	public function invoice_receipt_config() {
		return $this->hasOne(InvoiceReceiptConfig::class);
	}

}