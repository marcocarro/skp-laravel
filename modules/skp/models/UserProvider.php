<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class UserProvider extends Model {

	protected $table = 'user_provider';
	protected $_belongs_to = array(
		'user' => array(
			'model' => 'Skp_User',
			'foreign_key' => 'user_id',
		),
	);
}

