<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class ProductGroupProduct extends ModelNameDescription {

	protected $table = 'productgroup_product';
	protected $_table_columns = Array(
		'id' => Array(),
		'productgroup_id' => Array(),
		'product_id' => Array(),
		'name' => Array(),
		'sort' => Array(),
		'extra' => Array(),
	);
	protected $_serialize_columns = array('name', 'extra');
	
	protected $_belongs_to = array(
		'productgroup' => array(
			'model' => 'Skp_ProductGroup',
			'foreign_key' => 'productgroup_id',
		),
		'product' => array(
			'model' => 'Skp_Product',
			'foreign_key' => 'product_id',
		),
	);
	
	public static function getSelectPlaceholderList()
	{
		return $select_placeholder_list = array(
			'select_placeholder.choose_person_type'=>__('select_placeholder.choose_person_type'),
			'select_placeholder.choose_size'=>__('select_placeholder.choose_size'),
			'select_placeholder.choose_height'=>__('select_placeholder.choose_height'),
			'select_placeholder.choose_type'=>__('select_placeholder.choose_type'),
			'select_placeholder.choose_duration'=>__('select_placeholder.choose_duration'),
			'select_placeholder.choose_age_range'=>__('select_placeholder.choose_age_range'),
			'select_placeholder.choose_colour'=>__('select_placeholder.choose_colour'),
		);
	}
}
