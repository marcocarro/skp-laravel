<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;

class Availability extends Model {

	protected $table = 'availability';
	
	protected $casts = [
		'extra' => 'array',
	];

	public function merchant() {
      return $this->belongsTo(Merchant::class);
	}

	public function calendars() {
		return $this->hasManyThrough(
			Calender::class,
			AvailabilityProductSkuCalendarPricelist::class,
			'availability_id',
			'calendar_id'
		);
	}

	public function availability_product_sku_calendar_pricelist() {
		return $this->hasMAny(
			AvailabilityProductSkuCalendarPricelist::class,
			'availability_id'
		);
	}

	public function bookings() {
		return $this->hasMany(
			Booking::class,
			'availability_id'
		);
	}

}