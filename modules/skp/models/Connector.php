<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Connector extends Model {

	protected $table = 'connector';
	public $timestamps = false;
	protected $casts = [
		'connector_extra' => 'array',
	];

	public function merchant() {
		$this->belongsTo(Merchant::class);
	}

	public function catalogs() {
		return $this->hasMAny(
			Catalog::class,
			'connector_id'
		);
	}

}