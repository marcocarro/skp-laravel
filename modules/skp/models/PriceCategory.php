<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class PriceCategory extends ModelNameDescription implements Interfaces_ConnectorExtra{

	protected $table = 'price_category';
	
	protected $_table_columns = array(
		'id' => array(),
		'merchant_id' => array (),
		'catalog_id' => array(),
		'name' => array(),
		'description' => array(),
		'external_id' => array(),
		'sort' => array(),
		'sold_free' => array(),
		'min_age' => array(),
		'max_age' => array(),
		'from_date' => array(),
		'to_date' => array(),
		'price_category_extra' => array(),
		'active' => array(),
		'archived' => array(),
		'deleted' => array(),
	);
	
	protected $_serialize_columns = array('price_category_extra', 'name', 'description');
	
	protected $_belongs_to = array(
		'catalog' => array(
			'model' => 'Skp_Catalog',
			'foreign_key' => 'catalog_id',
		),
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
	);
	protected $_has_many = array(
		'skus' => array(
			'model' => 'Skp_Sku',
			'foreign_key' => 'pricecategory_id',
		),
		'price_list_price_category' => array(
			'model' => 'Skp_PriceListPricecategory',
			'foreign_key' => 'price_category_id',
		),
	);
	
	public function rules()
	{
		return array(
			'min_age' => array(
				//array('not_empty'),
				array('digit'),
				array('range',array(':value',0,199)),
			),
			'max_age' => array(
				//array('not_empty'),
				array('digit'),
				array('range',array(':value',0,199)),
			),
			'from_date' => array(
				//array('not_empty'),
				array('date'),

			),
			'to_date' => array(
				//array('not_empty'),
				array('date'),
			),
		);
	}
	
	public static function getPriceCategories($params = array())
	{
		
		$merchant = Arr::get($params,'merchant', ORM::factory('Skp_Merchant'));
		$merchant_id = $merchant->pk();
		$pool_id = Arr::get($params,'pool_id');
		$group_id = Arr::get($params,'group_id');
		$only_active = Arr::get($params,'only_active', TRUE);
		$only_for = Arr::get($params,'only_for');
		$only_product_type = Arr::get($params,'only_product_type');
		$user = Arr::get($params,'user', ORM::factory('Skp_User'));
		$b2b_company_id = $merchant->hasAddOn('B2B') ? Arr::get($params, 'b2b_company_id', $user->getB2BCompany($merchant)->pk()) : NULL;
		
		$r = ORM::factory('Skp_PriceCategory');
		if ($merchant_id !== NULL)
		{
			$r = $r->where('skp_pricecategory.merchant_id', '=', $merchant_id);
			$r->and_where('skp_pricecategory.active', '=', 1);
		}
		
		if ($group_id !== NULL OR $only_active OR $only_for !== NULL OR $pool_id !== NULL)
		{
			$r->distinct(TRUE)->join('sku')->on('sku.pricecategory_id','=','skp_pricecategory.id')->join('skugroup_sku')->on('skugroup_sku.sku_id','=','sku.id')->join('skugroup')->on('skugroup_sku.skugroup_id','=','skugroup.id')->join('product')->on('product.id','=','sku.product_id')->join('pool_product')->on('pool_product.product_id','=','sku.product_id');
			if ($b2b_company_id !== NULL)
			{
				$r->join('b2b_company_skugroup', 'LEFT')->on('b2b_company_skugroup.skugroup_id','=','skugroup_sku.skugroup_id');
			}

			if ($group_id !== NULL)
			{
				$r->and_where('skugroup.id','=',$group_id);
			}
			$r->and_where_open();
			if ($only_active)
			{
				$r->and_where('skugroup.active','=',1);
			}
			if ($only_for !== NULL)
			{
				$r->and_where('skugroup.'.$only_for,'=',1);
			}
			if ($b2b_company_id !== NULL)
			{
				$r->or_where('b2b_company_skugroup.b2b_company_id', '=', $b2b_company_id);
			}
			$r->and_where_close();	
			if ($only_product_type !== NULL)
			{
				$r->and_where('product.producttype_id','=',$only_product_type);
			}
			if ($pool_id !== NULL)
			{
				$r->and_where('pool_product.pool_id', '=', $pool_id);
			}
			$r->order_by('skp_pricecategory.sort');
		}
		return $r;
	}

	public function setConnectorExtra($extra)
	{
		$current_extra = $this->get('price_category_extra');
		$current_extra['connector_object'] = $extra;
		$this->set('price_category_extra', $current_extra);
	}

	public function setExtra($extra_name, $extra_value, $save_empty_value = FALSE)
	{
		$ue = $this->get('price_category_extra');

		if ($extra_value != NULL)
		{
			$ue[$extra_name] = $extra_value;
		}else{
			unset($ue[$extra_name]);
		}
		$this->set('price_category_extra', $ue);
	}
	
	public function getConnectorExtra()
	{
		$current_extra = $this->get('price_category_extra');
		return Arr::get($current_extra, 'connector_object', NULL);
	}

	public function getOriginalName($separator = ',')
	{
		$original_name = "";
		foreach (Arr::get($this->get('price_category_extra'), 'original_name', array()) as $l => $n)
		{
			$original_name .= $l.': '.$n.$separator;
		}
		return $original_name;
	}

}
