<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class UserLog extends Model {

	protected $table = 'user_log';
	protected $_belongs_to = array(
		'user' => array(
			'model' => 'Skp_User',
			'foreign_key' => 'user_id',
		),
	);
	
	public static function getLastLog($userid_to_be_found)
	{
		return ORM::factory('Skp_UserLog')->where('user_id', '=', $userid_to_be_found)->order_by('login','desc')->find();
	}
}
