<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Trkey extends Model {
	protected $table = 'trkey';
	
	protected $_table_columns = Array(
		'id' => Array(),
		'trkey' => Array(),
		'trtext' => Array(),
		'merchant_id' => Array(),
		'user_email' => Array(),
		'lastchanged' => Array(),
		'remote_trkey' => Array(),
		'remote_trtext' => Array(),
		'note' => Array(),
	);
	protected $_serialize_columns = array('trtext','remote_trtext');
	
	protected $_belongs_to = array(
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
	);
	
	public function getFieldInLang($field_name, $lang = NULL,$separator = ',')
	{
		$result = '';
		if (! is_array($this->get($field_name)))
		{
			return $result;
		}
		$lang_array = $this->get($field_name);
		if ($lang == NULL)
		{
			foreach ($lang_array as $lang=>$text)
			{
				$result .= "$lang: '$text'".$separator;
			}
		}
		else
		{
			if (! isset($lang_array[$lang]))
			{
				$lang = key($lang_array); // Return first language found
			}
			if (isset($lang_array[$lang]))
			{
				$result = $lang_array[$lang];
			}
		}
		return $result;
	}

	public function getFieldInMerchantLang($field_name, $m_lang = NULL,$separator = ',')
	{
		$result = '';
		if (!is_array($this->get($field_name)) OR $m_lang == NULL)
		{
			return $result;
		}
		!is_array($m_lang) AND $m_lang = array($m_lang);
		$lang_array = $this->get($field_name);
		foreach ($lang_array as $lang=>$text)
		{
			if (in_array($lang, $m_lang))
			{
				$result .= "$lang: '$text'".$separator;
			}
		}
		return $result;
	}

	public function getTruncatedText($text, $chars = 200) {
		if (strlen($text) > $chars)
		{
		    $text = $text." "; //add a whitespace at the text end
		    $text = substr($text, 0, $chars); //creates a substring of the text long as $chars
		    $text = substr($text, 0, strrpos($text, ' ')); //truncates the string at the last white space to avoid truncated words
		    $n = substr_count($text, "\"");
		    if ($n%2!=0)
		    {
		    	$text.="\"";  //necessary to avoid ugly bug with datatables caused by unpaired ";
		    }
		    $text = $text."...";
		    return $text;
		}
		return $text;
	}

	public function getText($lang = NULL,$separator = ',')
	{
		return $this->getFieldInLang('trtext', $lang, $separator);
	}

	public function getRemoteText($lang = NULL,$separator = ',')
	{
		return $this->getFieldInLang('remote_trtext', $lang, $separator);
	}

}
