<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class InvoiceReceiptProfile extends Model {

	protected $table = 'invoice_receipt_profile';
	public $timestamps = false;
	protected $casts = [
		'extra' => 'array',
	];

	public function merchant() {
		$this->belongsTo(Merchant::class);
	}

	public function user() {
		$this->belongsTo(User::class);
	}
	
}