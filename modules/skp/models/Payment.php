<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Payment extends Model {

	protected $table = 'payment';
	protected $_table_columns = array(
		'id' => array(),
		'merchant_id' => array(),
		'order_id' => array(),
		'b2b_order_id' => array(),
		'transaction_id' => array(),
		'payment_channel_id' => array(),
		'payment_type' => array(),
		'credit_card_id' => array(),
		'b2b_credit_card_id' => array(),
		'amount' => array(),
		'currency' => array(),
		'status' => array(),
		'expire_date' => array(),
		'authorization' => array(),
		'extra' => array(),
	);
	
	
protected $casts = [
		'extra' => 'array',
	];
	
	protected $_belongs_to = array(
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
		'order' => array(
			'model' => 'Skp_Order',
			'foreign_key' => 'order_id',
		),
		'b2b_order' => array(
			'model' => 'Skp_B2BOrder',
			'foreign_key' => 'b2b_order_id',
		),
		'payment_channel' => array(
			'model' => 'Skp_PaymentChannel',
			'foreign_key' => 'payment_channel_id',
		),
		'credit_card' => array(
			'model' => 'Skp_CreditCard',
			'foreign_key' => 'credit_card_id',
		),
		'b2b_credit_card' => array(
			'model' => 'Skp_B2BCreditCard',
			'foreign_key' => 'b2b_credit_card_id',
		),
	);
	
	const REGISTERED = 'REGISTERED';
	const AUTHORIZED = 'AUTHORIZED';
	const CAPTURED = 'CAPTURED';
	const CANCELLED = 'CANCELLED';
	const AUTHORIZEFAILED = 'AUTHORIZE FAILED';
	const CAPTUREFAILED = 'CAPTURE FAILED';
	const CANCELFAILED = 'CANCEL FAILED';
	const FAILURE = 'FAILURE';
	
	public function getMoney()
	{
		return new Skp_Money($this->amount, $this->currency);
	}
	
	public function getTerminalUrl()
	{
		return $this->payment_channel->call('getTerminalUrl', array('payment'=>$this));
	}
	
	public function getTerminalPostData()
	{
		return $this->payment_channel->call('getTerminalPostData', array('payment'=>$this, 'order'=>$this->get('order')));
	}
	
	public static function getFromTransactionId($paychannelId, $transactionId)
	{
		$payment = ORM::factory('Skp_Payment')->where('payment_channel_id', '=', $paychannelId)->and_where('transaction_id','=',$transactionId)->find();
		return $payment;
	}
	
	public function query()
	{
		return $this->payment_channel->call('query', array('query.transactionId'=>$this->transaction_id));
	}
	
	public function auth()
	{
		if ($this->get('status') == static::REGISTERED)
		{
			$this->payment_channel->call('auth', array('payment.payment' => $this));
		}
	}

	public function capture()
	{
		if ($this->get('status') == static::AUTHORIZED)
		{
			$this->payment_channel->call('capture', array('payment.payment' => $this));
		}
	}
	
	public function cancel()
	{
		return $this->payment_channel->call('cancel', array('payment.payment' => $this));
	}

	public function setPaymentExpiredDay()
	{
		$order = $this->get('order');
		$payment_channel = $this->get('payment_channel');
		$b2b_company = $order->get('b2b_company');
		$expire_date =  $order->getCreationDate();
		if ($payment_channel->loaded() == TRUE AND $b2b_company->loaded() == TRUE)
		{
			$expire_date_data = $b2b_company->getExpireDateData($payment_channel);
		} else if ($payment_channel->loaded() == TRUE)
		{
			$expire_date_data = $payment_channel->getExpireDateData();
		} else {
			$expire_date = NULL;
		}
		if ($expire_date != NULL)
		{
			if (is_numeric($expire_date_data->b2b_payment_manager_num_day_for_pay) AND $expire_date_data->b2b_payment_manager_num_day_for_pay > 0)
			{
				$expire_date->add(new DateInterval('P'.$expire_date_data->b2b_payment_manager_num_day_for_pay.'D'));
			}
			if ($expire_date_data->b2b_payment_manager_num_day_for_pay_month_end == TRUE)
			{
				$expire_date->setDate($expire_date->format('Y'), (1+$expire_date->format('m')), 0);
			}
			if (is_numeric($expire_date_data->b2b_payment_manager_num_day_for_pay_month_day) AND $expire_date_data->b2b_payment_manager_num_day_for_pay_month_day > 0)
			{
				if ($expire_date->format('d') > $expire_date_data->b2b_payment_manager_num_day_for_pay_month_day)
				{
					$expire_date->setDate($expire_date->format('Y'), (1+$expire_date->format('m')), $expire_date_data->b2b_payment_manager_num_day_for_pay_month_day);
				} else
				{
					$expire_date->setDate($expire_date->format('Y'), $expire_date->format('m'), $expire_date_data->b2b_payment_manager_num_day_for_pay_month_day);
				}
			}
			$this->set('expire_date', $expire_date->format('Y-m-d H:i:s'));
		} else
		{
			$this->set('expire_date', $expire_date);
		}
		$this->save();
	}

	public function createCancelationPayment($cancelation_order)
	{
		$original_payment_as_array = $this->as_array();
		array_shift($original_payment_as_array); // to remove the primary key
		$canc_payment = ORM::factory('Skp_Payment');
		$canc_payment->values($original_payment_as_array);
		// Now we remove what we don't need
		$canc_payment->set('order_id', $cancelation_order->pk());
		$canc_payment->set('amount', -($this->get('amount')));
		$canc_payment->set('status', static::AUTHORIZED);
		$canc_payment->set('expire_date', NULL);
		$canc_payment->save();
		$payment_channel = $this->get('payment_channel');
		if ($payment_channel->loaded())
		{
			$result = $payment_channel->call('processCancelationPayment', array('payment.payment' => $this, 'payment.cancelation_payment' => $canc_payment));
		}
		return $canc_payment;
	}
	
	public function getProcessCode()
	{
		if ($this->get('order')->loaded())
		{
			return $this->get('order')->getProcessCode();
		}
		else
		{
			return FALSE;
		}
	}
	
	public function releaseProcessCode($process_code)
	{
		if ($this->get('order')->loaded())
		{
			return $this->get('order')->releaseProcessCode($process_code);
		}
		else
		{
			return FALSE;
		}
	}
}
