<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class InvoiceReceiptOrder extends Model {

	protected $table = 'invoice_receipt_order';
	public $timestamps = false;
	protected $casts = [
		'header' => 'array',
		'extra' => 'array',
	];

	public function merchant() {
		$this->belongsTo(Merchant::class);
	}

	public function order() {
		$this->belongsTo(Order::class);
	}
	
}