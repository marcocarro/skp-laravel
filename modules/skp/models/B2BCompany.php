<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
use Order;

class B2BCompany extends Model {

	protected $table = 'b2b_company';
	public $timestamps = false;
	protected $casts = [
		'extra' => 'array',
	];

	public function merchant() {
		$this->belongsTo(Merchant::class);
	}

	public function companyusers() {
		$this->hasMany(CompanyUser::class, 'b2b_company_id');
	}

	public function users() {
		$this->hasManyThrough(
			User::class,
			CompanyUser::class,
			'b2b_company_id',
			user_id
		);
	}

	public function orders() {
		$this->hasMany(
			Order::class,
			'b2b_company_id'
		);
	}

	public function payment_channels() {
		$this->hasManyThrough(
			PaymentChannel::class,
			B2BCompanyPaymentChannel::class,
			'b2b_company_id',
			'payment_channel_id'
		);
	}

	public function skugroups() {
		$this->hasManyThrough(
			SkuGroup::class,
			B2BCompanySkuGroup::class,
			'b2b_company_id',
			'skugroup_id'
		);
	}

	public function virtual_cards() {
		$this->hasMany(
			VirtualCard::class,
			'b2b_company_id'
		);
	}

	public function promocodes() {
		$this->hasManyThrough(
			Promocode::class,
			B2BCompany::class,
			'b2b_company_id',
			'promocode_id'
		);
	}

}