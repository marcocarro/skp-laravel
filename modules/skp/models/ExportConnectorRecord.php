<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;

class ExportConnectorRecord extends Model {

	protected $table = 'export_connector_record';
	public $timestamps = false;
	protected $casts = [
		'record_extra' => 'array',
	];

	public function merchant() {
		$this->belongsTo(Merchant::class);
	}

	public function export_connector() {
		$this->belongsTo(ExportConnector::class, 'export_connector_id');
	}
		
}