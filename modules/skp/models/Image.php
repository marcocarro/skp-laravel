<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Image extends Model {

	protected $table = 'image';
	public $timestamps = false;
	protected $casts = [
		'name' => 'array',
	];

	public function merchant() {
		$this->belongsTo(Merchant::class);
	}

	public function image_category() {
		$this->belongsTo(ImageCategory::class, 'imagecategory_id');
	}

	public function products() {
		return $this->hasMAny(
			ProductImage::class,
			'image_id'
		);
	}
	
}