<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Vat extends Model {

	protected $table = 'vat';
	
	protected $_table_columns = array(
		'id' => array(), 
		'merchant_id' => array (),
		'perc' => array (),
		'vat_extra' => array (),
	);
	protected $_serialize_columns = array('vat_extra');
	
	protected $_belongs_to = array(
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id',
		),
	);
	
	protected $_has_many = array(
		'products' => array(
			'model' => 'Skp_Product',
			'foreign_key' => 'vat_id',
		),
	);

	function getPerc($with_perc = TRUE)
	{
		return $this->get('perc') !== NULL ? ($this->get('perc') . ($with_perc == TRUE ? '%' : '')) : '-';
	}
	
	static function getVatReport($params = array())
	{
		$merchant = Arr::get($params, 'merchant');
		$m_tz = $merchant->getTimeZone();
		if ($merchant == NULL)
		{
			throw new Exception('merchant not set');
		}
		$date_from = Arr::get($params, 'date_from', (new DateTime('today', $m_tz))->setTime(0,0));
		$date_to = Arr::get($params, 'date_to', (clone $date_from)->setTime(23,59,59));
		$vat_perc = Arr::get($params, 'vat_perc');
		$vat_prodType = Arr::get($params,'vat_prodType');
		$vat_product = Arr::get($params,'vat_product');
		$vat_priceCategory = Arr::get($params,'vat_priceCategory');
		$vat_split_receipt_type = Arr::get($params,'vat_splitReceiptType');
		
		$sql_query = 'SELECT DATE(CONVERT_TZ(o.order_datetime, "UTC", :m_tz)) AS date,';
		$vat_prodType != NULL AND $sql_query .= ' pt.name AS pt_name, pt.id AS pt_id, ';
		$vat_product != NULL AND $sql_query .= 'oi.product_name AS p_name, sku.product_id AS p_id, ';
		$vat_split_receipt_type != NULL AND $sql_query .= 'iro.model AS receipt_type, ';
		$vat_priceCategory != NULL AND $sql_query .= 'oi.pricecategory_name AS pc_name, sku.pricecategory_id AS pc_id, ';
		$sql_query .= ' oi.vat_perc AS vat_perc, SUM(oi.vat_amount) AS vat_amount, SUM(oi.total_amount) as total_amount, oi.currency AS currency FROM skp_order_item oi JOIN skp_order o ON o.id=oi.order_id';
		if ($vat_prodType != NULL OR $vat_product != NULL OR $vat_priceCategory != NULL)
		{
			$sql_query .= ' LEFT JOIN skp_sku sku ON sku.id=oi.sku_id ';
			if ($vat_prodType != NULL)
			{
				$sql_query .= 'LEFT JOIN skp_product p ON p.id=sku.product_id LEFT JOIN skp_product_type pt ON pt.id=p.producttype_id';
			}
		}
		if ($vat_split_receipt_type != NULL)
		{
			$sql_query .= ' LEFT JOIN skp_invoice_receipt_order iro ON iro.order_id = oi.order_id';
		}
		$sql_query .= ' WHERE o.merchant_id=:merchant_id AND o.status=\'' . Order::STATUS_BOOKED . '\' AND DATE(CONVERT_TZ(o.order_datetime, "UTC", :m_tz))>=:date_from AND DATE(CONVERT_TZ(o.order_datetime, "UTC", :m_tz))<=:date_to';
		if ($vat_perc != NULL)
		{
			$sql_query .= ' AND oi.vat_perc=:vat_perc';
		}
		if ($vat_prodType != NULL AND $vat_prodType != 'all')
		{
			$sql_query .= ' AND pt.id=:vat_prodType';
		}
		if ($vat_product != NULL AND $vat_product != 'all')
		{
			$sql_query .= ' AND sku.product_id=:vat_product';
		}
		if ($vat_priceCategory != NULL AND $vat_priceCategory != 'all')
		{
			$sql_query .= ' AND sku.pricecategory_id=:vat_priceCategory';
		}
		$sql_query .= ' GROUP BY date, vat_perc, currency';

		$vat_prodType != NULL AND $sql_query .= ', pt.id';
		$vat_product != NULL AND $sql_query .= ', sku.product_id';
		$vat_priceCategory != NULL AND $sql_query .= ', sku.pricecategory_id';
		$vat_split_receipt_type != NULL AND $sql_query .= ', iro.model';

		$sql_query .= ' ORDER BY date, currency, vat_perc';
		$vat_prodType != NULL AND $sql_query .= ', pt.id';
		$vat_product != NULL AND $sql_query .= ', sku.product_id';
		$vat_priceCategory != NULL AND $sql_query .= ', sku.pricecategory_id';
		$vat_split_receipt_type != NULL AND $sql_query .= ', iro.model';

		$query = DB::query(Database::SELECT, $sql_query);
		$query->param(':merchant_id', $merchant->pk());
		$query->param(':m_tz', $merchant->get('timezone'));
		$query->param(':date_from', $date_from->setTimeZone($m_tz)->format('Y-m-d H:i:s'));
		$query->param(':date_to', $date_to->setTimeZone($m_tz)->format('Y-m-d H:i:s'));
		$query->param(':vat_perc', $vat_perc);
		if ($vat_prodType != NULL AND $vat_prodType != 'all')
		{
			$query->param(':vat_prodType', $vat_prodType);
		}
		if ($vat_product != NULL AND $vat_product != 'all')
		{
			$query->param(':vat_product', $vat_product);
		}
		if ($vat_priceCategory != NULL AND $vat_priceCategory != 'all')
		{
			$query->param(':vat_priceCategory', $vat_priceCategory);
		}
		$rows = $query->as_object()->execute();
		$result = array();
		foreach ($rows as $row)
		{
			$r = new StdClass;
			$r->date = $row->date;
			if ($vat_prodType != NULL)
			{
				$r->pt_name = json_decode($row->pt_name, TRUE);
				$r->pt_id = $row->pt_id;
			}
			if ($vat_product != NULL)
			{
				$r->p_name = json_decode($row->p_name, TRUE);
				$r->p_id = $row->p_id;
			}
			if ($vat_priceCategory != NULL)
			{
				$r->pc_name = json_decode($row->pc_name, TRUE);
				$r->pc_id = $row->pc_id;
			}
			if ($vat_split_receipt_type != NULL)
			{
				$r->receipt_type = $row->receipt_type;
			}
			$r->vat = new Skp_Vat($row->vat_perc, $row->vat_amount, $row->currency);
			$r->net_amount = new Skp_Money($row->total_amount-$row->vat_amount, $row->currency);
			$r->total_amount = new Skp_Money($row->total_amount, $row->currency);
			$result[] = $r;
		}
		return $result;
	}

	public function setExtra($extra_name, $extra_value, $save_empty_value = FALSE)
	{
		$ue = $this->get('vat_extra');

		if ($extra_value != NULL)
		{
			$ue[$extra_name] = $extra_value;
		}else{
			unset($ue[$extra_name]);
		}
		$this->set('vat_extra', $ue);
	}
	
	public function getExtra($key, $default_value = NULL)
	{
		if ($key != NULL)
		{
			return Arr::get($this->get('vat_extra'), $key, $default_value);
		}
	}
}
