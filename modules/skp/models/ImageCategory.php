<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class ImageCategory extends Model {

	protected $table = 'image_category';
	public $timestamps = false;

	public function merchant() {
		$this->belongsTo(Merchant::class);
	}

}