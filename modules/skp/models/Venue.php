<?php namespace Skp\Models;

use Illuminate\Database\Eloquent\Model;
class Venue extends ModelNameDescription {

	protected $table = 'venue';

	protected $_belongs_to = array(
		'merchant' => array(
			'model' => 'Skp_Merchant',
			'foreign_key' => 'merchant_id'
		)
	);
	
	protected $_has_many = array(
		'products' => array(
			'model' => 'Skp_Product',
			'foreign_key' => 'venue_id'
		)
	);

	public function rules()
	{
		return array(
		);
	}

}
