<?php

	return [
		'username' => env('USERNAME'),
		'password' => env('PASSWORD'),
		'basic_auth_url' => env('ARR_URL_BASIC_AUTH')
	]

?>