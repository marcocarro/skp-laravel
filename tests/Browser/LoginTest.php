<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Tests\Browser\Components\Login\LoggedUserMenu;
use Tests\Browser\Components\ShoptabContent;
use Tests\Browser\Pages\HomePage;
use Tests\Browser\Pages\BasicAuthPage;

class LoginTest extends DuskTestCase
{
	/**
	 * test login
	*
	* @return void
	*/
	public function testLogin() {
		$this->browse(function (Browser $browser) {
			$browser->visit(new BasicAuthPage)
					->waitUntilMissing('.loader')
					->visit(new HomePage)
					->pause(1000)
					->doLogin()
					->pause(500)
					->within(new LoggedUserMenu, function(Browser $browser) {
						$browser->assertSeeIn('@welcome', 'Welcome');
					});
		});
	}
}
