<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Components\T_1_2_Daily_Skypass as ComponentsT_1_2_Daily_Skypass;
use Tests\Browser\Pages\T_1_2_Daily_Skypass;
use Tests\Browser\Pages\BasicAuthPage;
use Tests\DuskTestCase;

class testProductT1_2DailySkipass extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testProductT1_2DailySkipass()
    {
        $this->browse(function (Browser $browser) {
			$browser->visit(new T_1_2_Daily_Skypass)
                    ->pause(10000)
                    ->screenshot('T_1_2_Daily_Skypass')
                    ->doLogin()
                    ->pause(10000)
                    ->screenshot('T_1_2_Daily_Skypass_1');
        });
    }
}
