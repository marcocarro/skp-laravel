<?php

namespace Tests\Browser\Pages;

class T_1_1_Daily_Skypass extends Page
{
    public function url()
    {
        return '/autumn/store#/en/autumn/buy?skugroup_id=203&product_id=1208&bookable_y_n_a=a';
    }

}
