<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Page as BasePage;
use Laravel\Dusk\Browser;
use Tests\Browser\Components\Login\LoginLink;
use Tests\Browser\Components\Login\LoginModal;

abstract class Page extends BasePage
{
	 /**
	  * Get the global element shortcuts for the site.
	  *
	  * @return array
	  */
	 public static function siteElements()
	 {
		  return [];
	 }

	 public function doLogin(Browser $browser) {
		  $browser->waitUntilMissing('.loader', 5)
					 ->whenAvailable(new LoginLink, function(Browser $browser) {
							$browser->openLoginModal();
					 })
					 ->whenAvailable(new LoginModal, function(Browser $browser) {
							$browser->enterLoginInfo()->submit();
					 });
	 }
}
