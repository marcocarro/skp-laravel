<?php

namespace Tests\Browser\Pages;

class T_1_2_Daily_Skypass extends Page
{
    public function url()
    {
        return '/autumn/buy?skugroup_id=203&product_id=1209&bookable_y_n_a=a';
    }

}
