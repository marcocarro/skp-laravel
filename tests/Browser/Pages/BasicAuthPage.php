<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;

class BasicAuthPage extends Page {
	/**
	 * Get the URL for the page.
	*
	* @return string
	*/
	public function url() {
		return config('login.basic_auth_url') . '/';
	}

}