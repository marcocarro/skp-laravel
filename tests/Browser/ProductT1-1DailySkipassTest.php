<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Components\T_1_1_Daily_Skypass as ComponentsT_1_1_Daily_Skypass;
use Tests\Browser\Pages\T_1_1_Daily_Skypass;
use Tests\Browser\Pages\BasicAuthPage;
use Tests\DuskTestCase;

class testProductT1_1DailySkipass extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testProductT1_1DailySkipass()
    {
        $this->browse(function (Browser $browser) {
			$browser->visit(new BasicAuthPage)
					->waitUntilMissing('.loader')
					->visit(new T_1_1_Daily_Skypass)
                    ->pause(1000)
                    ->screenshot('T_1_1_Daily_Skypass')
                    ->doLogin()
                    ->pause(10000)
                    ->screenshot('T_1_1_Daily_Skypass_1')
                    ->click(new ComponentsT_1_1_Daily_Skypass)
                    ->within(new ComponentsT_1_1_Daily_Skypass, function(Browser $browser) {
                        $browser->click('@date_picker');
                    })
                    ->screenshot('date_picker');
        });
    }
}
