<?php

namespace Tests\Browser\Components\Login;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;

class LoggedUserMenu extends BaseComponent
{
    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector()
    {
        return '#js-page-header > div > div > div.header-user-menu.gc.xs-hide.xl-block.xl-gc--none.xl-mr2 > div';
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@welcome' => '> div:nth-child(1)'
        ];
    }
}
