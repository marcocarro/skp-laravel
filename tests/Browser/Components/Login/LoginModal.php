<?php

namespace Tests\Browser\Components\Login;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;

class LoginModal extends BaseComponent
{
	/**
	 * Get the root selector for the component.
	*
	* @return string
	*/
	public function selector()
	{
		return '#modal_content_html';
	}

	/**
	 * Get the element shortcuts for the component.
	*
	* @return array
	*/
	public function elements()
	 {
		return [
			'@email' => '#login-email',
			'@password' => '#login-password',
			'@submit' => '#login-form > fieldset:nth-child(7) > input'
		];
	}

	public function enterLoginInfo(Browser $browser) {
		$browser->type('@email', config('login.username'))
		->type('@password', config('login.password'));
	}

	public function submit(Browser $browser) {
		$browser->press('@submit');
	}
}
