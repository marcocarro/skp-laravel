<?php

namespace Tests\Browser\Components\Login;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;

class LoginLink extends BaseComponent
{
    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector()
    {
        return '#js-page-header > div > div > div.header-user-menu.gc.xs-hide.xl-block.xl-gc--none.xl-mr2 > div > div > a:nth-child(2)';
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements()
    {
        return [];
    }

    public function openLoginModal(Browser $browser) {
        $browser->press($this);
    }
}
