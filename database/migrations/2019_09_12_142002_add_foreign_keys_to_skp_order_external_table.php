<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpOrderExternalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('order_external', function(Blueprint $table)
		{
			$table->foreign('order_id', 'skp_order_external_ibfk_1')->references('id')->on('order')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('connector_id', 'skp_order_external_ibfk_2')->references('id')->on('connector')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('order_external', function(Blueprint $table)
		{
			$table->dropForeign('skp_order_external_ibfk_1');
			$table->dropForeign('skp_order_external_ibfk_2');
		});
	}

}
