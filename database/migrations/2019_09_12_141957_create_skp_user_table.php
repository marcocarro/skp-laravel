<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('email')->nullable()->index('email');
			$table->boolean('verified')->nullable()->default(0);
			$table->string('status')->default('virtual');
			$table->boolean('deleted')->default(0);
			$table->string('password', 64)->nullable();
			$table->string('title')->nullable();
			$table->string('first_name', 64)->nullable();
			$table->string('middle_name', 64)->nullable();
			$table->string('last_name', 64)->nullable();
			$table->date('birth_date')->nullable();
			$table->string('gender')->nullable();
			$table->string('street_num', 16)->nullable();
			$table->string('street', 128)->nullable();
			$table->string('city', 128)->nullable();
			$table->string('state_prov', 64)->nullable();
			$table->string('zip', 32)->nullable();
			$table->char('country', 2)->nullable();
			$table->string('phone', 32)->nullable();
			$table->binary('photo', 16777215)->nullable();
			$table->string('image_url_thumb')->nullable();
			$table->string('last_language', 5)->nullable();
			$table->string('is_admin')->default('N');
			$table->string('autologin_cookie', 64)->nullable();
			$table->text('user_extra', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user');
	}

}
