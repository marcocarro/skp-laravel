<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpProductgroupProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('productgroup_product', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('productgroup_id')->unsigned()->index('productgroup_id');
			$table->integer('product_id')->unsigned()->index('product_id');
			$table->text('name', 65535)->nullable();
			$table->boolean('sort')->default(1);
			$table->text('extra', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('productgroup_product');
	}

}
