<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpInvoiceReceiptOrderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('invoice_receipt_order', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_invoice_receipt_order_ibfk_1')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('user_id', 'skp_invoice_receipt_order_ibfk_2')->references('id')->on('user')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('order_id', 'skp_invoice_receipt_order_ibfk_3')->references('id')->on('order')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('invoice_receipt_order', function(Blueprint $table)
		{
			$table->dropForeign('skp_invoice_receipt_order_ibfk_1');
			$table->dropForeign('skp_invoice_receipt_order_ibfk_2');
			$table->dropForeign('skp_invoice_receipt_order_ibfk_3');
		});
	}

}
