<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpUserDeleteTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_delete', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->index('merchant_id');
			$table->integer('user_id')->unsigned()->index('user_id');
			$table->string('delete_status')->default('REQUESTED');
			$table->timestamp('request_datetime')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->dateTime('delete_datetime')->nullable();
			$table->text('extra', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_delete');
	}

}
