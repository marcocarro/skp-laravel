<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpOrderExternalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_external', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('order_id')->unsigned()->index('order_id');
			$table->integer('connector_id')->unsigned()->nullable()->index('connector_it');
			$table->string('external_order_type', 32)->nullable();
			$table->text('external_reference', 16777215)->nullable();
			$table->text('external_extra', 16777215)->nullable();
			$table->string('status')->nullable();
			$table->dateTime('next_sync')->nullable();
			$table->integer('sync_tries')->nullable();
			$table->index(['status','external_order_type','next_sync'], 'status');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_external');
	}

}
