<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpTmpCardImportTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tmp_card_import', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('card_id')->unsigned()->nullable()->index('card_id');
			$table->integer('user_id')->unsigned()->nullable()->index('user_id');
			$table->integer('order_external_id')->unsigned()->nullable()->index('order_external_id');
			$table->string('card_type', 64)->nullable();
			$table->string('card_serial', 32)->nullable();
			$table->text('external_extra', 16777215)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tmp_card_import');
	}

}
