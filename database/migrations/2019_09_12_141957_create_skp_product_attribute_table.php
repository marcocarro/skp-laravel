<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpProductAttributeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_attribute', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('product_id')->unsigned()->index('product_id');
			$table->string('name')->nullable();
			$table->string('type')->nullable();
			$table->string('ext_key')->nullable();
			$table->string('ext_default')->nullable();
			$table->string('ext_type_id')->nullable();
			$table->boolean('ext_send')->default(0);
			$table->boolean('variable')->default(1);
			$table->integer('data_field_id')->unsigned()->nullable()->index('data_field_id');
			$table->string('fixed_value')->nullable();
			$table->string('status');
			$table->text('extra', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_attribute');
	}

}
