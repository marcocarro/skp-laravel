<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpProductgroupProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('productgroup_product', function(Blueprint $table)
		{
			$table->foreign('productgroup_id', 'skp_productgroup_product_ibfk_1')->references('id')->on('productgroup')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('product_id', 'skp_productgroup_product_ibfk_2')->references('id')->on('product')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('productgroup_product', function(Blueprint $table)
		{
			$table->dropForeign('skp_productgroup_product_ibfk_1');
			$table->dropForeign('skp_productgroup_product_ibfk_2');
		});
	}

}
