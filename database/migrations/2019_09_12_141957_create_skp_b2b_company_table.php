<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpB2bCompanyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('b2b_company', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->nullable()->index('merchant_id');
			$table->string('company_type')->default('Other');
			$table->string('name')->nullable();
			$table->string('street_num')->nullable();
			$table->string('street')->nullable();
			$table->string('city', 128)->nullable();
			$table->string('state_prov', 64)->nullable();
			$table->string('zip', 32)->nullable();
			$table->char('country', 2)->nullable();
			$table->string('phone', 32)->nullable();
			$table->string('fax', 32)->nullable();
			$table->string('org_number', 32)->nullable();
			$table->string('website', 128)->nullable();
			$table->string('email')->nullable();
			$table->string('invoice_email')->nullable();
			$table->string('vat_num', 64)->nullable();
			$table->decimal('commission_perc', 5)->nullable()->default(0.00);
			$table->decimal('commission_fixed', 16, 4)->nullable()->default(0.0000);
			$table->string('currency', 5)->nullable();
			$table->boolean('deleted')->default(0);
			$table->string('active')->default('Y');
			$table->text('extra', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('b2b_company');
	}

}
