<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpPromoGenerationGroupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('promo_generation_group', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'merchant')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('promo_generation_group', function(Blueprint $table)
		{
			$table->dropForeign('merchant');
		});
	}

}
