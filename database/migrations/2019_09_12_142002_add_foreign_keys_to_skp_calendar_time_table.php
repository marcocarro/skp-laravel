<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpCalendarTimeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('calendar_time', function(Blueprint $table)
		{
			$table->foreign('calendar_id', 'skp_calendar_time_ibfk_1')->references('id')->on('calendar')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('calendar_time', function(Blueprint $table)
		{
			$table->dropForeign('skp_calendar_time_ibfk_1');
		});
	}

}
