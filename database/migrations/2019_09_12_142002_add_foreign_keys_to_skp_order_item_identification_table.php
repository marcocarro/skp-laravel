<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpOrderItemIdentificationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('order_item_identification', function(Blueprint $table)
		{
			$table->foreign('orderitem_id', 'skp_order_item_identification_ibfk_1')->references('id')->on('order_item')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('order_item_identification', function(Blueprint $table)
		{
			$table->dropForeign('skp_order_item_identification_ibfk_1');
		});
	}

}
