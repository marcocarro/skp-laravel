<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpProductAssociationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('product_association', function(Blueprint $table)
		{
			$table->foreign('main_product_id', 'skp_product_association_ibfk_1')->references('id')->on('product')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('main_sku_id', 'skp_product_association_ibfk_2')->references('id')->on('sku')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('sub_product_id', 'skp_product_association_ibfk_3')->references('id')->on('product')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('sub_sku_id', 'skp_product_association_ibfk_4')->references('id')->on('sku')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product_association', function(Blueprint $table)
		{
			$table->dropForeign('skp_product_association_ibfk_1');
			$table->dropForeign('skp_product_association_ibfk_2');
			$table->dropForeign('skp_product_association_ibfk_3');
			$table->dropForeign('skp_product_association_ibfk_4');
		});
	}

}
