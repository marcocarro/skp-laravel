<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpB2bOrderExternalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('b2b_order_external', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('b2b_order_id')->unsigned()->index('order_id');
			$table->integer('connector_id')->unsigned()->nullable()->index('connector_it');
			$table->string('external_order_type', 32)->nullable();
			$table->text('external_reference', 65535)->nullable();
			$table->text('external_extra', 65535)->nullable();
			$table->string('status')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('b2b_order_external');
	}

}
