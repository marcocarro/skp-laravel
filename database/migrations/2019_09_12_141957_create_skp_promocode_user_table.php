<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpPromocodeUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('promocode_user', function(Blueprint $table)
		{
			$table->integer('promocode_id')->unsigned();
			$table->integer('user_id')->unsigned()->index('user_id');
			$table->primary(['promocode_id','user_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('promocode_user');
	}

}
