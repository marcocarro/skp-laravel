<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpOrderItemTagsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('order_item_tags', function(Blueprint $table)
		{
			$table->foreign('order_id', 'skp_order_item_tags_ibfk_1')->references('id')->on('order')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('order_item_id', 'skp_order_item_tags_ibfk_2')->references('id')->on('order_item')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('order_item_tags', function(Blueprint $table)
		{
			$table->dropForeign('skp_order_item_tags_ibfk_1');
			$table->dropForeign('skp_order_item_tags_ibfk_2');
		});
	}

}
