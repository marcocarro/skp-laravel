<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpOrderItemIdentificationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_item_identification', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('orderitem_id')->unsigned()->index('orderitem_id');
			$table->string('type')->nullable();
			$table->string('subtype')->nullable();
			$table->string('value')->nullable();
			$table->text('extra', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_item_identification');
	}

}
