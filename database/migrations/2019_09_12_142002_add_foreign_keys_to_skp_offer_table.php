<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpOfferTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('offer', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_offer_ibfk_1')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('image_id', 'skp_offer_ibfk_2')->references('id')->on('image')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('offer', function(Blueprint $table)
		{
			$table->dropForeign('skp_offer_ibfk_1');
			$table->dropForeign('skp_offer_ibfk_2');
		});
	}

}
