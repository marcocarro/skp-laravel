<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpB2bOrderItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('b2b_order_item', function(Blueprint $table)
		{
			$table->foreign('sku_id', 'skp_b2b_order_item_ibfk_2')->references('id')->on('sku')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('pool_id', 'skp_b2b_order_item_ibfk_4')->references('id')->on('pool')->onUpdate('CASCADE')->onDelete('NO ACTION');
			$table->foreign('order_id', 'skp_b2b_order_item_ibfk_5')->references('id')->on('b2b_order')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('sub_orderitem_of_id', 'skp_b2b_order_item_ibfk_6')->references('id')->on('b2b_order_item')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('vat_id', 'skp_b2b_order_item_ibfk_7')->references('id')->on('vat')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('related_orderitem_id', 'skp_b2b_order_item_ibfk_8')->references('id')->on('b2b_order_item')->onUpdate('CASCADE')->onDelete('NO ACTION');
			$table->foreign('promocode_id', 'skp_b2b_order_item_ibfk_9')->references('id')->on('promocode')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('b2b_order_item', function(Blueprint $table)
		{
			$table->dropForeign('skp_b2b_order_item_ibfk_2');
			$table->dropForeign('skp_b2b_order_item_ibfk_4');
			$table->dropForeign('skp_b2b_order_item_ibfk_5');
			$table->dropForeign('skp_b2b_order_item_ibfk_6');
			$table->dropForeign('skp_b2b_order_item_ibfk_7');
			$table->dropForeign('skp_b2b_order_item_ibfk_8');
			$table->dropForeign('skp_b2b_order_item_ibfk_9');
		});
	}

}
