<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpUserMerchantOptinTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_merchant_optin', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->index('merchant_id');
			$table->integer('user_id')->unsigned()->index('user_id');
			$table->integer('optin_id')->unsigned()->index('optin_id');
			$table->boolean('accepted')->nullable();
			$table->dateTime('last_change')->nullable();
			$table->unique(['merchant_id','user_id','optin_id'], 'merchant_id_2');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_merchant_optin');
	}

}
