<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpPaymentChannelTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('payment_channel', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_payment_channel_ibfk_1')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('payment_channel', function(Blueprint $table)
		{
			$table->dropForeign('skp_payment_channel_ibfk_1');
		});
	}

}
