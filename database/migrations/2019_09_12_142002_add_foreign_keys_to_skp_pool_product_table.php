<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpPoolProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pool_product', function(Blueprint $table)
		{
			$table->foreign('pool_id', 'skp_pool_product_ibfk_1')->references('id')->on('pool')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('product_id', 'skp_pool_product_ibfk_2')->references('id')->on('product')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pool_product', function(Blueprint $table)
		{
			$table->dropForeign('skp_pool_product_ibfk_1');
			$table->dropForeign('skp_pool_product_ibfk_2');
		});
	}

}
