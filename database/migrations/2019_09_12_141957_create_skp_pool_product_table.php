<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpPoolProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pool_product', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('pool_id')->unsigned()->index('pool_id');
			$table->integer('product_id')->unsigned()->index('product_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pool_product');
	}

}
