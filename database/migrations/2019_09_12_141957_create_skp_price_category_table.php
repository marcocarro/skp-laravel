<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpPriceCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('price_category', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->index('merchant_id_2');
			$table->integer('catalog_id')->unsigned()->index('catalog_id');
			$table->text('name', 65535);
			$table->text('description', 65535)->nullable();
			$table->string('external_id')->nullable();
			$table->integer('sort')->unsigned()->default(1);
			$table->boolean('sold_free')->nullable()->default(0);
			$table->smallInteger('min_age')->unsigned()->nullable();
			$table->smallInteger('max_age')->unsigned()->nullable();
			$table->date('from_date')->nullable();
			$table->date('to_date')->nullable();
			$table->text('price_category_extra', 65535)->nullable();
			$table->boolean('active');
			$table->boolean('archived')->default(0);
			$table->boolean('deleted')->default(0);
			$table->index(['merchant_id','catalog_id'], 'merchant_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('price_category');
	}

}
