<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpAddonTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('addon', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 64);
			$table->integer('merchant_id')->unsigned()->index('merchant_id');
			$table->char('is_active', 1)->default('N');
			$table->dateTime('active_from')->nullable();
			$table->dateTime('active_to')->nullable();
			$table->text('extra', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('addon');
	}

}
