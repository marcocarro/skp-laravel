<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpInvoiceReceiptOrderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoice_receipt_order', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->index('merchant_id');
			$table->integer('user_id')->unsigned()->index('user_id');
			$table->integer('order_id')->unsigned()->index('order_id');
			$table->string('model', 32);
			$table->string('reference_num', 32)->nullable();
			$table->integer('fiscal_year_num')->unsigned()->nullable();
			$table->integer('model_num')->unsigned()->nullable();
			$table->integer('universal_num')->unsigned()->nullable();
			$table->text('header', 65535)->nullable();
			$table->date('creation_date')->nullable();
			$table->text('extra', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invoice_receipt_order');
	}

}
