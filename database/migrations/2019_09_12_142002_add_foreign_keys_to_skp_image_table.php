<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpImageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('image', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_image_ibfk_1')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('imagecategory_id', 'skp_image_ibfk_2')->references('id')->on('image_category')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('image', function(Blueprint $table)
		{
			$table->dropForeign('skp_image_ibfk_1');
			$table->dropForeign('skp_image_ibfk_2');
		});
	}

}
