<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpB2bCompanyUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('b2b_company_user', function(Blueprint $table)
		{
			$table->foreign('b2b_company_id', 'skp_b2b_company_user_ibfk_1')->references('id')->on('b2b_company')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('user_id', 'skp_b2b_company_user_ibfk_2')->references('id')->on('user')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('b2b_company_user', function(Blueprint $table)
		{
			$table->dropForeign('skp_b2b_company_user_ibfk_1');
			$table->dropForeign('skp_b2b_company_user_ibfk_2');
		});
	}

}
