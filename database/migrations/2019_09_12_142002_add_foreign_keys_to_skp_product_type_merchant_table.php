<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpProductTypeMerchantTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('product_type_merchant', function(Blueprint $table)
		{
			$table->foreign('product_type_id', 'skp_product_type_merchant_ibfk_1')->references('id')->on('product_type')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('merchant_id', 'skp_product_type_merchant_ibfk_2')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product_type_merchant', function(Blueprint $table)
		{
			$table->dropForeign('skp_product_type_merchant_ibfk_1');
			$table->dropForeign('skp_product_type_merchant_ibfk_2');
		});
	}

}
