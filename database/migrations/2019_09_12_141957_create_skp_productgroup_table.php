<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpProductgroupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('productgroup', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->index('merchant_id');
			$table->text('name', 65535);
			$table->text('short_description', 65535)->nullable();
			$table->text('description', 65535)->nullable();
			$table->text('practical_info', 65535)->nullable();
			$table->integer('venue_id')->unsigned()->nullable()->index('venue_id');
			$table->text('extra', 65535)->nullable();
			$table->boolean('active')->default(0);
			$table->boolean('archived')->default(0);
			$table->boolean('deleted')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('productgroup');
	}

}
