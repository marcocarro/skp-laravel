<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('product', function(Blueprint $table)
		{
			$table->foreign('catalog_id', 'skp_product_ibfk_1')->references('id')->on('catalog')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('validity_calendar_id', 'skp_product_ibfk_10')->references('id')->on('calendar')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('merchant_id', 'skp_product_ibfk_2')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('productcategory_id', 'skp_product_ibfk_3')->references('id')->on('product_category')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('producttype_id', 'skp_product_ibfk_4')->references('id')->on('product_type')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('vat_id', 'skp_product_ibfk_5')->references('id')->on('vat')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('venue_id', 'skp_product_ibfk_6')->references('id')->on('venue')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('onsale_calendar_id', 'skp_product_ibfk_7')->references('id')->on('calendar')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product', function(Blueprint $table)
		{
			$table->dropForeign('skp_product_ibfk_1');
			$table->dropForeign('skp_product_ibfk_10');
			$table->dropForeign('skp_product_ibfk_2');
			$table->dropForeign('skp_product_ibfk_3');
			$table->dropForeign('skp_product_ibfk_4');
			$table->dropForeign('skp_product_ibfk_5');
			$table->dropForeign('skp_product_ibfk_6');
			$table->dropForeign('skp_product_ibfk_7');
		});
	}

}
