<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpProductgroupImageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('productgroup_image', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('productgroup_id')->unsigned()->index('productgroup_id');
			$table->integer('image_id')->unsigned()->index('image_id');
			$table->boolean('sort')->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('productgroup_image');
	}

}
