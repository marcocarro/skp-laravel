<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpProductgroupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('productgroup', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_productgroup_ibfk_1')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('venue_id', 'skp_productgroup_ibfk_2')->references('id')->on('venue')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('productgroup', function(Blueprint $table)
		{
			$table->dropForeign('skp_productgroup_ibfk_1');
			$table->dropForeign('skp_productgroup_ibfk_2');
		});
	}

}
