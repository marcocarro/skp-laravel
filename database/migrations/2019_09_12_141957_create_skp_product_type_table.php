<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpProductTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_type', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->nullable()->index('merchant_id');
			$table->string('type')->default('Ticket');
			$table->string('name');
			$table->boolean('preselect')->default(0);
			$table->boolean('aggregate')->default(0);
			$table->boolean('store_show')->default(0);
			$table->text('extra', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_type');
	}

}
