<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpLayoutTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('layout', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->nullable()->index('merchant_id')->comment('set it to NULL if this has to be shared with all the merchants');
			$table->string('layout_category', 32)->nullable()->index('layout_category');
			$table->string('name', 64);
			$table->string('controller')->nullable();
			$table->string('action')->nullable();
			$table->string('family');
			$table->text('options', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('layout');
	}

}
