<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpVirtualCardRealCardTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('virtual_card_real_card', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('virtual_card_id')->unsigned()->index('virtual_card_id');
			$table->string('card_type', 64);
			$table->string('card_serial', 32);
			$table->text('extra', 65535)->nullable();
			$table->boolean('deleted')->default(0);
			$table->index(['virtual_card_id','card_type'], 'virtual_card_id_2');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('virtual_card_real_card');
	}

}
