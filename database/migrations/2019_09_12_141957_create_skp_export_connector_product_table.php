<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpExportConnectorProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('export_connector_product', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('export_connector_id')->unsigned()->index('export_connector_id');
			$table->integer('product_id')->unsigned()->index('product_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('export_connector_product');
	}

}
