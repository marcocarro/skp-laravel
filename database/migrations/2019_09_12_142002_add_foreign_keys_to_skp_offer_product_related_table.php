<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpOfferProductRelatedTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('offer_product_related', function(Blueprint $table)
		{
			$table->foreign('offer_id', 'skp_offer_product_related_ibfk_1')->references('id')->on('offer')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('product_id', 'skp_offer_product_related_ibfk_2')->references('id')->on('product')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('offer_product_related', function(Blueprint $table)
		{
			$table->dropForeign('skp_offer_product_related_ibfk_1');
			$table->dropForeign('skp_offer_product_related_ibfk_2');
		});
	}

}
