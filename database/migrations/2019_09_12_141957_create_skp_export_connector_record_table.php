<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpExportConnectorRecordTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('export_connector_record', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->index('merchant_id');
			$table->integer('export_connector_id')->unsigned()->nullable()->index('export_connector_id');
			$table->integer('order_id')->unsigned()->nullable()->index('order_id');
			$table->integer('orderitem_id')->unsigned()->nullable()->index('orderitem_id');
			$table->string('export_status', 32)->default('New');
			$table->dateTime('order_datetime')->nullable();
			$table->dateTime('validity_datetime')->nullable();
			$table->timestamp('record_datetime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->text('record_extra', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('export_connector_record');
	}

}
