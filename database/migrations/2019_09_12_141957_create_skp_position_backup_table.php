<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpPositionBackupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('position_backup', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned()->index('user_id');
			$table->string('domain', 64)->nullable()->index('domain');
			$table->timestamp('when')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('device_type', 16)->nullable()->default('Desktop');
			$table->string('device_platform', 32)->nullable()->default('Unknown');
			$table->decimal('latitude', 10, 7);
			$table->decimal('longitude', 10, 7);
			$table->string('street_num', 16)->nullable();
			$table->string('street', 128)->nullable();
			$table->string('city', 128)->nullable();
			$table->string('area_level_3', 64)->nullable();
			$table->string('area_level_2', 64)->nullable();
			$table->string('area_level_1', 64)->nullable();
			$table->char('country', 2)->nullable();
			$table->string('zip', 32)->nullable();
			$table->string('formatted_address')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('position_backup');
	}

}
