<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpB2bCompanyPaymentChannelTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('b2b_company_payment_channel', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('b2b_company_id')->unsigned()->index('b2b_company_id');
			$table->integer('payment_channel_id')->unsigned()->index('payment_channel_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('b2b_company_payment_channel');
	}

}
