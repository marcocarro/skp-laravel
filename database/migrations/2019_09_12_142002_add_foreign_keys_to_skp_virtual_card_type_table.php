<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpVirtualCardTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('virtual_card_type', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_virtual_card_type_ibfk_1')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('virtual_card_type', function(Blueprint $table)
		{
			$table->dropForeign('skp_virtual_card_type_ibfk_1');
		});
	}

}
