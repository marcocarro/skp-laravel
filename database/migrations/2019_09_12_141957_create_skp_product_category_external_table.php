<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpProductCategoryExternalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_category_external', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('product_category_id')->unsigned()->nullable()->index('product_category_id');
			$table->integer('catalog_id')->unsigned()->index('catalog_id');
			$table->text('name', 65535)->nullable();
			$table->text('description', 65535)->nullable();
			$table->string('external_id');
			$table->text('extra', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_category_external');
	}

}
