<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpMessageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('message', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_message_ibfk_3')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('order_id', 'skp_message_ibfk_4')->references('id')->on('order')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('user_id', 'skp_message_ibfk_5')->references('id')->on('user')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('message_channel_id', 'skp_message_ibfk_6')->references('id')->on('message_channel')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('b2b_order_id', 'skp_message_ibfk_7')->references('id')->on('b2b_order')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('message', function(Blueprint $table)
		{
			$table->dropForeign('skp_message_ibfk_3');
			$table->dropForeign('skp_message_ibfk_4');
			$table->dropForeign('skp_message_ibfk_5');
			$table->dropForeign('skp_message_ibfk_6');
			$table->dropForeign('skp_message_ibfk_7');
		});
	}

}
