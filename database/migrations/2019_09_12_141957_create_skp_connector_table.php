<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpConnectorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('connector', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->index('merchant');
			$table->string('name')->nullable();
			$table->string('connection_status')->default('NOT CHECKED');
			$table->string('connector_class');
			$table->boolean('test_env')->default(1);
			$table->text('connector_extra', 65535)->nullable();
			$table->string('dta_user')->nullable();
			$table->string('dta_password')->nullable();
			$table->string('dta_clientId')->nullable();
			$table->string('wtp_location', 128)->nullable();
			$table->string('wtp_username', 32)->nullable();
			$table->string('wtp_password', 32)->nullable();
			$table->string('wtp_wtploginid', 32)->nullable();
			$table->string('wtp_wtpuser', 32)->nullable();
			$table->string('wtp_wtppassword', 32)->nullable();
			$table->string('wtp_session_id', 32)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('connector');
	}

}
