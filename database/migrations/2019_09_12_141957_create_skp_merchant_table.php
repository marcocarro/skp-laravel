<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpMerchantTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('merchant', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->string('domain')->nullable()->unique('domain');
			$table->string('api_key', 40)->nullable();
			$table->string('currency', 5);
			$table->string('date_format', 10)->default('d/m/Y');
			$table->string('default_card_type', 64)->nullable()->default('Skidata Keycard');
			$table->integer('contact_profile_id')->unsigned()->nullable()->index('contact_profile_id');
			$table->integer('team_contact_profile_id')->unsigned()->nullable()->index('team_contact_profile_id');
			$table->string('timezone', 32)->default('Europe/Oslo');
			$table->string('service_status')->default('Off');
			$table->text('merchant_extra', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('merchant');
	}

}
