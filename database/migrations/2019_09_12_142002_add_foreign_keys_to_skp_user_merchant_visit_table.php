<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpUserMerchantVisitTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_merchant_visit', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_user_merchant_visit_ibfk_1')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('user_id', 'skp_user_merchant_visit_ibfk_2')->references('id')->on('user')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_merchant_visit', function(Blueprint $table)
		{
			$table->dropForeign('skp_user_merchant_visit_ibfk_1');
			$table->dropForeign('skp_user_merchant_visit_ibfk_2');
		});
	}

}
