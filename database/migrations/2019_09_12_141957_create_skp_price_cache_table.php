<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpPriceCacheTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('price_cache', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('sku_id')->unsigned()->index('sku_id');
			$table->dateTime('validity_date')->nullable()->comment('This is always in server local time');
			$table->decimal('amount', 16, 4)->nullable();
			$table->string('currency', 5)->nullable();
			$table->timestamp('last_update')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->unique(['sku_id','validity_date'], 'sku_id_2');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('price_cache');
	}

}
