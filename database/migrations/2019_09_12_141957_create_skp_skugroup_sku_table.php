<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpSkugroupSkuTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('skugroup_sku', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('skugroup_id')->unsigned()->index('skugroup_id');
			$table->integer('sku_id')->unsigned()->index('sku_id');
			$table->unique(['skugroup_id','sku_id'], 'skugroup_id_sku_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('skugroup_sku');
	}

}
