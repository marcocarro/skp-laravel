<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpProductContactProfileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('product_contact_profile', function(Blueprint $table)
		{
			$table->foreign('product_id', 'skp_product_contact_profile_ibfk_1')->references('id')->on('product')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('sku_id', 'skp_product_contact_profile_ibfk_2')->references('id')->on('sku')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('profile_id', 'skp_product_contact_profile_ibfk_3')->references('id')->on('contact_profile')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product_contact_profile', function(Blueprint $table)
		{
			$table->dropForeign('skp_product_contact_profile_ibfk_1');
			$table->dropForeign('skp_product_contact_profile_ibfk_2');
			$table->dropForeign('skp_product_contact_profile_ibfk_3');
		});
	}

}
