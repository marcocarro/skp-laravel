<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpPaymentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('payment', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_payment_ibfk_1')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('order_id', 'skp_payment_ibfk_2')->references('id')->on('order')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('payment_channel_id', 'skp_payment_ibfk_3')->references('id')->on('payment_channel')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('credit_card_id', 'skp_payment_ibfk_4')->references('id')->on('credit_card')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('b2b_order_id', 'skp_payment_ibfk_5')->references('id')->on('b2b_order')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('b2b_credit_card_id', 'skp_payment_ibfk_6')->references('id')->on('b2b_credit_card')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('payment', function(Blueprint $table)
		{
			$table->dropForeign('skp_payment_ibfk_1');
			$table->dropForeign('skp_payment_ibfk_2');
			$table->dropForeign('skp_payment_ibfk_3');
			$table->dropForeign('skp_payment_ibfk_4');
			$table->dropForeign('skp_payment_ibfk_5');
			$table->dropForeign('skp_payment_ibfk_6');
		});
	}

}
