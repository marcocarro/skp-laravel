<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpPriceCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('price_category', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_price_category_ibfk_1')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('catalog_id', 'skp_price_category_ibfk_2')->references('id')->on('catalog')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('price_category', function(Blueprint $table)
		{
			$table->dropForeign('skp_price_category_ibfk_1');
			$table->dropForeign('skp_price_category_ibfk_2');
		});
	}

}
