<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpMerchantUserOptionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('merchant_user_option', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('merchant_id')->unsigned()->index('merchant_id');
			$table->integer('user_id')->unsigned()->index('user_id');
			$table->boolean('newsletter')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('merchant_user_option');
	}

}
