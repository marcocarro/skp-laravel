<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpDataFieldTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('data_field', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->index('merchant_id');
			$table->text('name', 65535);
			$table->text('field_type', 65535)->nullable();
			$table->string('type');
			$table->text('label', 65535)->nullable();
			$table->text('extra', 65535)->nullable();
			$table->text('notes', 65535)->nullable();
			$table->boolean('archived')->default(0);
			$table->boolean('deleted')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('data_field');
	}

}
