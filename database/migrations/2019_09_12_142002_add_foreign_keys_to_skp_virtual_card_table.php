<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpVirtualCardTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('virtual_card', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_virtual_card_ibfk_1')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('virtual_card_type_id', 'skp_virtual_card_ibfk_2')->references('id')->on('virtual_card_type')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('b2b_company_id', 'skp_virtual_card_ibfk_3')->references('id')->on('b2b_company')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('virtual_card', function(Blueprint $table)
		{
			$table->dropForeign('skp_virtual_card_ibfk_1');
			$table->dropForeign('skp_virtual_card_ibfk_2');
			$table->dropForeign('skp_virtual_card_ibfk_3');
		});
	}

}
