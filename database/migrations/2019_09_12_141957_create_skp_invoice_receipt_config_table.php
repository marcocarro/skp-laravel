<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpInvoiceReceiptConfigTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoice_receipt_config', function(Blueprint $table)
		{
			$table->integer('merchant_id')->unsigned()->primary();
			$table->text('invoice_fields', 65535)->nullable();
			$table->text('receipt_fields', 65535)->nullable();
			$table->integer('next_invoice_number')->default(1);
			$table->integer('next_receipt_number')->default(1);
			$table->integer('next_universal_invoice_number')->unsigned()->nullable();
			$table->integer('next_universal_receipt_number')->unsigned()->nullable();
			$table->integer('next_universal_number')->unsigned()->nullable();
			$table->date('last_invoice_date')->nullable();
			$table->date('last_receipt_date')->nullable();
			$table->text('extra', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invoice_receipt_config');
	}

}
