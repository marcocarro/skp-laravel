<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpTrkeyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('trkey', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('trkey')->index('trkey');
			$table->text('trtext', 16777215)->nullable();
			$table->integer('merchant_id')->unsigned()->nullable()->index('merchant_id');
			$table->string('user_email')->nullable();
			$table->timestamp('lastchanged')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('remote_trkey')->nullable()->index('remote_trkey');
			$table->text('remote_trtext', 65535)->nullable();
			$table->text('note', 65535)->nullable();
			$table->unique(['trkey','merchant_id'], 'trkey_2');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('trkey');
	}

}
