<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpProductTypeMerchantTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_type_merchant', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('product_type_id')->unsigned()->index('product_type_id');
			$table->integer('merchant_id')->unsigned()->index('merchant_id');
			$table->boolean('preselect')->default(0);
			$table->boolean('aggregate')->default(0);
			$table->boolean('store_show')->default(0);
			$table->text('extra', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_type_merchant');
	}

}
