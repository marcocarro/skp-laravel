<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpGlobalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('global', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('config_name', 64)->unique('config_name');
			$table->text('config_value', 16777215)->nullable();
			$table->text('extra', 16777215)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('global');
	}

}
