<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpOrderItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('order_item', function(Blueprint $table)
		{
			$table->foreign('order_id', 'skp_order_item_ibfk_1')->references('id')->on('order')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('card_id', 'skp_order_item_ibfk_10')->references('id')->on('card')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('person_id', 'skp_order_item_ibfk_11')->references('id')->on('person')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('pool_id', 'skp_order_item_ibfk_12')->references('id')->on('pool')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('vat_id', 'skp_order_item_ibfk_13')->references('id')->on('vat')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('related_orderitem_id', 'skp_order_item_ibfk_14')->references('id')->on('order_item')->onUpdate('CASCADE')->onDelete('NO ACTION');
			$table->foreign('promocode_id', 'skp_order_item_ibfk_15')->references('id')->on('promocode')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('sub_orderitem_of_id', 'skp_order_item_ibfk_8')->references('id')->on('order_item')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('sku_id', 'skp_order_item_ibfk_9')->references('id')->on('sku')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('order_item', function(Blueprint $table)
		{
			$table->dropForeign('skp_order_item_ibfk_1');
			$table->dropForeign('skp_order_item_ibfk_10');
			$table->dropForeign('skp_order_item_ibfk_11');
			$table->dropForeign('skp_order_item_ibfk_12');
			$table->dropForeign('skp_order_item_ibfk_13');
			$table->dropForeign('skp_order_item_ibfk_14');
			$table->dropForeign('skp_order_item_ibfk_15');
			$table->dropForeign('skp_order_item_ibfk_8');
			$table->dropForeign('skp_order_item_ibfk_9');
		});
	}

}
