<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpExportConnectorRecordTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('export_connector_record', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_export_connector_record_ibfk_1')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('export_connector_id', 'skp_export_connector_record_ibfk_2')->references('id')->on('export_connector')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('order_id', 'skp_export_connector_record_ibfk_3')->references('id')->on('order')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('orderitem_id', 'skp_export_connector_record_ibfk_4')->references('id')->on('order_item')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('export_connector_record', function(Blueprint $table)
		{
			$table->dropForeign('skp_export_connector_record_ibfk_1');
			$table->dropForeign('skp_export_connector_record_ibfk_2');
			$table->dropForeign('skp_export_connector_record_ibfk_3');
			$table->dropForeign('skp_export_connector_record_ibfk_4');
		});
	}

}
