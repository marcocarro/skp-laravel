<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpPromocodeB2bCompanyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('promocode_b2b_company', function(Blueprint $table)
		{
			$table->foreign('promocode_id', 'skp_promocode_b2b_company_ibfk_1')->references('id')->on('promocode')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('b2b_company_id', 'skp_promocode_b2b_company_ibfk_2')->references('id')->on('b2b_company')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('promocode_b2b_company', function(Blueprint $table)
		{
			$table->dropForeign('skp_promocode_b2b_company_ibfk_1');
			$table->dropForeign('skp_promocode_b2b_company_ibfk_2');
		});
	}

}
