<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpOrderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('order', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_order_ibfk_2')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('user_id', 'skp_order_ibfk_3')->references('id')->on('user')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('related_order_id', 'skp_order_ibfk_4')->references('id')->on('order')->onUpdate('CASCADE')->onDelete('NO ACTION');
			$table->foreign('note_id', 'skp_order_ibfk_5')->references('id')->on('note')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('b2b_company_id', 'skp_order_ibfk_6')->references('id')->on('b2b_company')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('order', function(Blueprint $table)
		{
			$table->dropForeign('skp_order_ibfk_2');
			$table->dropForeign('skp_order_ibfk_3');
			$table->dropForeign('skp_order_ibfk_4');
			$table->dropForeign('skp_order_ibfk_5');
			$table->dropForeign('skp_order_ibfk_6');
		});
	}

}
