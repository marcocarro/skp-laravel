<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpPriceListPriceCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('price_list_price_category', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('price_list_id')->unsigned()->index('price_list_id');
			$table->integer('price_category_id')->unsigned()->index('price_category_id');
			$table->decimal('amount', 16, 4)->nullable();
			$table->string('currency', 5)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('price_list_price_category');
	}

}
