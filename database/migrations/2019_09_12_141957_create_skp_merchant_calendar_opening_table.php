<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpMerchantCalendarOpeningTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('merchant_calendar_opening', function(Blueprint $table)
		{
			$table->integer('merchant_id')->unsigned()->index('merchant_id');
			$table->integer('calendar_id')->unsigned()->index('calendar_id');
			$table->primary(['merchant_id','calendar_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('merchant_calendar_opening');
	}

}
