<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpUserMerchantAuthTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_merchant_auth', function(Blueprint $table)
		{
			$table->foreign('user_merchant_id', 'skp_user_merchant_auth_ibfk_1')->references('id')->on('user_merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_merchant_auth', function(Blueprint $table)
		{
			$table->dropForeign('skp_user_merchant_auth_ibfk_1');
		});
	}

}
