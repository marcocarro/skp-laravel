<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpB2bCreditCardTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('b2b_credit_card', function(Blueprint $table)
		{
			$table->foreign('b2b_company_id', 'skp_b2b_credit_card_ibfk_1')->references('id')->on('b2b_company')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('payment_channel_id', 'skp_b2b_credit_card_ibfk_2')->references('id')->on('payment_channel')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('b2b_credit_card', function(Blueprint $table)
		{
			$table->dropForeign('skp_b2b_credit_card_ibfk_1');
			$table->dropForeign('skp_b2b_credit_card_ibfk_2');
		});
	}

}
