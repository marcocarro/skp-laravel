<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpPriceCacheTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('price_cache', function(Blueprint $table)
		{
			$table->foreign('sku_id', 'skp_price_cache_ibfk_1')->references('id')->on('sku')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('price_cache', function(Blueprint $table)
		{
			$table->dropForeign('skp_price_cache_ibfk_1');
		});
	}

}
