<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpB2bCompanySkugroupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('b2b_company_skugroup', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('b2b_company_id')->unsigned()->index('b2b_company_id');
			$table->integer('skugroup_id')->unsigned()->index('skugroup_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('b2b_company_skugroup');
	}

}
