<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpOfferProductSuggestedTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('offer_product_suggested', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_offer_product_suggested_ibfk_1')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('offer_id', 'skp_offer_product_suggested_ibfk_2')->references('id')->on('offer')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('product_id', 'skp_offer_product_suggested_ibfk_3')->references('id')->on('product')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('offer_product_suggested', function(Blueprint $table)
		{
			$table->dropForeign('skp_offer_product_suggested_ibfk_1');
			$table->dropForeign('skp_offer_product_suggested_ibfk_2');
			$table->dropForeign('skp_offer_product_suggested_ibfk_3');
		});
	}

}
