<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpPromocodeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('promocode', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned();
			$table->integer('promo_generation_group_id')->unsigned()->nullable();
			$table->integer('promo_group_id')->unsigned()->nullable();
			$table->string('code', 32)->nullable();
			$table->char('is_active', 1)->default('N');
			$table->boolean('archived')->default(0);
			$table->boolean('deleted')->default(0);
			$table->text('name', 65535)->nullable();
			$table->string('type')->default('code');
			$table->text('description', 65535)->nullable();
			$table->dateTime('valid_from')->nullable();
			$table->dateTime('valid_to')->nullable();
			$table->text('extra', 65535)->nullable();
			$table->char('show_to_admins', 1)->default('N');
			$table->char('valid_for_everyone', 1)->default('N');
			$table->char('valid_for_every_b2b', 1)->default('N');
			$table->char('user_list_limit', 1)->default('N');
			$table->char('company_list_limit', 1)->default('N');
			$table->index(['merchant_id','promo_group_id'], 'merchant_id');
			$table->index(['merchant_id','promo_generation_group_id'], 'merchant_id_3');
			$table->unique(['merchant_id','code'], 'merchant_id_2');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('promocode');
	}

}
