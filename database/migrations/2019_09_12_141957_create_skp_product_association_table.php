<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpProductAssociationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_association', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('main_product_id')->unsigned()->nullable()->index('main_product_id');
			$table->integer('main_sku_id')->unsigned()->nullable()->index('main_sku_id');
			$table->integer('sub_product_id')->unsigned()->nullable()->index('sub_product_id');
			$table->integer('sub_sku_id')->unsigned()->nullable()->index('sub_sku_id');
			$table->string('relation')->default('optional');
			$table->smallInteger('qty')->unsigned()->nullable()->default(1);
			$table->boolean('archived')->default(0);
			$table->boolean('deleted')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_association');
	}

}
