<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpCreditCardTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('credit_card', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('payment_channel_id')->unsigned()->index('payment_channel_id');
			$table->string('pan_hash')->nullable();
			$table->string('issuer', 32)->nullable();
			$table->string('issuer_country', 2)->nullable();
			$table->string('masked_pan', 19)->nullable();
			$table->string('expiry_dateYYMM', 4)->nullable();
			$table->string('payment_method', 32)->nullable();
			$table->index(['user_id','payment_channel_id'], 'user_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('credit_card');
	}

}
