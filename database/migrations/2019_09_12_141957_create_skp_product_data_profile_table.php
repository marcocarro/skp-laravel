<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpProductDataProfileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_data_profile', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('product_id')->unsigned()->nullable();
			$table->integer('sku_id')->unsigned()->nullable()->index('sku_id');
			$table->integer('profile_id')->unsigned()->nullable()->index('profile_id');
			$table->string('profile_type');
			$table->string('conditions')->nullable();
			$table->unique(['product_id','sku_id','conditions'], 'product_sku_conditions');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_data_profile');
	}

}
