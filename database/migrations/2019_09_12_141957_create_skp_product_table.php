<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->index('merchant_id');
			$table->integer('catalog_id')->unsigned()->index('catalog');
			$table->integer('producttype_id')->unsigned()->nullable()->index('producttype_id');
			$table->integer('productcategory_id')->unsigned()->nullable()->index('productcategory_id');
			$table->integer('vat_id')->unsigned()->nullable()->index('vat_id');
			$table->boolean('add_prod_only')->default(0);
			$table->text('name', 65535);
			$table->text('short_description', 65535)->nullable();
			$table->text('description', 65535)->nullable();
			$table->text('practical_info', 65535)->nullable();
			$table->string('external_id')->nullable();
			$table->boolean('preselected')->default(0);
			$table->integer('sort')->unsigned()->nullable()->default(1);
			$table->string('validity_unit')->nullable();
			$table->decimal('validity_value', 19, 6)->nullable();
			$table->decimal('validity_min_value', 19, 6)->nullable();
			$table->decimal('validity_max_value', 19, 6)->nullable();
			$table->char('buy_interval_before_validity', 5)->nullable()->comment('Buy interval can be (M=Months, D=Days, H=Hours, m=minutes');
			$table->boolean('ext_need_contact')->default(0);
			$table->text('product_extra', 65535)->nullable();
			$table->integer('venue_id')->unsigned()->nullable()->index('venue_id');
			$table->integer('onsale_calendar_id')->unsigned()->nullable()->index('onsale_calendar_id');
			$table->integer('validity_calendar_id')->unsigned()->nullable()->index('validity_calendar_id');
			$table->boolean('active')->default(0);
			$table->boolean('archived')->default(0);
			$table->boolean('deleted')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product');
	}

}
