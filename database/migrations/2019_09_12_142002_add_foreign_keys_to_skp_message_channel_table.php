<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpMessageChannelTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('message_channel', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_message_channel_ibfk_1')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('message_channel', function(Blueprint $table)
		{
			$table->dropForeign('skp_message_channel_ibfk_1');
		});
	}

}
