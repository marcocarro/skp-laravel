<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpUserMerchantVisitTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_merchant_visit', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->index('merchant_id');
			$table->integer('user_id')->unsigned()->index('user_id');
			$table->string('session_id', 24)->nullable()->index('session_id');
			$table->dateTime('visit_start')->nullable();
			$table->dateTime('visit_end')->nullable();
			$table->string('device_type')->nullable();
			$table->string('device_platform', 32)->nullable()->default('Unknown');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_merchant_visit');
	}

}
