<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpPriceListPriceCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('price_list_price_category', function(Blueprint $table)
		{
			$table->foreign('price_list_id', 'skp_price_list_price_category_ibfk_1')->references('id')->on('price_list')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('price_category_id', 'skp_price_list_price_category_ibfk_2')->references('id')->on('price_category')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('price_list_price_category', function(Blueprint $table)
		{
			$table->dropForeign('skp_price_list_price_category_ibfk_1');
			$table->dropForeign('skp_price_list_price_category_ibfk_2');
		});
	}

}
