<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpPromocodeB2bCompanyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('promocode_b2b_company', function(Blueprint $table)
		{
			$table->integer('promocode_id')->unsigned()->index('promocode_id');
			$table->integer('b2b_company_id')->unsigned()->index('b2b_company_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('promocode_b2b_company');
	}

}
