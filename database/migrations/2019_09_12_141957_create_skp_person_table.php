<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpPersonTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('person', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned()->index('user_id');
			$table->string('relation')->nullable();
			$table->string('email')->nullable();
			$table->string('title')->nullable();
			$table->string('first_name', 64)->nullable();
			$table->string('middle_name', 64)->nullable();
			$table->string('last_name', 64)->nullable();
			$table->date('birth_date')->nullable();
			$table->string('gender')->nullable();
			$table->string('street_num', 16)->nullable();
			$table->string('street', 128)->nullable();
			$table->string('city', 128)->nullable();
			$table->string('state_prov', 64)->nullable();
			$table->string('zip', 32)->nullable();
			$table->char('country', 2)->nullable();
			$table->string('phone', 64)->nullable();
			$table->binary('photo', 16777215)->nullable();
			$table->string('image_url_thumb')->nullable();
			$table->boolean('deleted')->default(0);
			$table->text('extra', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('person');
	}

}
