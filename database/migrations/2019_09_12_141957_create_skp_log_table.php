<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('log', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->nullable()->index('merchant_id');
			$table->integer('order_id')->unsigned()->nullable()->index('order_id');
			$table->string('connector', 32)->nullable()->comment('this is the table of the connector');
			$table->integer('connector_id')->unsigned()->nullable()->comment('this is the table of the connector');
			$table->string('what');
			$table->string('status', 16)->nullable();
			$table->timestamp('logdatetime')->default(DB::raw('CURRENT_TIMESTAMP'))->index('logdatetime');
			$table->text('message', 16777215)->nullable();
			$table->index(['what','logdatetime','id'], 'what_logdatetime');
			$table->index(['connector','connector_id'], 'connector');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('log');
	}

}
