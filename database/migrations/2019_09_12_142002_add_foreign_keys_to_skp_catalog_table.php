<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpCatalogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('catalog', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_catalog_ibfk_1')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('connector_id', 'skp_catalog_ibfk_2')->references('id')->on('connector')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('catalog', function(Blueprint $table)
		{
			$table->dropForeign('skp_catalog_ibfk_1');
			$table->dropForeign('skp_catalog_ibfk_2');
		});
	}

}
