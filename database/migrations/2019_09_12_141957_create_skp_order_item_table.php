<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpOrderItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_item', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('sub_orderitem_of_id')->unsigned()->nullable()->index('sub_orderitem_of_id');
			$table->integer('order_id')->unsigned();
			$table->integer('sku_id')->unsigned()->nullable()->index('sku_id');
			$table->integer('pool_id')->unsigned()->nullable()->index('pool_id');
			$table->integer('card_id')->unsigned()->nullable()->index('card_id');
			$table->integer('person_id')->unsigned()->nullable()->index('person_id');
			$table->integer('promocode_id')->unsigned()->nullable()->index('promocode_id');
			$table->text('product_name', 65535)->nullable();
			$table->text('pricecategory_name', 65535)->nullable();
			$table->string('original_connector_class')->nullable();
			$table->integer('original_connector_id')->unsigned()->nullable();
			$table->string('card_type', 64)->nullable();
			$table->string('card_serial', 32)->nullable();
			$table->string('first_name', 64)->nullable();
			$table->string('middle_name', 64)->nullable();
			$table->string('last_name', 64)->nullable();
			$table->date('birth_date')->nullable();
			$table->dateTime('validity_date')->nullable()->comment('This is always in server local timezone');
			$table->smallInteger('qty')->default(0);
			$table->decimal('single_amount', 16, 4);
			$table->integer('vat_id')->unsigned()->nullable()->index('vat_id');
			$table->decimal('vat_perc', 5)->unsigned()->nullable();
			$table->decimal('vat_amount', 16, 4)->nullable();
			$table->decimal('total_amount', 16, 4);
			$table->string('currency', 5);
			$table->string('status')->nullable();
			$table->string('status_extra')->nullable();
			$table->integer('related_orderitem_id')->unsigned()->nullable()->index('related_orderitem_id');
			$table->text('orderitem_extra', 65535)->nullable();
			$table->text('person_data', 65535)->nullable();
			$table->binary('person_photo', 16777215)->nullable();
			$table->index(['order_id','sku_id'], 'order_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_item');
	}

}
