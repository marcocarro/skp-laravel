<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpVirtualCardTypeMerchantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('virtual_card_type_merchants', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('virtual_card_type_id')->unsigned()->index('virtual_card_type_id');
			$table->integer('merchant_id')->unsigned()->index('merchant_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('virtual_card_type_merchants');
	}

}
