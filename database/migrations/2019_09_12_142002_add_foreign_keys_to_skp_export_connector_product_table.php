<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpExportConnectorProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('export_connector_product', function(Blueprint $table)
		{
			$table->foreign('export_connector_id', 'skp_export_connector_product_ibfk_1')->references('id')->on('export_connector')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('product_id', 'skp_export_connector_product_ibfk_2')->references('id')->on('product')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('export_connector_product', function(Blueprint $table)
		{
			$table->dropForeign('skp_export_connector_product_ibfk_1');
			$table->dropForeign('skp_export_connector_product_ibfk_2');
		});
	}

}
