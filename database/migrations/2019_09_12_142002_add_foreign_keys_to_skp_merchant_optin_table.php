<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpMerchantOptinTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('merchant_optin', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_merchant_optin_ibfk_1')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('merchant_id', 'skp_merchant_optin_ibfk_2')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('merchant_optin', function(Blueprint $table)
		{
			$table->dropForeign('skp_merchant_optin_ibfk_1');
			$table->dropForeign('skp_merchant_optin_ibfk_2');
		});
	}

}
