<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpSkuTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sku', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->index('merchant_id');
			$table->integer('catalog_id')->unsigned()->index('catalog_id');
			$table->integer('product_id')->unsigned();
			$table->string('product_external_id')->nullable();
			$table->integer('pricecategory_id')->unsigned()->index('pricecategory_id');
			$table->string('pricecategory_external_id')->nullable();
			$table->string('need_card')->default('N');
			$table->string('need_contact')->default('N');
			$table->string('b2b_need_card')->default('N');
			$table->string('b2b_need_contact')->default('N');
			$table->boolean('ext_need_contact')->default(0);
			$table->smallInteger('min_qty')->unsigned()->nullable();
			$table->smallInteger('max_qty')->unsigned()->nullable();
			$table->smallInteger('mod_qty')->unsigned()->nullable();
			$table->boolean('aggregate')->nullable();
			$table->string('package')->default('N');
			$table->text('extra', 65535)->nullable();
			$table->boolean('archived')->default(0);
			$table->boolean('deleted')->default(0);
			$table->index(['product_id','pricecategory_id'], 'product_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sku');
	}

}
