<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpCalendarTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('calendar', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 64);
			$table->integer('merchant_id')->unsigned()->nullable()->index('merchant_id');
			$table->boolean('priority')->default(10)->index('priority');
			$table->char('deleted', 1)->default('N');
			$table->boolean('archived')->default(0);
			$table->index(['id','priority'], 'id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('calendar');
	}

}
