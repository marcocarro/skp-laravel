<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpBookingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('booking', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('calendar_date_time_id')->unsigned()->nullable()->index('calendar_date_time_id');
			$table->dateTime('from_datetime')->nullable()->comment('always in server timezone');
			$table->dateTime('to_datetime')->nullable()->comment('always in server timezone');
			$table->string('reference_num', 32)->nullable()->index('reference_num');
			$table->integer('availability_id')->unsigned()->index('availability_id');
			$table->integer('order_item_id')->unsigned()->nullable()->index('order_item_id');
			$table->integer('booking_manual_id')->unsigned()->nullable()->index('booking_manual_id');
			$table->timestamp('last_change')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('always in UTC timezone');
			$table->dateTime('time_out')->nullable()->comment('always in UTC timezone');
			$table->string('status')->default('TMP');
			$table->text('extra', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('booking');
	}

}
