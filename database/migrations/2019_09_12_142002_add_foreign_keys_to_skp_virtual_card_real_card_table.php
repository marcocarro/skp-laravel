<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpVirtualCardRealCardTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('virtual_card_real_card', function(Blueprint $table)
		{
			$table->foreign('virtual_card_id', 'skp_virtual_card_real_card_ibfk_1')->references('id')->on('virtual_card')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('virtual_card_real_card', function(Blueprint $table)
		{
			$table->dropForeign('skp_virtual_card_real_card_ibfk_1');
		});
	}

}
