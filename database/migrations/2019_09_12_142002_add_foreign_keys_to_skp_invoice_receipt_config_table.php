<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpInvoiceReceiptConfigTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('invoice_receipt_config', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_invoice_receipt_config_ibfk_1')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('invoice_receipt_config', function(Blueprint $table)
		{
			$table->dropForeign('skp_invoice_receipt_config_ibfk_1');
		});
	}

}
