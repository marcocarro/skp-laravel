<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpPromocodeSkugroupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('promocode_skugroup', function(Blueprint $table)
		{
			$table->integer('promocode_id')->unsigned();
			$table->integer('skugroup_id')->unsigned()->index('skugroup_id');
			$table->primary(['promocode_id','skugroup_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('promocode_skugroup');
	}

}
