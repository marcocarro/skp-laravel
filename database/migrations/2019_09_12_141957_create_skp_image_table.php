<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpImageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('image', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->index('merchant_id');
			$table->integer('imagecategory_id')->unsigned()->index('imagecategory_id');
			$table->text('name', 65535)->nullable();
			$table->string('file_extension', 5)->default('jpg');
			$table->text('extra', 65535)->nullable();
			$table->boolean('archived')->default(0);
			$table->boolean('deleted')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('image');
	}

}
