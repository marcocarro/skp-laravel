<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpB2bOrderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('b2b_order', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('reference_num', 32)->nullable();
			$table->integer('merchant_id')->unsigned()->index('merchant_id_2');
			$table->integer('b2b_company_id')->unsigned()->index('b2b_company_id');
			$table->integer('user_id')->unsigned()->index('user_id');
			$table->timestamp('creation_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->dateTime('order_datetime')->nullable();
			$table->dateTime('lastupdate_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('contact_first_name', 64)->nullable();
			$table->string('contact_last_name', 64)->nullable();
			$table->date('contact_birth_date')->nullable();
			$table->string('contact_mobile_phone', 32)->nullable();
			$table->string('contact_email', 64)->nullable();
			$table->decimal('amount', 16, 4)->nullable();
			$table->string('currency', 5)->nullable();
			$table->string('type')->default('BASKET');
			$table->boolean('booked')->default(0);
			$table->boolean('payed')->default(0);
			$table->string('status', 32)->default('New');
			$table->string('status_info')->nullable();
			$table->integer('related_order_id')->unsigned()->nullable()->index('related_order_id');
			$table->string('marker')->nullable()->index('marker');
			$table->integer('note_id')->unsigned()->nullable()->index('note_id');
			$table->index(['merchant_id','status_info'], 'merchant_id_3');
			$table->index(['merchant_id','user_id'], 'merchant_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('b2b_order');
	}

}
