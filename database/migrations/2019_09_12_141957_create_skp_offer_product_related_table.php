<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpOfferProductRelatedTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('offer_product_related', function(Blueprint $table)
		{
			$table->integer('offer_id')->unsigned()->index('offer_id');
			$table->integer('product_id')->unsigned()->index('product_id');
			$table->unique(['offer_id','product_id'], 'offer_id_2');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('offer_product_related');
	}

}
