<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpDataProfileFieldTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('data_profile_field', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('data_profile_id')->unsigned()->index('data_profile_id');
			$table->integer('data_field_id')->unsigned()->index('data_field_id');
			$table->integer('sort')->unsigned();
			$table->boolean('mandatory')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('data_profile_field');
	}

}
