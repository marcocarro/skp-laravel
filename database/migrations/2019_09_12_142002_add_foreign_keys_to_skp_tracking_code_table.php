<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpTrackingCodeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tracking_code', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_tracking_code_ibfk_1')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tracking_code', function(Blueprint $table)
		{
			$table->dropForeign('skp_tracking_code_ibfk_1');
		});
	}

}
