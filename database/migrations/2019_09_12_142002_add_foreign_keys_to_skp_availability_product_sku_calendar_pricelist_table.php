<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpAvailabilityProductSkuCalendarPricelistTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('availability_product_sku_calendar_pricelist', function(Blueprint $table)
		{
			$table->foreign('availability_id', 'skp_availability_product_sku_calendar_pricelist_ibfk_1')->references('id')->on('availability')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('product_id', 'skp_availability_product_sku_calendar_pricelist_ibfk_2')->references('id')->on('product')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('sku_id', 'skp_availability_product_sku_calendar_pricelist_ibfk_3')->references('id')->on('sku')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('calendar_id', 'skp_availability_product_sku_calendar_pricelist_ibfk_4')->references('id')->on('calendar')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('price_list_id', 'skp_availability_product_sku_calendar_pricelist_ibfk_5')->references('id')->on('price_list')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('availability_product_sku_calendar_pricelist', function(Blueprint $table)
		{
			$table->dropForeign('skp_availability_product_sku_calendar_pricelist_ibfk_1');
			$table->dropForeign('skp_availability_product_sku_calendar_pricelist_ibfk_2');
			$table->dropForeign('skp_availability_product_sku_calendar_pricelist_ibfk_3');
			$table->dropForeign('skp_availability_product_sku_calendar_pricelist_ibfk_4');
			$table->dropForeign('skp_availability_product_sku_calendar_pricelist_ibfk_5');
		});
	}

}
