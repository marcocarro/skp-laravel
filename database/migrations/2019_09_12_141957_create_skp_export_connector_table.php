<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpExportConnectorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('export_connector', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('api_key', 40)->nullable()->unique('api_key');
			$table->integer('merchant_id')->unsigned()->index('merchant_id');
			$table->string('name')->nullable();
			$table->string('connector_class');
			$table->boolean('active')->default(0);
			$table->dateTime('last_export_datetime')->nullable();
			$table->text('connector_extra', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('export_connector');
	}

}
