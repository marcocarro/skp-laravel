<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpCardTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('card', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned()->nullable()->index('user_id');
			$table->integer('person_id')->unsigned()->nullable()->index('person_id');
			$table->string('nick', 32);
			$table->string('card_type', 64)->nullable()->default('Skidata Keycard');
			$table->integer('virtual_card_id')->unsigned()->nullable()->index('virtual_card_id');
			$table->string('card_serial', 32)->nullable();
			$table->boolean('deleted')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('card');
	}

}
