<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpMerchantTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('merchant', function(Blueprint $table)
		{
			$table->foreign('contact_profile_id', 'skp_merchant_ibfk_1')->references('id')->on('contact_profile')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('team_contact_profile_id', 'skp_merchant_ibfk_2')->references('id')->on('contact_profile')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('merchant', function(Blueprint $table)
		{
			$table->dropForeign('skp_merchant_ibfk_1');
			$table->dropForeign('skp_merchant_ibfk_2');
		});
	}

}
