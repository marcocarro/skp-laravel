<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpSkugroupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('skugroup', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->index('merchant_id');
			$table->integer('sort')->unsigned()->default(10)->index('sort');
			$table->text('name', 65535)->nullable();
			$table->boolean('active')->default(0);
			$table->boolean('web_active')->default(0);
			$table->dateTime('publish_from')->nullable();
			$table->dateTime('publish_to')->nullable();
			$table->integer('layout_id')->unsigned()->nullable()->index('layout_id');
			$table->text('layout_options', 16777215)->nullable();
			$table->text('skugroup_extra', 65535)->nullable();
			$table->boolean('archived')->default(0);
			$table->boolean('deleted')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('skugroup');
	}

}
