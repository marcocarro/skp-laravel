<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpExportConnectorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('export_connector', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_export_connector_ibfk_1')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('export_connector', function(Blueprint $table)
		{
			$table->dropForeign('skp_export_connector_ibfk_1');
		});
	}

}
