<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpB2bOrderExternalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('b2b_order_external', function(Blueprint $table)
		{
			$table->foreign('b2b_order_id', 'skp_b2b_order_external_ibfk_1')->references('id')->on('b2b_order')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('connector_id', 'skp_b2b_order_external_ibfk_2')->references('id')->on('connector')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('b2b_order_external', function(Blueprint $table)
		{
			$table->dropForeign('skp_b2b_order_external_ibfk_1');
			$table->dropForeign('skp_b2b_order_external_ibfk_2');
		});
	}

}
