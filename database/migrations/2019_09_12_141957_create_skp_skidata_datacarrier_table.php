<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpSkidataDatacarrierTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('skidata_datacarrier', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('ChipId', 5)->nullable();
			$table->string('SerialNumber', 20)->nullable();
			$table->char('LuhnNumber', 1)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('skidata_datacarrier');
	}

}
