<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpCalendarDateTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('calendar_date', function(Blueprint $table)
		{
			$table->foreign('calendar_id', 'skp_calendar_date_ibfk_1')->references('id')->on('calendar')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('calendar_date', function(Blueprint $table)
		{
			$table->dropForeign('skp_calendar_date_ibfk_1');
		});
	}

}
