<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpOrditLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ordit_logs', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->char('model', 50)->index('model');
			$table->char('action', 7);
			$table->text('values', 16777215);
			$table->char('user', 50);
			$table->timestamp('timestamp_created')->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ordit_logs');
	}

}
