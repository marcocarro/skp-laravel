<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpProductCategoryBackupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_category_backup', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->index('merchant_id');
			$table->integer('catalog_id')->unsigned()->index('catalog_id');
			$table->string('external_id')->nullable();
			$table->string('name');
			$table->text('description', 65535)->nullable();
			$table->text('product_category_extra', 65535)->nullable();
			$table->boolean('active')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_category_backup');
	}

}
