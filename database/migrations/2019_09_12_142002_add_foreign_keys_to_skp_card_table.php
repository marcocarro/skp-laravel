<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpCardTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('card', function(Blueprint $table)
		{
			$table->foreign('user_id', 'skp_card_ibfk_5')->references('id')->on('user')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('person_id', 'skp_card_ibfk_6')->references('id')->on('person')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('virtual_card_id', 'skp_card_ibfk_7')->references('id')->on('virtual_card')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('card', function(Blueprint $table)
		{
			$table->dropForeign('skp_card_ibfk_5');
			$table->dropForeign('skp_card_ibfk_6');
			$table->dropForeign('skp_card_ibfk_7');
		});
	}

}
