<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpVirtualCardTypeMerchantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('virtual_card_type_merchants', function(Blueprint $table)
		{
			$table->foreign('virtual_card_type_id', 'skp_virtual_card_type_merchants_ibfk_1')->references('id')->on('virtual_card_type')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('merchant_id', 'skp_virtual_card_type_merchants_ibfk_2')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('virtual_card_type_merchants', function(Blueprint $table)
		{
			$table->dropForeign('skp_virtual_card_type_merchants_ibfk_1');
			$table->dropForeign('skp_virtual_card_type_merchants_ibfk_2');
		});
	}

}
