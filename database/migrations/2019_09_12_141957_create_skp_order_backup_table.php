<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpOrderBackupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_backup', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('reference_num', 32)->nullable()->index('reference_num');
			$table->integer('merchant_id')->unsigned()->nullable()->index('merchant_id_2');
			$table->integer('b2b_company_id')->unsigned()->nullable()->index('b2b_company_id');
			$table->integer('user_id')->unsigned()->nullable()->index('user_id');
			$table->timestamp('creation_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->dateTime('order_datetime')->nullable()->index('order_datetime');
			$table->timestamp('lastupdate_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->decimal('amount', 16, 4)->nullable();
			$table->string('currency', 5)->nullable();
			$table->string('type')->default('BASKET');
			$table->boolean('user_profile_confirmed')->default(0);
			$table->string('status', 32)->default('New');
			$table->string('status_info')->nullable();
			$table->integer('related_order_id')->unsigned()->nullable()->index('related_order_id');
			$table->string('marker')->nullable()->index('marker');
			$table->integer('note_id')->unsigned()->nullable()->index('note_id');
			$table->integer('process_code')->nullable();
			$table->index(['merchant_id','type'], 'merchant_id_4');
			$table->index(['merchant_id','status'], 'merchant_id_3');
			$table->index(['merchant_id','user_id'], 'merchant_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_backup');
	}

}
