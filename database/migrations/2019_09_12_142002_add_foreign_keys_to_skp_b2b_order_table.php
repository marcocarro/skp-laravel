<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpB2bOrderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('b2b_order', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_b2b_order_ibfk_1')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('NO ACTION');
			$table->foreign('user_id', 'skp_b2b_order_ibfk_2')->references('id')->on('user')->onUpdate('CASCADE')->onDelete('NO ACTION');
			$table->foreign('b2b_company_id', 'skp_b2b_order_ibfk_3')->references('id')->on('b2b_company')->onUpdate('CASCADE')->onDelete('NO ACTION');
			$table->foreign('related_order_id', 'skp_b2b_order_ibfk_4')->references('id')->on('b2b_order')->onUpdate('CASCADE')->onDelete('NO ACTION');
			$table->foreign('note_id', 'skp_b2b_order_ibfk_5')->references('id')->on('note')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('b2b_order', function(Blueprint $table)
		{
			$table->dropForeign('skp_b2b_order_ibfk_1');
			$table->dropForeign('skp_b2b_order_ibfk_2');
			$table->dropForeign('skp_b2b_order_ibfk_3');
			$table->dropForeign('skp_b2b_order_ibfk_4');
			$table->dropForeign('skp_b2b_order_ibfk_5');
		});
	}

}
