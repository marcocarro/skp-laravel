<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpPersonConnectorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('person_connector', function(Blueprint $table)
		{
			$table->foreign('person_id', 'skp_person_connector_ibfk_1')->references('id')->on('person')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('connector_id', 'skp_person_connector_ibfk_2')->references('id')->on('connector')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('merchant_id', 'skp_person_connector_ibfk_3')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('person_connector', function(Blueprint $table)
		{
			$table->dropForeign('skp_person_connector_ibfk_1');
			$table->dropForeign('skp_person_connector_ibfk_2');
			$table->dropForeign('skp_person_connector_ibfk_3');
		});
	}

}
