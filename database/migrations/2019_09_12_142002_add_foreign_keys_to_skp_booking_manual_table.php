<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpBookingManualTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('booking_manual', function(Blueprint $table)
		{
			$table->foreign('user_id', 'skp_booking_manual_ibfk_1')->references('id')->on('user')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('booking_manual', function(Blueprint $table)
		{
			$table->dropForeign('skp_booking_manual_ibfk_1');
		});
	}

}
