<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpInvoiceReceiptProfileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoice_receipt_profile', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->index('merchant_id_2');
			$table->integer('user_id')->unsigned()->index('user_id');
			$table->string('model', 32);
			$table->text('extra', 65535)->nullable();
			$table->index(['merchant_id','user_id'], 'merchant_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invoice_receipt_profile');
	}

}
