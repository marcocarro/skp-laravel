<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpCreditCardTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('credit_card', function(Blueprint $table)
		{
			$table->foreign('user_id', 'skp_credit_card_ibfk_1')->references('id')->on('user')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('payment_channel_id', 'skp_credit_card_ibfk_3')->references('id')->on('payment_channel')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('credit_card', function(Blueprint $table)
		{
			$table->dropForeign('skp_credit_card_ibfk_1');
			$table->dropForeign('skp_credit_card_ibfk_3');
		});
	}

}
