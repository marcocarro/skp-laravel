<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpOfferProductSuggestedTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('offer_product_suggested', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->index('merchant_id');
			$table->integer('offer_id')->unsigned()->index('offer_id_2');
			$table->integer('product_id')->unsigned()->index('product_id');
			$table->integer('skugroup_id')->unsigned()->nullable()->index('skugroup_id');
			$table->integer('sort')->default(0);
			$table->text('extra', 65535)->nullable();
			$table->unique(['offer_id','product_id'], 'offer_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('offer_product_suggested');
	}

}
