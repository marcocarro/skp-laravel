<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpDataProfileFieldTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('data_profile_field', function(Blueprint $table)
		{
			$table->foreign('data_profile_id', 'skp_data_profile_field_ibfk_1')->references('id')->on('data_profile')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('data_field_id', 'skp_data_profile_field_ibfk_2')->references('id')->on('data_field')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('data_profile_field', function(Blueprint $table)
		{
			$table->dropForeign('skp_data_profile_field_ibfk_1');
			$table->dropForeign('skp_data_profile_field_ibfk_2');
		});
	}

}
