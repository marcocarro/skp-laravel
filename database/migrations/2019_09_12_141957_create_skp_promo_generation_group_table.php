<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpPromoGenerationGroupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('promo_generation_group', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->nullable()->index('merchant_id');
			$table->string('name')->nullable();
			$table->timestamp('creation_timestamp')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->char('codes_signature', 40)->default('');
			$table->integer('qty')->unsigned();
			$table->text('extra', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('promo_generation_group');
	}

}
