<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpMessageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('message', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('message_channel_id')->unsigned()->nullable()->index('message_channel_id');
			$table->integer('merchant_id')->unsigned()->nullable()->index('merchant_id');
			$table->integer('user_id')->unsigned()->nullable()->index('user_id');
			$table->integer('order_id')->unsigned()->nullable()->index('order_id');
			$table->integer('b2b_order_id')->unsigned()->nullable()->index('b2b_order_id');
			$table->string('from')->nullable();
			$table->string('to')->nullable();
			$table->string('cc')->nullable();
			$table->string('bcc')->nullable();
			$table->dateTime('sent')->nullable();
			$table->dateTime('read')->nullable();
			$table->string('subject');
			$table->text('body', 65535);
			$table->string('language', 4);
			$table->boolean('deleted')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('message');
	}

}
