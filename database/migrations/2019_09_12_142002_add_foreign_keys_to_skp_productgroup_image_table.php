<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpProductgroupImageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('productgroup_image', function(Blueprint $table)
		{
			$table->foreign('productgroup_id', 'skp_productgroup_image_ibfk_1')->references('id')->on('productgroup')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('image_id', 'skp_productgroup_image_ibfk_2')->references('id')->on('image')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('productgroup_image', function(Blueprint $table)
		{
			$table->dropForeign('skp_productgroup_image_ibfk_1');
			$table->dropForeign('skp_productgroup_image_ibfk_2');
		});
	}

}
