<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpMerchantUserOptionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('merchant_user_option', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_merchant_user_option_ibfk_1')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('user_id', 'skp_merchant_user_option_ibfk_2')->references('id')->on('user')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('merchant_user_option', function(Blueprint $table)
		{
			$table->dropForeign('skp_merchant_user_option_ibfk_1');
			$table->dropForeign('skp_merchant_user_option_ibfk_2');
		});
	}

}
