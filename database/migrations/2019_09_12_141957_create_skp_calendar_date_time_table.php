<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpCalendarDateTimeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('calendar_date_time', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('calendar_id')->unsigned()->index('calendar_id');
			$table->integer('date_id')->unsigned()->nullable()->index('date_id');
			$table->integer('time_id')->unsigned()->nullable()->index('time_id');
			$table->date('day')->nullable()->index('day')->comment('This is always in merchant local time zone');
			$table->time('time_start')->nullable()->comment('This is always in merchant local time zone');
			$table->time('time_end')->nullable()->comment('This is always in merchant local time zone');
			$table->string('deleted')->nullable()->default('CN');
			$table->index(['day','time_start','deleted'], 'day_2');
			$table->index(['calendar_id','day','time_start','deleted'], 'calendar_id_2');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('calendar_date_time');
	}

}
