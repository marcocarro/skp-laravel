<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpSkugroupSkuTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('skugroup_sku', function(Blueprint $table)
		{
			$table->foreign('skugroup_id', 'skp_skugroup_sku_ibfk_1')->references('id')->on('skugroup')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('sku_id', 'skp_skugroup_sku_ibfk_3')->references('id')->on('sku')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('skugroup_sku', function(Blueprint $table)
		{
			$table->dropForeign('skp_skugroup_sku_ibfk_1');
			$table->dropForeign('skp_skugroup_sku_ibfk_3');
		});
	}

}
