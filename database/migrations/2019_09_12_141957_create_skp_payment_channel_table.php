<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpPaymentChannelTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payment_channel', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->nullable()->index('merchant_id');
			$table->string('name', 32)->nullable();
			$table->string('payment_class')->nullable();
			$table->boolean('capture_on_process')->default(1);
			$table->boolean('b2c_enabled')->default(0);
			$table->boolean('test_env')->default(1);
			$table->string('username', 32)->nullable();
			$table->string('password', 32)->nullable();
			$table->string('connection_status')->default('NOT CHECKED');
			$table->boolean('sort')->nullable()->default(10);
			$table->text('parameters', 65535)->nullable();
			$table->text('extra', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payment_channel');
	}

}
