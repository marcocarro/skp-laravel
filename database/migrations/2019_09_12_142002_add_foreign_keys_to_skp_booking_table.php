<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpBookingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('booking', function(Blueprint $table)
		{
			$table->foreign('calendar_date_time_id', 'skp_booking_ibfk_1')->references('id')->on('calendar_date_time')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('availability_id', 'skp_booking_ibfk_2')->references('id')->on('availability')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('order_item_id', 'skp_booking_ibfk_3')->references('id')->on('order_item')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('booking_manual_id', 'skp_booking_ibfk_5')->references('id')->on('booking_manual')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('booking', function(Blueprint $table)
		{
			$table->dropForeign('skp_booking_ibfk_1');
			$table->dropForeign('skp_booking_ibfk_2');
			$table->dropForeign('skp_booking_ibfk_3');
			$table->dropForeign('skp_booking_ibfk_5');
		});
	}

}
