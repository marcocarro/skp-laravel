<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpPoolTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pool', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_pool_ibfk_1')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pool', function(Blueprint $table)
		{
			$table->dropForeign('skp_pool_ibfk_1');
		});
	}

}
