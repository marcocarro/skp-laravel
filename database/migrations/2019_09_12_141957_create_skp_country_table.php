<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpCountryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('country', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->char('iso2', 2)->nullable();
			$table->string('short_name', 80)->default('');
			$table->string('long_name', 80)->default('');
			$table->char('iso3', 3)->nullable();
			$table->string('numcode', 6)->nullable();
			$table->string('un_member', 12)->nullable();
			$table->string('calling_code', 8)->nullable();
			$table->string('cctld', 5)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('country');
	}

}
