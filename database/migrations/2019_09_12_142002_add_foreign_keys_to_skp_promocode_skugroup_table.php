<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpPromocodeSkugroupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('promocode_skugroup', function(Blueprint $table)
		{
			$table->foreign('promocode_id', 'skp_promocode_skugroup_ibfk_1')->references('id')->on('promocode')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('skugroup_id', 'skp_promocode_skugroup_ibfk_2')->references('id')->on('skugroup')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('promocode_skugroup', function(Blueprint $table)
		{
			$table->dropForeign('skp_promocode_skugroup_ibfk_1');
			$table->dropForeign('skp_promocode_skugroup_ibfk_2');
		});
	}

}
