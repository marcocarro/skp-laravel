<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpOfferTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('offer', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->index('merchant_id');
			$table->string('offer_type');
			$table->boolean('active')->default(0);
			$table->dateTime('publish_from_datetime')->nullable();
			$table->dateTime('publish_to_datetime')->nullable();
			$table->integer('image_id')->unsigned()->nullable()->index('image_id');
			$table->text('extra', 16777215)->nullable();
			$table->text('rules', 65535)->nullable();
			$table->boolean('archived')->default(0);
			$table->boolean('deleted')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('offer');
	}

}
