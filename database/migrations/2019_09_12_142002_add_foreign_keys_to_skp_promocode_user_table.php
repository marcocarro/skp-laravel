<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpPromocodeUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('promocode_user', function(Blueprint $table)
		{
			$table->foreign('promocode_id', 'skp_promocode_user_ibfk_1')->references('id')->on('promocode')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('user_id', 'skp_promocode_user_ibfk_2')->references('id')->on('user')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('promocode_user', function(Blueprint $table)
		{
			$table->dropForeign('skp_promocode_user_ibfk_1');
			$table->dropForeign('skp_promocode_user_ibfk_2');
		});
	}

}
