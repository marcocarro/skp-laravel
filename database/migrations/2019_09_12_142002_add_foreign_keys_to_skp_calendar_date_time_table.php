<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpCalendarDateTimeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('calendar_date_time', function(Blueprint $table)
		{
			$table->foreign('calendar_id', 'skp_calendar_date_time_ibfk_1')->references('id')->on('calendar')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('time_id', 'skp_calendar_date_time_ibfk_3')->references('id')->on('calendar_time')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('date_id', 'skp_calendar_date_time_ibfk_4')->references('id')->on('calendar_date')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('calendar_date_time', function(Blueprint $table)
		{
			$table->dropForeign('skp_calendar_date_time_ibfk_1');
			$table->dropForeign('skp_calendar_date_time_ibfk_3');
			$table->dropForeign('skp_calendar_date_time_ibfk_4');
		});
	}

}
