<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpPaymentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payment', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->nullable()->index('merchant_id');
			$table->integer('order_id')->unsigned()->nullable()->index('order_id');
			$table->integer('b2b_order_id')->unsigned()->nullable()->index('b2b_order_id');
			$table->string('transaction_id', 64)->nullable();
			$table->integer('payment_channel_id')->unsigned()->nullable()->index('payment_channel_id');
			$table->string('payment_type')->nullable();
			$table->integer('credit_card_id')->unsigned()->nullable()->index('credit_card_id');
			$table->integer('b2b_credit_card_id')->unsigned()->nullable()->index('b2b_credit_card_id');
			$table->decimal('amount', 16, 4)->nullable();
			$table->string('currency', 5)->nullable();
			$table->string('status')->default('REGISTERED')->index('status');
			$table->dateTime('expire_date')->nullable();
			$table->string('authorization', 64)->nullable();
			$table->text('extra', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payment');
	}

}
