<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpUserMerchantPositionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_merchant_position', function(Blueprint $table)
		{
			$table->foreign('user_id', 'skp_user_merchant_position_ibfk_1')->references('id')->on('user')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('domain', 'skp_user_merchant_position_ibfk_2')->references('domain')->on('merchant')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('merchant_id', 'skp_user_merchant_position_ibfk_3')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('user_merchant_visit_id', 'skp_user_merchant_position_ibfk_4')->references('id')->on('user_merchant_visit')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_merchant_position', function(Blueprint $table)
		{
			$table->dropForeign('skp_user_merchant_position_ibfk_1');
			$table->dropForeign('skp_user_merchant_position_ibfk_2');
			$table->dropForeign('skp_user_merchant_position_ibfk_3');
			$table->dropForeign('skp_user_merchant_position_ibfk_4');
		});
	}

}
