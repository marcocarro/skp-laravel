<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpUserMerchantTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_merchant', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->index('merchant_id');
			$table->integer('user_id')->unsigned()->index('user_id');
			$table->string('user_type')->default('LEAD');
			$table->timestamp('registration_date')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->dateTime('last_visit_date')->nullable();
			$table->integer('num_visit')->nullable();
			$table->string('gdpr_status')->default('CONSENT_NEEDED');
			$table->unique(['merchant_id','user_id'], 'merchant_id_2');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_merchant');
	}

}
