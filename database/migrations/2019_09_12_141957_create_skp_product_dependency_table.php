<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpProductDependencyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_dependency', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('main_product_id')->unsigned()->nullable();
			$table->integer('main_pricecategory_id')->unsigned()->nullable()->index('main_pricecategory_id');
			$table->integer('sub_product_id')->unsigned()->nullable()->index('sub_product_id');
			$table->integer('sub_pricecategory_id')->unsigned()->nullable()->index('sub_pricecategory_id');
			$table->boolean('same_day')->default(0);
			$table->boolean('same_product')->default(0);
			$table->boolean('same_pricecategory')->default(0);
			$table->decimal('child_parent_rate', 5)->unsigned()->default(1.00);
			$table->index(['main_product_id','main_pricecategory_id','sub_product_id'], 'main_product_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_dependency');
	}

}
