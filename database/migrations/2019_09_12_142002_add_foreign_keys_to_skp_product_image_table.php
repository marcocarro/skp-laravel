<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpProductImageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('product_image', function(Blueprint $table)
		{
			$table->foreign('product_id', 'skp_product_image_ibfk_1')->references('id')->on('product')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('image_id', 'skp_product_image_ibfk_2')->references('id')->on('image')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product_image', function(Blueprint $table)
		{
			$table->dropForeign('skp_product_image_ibfk_1');
			$table->dropForeign('skp_product_image_ibfk_2');
		});
	}

}
