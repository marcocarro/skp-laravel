<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpB2bCompanyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('b2b_company', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_b2b_company_ibfk_1')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('b2b_company', function(Blueprint $table)
		{
			$table->dropForeign('skp_b2b_company_ibfk_1');
		});
	}

}
