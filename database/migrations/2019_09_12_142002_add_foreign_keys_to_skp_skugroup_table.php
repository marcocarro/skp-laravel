<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpSkugroupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('skugroup', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_skugroup_ibfk_1')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('layout_id', 'skp_skugroup_ibfk_2')->references('id')->on('layout')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('skugroup', function(Blueprint $table)
		{
			$table->dropForeign('skp_skugroup_ibfk_1');
			$table->dropForeign('skp_skugroup_ibfk_2');
		});
	}

}
