<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpPersonConnectorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('person_connector', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('person_id')->unsigned()->index('person_id');
			$table->integer('connector_id')->unsigned()->index('connector_id');
			$table->integer('merchant_id')->unsigned()->index('merchant_id');
			$table->text('external', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('person_connector');
	}

}
