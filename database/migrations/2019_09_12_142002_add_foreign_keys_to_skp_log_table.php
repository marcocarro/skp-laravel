<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('log', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_log_ibfk_1')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('order_id', 'skp_log_ibfk_2')->references('id')->on('order')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('log', function(Blueprint $table)
		{
			$table->dropForeign('skp_log_ibfk_1');
			$table->dropForeign('skp_log_ibfk_2');
		});
	}

}
