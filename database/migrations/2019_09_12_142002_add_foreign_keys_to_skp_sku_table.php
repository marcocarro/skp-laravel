<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpSkuTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sku', function(Blueprint $table)
		{
			$table->foreign('merchant_id', 'skp_sku_ibfk_1')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('catalog_id', 'skp_sku_ibfk_2')->references('id')->on('catalog')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('product_id', 'skp_sku_ibfk_3')->references('id')->on('product')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('pricecategory_id', 'skp_sku_ibfk_4')->references('id')->on('price_category')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sku', function(Blueprint $table)
		{
			$table->dropForeign('skp_sku_ibfk_1');
			$table->dropForeign('skp_sku_ibfk_2');
			$table->dropForeign('skp_sku_ibfk_3');
			$table->dropForeign('skp_sku_ibfk_4');
		});
	}

}
