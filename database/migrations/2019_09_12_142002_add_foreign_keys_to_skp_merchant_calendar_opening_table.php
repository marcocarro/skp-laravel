<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpMerchantCalendarOpeningTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('merchant_calendar_opening', function(Blueprint $table)
		{
			$table->foreign('calendar_id', 'skp_merchant_calendar_opening_ibfk_1')->references('id')->on('calendar')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('merchant_id', 'skp_merchant_calendar_opening_ibfk_2')->references('id')->on('merchant')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('merchant_calendar_opening', function(Blueprint $table)
		{
			$table->dropForeign('skp_merchant_calendar_opening_ibfk_1');
			$table->dropForeign('skp_merchant_calendar_opening_ibfk_2');
		});
	}

}
