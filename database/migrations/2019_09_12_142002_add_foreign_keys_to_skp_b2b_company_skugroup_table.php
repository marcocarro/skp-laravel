<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpB2bCompanySkugroupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('b2b_company_skugroup', function(Blueprint $table)
		{
			$table->foreign('b2b_company_id', 'skp_b2b_company_skugroup_ibfk_1')->references('id')->on('b2b_company')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('skugroup_id', 'skp_b2b_company_skugroup_ibfk_2')->references('id')->on('skugroup')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('b2b_company_skugroup', function(Blueprint $table)
		{
			$table->dropForeign('skp_b2b_company_skugroup_ibfk_1');
			$table->dropForeign('skp_b2b_company_skugroup_ibfk_2');
		});
	}

}
