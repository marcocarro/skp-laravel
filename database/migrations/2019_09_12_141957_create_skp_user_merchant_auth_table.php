<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpUserMerchantAuthTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_merchant_auth', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_merchant_id')->unsigned()->index('user_merchant_id');
			$table->boolean('auth_id');
			$table->boolean('accepted')->default(0);
			$table->dateTime('changed')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_merchant_auth');
	}

}
