<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpTmpCardImportTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tmp_card_import', function(Blueprint $table)
		{
			$table->foreign('card_id', 'skp_tmp_card_import_ibfk_1')->references('id')->on('card')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('user_id', 'skp_tmp_card_import_ibfk_2')->references('id')->on('user')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('order_external_id', 'skp_tmp_card_import_ibfk_3')->references('id')->on('order_external')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tmp_card_import', function(Blueprint $table)
		{
			$table->dropForeign('skp_tmp_card_import_ibfk_1');
			$table->dropForeign('skp_tmp_card_import_ibfk_2');
			$table->dropForeign('skp_tmp_card_import_ibfk_3');
		});
	}

}
