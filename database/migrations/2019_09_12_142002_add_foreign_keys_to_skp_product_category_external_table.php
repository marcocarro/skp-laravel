<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkpProductCategoryExternalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('product_category_external', function(Blueprint $table)
		{
			$table->foreign('product_category_id', 'skp_product_category_external_ibfk_1')->references('id')->on('product_category')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('catalog_id', 'skp_product_category_external_ibfk_2')->references('id')->on('catalog')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product_category_external', function(Blueprint $table)
		{
			$table->dropForeign('skp_product_category_external_ibfk_1');
			$table->dropForeign('skp_product_category_external_ibfk_2');
		});
	}

}
