<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpAvailabilityProductSkuCalendarPricelistTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('availability_product_sku_calendar_pricelist', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('availability_id')->unsigned()->nullable()->index('availability_id');
			$table->integer('product_id')->unsigned()->nullable()->index('product_id');
			$table->integer('sku_id')->unsigned()->nullable()->index('sku_id');
			$table->integer('calendar_id')->unsigned()->nullable()->index('calendar_id');
			$table->integer('price_list_id')->unsigned()->nullable()->index('price_list_id');
			$table->unique(['availability_id','product_id','sku_id','calendar_id'], 'availability_id_5');
			$table->index(['availability_id','calendar_id'], 'availability_id_4');
			$table->index(['availability_id','product_id'], 'availability_id_2');
			$table->index(['availability_id','sku_id'], 'availability_id_3');
			$table->index(['calendar_id','product_id'], 'calendar_id_2');
			$table->index(['product_id','sku_id','calendar_id','price_list_id'], 'product_sku_calendar_price_list');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('availability_product_sku_calendar_pricelist');
	}

}
