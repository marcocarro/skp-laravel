<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkpVirtualCardTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('virtual_card', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->index('merchant_id');
			$table->integer('virtual_card_type_id')->unsigned()->nullable()->index('virtual_card_type_id');
			$table->string('card_serial', 32);
			$table->integer('b2b_company_id')->unsigned()->nullable()->index('b2b_company_id');
			$table->boolean('active')->default(0);
			$table->boolean('active_b2b')->default(0);
			$table->dateTime('valid_from')->nullable();
			$table->dateTime('valid_to')->nullable();
			$table->text('extra', 65535)->nullable();
			$table->boolean('deleted')->default(0);
			$table->unique(['virtual_card_type_id','card_serial'], 'virtual_card_type_id_2');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('virtual_card');
	}

}
